import React from 'react';
import {ScrollView,TouchableOpacity,Text, View, Modal,Dimensions} from 'react-native';
import {Icon} from 'react-native-elements';
import {Searchbar} from 'react-native-paper';
import Item from '../ItemsRender/item';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
export default class ModalSelectNote extends React.Component{
    render(){
        return(
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleItem}>
                  <View style={[styles.align_center_view]}>
                    <View style={{ backgroundColor: 'white',  borderRadius: 10, elevation: 2, overflow: 'hidden' }}>
                            <View style={styles.titleModal1}>
                                <Text style={{ fontSize: 23,  flex: 10, fontWeight: 'bold', color: 'white' }}>Note</Text>
                                <TouchableOpacity style={{ fontSize: 25,  flex: 1 }} onPress={this.props.onPressClose}><Icon name="close" color='white' size={25} type='material-community'/></TouchableOpacity>
                            </View>                    
                            <View style={{ backgroundColor: 'white', height: height * 0.5, width: width * 0.9, borderRadius: 10, elevation: 2, overflow: 'hidden' }}>  
                            <ScrollView>
                                <Item text="Sans" onPress={()=>this.props.onPressOption('Sans')} />
                                <Item text="Pr agrégé" onPress={()=>this.props.onPressOption('Pr agrégé')} />   
                                <Item text="Managing Partner" onPress={()=>this.props.onPressOption('Managing Partner')} />                
                                <Item text="Directeur" onPress={()=>this.props.onPressOption('Directeur')} />                
                                <Item text="Coordinateur pays" onPress={()=>this.props.onPressOption('Coordinateur pays')} />                
                                <Item text="Logistics manager" onPress={()=>this.props.onPressOption('Logistics manager')} />                
                                <Item text="Consultant QHSE" onPress={()=>this.props.onPressOption('Consultant QHSE')} />                
                                <Item text="Ambassadeur devloppement durable" onPress={()=>this.props.onPressOption('Ambassadeur devloppement durable')} />                
                                <Item text="Eléve ingenieur" onPress={()=>this.props.onPressOption('Eléve ingenieur')} />                
                                <Item text="Executif assistant" onPress={()=>this.props.onPressOption('Executif assistant')} />                
                                <Item text="Gérant" onPress={()=>this.props.onPressOption('Gérant')} />                
                                <Item text="DGA" onPress={()=>this.props.onPressOption('DGA')} />                
                                <Item text="Chargé de développement" onPress={()=>this.props.onPressOption('Chargé de développement')} />                
                                <Item text="Secrétaire général" onPress={()=>this.props.onPressOption('Secrétaire général')} />                
                                <Item text="Directeur Avant Vente" onPress={()=>this.props.onPressOption('Directeur Avant Vente')} />                
                                <Item text="Consultant SIRH" onPress={()=>this.props.onPressOption('Consultant SIRH')} />                
                                <Item text="Responsable" onPress={()=>this.props.onPressOption('Responsable')} />                
                                <Item text="Chef de projet" onPress={()=>this.props.onPressOption('Chef de projet')} />                
                                <Item text="Conseiller en emploi" onPress={()=>this.props.onPressOption('Conseiller en emploi')} />                
                                <Item text="Manager" onPress={()=>this.props.onPressOption('Manager')} />                
                                <Item text="Directeur CMDG" onPress={()=>this.props.onPressOption('Directeur CMDG')} />                
                                <Item text="Maintenance Planner" onPress={()=>this.props.onPressOption('Maintenance Planner')} />                
                                <Item text="Directeur Adjoint" onPress={()=>this.props.onPressOption('Directeur Adjoint')} />                
                                <Item text="Neon" onPress={()=>this.props.onPressOption('Neon')} />                
                                <Item text="Consultant en sécurité" onPress={()=>this.props.onPressOption('Consultant en sécurité')} />                
                                <Item text="Project Manager" onPress={()=>this.props.onPressOption('Project Manager')} />                
                                <Item text="Changement transformationnel" onPress={()=>this.props.onPressOption('Changement transformationnel')} />                
                                <Item text="Owner" onPress={()=>this.props.onPressOption('Owner')} />                
                                <Item text="Coach professionnel cértifié" onPress={()=>this.props.onPressOption('Coach professionnel cértifié')} />                
                                <Item text="Responsable Inddustriel" onPress={()=>this.props.onPressOption('Responsable Inddustriel')} />                
                                <Item text="Energy Manager" onPress={()=>this.props.onPressOption('Energy Manager')} />                
                                <Item text="Etudiant" onPress={()=>this.props.onPressOption('Etudiant')} />                
                                <Item text="Chef de division" onPress={()=>this.props.onPressOption('Chef de division')} />                
                                </ScrollView>  
                            </View>
                    </View>
                  </View>
            </Modal>
        )}}