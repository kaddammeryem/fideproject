import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import { ScrollView } from 'react-native';
import { StyleSheet,Modal,View ,Text ,TouchableOpacity,Dimensions,Image} from 'react-native';
import {  TextInput,IconButton } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';//icons library
import { format } from "date-fns";
import DateTimePickerModal from "react-native-modal-datetime-picker";//DatePicker library
import AsyncStorage from '@react-native-async-storage/async-storage';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
var date=new Date()
export default class ModalTask extends Component {
   constructor(props){
     super(props);
     this.state={
       name:'',
       loadingShow:false,
       enable:false,
       hours:'',
       jours:'',
       description:'',
       solution:'',
       progres:'',
       enddate:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
       solution:'',
       number:0,
       startdate:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
       visibledatedebut:false,
       visibledatefin:false,
       userId:'',
       videName:false,
       videProject:false,
       
     }  
   }
   initialiserValues=()=>{
     this.setState({name:'',
     enable:false,
     hours:'',
     jours:'',
     description:'',
     solution:'',
     progres:'',
     enddate:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
     solution:'',
     number:0,
     startdate:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
     visibledatedebut:false,
     visibledatefin:false,
     videName:false,
     videProject:false,
    session:''})

   }
   postFunc=async()=>{
    this.setState({videName:false,videProject:false})   

    if(this.state.name=='' ){console.log('inside vide ');this.setState({videName:true})}
    else if (this.props.idProject=='' ){console.log('inside vide conpte');this.setState({videProject:true})}
    else{
    this.setState({loadingShow:true,enable:true,videName:false,videProject:false})  
    try{ 
      await AsyncStorage.multiGet(['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]});console.log(result)});

    var data={"projecttaskname":this.state.name,
   "description":this.state.description,
    "enddate":this.state.enddate,"projectid":this.props.idProject,
    "projecttaskhours":this.state.hours,"solution":this.state.solution,
    "projecttasknumber":this.props.number,"projecttaskcategories":this.props.categorie,"projecttaskpriority":this.props.priority,
    "projecttaskprogress":this.state.progres,"projecttaskstatus":this.props.statut,"assigned_user_id":this.state.userId,
    "projecttasktype":this.props.type,"startdate":this.state.startdate}
    await fetch('https://portail.crm4you.ma/webservice.php', {
          method: 'POST',
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
         body:'operation=create&sessionName='+this.state.session+'&element='+JSON.stringify(data)+'&elementType=ProjectTask'})
    .then((responses)  => responses.json()) 
    .then((json)=> this.setState({loadingShow:false,enable:false})   )
    .then(this.props.onPressSave) 
    .then(this.initialiserValues)
    .then(this.props.onPressCancel)   
    .catch((error) => console.error(error));
        }catch(r){}}
}
    render() { 
        return (
         
 
            <Modal
            animationType="none"
            transparent={true}
            visible={this.props.modalVisibleTask} 
            >
            <View style={styles.align_center_view}>
                <SafeAreaView style={{height : '80%',elevation : 5,borderRadius : 10,overflow : 'hidden' }}>
                    <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}>
                        <Text style={{ backgroundColor: 'white', color: '#495057', marginTop: 13, fontSize: 25, width: width * 0.8, height: 40, marginBottom: 9 }}>
                            Créer nouvelle Tache
                        </Text>
                        {this.state.videName ?
                            <Text style={{  color:'red'}} >
                                Le champs Nom de la tache est vide
                            </Text>
                        : null}
                        {this.state.videProject?
                            <Text style={{  color:'red'}} >
                                Le champs Relatif à est vide
                            </Text>
                        : null}
                        <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',color: '#53aefe' }}>
                            Détails du Tasks
                        </Text>
                        <TextInput label="*Nom de la tache" onChangeText={(change)=>this.setState({name:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                           
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Priorité
                            </Text>
                            <View style={styles.option}>
                                <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressPriority}>
                                    <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.selectPriority}</Text>
                                    <View style={styles.align_center_view}>
                                        <Icon name="caret-down" size={20} />
                                    </View>
                                </TouchableOpacity>
                            </View> 

                            <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',marginTop: 20, color: '#53aefe' }}>
                                  Date de début
                              </Text>
                            <View style={styles.option}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.setState({visibledatedebut:true})} >
                                        <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.state.startdate}</Text>
                                        <View style={styles.align_center_view}>
                                          <Icon name="calendar" size={20} type='material-community'/>
                                        </View>
                                        <DateTimePickerModal
                                            isVisible={this.state.visibledatedebut}
                                            mode="date"                           
                                            onConfirm={(x) => {this.setState({startdate:x.getFullYear()+'-'+x.getMonth()+'-'+x.getDate(),visibledatedebut:false});console.log(this.state.startdate)}}
                                            onCancel={() => {}}
                                        />
                                  </TouchableOpacity>
                              </View>        
                              <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',marginTop: 20, color: '#53aefe' }}>
                                  Date de fin
                              </Text>
                            <View style={styles.option}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.setState({visibledatefin:true})} >
                                        <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.state.enddate}</Text>
                                        <View style={styles.align_center_view}>
                                          <Icon name="calendar" size={20} type='material-community'/>
                                        </View>
                                        <DateTimePickerModal
                                            isVisible={this.state.visibledatefin}
                                            mode="date"                           
                                            onConfirm={(x) => {this.setState({enddate:x.getFullYear()+'-'+x.getMonth()+'-'+x.getDate(),visibledatefin:false});console.log(this.state.enddate)}}
                                            onCancel={() => {}}
                                        />
                                  </TouchableOpacity>
                              </View>                                           
                            <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Status
                        </Text>
                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressStatut}>
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.selectStatut}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="caret-down" size={20} />
                                </View>
                            </TouchableOpacity>
                        </View>
                         <TouchableOpacity style={{flexDirection : 'row',marginTop : 10,justifyContent : 'center' }} 
                            onPress={this.props.onPressInfo} >
                           <Text style={{color : 'blue',borderBottomWidth : 1 , borderBottomColor :'blue'}} > {this.props.MoreinfoText} d'information   </Text>
                           <Icon name={this.props.MoreinfoIcon} size={20} color = 'blue' style={{borderBottomWidth : 1, borderBottomColor :'blue'}} type='material-community'/>
                        </TouchableOpacity>
                        {this.props.Moreinfo ? (
                        <View>                           
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            *Relatif à la
                        </Text>
                        <View style={styles.view_Check}>
                          <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.props.onPressProject()}>
                            <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.projectname} </Text>
                            <View style={styles.align_center_view}>
                              <Icon name="caret-down" size={20} />
                            </View>
                          </TouchableOpacity>                        
                        </View>
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Engagement
                        </Text>
                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressEngagement}>
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.engagement}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="caret-down" size={20} />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <TextInput label="Heures" style={{ backgroundColor: 'white', width: width * 0.8 }} />
                        <TextInput label="Jours" style={{ backgroundColor: 'white', width: width * 0.8 }} />
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Catégorie
                        </Text>
                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressCategorie}>
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.categorie}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="caret-down" size={20} />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Type
                        </Text>
                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressType}>
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.type}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="chevron-down" size={20} type='material-community'/>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <TextInput label="Progres" onChangeText={(change)=>this.setState({progres:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                           

                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                          Information sur la description
                        </Text>
                        <TextInput label="Description" onChangeText={(change)=>this.setState({description:change})} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                          Détail de la solution
                        </Text>
                        <TextInput onChangeText={(change)=>this.setState({solution:change})} label="Solution" style={{ backgroundColor: 'white', width: width * 0.8 }} />
                           </View>)  :null}

                        <View style={{ width: width * 0.8, marginBottom : 20, height: 60, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', marginTop: 20, marginBottom: 10 }}>
                            <TouchableOpacity disabled={this.state.enable}onPress={this.postFunc} style={{ backgroundColor: '#53aefe', height: 50, width: 0.35 * width, borderRadius: 10, marginTop: 20, marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}>
                              <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>
                                Save
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>{this.props.onPressCancel();this.initialiserValues()}} style={{ backgroundColor: '#53aefe', height: 50, width: 0.35 * width, borderRadius: 10, marginTop: 20, alignItems: 'center', elevation: 5, justifyContent: 'center' }} >
                              <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >
                                Cancel
                              </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: width * 0.8, height: 10,}}> 
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View>

        </Modal>

      
        );
    }
}
 





const styles =StyleSheet.create({
    view_Check:{
      marginTop : 10,
      flexDirection : 'row',
      height : 40 ,
      width : width*0.8,
      borderRadius : 5,
      borderWidth : 1,
      borderColor : 'blue'
    },
    option:{
      marginTop : 10,
      flexDirection : 'row',
      height : 40 ,
      width : width*0.8,
      borderRadius : 5,
      borderWidth : 1,
      borderColor : 'gray'
    },
    align_center_view:{
      flex:1,
      justifyContent:'center',
      alignItems:'center',
    },
    scrollView: {
      backgroundColor: 'white',
      padding : 10
    },
    text: {
      fontSize: 42,
    },
})