import React, { Component } from 'react';
import { StyleSheet,Modal,View ,Text ,TouchableOpacity,Dimensions} from 'react-native';
import {Icon} from 'react-native-elements';

import SearchbyItem from '../ItemsRender/SearchbyItem';



const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class ModalSearchBy extends Component {
    
        constructor(props) {
            super(props);
            this.test = this.test.bind(this);
        }

        


        test = (a,b,c) => {
            this.props.onpresst(a,b,c);
        }
        

            
        
        render() { 
            return (
               
                  <Modal
                  animationType='fade'
                  transparent={true}
                  visible={this.props.MSearchVisibility}>
                  <View style={{position : 'absolute',height : height,width : width ,backgroundColor : 'gray' , opacity : 0.5}}></View>
                  <View style={[styles.align_center_view ]}>
                      <View style={{ backgroundColor: 'white', width: width * 0.9, borderRadius: 2, elevation: 2,overflow : 'hidden' }}>
                          <View style={styles.titleModal1}>
                              <Text style={{ fontSize: 23, marginRight: 10, flex: 10, fontWeight : 'bold' ,  color: 'white' }}>Select Field</Text> 
                              <Icon
                                name='close'
                                color='white'
                                type='font-awesome'
                                style={{marginRight: 10, flex: 1}}
                                size={30}
                                onPress={this.props.onCancel}
                              />
                          </View>
                          {this.props.Items.map((object , index ) => {
                             return  <SearchbyItem title={object.title} value={object.value}  color={this.props.colorObject[`${object.colorname}`]} chosenValue={this.props.searchby}  onPress={() => this.test(object.colorname,object.value,object.title)} />
                          })}
                          
                          <View style={{ height : 30 , alignItems : 'center',justifyContent : 'center' }}/>
                      </View>
                  </View>
              </Modal> 
            );
        }
    }
     





const styles =StyleSheet.create({
    view_Check:{
      marginTop : 10,
      flexDirection : 'row',
      height : 40 ,
      width : width*0.8,
      borderRadius : 5,
      borderWidth : 1,
      borderColor : 'blue'
    },
    option:{
      marginTop : 10,
      flexDirection : 'row',
      height : 40 ,
      width : width*0.8,
      borderRadius : 5,
      borderWidth : 1,
      borderColor : 'gray'
    },
    align_center_view:{
      flex:1,
      justifyContent:'center',
      alignItems:'center',
    },
    scrollView: {
      backgroundColor: 'white',
      padding : 10
    },
    text: {
      fontSize: 42,
    },
    titleModal1 : {
        flexDirection : 'row',
        height : 50,
        width : width*0.9,
        padding : 7,
        backgroundColor :'#2860b6',
        fontSize : 25,
        alignSelf : 'center',
        marginBottom : 10,
      },
})