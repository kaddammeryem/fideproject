import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import { ScrollView } from 'react-native';
import { StyleSheet,Modal,View ,Text ,TouchableOpacity,Dimensions} from 'react-native';
import {  TextInput,IconButton } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';//icons library
import AsyncStorage from '@react-native-async-storage/async-storage';
const width = Dimensions.get('window').width;
class AddTicket extends Component {
   constructor(props){
     super(props);
     this.state={
       titre:'',
       loadingShow:true,
       enable:false,
       heures:'',
       jours:'',
       description:'',
       session:'',
       solution:'',
       userId:'',
       videTitre:false
       
     }
    
   }
   initialiserValeurs=()=>{
    this.setState({titre:'',
    enable:false,
    heures:'',
    jours:'',
    description:'',
    solution:'',
    userId:'',
    videTitre:false})

   }
   postFunc=async()=>{
    this.setState({videTitre:false})   

    if(this.state.titre=='' ){console.log('inside vide ');this.setState({videTitre:true})}
else{this.setState({loadingShow:true,enable:true,videTitre:false})
     try{
    await AsyncStorage.multiGet(['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]});console.log(result)});
     
  
    var data={"ticket_title":this.state.titre,"contact_id":this.props.idContact,
    "parent_id":this.props.idCompte,"description":this.state.description,
    "hours":this.state.heures,"ticketpriorities":this.props.selectPriority,
    "solution":this.state.solution,"ticketstatus":this.props.selectPriority,
    "ticketseverities":this.props.engagement,"ticketcategories":this.props.categorie,"days":this.state.jours,
    "assigned_user_id":this.state.userId}
    await fetch('https://portail.crm4you.ma/webservice.php', {
          method: 'POST',
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
         body:'operation=create&sessionName='+this.state.session+'&element='+JSON.stringify(data)+'&elementType=HelpDesk'})
    .then((responses)  => responses.json()) 
    .then((json)=> {console.log(json);this.setState({loadingShow:false,enable:false,titre:'',})})
    .then(this.props.onPressSave) 
    .then(this.initialiserValeurs)
    .then(this.props.onPressCancel)   
    .catch((error) => console.error(error));
        }
        catch(r){
          console.log(r)
        }}
}
    render() { 
        return (
         
  
            <Modal
            animationType="none"
            transparent={true}
            visible={this.props.modalVisibleTickets} 
            >
            <View style={styles.align_center_view}>
              <SafeAreaView style={{height : '71%',elevation : 5,borderRadius : 10,overflow : 'hidden', backgroundColor : 'white' , alignItems : 'center'}}>
                    <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}>
                        <Text style={{ backgroundColor: 'white', color: '#495057', marginTop: 13, fontSize: 25, width: width * 0.8, height: 40, marginBottom: 9 }}>
                            Créer nouveau Ticket
                        </Text>
                        {this.state.videTitre ?
                
                            <Text style={{  color:'red'}} >
                            Le champs Titre est vide
                            </Text>
                    
                        : null}
                        <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',color: '#53aefe' }}>
                            Détails du Ticket
                        </Text>
                        <TextInput label="*Titre" value={this.state.titre}onChangeText={(change)=>this.setState({titre:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                           
                       
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Priorité
                            </Text>
                            <View style={styles.option}>
                                <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressPriority}>
                                    <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.selectPriority}</Text>
                                    <View style={styles.align_center_view}>
                                        <Icon name="caret-down" size={20} />
                                    </View>
                                </TouchableOpacity>
                            </View>                                              
                            <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Status
                        </Text>
                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressStatut}>
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.selectStatut}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="caret-down" size={20} />
                                </View>
                            </TouchableOpacity>
                        </View>
                         <TouchableOpacity style={{flexDirection : 'row',marginTop : 10,justifyContent : 'center' }} 
                            onPress={this.props.onPressInfo} >
                           <Text style={{color : 'blue',borderBottomWidth : 1 , borderBottomColor :'blue'}} > {this.props.MoreinfoText} d'information   </Text>
                           <Icon name={this.props.MoreinfoIcon} size={20} color = 'blue' style={{borderBottomWidth : 1, borderBottomColor :'blue'}} type='material-community'/>
                        </TouchableOpacity>
                        {this.props.Moreinfo ? (
                        <View>                           
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Nom du compte
                        </Text>
                        <View style={styles.view_Check}>
                          <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressCompte}>
                            <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.accountname} </Text>
                            <View style={styles.align_center_view}>
                              <Icon name="caret-down" size={20} />
                            </View>
                          </TouchableOpacity>                        
                        </View>
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Nom du Contact
                        </Text>
                        <View style={styles.view_Check}>
                          <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressItemContact}>
                            <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.lastname}</Text>
                            <View style={styles.align_center_view}>
                              <Icon name="caret-down" size={20} />
                            </View>
                          </TouchableOpacity>
                          
                        </View>
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Engagement
                        </Text>
                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressEngagement}>
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.engagement}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="caret-down" size={20} />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <TextInput label="Heures" value={this.state.heures} onChangeText={(change)=>this.setState({heures:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />
                        <TextInput label="Jours" onChangeText={(change)=>this.setState({jours:change})} value={this.state.jours} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Catégorie
                        </Text>
                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressCategorie}>
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.categorie}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="caret-down" size={20} />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                          Information sur la description
                        </Text>
                        <TextInput label="Description" value={this.state.description}onChangeText={(change)=>this.setState({description:change})} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                          Détail de la solution
                        </Text>
                        <TextInput onChangeText={(change)=>this.setState({solution:change})}value={this.state.solution} label="Solution" style={{ backgroundColor: 'white', width: width * 0.8 }} />
                           </View>)  :null}
                    </ScrollView>
                    <View style={{borderTopWidth :1 ,borderTopColor : '#53aefe', width: width * 0.8, height: 60, justifyContent: 'space-evenly' ,flexDirection: 'row', alignItems: 'center' }}>
                      <TouchableOpacity disabled={this.state.enable}onPress={this.postFunc}style={{position : 'relative', backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>Save</Text></TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.props.onPressCancel();this.setState({videTitre:false});;this.initaliserValues()}}style={{ backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  alignItems: 'center', elevation: 5, justifyContent: 'center' }} ><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >Cancel</Text></TouchableOpacity>
                    </View>
                </SafeAreaView>
                
            </View>

        </Modal>

      
        );
    }
}
 
export default AddTicket;




const styles =StyleSheet.create({
    view_Check:{
      marginTop : 10,
      flexDirection : 'row',
      height : 40 ,
      width : width*0.8,
      borderRadius : 5,
      borderWidth : 1,
      borderColor : 'blue'
    },
    option:{
      marginTop : 10,
      flexDirection : 'row',
      height : 40 ,
      width : width*0.8,
      borderRadius : 5,
      borderWidth : 1,
      borderColor : 'gray'
    },
    align_center_view:{
      flex:1,
      justifyContent:'center',
      alignItems:'center',
    },
    scrollView: {
      backgroundColor: 'white',
      padding : 10
    },
    text: {
      fontSize: 42,
    },
})