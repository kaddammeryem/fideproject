import React from 'react';
import {TouchableOpacity,Text, View, Modal,Dimensions} from 'react-native';
import {Icon} from 'react-native-elements';
import Item from '../ItemsRender/item';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
export default class ModalSelectItemStatut extends React.Component{
    constructor(props){
        super(props);
       
    }
    render(){
        return(
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleItemTickets}>
                  <View style={[styles.align_center_view]}>
                    <View style={{ backgroundColor: 'white',  borderRadius: 10, elevation: 2, overflow: 'hidden' }}>
                            <View style={styles.titleModal1}>
                                <Text style={{ fontSize: 25,  flex: 10, fontWeight: 'bold', color: 'white' }}>{this.props.select}</Text>
                                <TouchableOpacity style={{ fontSize: 30,  flex: 1 }} onPress={this.props.onPressClose}><Icon name="close" color='white' size={25} type='material-community'/></TouchableOpacity>
                            </View>                    
                            <View style={{ backgroundColor: 'white', height: height * 0.5, width: width * 0.9, borderRadius: 10, elevation: 2, overflow: 'hidden' }}>    
                                <Item text="Ouvert" onPress={()=>this.props.onPressOption('Ouvert')} />
                                <Item text="En cours" onPress={()=>this.props.onPressOption('En cours')} />
                                <Item text="Indéfini" onPress={()=>this.props.onPressOption('Indéfini')} />
                                <Item text="Dans la plannification" onPress={()=>this.props.onPressOption('Dans la plannification')} />
                                <Item text="En attente" onPress={()=>this.props.onPressOption('En attente')}/>
                                <Item text="Complet" onPress={()=>this.props.onPressOption('Complet')}/>
                                <Item text="Archivé" onPress={()=>this.props.onPressOption('Archivé')}/>
                                       
                            </View>
                    </View>
                  </View>
            </Modal>
        )}}