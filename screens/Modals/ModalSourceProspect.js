import React from 'react';
import {ScrollView,TouchableOpacity,Text, View, Modal,Dimensions} from 'react-native';
import {Icon} from 'react-native-elements';
import {Searchbar} from 'react-native-paper';
import Item from '../ItemsRender/item';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
export default class ModalSourceProspect extends React.Component{
    render(){
        return(
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleItem}>
                  <View style={[styles.align_center_view]}>
                    <View style={{ backgroundColor: 'white',  borderRadius: 10, elevation: 2, overflow: 'hidden' }}>
                            <View style={styles.titleModal1}>
                                <Text style={{ fontSize: 23,  flex: 10, fontWeight: 'bold', color: 'white' }}>Source</Text>
                                <TouchableOpacity style={{ fontSize: 25,  flex: 1 }} onPress={this.props.onPressClose}><Icon name="close" color='white' size={25} type='material-community'/></TouchableOpacity>
                            </View>                    
                            <View style={{ backgroundColor: 'white', height: height * 0.5, width: width * 0.9, borderRadius: 10, elevation: 2, overflow: 'hidden' }}> 
                            <ScrollView> 
                                <Item text="Propection Téléphonique" onPress={()=>this.props.onPressOption('Propection Téléphonique')} />
                                <Item text="Client existant" onPress={()=>this.props.onPressOption('Client existant')} />   
                                <Item text="Auto Géneré" onPress={()=>this.props.onPressOption('Auto Géneré')} />                
                                <Item text="Employé" onPress={()=>this.props.onPressOption('Employé')} />                
                                <Item text="Partenaire" onPress={()=>this.props.onPressOption('Partenaire')} />                
                                <Item text="Relations publiques" onPress={()=>this.props.onPressOption('Relations publiques')} />                
                                <Item text="Email Direct" onPress={()=>this.props.onPressOption('Email Direct')} />                
                                <Item text="Conférence" onPress={()=>this.props.onPressOption('Conférence')} />                
                                <Item text="Salon" onPress={()=>this.props.onPressOption('Salon')} />                
                                <Item text="Site Internet" onPress={()=>this.props.onPressOption('Site Internet')} />                
                                <Item text="Bouche à Oreille" onPress={()=>this.props.onPressOption('Bouche à Oreille')} />                
                                <Item text="Facebook" onPress={()=>this.props.onPressOption('Facebook')} />                
                                <Item text="Linkedin" onPress={()=>this.props.onPressOption('Linkedin')} />                
                                <Item text="Instagram" onPress={()=>this.props.onPressOption('Instagram')} />                
                                <Item text="Autre" onPress={()=>this.props.onPressOption('Autre')} />                
                                </ScrollView>  
                            </View>
                    </View>
                  </View>
            </Modal>
        )}}