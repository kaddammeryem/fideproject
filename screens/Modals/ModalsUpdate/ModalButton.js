import React from 'react';
import {TouchableOpacity,Text, View, Modal,Dimensions} from 'react-native';
import { TextInput} from 'react-native-paper';//component library
import {Icon,} from 'react-native-elements';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../../styles';
export default class ModalButton extends React.Component{
    constructor(props){
        super(props);
        console.log(this.props.valeur)
    }
 
    render(){
        return(
            <Modal
                animationType="none"
                transparent={true}
                visible={this.props.modalVisibleButton} >
                  <View style={styles.align_center_view}>
                        <View style={{width:0.8*width, backgroundColor:'white',elevation : 5,borderRadius : 10,overflow : 'hidden'}}>
                            <Text style={{fontSize:23,color:'black',fontWeight:'500'}}>{this.props.edit}</Text>
                            <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                {this.props.title}
                            </Text>
                            <View style={styles.option}>
                            <TouchableOpacity style={{flexDirection:'row',justifyContent:'space-between'}} onPress={this.props.onPressSelect} >
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>
                                {
                                    this.props.chosen=='Select Item'?
                                        this.props.valeur
                                    :
                                    this.props.chosen
                                }
                                    </Text>                               
                                <Icon name="chevron-down" size={20} type='material-community'/>
                            </TouchableOpacity>
                            </View>                        
                            <View style={{flexDirection:'row'}}>
                                <TouchableOpacity 
                                onPress={()=>{
                                    if(this.props.chosen=='Select Item')
                                {
                                    this.props.onPressSave(this.props.valeur2);
                                    this.props.onPressCancel();
                                }
                                else{
                                    this.props.onPressSave(this.props.value);this.props.onPressCancel();
                                };
                                }}
                                style={{ backgroundColor: '#53aefe', height: 50, width: 0.35 * width, borderRadius: 10, marginTop: 20, marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>Save</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.props.onPressCancel} style={{ backgroundColor: '#53aefe', height: 50, width: 0.35 * width, borderRadius: 10, marginTop: 20, alignItems: 'center', elevation: 5, justifyContent: 'center' }}  >
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >Cancel</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
            </Modal>
        )}}