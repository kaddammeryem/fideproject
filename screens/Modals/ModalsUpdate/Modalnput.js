import React from 'react';
import {TouchableOpacity,Text, View, Modal,Dimensions} from 'react-native';
import { TextInput} from 'react-native-paper';//component library
import {Icon,} from 'react-native-elements';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../../styles';
export default class ModalInput extends React.Component{
    constructor(props){
        super(props);
        this.state={
            potentialname:'',
        }
       
       
       
    }
    
    
    render(){

       
        return(
            <Modal
                animationType="none"
                transparent={true}
                visible={this.props.modalVisibleInput} >
                  <View style={styles.align_center_view}>
                        <View style={{width:0.8*width, backgroundColor:'white',elevation : 5,borderRadius : 10,overflow : 'hidden'}}>
                            <Text style={{fontSize:23,color:'black',fontWeight:'500'}}>{this.props.edit}</Text>
                            <Text style={{marginTop:8,color: '#53aefe',fontWeight:'bold'}}>{this.props.title}</Text>
                            <TextInput label={this.props.valeur} placeholder={this.props.title} value={this.state.potentialname} onChangeText={text => {this.setState({potentialname:text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                            <View style={{flexDirection:'row'}}>
                                <TouchableOpacity onPress={()=>{
                                    if(this.state.potentialname=='')
                                {
                                    this.props.onPressSave(this.props.valeur)
                                }
                                else{
                                    this.props.onPressSave(this.state.potentialname)
                                };
                                this.setState({potentialname:''})}}
                                style={{ backgroundColor: '#53aefe', height: 50, width: 0.35 * width, borderRadius: 10, marginTop: 20, marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>Save</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>{this.props.onPressCancel();this.setState({potentialname:''}) }} style={{ backgroundColor: '#53aefe', height: 50, width: 0.35 * width, borderRadius: 10, marginTop: 20, alignItems: 'center', elevation: 5, justifyContent: 'center' }}  >
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >Cancel</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
            </Modal>
        )}}