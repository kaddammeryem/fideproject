import React from 'react';
import {TouchableOpacity,Text, View, Modal,Dimensions} from 'react-native';
import { TextInput} from 'react-native-paper';//component library
import {Icon,} from 'react-native-elements';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { format } from "date-fns";
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../../styles';
var date=new Date();
export default class ModalDate extends React.Component{
    constructor(props){
        super(props);
        this.state={
            value:'',
            visibleDate:false,
        }
      
    }
    render(){
        return(
            <Modal
                animationType="none"
                transparent={true}
                visible={this.props.modalVisibleDate} >
                  <View style={styles.align_center_view}>
                        <View style={{width:0.8*width, backgroundColor:'white',elevation : 5,borderRadius : 10,overflow : 'hidden'}}>
                            <Text style={{fontSize:23,color:'black',fontWeight:'500'}}>{this.props.edit}</Text>
                            <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',marginTop: 20, color: '#53aefe' }}>
                                  {this.props.title}
                            </Text>
                           
                            <TouchableOpacity style={{flexDirection:'row',justifyContent:'space-between'}}onPress={()=>this.setState({visibleDate:true})} >
                               
                            <Text style={{  fontSize: 18, color: 'gray' }}>
                                {
                                    this.state.value==''?
                                        this.props.valeur
                                    :
                                    this.state.value
                                }
                                
                            </Text>
                                   
                                    
                                <Icon name="calendar" size={20} type='material-community'/>
                            </TouchableOpacity>
                            <DateTimePickerModal
                                isVisible={this.state.visibleDate}
                                mode="date"                           
                                onConfirm={(x) => {this.setState({value:x.getFullYear()+'-'+x.getMonth()+'-'+x.getDate(),visibleDate:false})}}
                                onCancel={() => {}}
                            />                               
                            <View style={{flexDirection:'row'}}>
                                <TouchableOpacity  onPress={()=>{
                                        if(this.state.value=='')
                                    {
                                        this.props.onPressSave(this.props.valeur);
                                        this.props.onPressCancel();
                                    }
                                    else{
                                        this.props.onPressSave(this.state.value);this.props.onPressCancel();
                                    };
                                    this.setState({value:''})}}
                                    style={{ backgroundColor: '#53aefe', height: 50, width: 0.35 * width, borderRadius: 10, marginTop: 50, marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>Save</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>{this.props.onPressCancel();this.setState({value:''});}} style={{ backgroundColor: '#53aefe', height: 50, width: 0.35 * width, borderRadius: 10, marginTop: 50, alignItems: 'center', elevation: 5, justifyContent: 'center' }}  >
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >Cancel</Text>
                                </TouchableOpacity>
                            </View>
                                                  
                                  
                                   
                           
                        </View>
                    </View>
            </Modal>
        )}}