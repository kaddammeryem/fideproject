import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import { ScrollView } from 'react-native';
import { StyleSheet,Modal,View ,Text ,TouchableOpacity,Dimensions,Image} from 'react-native';
import {  TextInput,IconButton } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';//icons library
import { format } from "date-fns";
import DateTimePickerModal from "react-native-modal-datetime-picker";//DatePicker library
import AsyncStorage from '@react-native-async-storage/async-storage';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
var date=new Date()
export default class ModalProspects extends Component {
   constructor(props){
     super(props);
     this.state={
       firstname:'',
       lastname:'',
       company:'',
       annualrevenue:'',
       city:'',
       code:'',
       email:'',
       secondaryemail:'',
       fax:'',
       phone:'',
       noofemployees:'',
       mobile:'',
       departement:'',
       country:'',
       videName:false,
       loadingShow:false,
       enable:false,    
       description:'',       
       userId:'',


       
     }  
     console.log(this.props.note)
   }
   initialiserValues=()=>{
     this.setState({name:'',
     firstname:'',
     lastname:'',
     company:'',
     annualrevenue:'',
     city:'',
     code:'',
     email:'',
     secondaryemail:'',
     fax:'',
     phone:'',
     noofemployees:'',
     mobile:'',
     departement:'',
     country:'',
     videName:false,
     enable:false,    
     description:'',       
     userId:'',
})

   }
   postFunc=async()=>{
    this.setState({videName:false})   

    if(this.state.lastname=='' ){console.log('inside vide ');this.setState({videName:true})}
    else{
    this.setState({loadingShow:true,enable:true,videName:false})  
    try{ 
      await AsyncStorage.multiGet(['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]});console.log(result)});

    var data={"firstname":this.state.firstname,"lastname":this.state.lastname,
   "description":this.state.description,"company":this.state.company,
    "annualrevenue":this.state.annualrevenue,"city":this.state.city,
    "code":this.state.code,"email":this.state.email,"secondaryemail":this.state.secondaryemail,
    "fax":this.state.fax,
    "noofemployees":this.state.noofemployees,"mobile":this.state.mobile,"state":this.state.departement,
    "phone":this.state.phone,"pobox":this.state.pobox,"website":this.state.website,
    "industry":this.props.secteur,"leadsource":this.props.source,"rating":this.props.note,
    "leadstatus":this.props.statut,"assigned_user_id":this.state.userId,"country":this.state.country}
    await fetch('https://portail.crm4you.ma/webservice.php', {
          method: 'POST',
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
         body:'operation=create&sessionName='+this.state.session+'&element='+JSON.stringify(data)+'&elementType=Leads'})
    .then((responses)  => responses.json()) 
    .then((json)=> {this.setState({loadingShow:false,enable:false}) ;console.log(json) } )
    .then(this.props.onPressSave) 
    .then(this.initialiserValues)
    .then(this.props.onPressCancel)   
    .catch((error) => console.error(error));
        }catch(r){}}
}
    render() { 
        return (
         
 
            <Modal
            animationType="none"
            transparent={true}
            visible={this.props.modalVisibleItem} 
            >
            <View style={styles.align_center_view}>
                <SafeAreaView style={{height : '80%',elevation : 5,borderRadius : 10,overflow : 'hidden' }}>
                    <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}>
                        <Text style={{ backgroundColor: 'white', color: '#495057', marginTop: 13, fontSize: 25, width: width * 0.8, height: 40, marginBottom: 9 }}>
                            Créer nouveau Prospect
                        </Text>
                        {this.state.videName ?
                            <Text style={{  color:'red'}} >
                                Le champs Nom  est vide
                            </Text>
                        : null}
                        <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',color: '#53aefe',fontSize:20 }}>
                            Détails Prospect
                        </Text>
                        <TextInput label="*Nom " value={this.state.lastname}onChangeText={(change)=>this.setState({lastname:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />      
                        <TextInput label="Prénom" value={this.state.firstname}onChangeText={(change)=>this.setState({firstname:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                           
                        <TextInput label="Télephone principal" value={this.state.phone}onChangeText={(change)=>this.setState({phone:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />     
                        <TextInput label="Email principal"value={this.state.email} onChangeText={(change)=>this.setState({email:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                                                                
                        <TextInput label="Société" value={this.state.company}onChangeText={(change)=>this.setState({company:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                           
                        <TextInput label="Mobile" value={this.state.mobile}onChangeText={(change)=>this.setState({mobile:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                           
                        <TextInput label="Fax" value={this.state.fax}onChangeText={(change)=>this.setState({fax:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                           
                        <TextInput label="Titre" value={this.state.designation}onChangeText={(change)=>this.setState({designation:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                           

                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Source 
                            </Text>
                            <View style={styles.option}>
                                <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressSource}>
                                    <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.source}</Text>
                                    <View style={styles.align_center_view}>
                                        <Icon name="caret-down" size={20} />
                                    </View>
                                </TouchableOpacity>
                            </View> 

                            <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',marginTop: 20, color: '#53aefe' }}>
                                  Secteur
                              </Text>
                              <View style={styles.option}>
                                <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressSecteur}>
                                    <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.secteur}</Text>
                                    <View style={styles.align_center_view}>
                                        <Icon name="caret-down" size={20} />
                                    </View>
                                </TouchableOpacity>
                            </View> 

                              <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',marginTop: 20, color: '#53aefe' }}>
                                  Statut
                              </Text>
                              <View style={styles.option}>
                                <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressStatut}>
                                    <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.statut}</Text>
                                    <View style={styles.align_center_view}>
                                        <Icon name="caret-down" size={20} />
                                    </View>
                                </TouchableOpacity>
                            </View> 
                                          
                            <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Note
                        </Text>
                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressNote}>
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.note}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="caret-down" size={20} />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <TextInput label="Revenu annuel" value={this.state.annualrevenue}onChangeText={(change)=>this.setState({annualrevenue:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                           
                        <TextInput label="Effectif" value={this.state.noofemployees}onChangeText={(change)=>this.setState({noofemployees:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                           
                        <TextInput label="Email secondaire" value={this.state.secondaryemail}onChangeText={(change)=>this.setState({secondaryemail:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                           
                        <TextInput label="Site web" value={this.state.website}onChangeText={(change)=>this.setState({website:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                           

                        <View>                           
                        <Text style={{ backgroundColor: 'white',fontSize:20, width: width * 0.8, fontWeight : 'bold',color: '#53aefe',marginTop: 20, }}>
                            Détails de l'adresse
                        </Text>
                        <TextInput label="Rue" value={this.state.lane}onChangeText={(change)=>this.setState({lane:change})} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                        <TextInput label="Code postal" value={this.state.code}onChangeText={(change)=>this.setState({code:change})} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                        <TextInput label="Pays" value={this.state.country}onChangeText={(change)=>this.setState({country:change})} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                        <TextInput label="Boite postale"value={this.state.pobox} onChangeText={(change)=>this.setState({pobox:change})} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                        <TextInput label="Ville" value={this.state.city}onChangeText={(change)=>this.setState({city:change})} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                        <TextInput label="Département" value={this.state.departement}onChangeText={(change)=>this.setState({departement:change})} style={{ backgroundColor: 'white', width: width * 0.8 }} />

                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe',fontSize:20, }}>
                          Information sur la description
                        </Text>
                        <TextInput label="Description" value={this.state.description}onChangeText={(change)=>this.setState({description:change})} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                       
                           </View>

                       
                    </ScrollView>
                    <View style={{borderTopWidth :1 ,borderTopColor : '#53aefe', width: width * 0.8, height: 60, justifyContent: 'space-evenly' ,flexDirection: 'row', alignItems: 'center' }}>
                                            <TouchableOpacity disabled={this.state.enable}onPress={this.postFunc}style={{position : 'relative', backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>Save</Text></TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.props.onPressCancel();this.initialiserValues()}}style={{ backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  alignItems: 'center', elevation: 5, justifyContent: 'center' }} ><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >Cancel</Text></TouchableOpacity>
                    </View>
                </SafeAreaView>
            </View>

        </Modal>

      
        );
    }
}
 





const styles =StyleSheet.create({
    view_Check:{
      marginTop : 10,
      flexDirection : 'row',
      height : 40 ,
      width : width*0.8,
      borderRadius : 5,
      borderWidth : 1,
      borderColor : 'blue'
    },
    option:{
      marginTop : 10,
      flexDirection : 'row',
      height : 40 ,
      width : width*0.8,
      borderRadius : 5,
      borderWidth : 1,
      borderColor : 'gray'
    },
    align_center_view:{
      flex:1,
      justifyContent:'center',
      alignItems:'center',
    },
    scrollView: {
      backgroundColor: 'white',
      padding : 10
    },
    text: {
      fontSize: 42,
    },
})