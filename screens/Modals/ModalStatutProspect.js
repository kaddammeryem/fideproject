import React from 'react';
import {ScrollView,TouchableOpacity,Text, View, Modal,Dimensions} from 'react-native';
import {Icon} from 'react-native-elements';
import {Searchbar} from 'react-native-paper';
import Item from '../ItemsRender/item';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
export default class ModalStatutProspect extends React.Component{
    render(){
        return(
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleItem}>
                  <View style={[styles.align_center_view]}>
                    <View style={{ backgroundColor: 'white',  borderRadius: 10, elevation: 2, overflow: 'hidden' }}>
                            <View style={styles.titleModal1}>
                                <Text style={{ fontSize: 23,  flex: 10, fontWeight: 'bold', color: 'white' }}>Statut</Text>
                                <TouchableOpacity style={{ fontSize: 25,  flex: 1 }} onPress={this.props.onPressClose}><Icon name="close" color='white' size={25} type='material-community'/></TouchableOpacity>
                            </View>                    
                            <View style={{ backgroundColor: 'white', height: height * 0.5, width: width * 0.9, borderRadius: 10, elevation: 2, overflow: 'hidden' }}>    
                            <ScrollView>
                                <Item text="Attente de contact" onPress={()=>this.props.onPressOption('Attente de contact')} />
                                <Item text="Froid" onPress={()=>this.props.onPressOption('Froid')} />   
                                <Item text="A contacter" onPress={()=>this.props.onPressOption('A contacter')} />                
                                <Item text="Contacté" onPress={()=>this.props.onPressOption('Contacté')} />                
                                <Item text="Chaud" onPress={()=>this.props.onPressOption('Chaud')} />                
                                <Item text="En sommeil" onPress={()=>this.props.onPressOption('En sommeil')} />                
                                <Item text="Perdu" onPress={()=>this.props.onPressOption('Perdu')} />                
                                <Item text="Non contacté" onPress={()=>this.props.onPressOption('Non contacté')} />                
                                <Item text="Pré-qualifié" onPress={()=>this.props.onPressOption('Pré-qualifié')} />                
                                <Item text="Qualifié" onPress={()=>this.props.onPressOption('Qualifié')} />                
                                <Item text="Brulant" onPress={()=>this.props.onPressOption('Brulant')} />                
                                </ScrollView>
                            </View>
                    </View>
                  </View>
            </Modal>
        )}}