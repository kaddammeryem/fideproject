import React from 'react';
import {ScrollView,TouchableOpacity,Text, View, Modal,Dimensions} from 'react-native';
import {Icon} from 'react-native-elements';
import {Searchbar} from 'react-native-paper';
import Item from '../ItemsRender/item';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
export default class ModalSelectType extends React.Component{
    render(){
        return(
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleItem}>
                  <View style={[styles.align_center_view]}>
                    <View style={{ backgroundColor: 'white',  borderRadius: 10, elevation: 2, overflow: 'hidden' }}>
                            <View style={styles.titleModal1}>
                                <Text style={{ fontSize: 23,  flex: 10, fontWeight: 'bold', color: 'white' }}>Type</Text>
                                <TouchableOpacity style={{ fontSize: 25,  flex: 1 }} onPress={this.props.onPressClose}><Icon name="close" color='white' size={25} type='material-community'/></TouchableOpacity>
                            </View>                    
                            <View style={{ backgroundColor: 'white', height: height * 0.5, width: width * 0.9, borderRadius: 10, elevation: 2, overflow: 'hidden' }}>    
                                <Item text="Client existant" onPress={()=>this.props.onPressOption('Client existant')} />
                                <Item text="Nouveau client" onPress={()=>this.props.onPressOption('Nouveau client')} />                
                            </View>
                    </View>
                  </View>
            </Modal>
        )}}