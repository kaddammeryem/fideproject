import React from 'react';
import {TouchableOpacity,Text, View, Modal,Dimensions} from 'react-native';
import {Icon} from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import Item from '../ItemsRender/item';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
export default class ModalSelectSecteur extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleSecteur}>
                  <View style={[styles.align_center_view]}>
                    <View style={{ backgroundColor: 'white',  borderRadius: 10, elevation: 2, overflow: 'hidden' }}>
                            <View style={styles.titleModal1}>
                                <Text style={{ fontSize: 25,  flex: 10, fontWeight: 'bold', color: 'white' }}>{this.props.select}</Text>
                                <TouchableOpacity style={{ fontSize: 30,  flex: 1 }} onPress={this.props.onPressClose}><Icon name="close" color='white' size={25} type='material-community'/></TouchableOpacity>
                            </View>                    
                            <View style={{ backgroundColor: 'white', height: height * 0.5, width: width * 0.9, borderRadius: 10, elevation: 2, overflow: 'hidden' }}>    
                            <ScrollView>
                                <Item text="Apparel" onPress={()=>this.props.onPressOption('Apparel')} />
                                <Item text="VAR/Systems Integrator" onPress={()=>this.props.onPressOption('VAR/Systems Integrator')} />
                                <Item text="Energie, Chimique, Utilities" onPress={()=>this.props.onPressOption('Energie, Chimique, Utilities')}/>
                                <Item text="High Tech-Hardware" onPress={()=>this.props.onPressOption('High Tech-Hardware')}/>
                                <Item text="Financial Services-Other" onPress={()=>this.props.onPressOption('Financial Services-Other')}/>
                                <Item text="Medical,Pharma,Biotech" onPress={()=>this.props.onPressOption('Medical,Pharma,Biotech')}/>
                                <Item text="High Tech-Other" onPress={()=>this.props.onPressOption('High Tech-Other')}/>
                                <Item text="Software-Other" onPress={()=>this.props.onPressOption('Software-Other')}/>
                                <Item text="Real Estate" onPress={()=>this.props.onPressOption('Real Estate')}/>
                                <Item text="banking and Securities" onPress={()=>this.props.onPressOption('banking and Securities')}/>
                                <Item text="Accounting" onPress={()=>this.props.onPressOption('Accounting')}/>
                                <Item text="Transportation and Distributions" onPress={()=>this.props.onPressOption('Transportation and Distributions')}/>
                                <Item text="Government-Federal" onPress={()=>this.props.onPressOption('Government-Federal')}/>
                                <Item text="Hospitality,Travel,Tourism-Federal" onPress={()=>this.props.onPressOption('Hospitality,Travel,Tourism-Federal')}/>
                                <Item text="Legal" onPress={()=>this.props.onPressOption('Legal')}/>
                                <Item text="Advertising/Marketing/PR" onPress={()=>this.props.onPressOption('Advertising/Marketing/PR')}/>
                                <Item text="Aerospace and Defense" onPress={()=>this.props.onPressOption('Aerospace and Defense')}/>
                                </ScrollView>
                                       
                            </View>
                    </View>
                  </View>
            </Modal>
        )}}