import React from 'react';
import {TouchableOpacity,Text, View, Modal,Dimensions,ScrollView} from 'react-native';
import {Icon} from 'react-native-elements';
import Item from '../ItemsRender/item';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
export default class ModalTypeUpdate extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleTypeUpdate}>
                  <View style={[styles.align_center_view]}>
                    <View style={{ backgroundColor: 'white',  borderRadius: 10, elevation: 2, overflow: 'hidden' }}>
                            <View style={styles.titleModal1}>
                                <Text style={{ fontSize: 25,  flex: 10, fontWeight: 'bold', color: 'white' }}>{this.props.select}</Text>
                                <TouchableOpacity style={{ fontSize: 30,  flex: 1 }} onPress={this.props.onPressClose}><Icon name="close" color='white' size={25} type='material-community'/></TouchableOpacity>
                            </View>                    
                            <View style={{ backgroundColor: 'white', height: height * 0.5, width: width * 0.9, borderRadius: 10, elevation: 2, overflow: 'hidden' }}>    
                            <ScrollView>
                                <Item text="Prospect" onPress={()=>this.props.onPressOption('Prospect')} />
                                <Item text="Client" onPress={()=>this.props.onPressOption('Client')} />
                                <Item text="Revendeur" onPress={()=>this.props.onPressOption('Revendeur')}/>
                                <Item text="Partenaire" onPress={()=>this.props.onPressOption('Partenaire')}/>
                                <Item text="Presse" onPress={()=>this.props.onPressOption('Presse')}/>
                                <Item text="Analyste" onPress={()=>this.props.onPressOption('Analyste')}/>
                                <Item text="Concurrent" onPress={()=>this.props.onPressOption('Concurrent')}/>
                                <Item text="Integrateur" onPress={()=>this.props.onPressOption('Integrateur')}/>
                                <Item text="Investisseur" onPress={()=>this.props.onPressOption('Investisseur')}/>
                                <Item text="Autre" onPress={()=>this.props.onPressOption('Autre')}/>
                                </ScrollView>

                                       
                            </View>
                    </View>
                  </View>
            </Modal>
        )}}