import React from 'react';
import {SafeAreaView,ScrollView,TouchableOpacity,Text, View, Modal,Dimensions,Image} from 'react-native';
import { TextInput} from 'react-native-paper';//component library
import {Icon,} from 'react-native-elements';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { format } from "date-fns";
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
import AsyncStorage from '@react-native-async-storage/async-storage';
var date=new Date();
export default class ModalAffaires extends React.Component{
    constructor(props){
        super(props);
        this.state={
            potentialname:'',
            loadingShow:true,
            amount:'', 
            enable:false,         
            closingdate:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
            probability:'',
            nextstep:'',
            forecast_amount:'',
            description:'',
            visibleDate:false,
            userId:'',
            videName:false,
            session:''
           
            

        }
    }
    initialiserValues=()=>{
        this.setState({
        potentialname:'',      
        amount:'',          
        closingdate:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
        probability:'',
        nextstep:'',
        description:'',
       })

    }
    postFunc=async()=>{
        this.setState({videName:false})   

        if(this.state.potentialname=='' ){console.log('inside vide ');this.setState({videName:true})}
        else{
        this.setState({loadingShow:true,enable:true,videName:false})
        try{
        await AsyncStorage.multiGet(['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]});console.log(result)});

        var data={"potentialname":this.state.potentialname,"contact_id":this.props.idContact,
        "related_to":this.props.idCompte,"description":this.state.description,
        "leadsource":this.state.leadsource,"forecast_amount":this.state.forecast_amount,
        "probability":this.state.probability,"opportunity_type":this.props.type,
        "nextstep":this.state.nextstep,"closingdate":this.state.closingdate,
        "sales_stage":this.props.phase,"assigned_user_id":this.state.userId,"amount":this.state.amount}
        await fetch('https://portail.crm4you.ma/webservice.php', {
              method: 'POST',
              headers: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
             body:'operation=create&sessionName='+this.state.session+'&element='+JSON.stringify(data)+'&elementType=Potentials'})
        .then((responses)  => responses.json()) 
        .then((json)=> this.setState({loadingShow:false,enable:false}))
        .then(this.props.onPressSave)
        .then(this.initialiserValues) 
        .then(this.props.onPressCancel)   
        .catch((error) => console.error(error));
            }catch(r){}
        }
    }
    render(){
        return(
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.AffaireModal} >
                  <View style={styles.align_center_view}>
                      <SafeAreaView style={{height : '71%',elevation : 5,borderRadius : 10,overflow : 'hidden' }}>
                          <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}>
                              <Text style={{ backgroundColor: 'white', color: '#495057', marginTop: 13, fontSize: 25, width: width * 0.8, height: 40, marginBottom: 9 }}>
                                  Créer nouvelle Affaire
                              </Text>
                              {this.state.videName ?
                          <View >
                            <Text style={{  color:'red'}} >
                            Le champs Nom de l'affaire est vide
                            </Text>
                            </View>
                        : null}
                              <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',color: '#53aefe' }}>
                                  Détail 
                              </Text>
                              <TextInput label="*Nom de l'affaire" onChangeText={text => {this.setState({potentialname:text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                            
                              <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                  Nom de l'organisation
                              </Text>
                              <View style={styles.view_Check}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.props.onPressCompte()}>
                                      <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.accountname}</Text>
                                      <View style={styles.align_center_view}>
                                          <Icon name="chevron-down" size={20} type='material-community'/>
                                      </View>
                                  </TouchableOpacity>
                                 
                              </View>

                              <TextInput label="Montant(DH)" onChangeText={text => {this.setState({amount:text})}}style={{ backgroundColor: 'white', width: width * 0.8 }} />


                              <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',marginTop: 20, color: '#53aefe' }}>
                                  *Date de fermeture attendue
                              </Text>
                              <View style={styles.option}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.setState({visibleDate:true})} >
                                        <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.state.closingdate}</Text>
                                        <View style={styles.align_center_view}>
                                          <Icon name="calendar" size={20} type='material-community'/>
                                        </View>
                                        <DateTimePickerModal
                                            isVisible={this.state.visibleDate}
                                            mode="date"                           
                                            onConfirm={(x) => {this.setState({closingdate:x.getFullYear()+'-'+x.getMonth()+'-'+x.getDate(),visibleDate:false})}}
                                            onCancel={() => {}}
                                        />
                                  </TouchableOpacity>
                              </View>
                              <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                  *Phase de vente
                              </Text>
                              <View style={styles.option}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressPhase}>
                                      <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.phase}</Text>
                                      <View style={styles.align_center_view}>
                                          <Icon name="chevron-down" size={20} type='material-community'/>
                                      </View>
                                  </TouchableOpacity>
                              </View>
                              <TouchableOpacity style={{flexDirection : 'row',marginTop : 10,justifyContent : 'center' }} 
                              onPress={this.props.onPressInfo} >
                                <Text style={{color : 'blue',borderBottomWidth : 1 , borderBottomColor :'blue'}} > {this.props.MoreinfoText} d'information   </Text>
                                <Icon name={this.props.MoreinfoIcon} size={20} color = 'blue' style={{borderBottomWidth : 1, borderBottomColor :'blue'}} type='material-community'/>

                              </TouchableOpacity>
                              {this.props.Moreinfo ? (
                                 <View>

                                    <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                        Nom Du contact
                                    </Text>
                                    <View style={styles.view_Check}>
                                        <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.props.onPressContact()}>
                                            <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.lastname}</Text>
                                            <View style={styles.align_center_view}>
                                                <Icon name="chevron-down" size={20} type='material-community'/>
                                            </View>
                                           
                                        </TouchableOpacity>
                                       
                                    </View>

                                    <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                        Type
                                    </Text>
                                    <View style={styles.option}>
                                        <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.props.onPressType()}>
                                            <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.type}</Text>
                                            <View style={styles.align_center_view}>
                                                <Icon name="chevron-down" size={20} type='material-community'/>
                                            </View>
                                        </TouchableOpacity>
                                    </View>

                                    
                                    <TextInput label="Suivant" onChangeText={text => {this.setState({nextstep:text})}}style={{ backgroundColor: 'white', width: width * 0.8 }} />

                                    
                                      <TextInput label="Probabilité(%)" onChangeText={text => {this.setState({probability:text})}}style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                      
                                      <TextInput label="Description" onChangeText={text => {this.setState({description:text})}}multiline={true} style={{ backgroundColor: 'white', height: 70, width: width * 0.8 }} />
                                   </View>)  :null}
                          </ScrollView>
                          <View style={{borderTopWidth :1 ,borderTopColor : '#53aefe', width: width * 0.8, height: 60, justifyContent: 'space-evenly' ,flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity disabled={this.state.enable}onPress={this.postFunc}style={{position : 'relative', backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>Save</Text></TouchableOpacity>
                                <TouchableOpacity onPress={()=>{this.props.onPressCancel();this.initialiserValues()}}style={{ backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  alignItems: 'center', elevation: 5, justifyContent: 'center' }} ><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >Cancel</Text></TouchableOpacity>
                            </View>
                      </SafeAreaView>
                  </View>

              </Modal>
     
        )}}