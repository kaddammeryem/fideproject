import React from 'react';
import {SafeAreaView,ScrollView,TouchableOpacity,Text, View, Modal,Dimensions,Image} from 'react-native';
import { TextInput} from 'react-native-paper';//component library
import {Icon} from 'react-native-elements';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class ModalComptes extends React.Component{
    constructor(props){
        super(props);
        this.state={
            accountname:"",
            enable:false,
            loadingShow:true,
            phone:'',
            website:'',
            userId:'',
            videName:false,
            session:'',
           
           
        }
    }
    initialiserValues=()=>{
        this.setState({
            accountname:"",           
            phone:'',
            website:'',
        })
    }
    postFunc=async()=>{
        this.setState({videName:false})   
        if(this.state.accountname=='' ){console.log('inside vide ');this.setState({videName:true})}
        else{
        this.setState({loadingShow:true,enable:true,videName:false,})
        try{
        await AsyncStorage.multiGet(['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]});console.log(result)});

        var data={"accountname":this.state.accountname,"phone":this.state.phone,"website":this.state.website,"assigned_user_id":this.state.userId}
        await fetch('https://portail.crm4you.ma/webservice.php', {
              method: 'POST',
              headers: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
             // body:'operation=create&sessionName='+this.props.result+'&element={"accountname":'+JSON.stringify(this.state.accountname)+',"phone":'+JSON.stringify(this.state.phone)+',"website":'+JSON.stringify(this.state.website)+',"assigned_user_id":"19x7"}&elementType=Accounts'
             body:'operation=create&sessionName='+this.state.session+'&element='+JSON.stringify(data)+'&elementType=Accounts'

            })
            .then((responses)  => responses.json())
            .then((json) => {    console.log(json); this.setState({loadingShow:false,enable:false})})
           .then(this.props.onPressSave)
           .then(this.initialiserValues())
           .then(this.props.onPressCancel)
            .catch((error) => console.error(error));
            }catch(r){

            }
        }
      }
    render(){
        return(
           
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisible} >
                  <View style={styles.align_center_view}>
                    <SafeAreaView style={{height : '71%',elevation : 5,borderRadius : 10,overflow : 'hidden', backgroundColor : 'white' , alignItems : 'center'}}>
                          <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}>
                              <Text style={{ backgroundColor: 'white', color: '#495057', marginTop: 13, fontSize: 25, width: width * 0.8, height: 40, marginBottom: 9 }}>
                                  Créer Nouveaux Comptes
                              </Text>
                              {this.state.videName ?
                          <View >
                            <Text style={{  color:'red'}} >
                            Le champs Nom du compte est vide
                            </Text>
                            </View>
                        : null}
                              <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', color: '#53aefe' }}>
                                  Détail du compte
                              </Text>
                              <TextInput label="*Nom compte"  onChangeText={text => {this.setState({accountname:text});console.log(this.state.accountname)}}style={{ backgroundColor: 'white', width: width * 0.8 }} />
                              <TextInput label="Télephone principal" onChangeText={text => this.setState({phone:text})}style={{ backgroundColor: 'white', width: width * 0.8 }} />
                              <TextInput label="Site web" onChangeText={text => this.setState({website:text})}style={{ backgroundColor: 'white', width: width * 0.8 }} />
                              
                          </ScrollView>
                          <View style={{borderTopWidth :1 ,borderTopColor : '#53aefe', width: width * 0.8, height: 60, justifyContent: 'space-evenly' ,flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity disabled={this.state.enable}onPress={this.postFunc}style={{position : 'relative', backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>Save</Text></TouchableOpacity>
                                <TouchableOpacity onPress={()=>{this.props.onPressCancel();this.setState({videName:false});this.initialiserValues()}}style={{ backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  alignItems: 'center', elevation: 5, justifyContent: 'center' }} ><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >Cancel</Text></TouchableOpacity>
                            </View>
                      </SafeAreaView>
                  </View>

              </Modal>
       
        )}}