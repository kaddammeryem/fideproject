import React from 'react';
import {TouchableOpacity,Text, View, Modal,Dimensions} from 'react-native';
import {Icon} from 'react-native-elements';
import Item from '../ItemsRender/item';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
export default class ModalSelectItem extends React.Component{
    render(){
        return(
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleItem}>
                  <View style={[styles.align_center_view]}>
                    <View style={{ backgroundColor: 'white',  borderRadius: 10, elevation: 2, overflow: 'hidden' }}>
                            <View style={styles.titleModal1}>
                                <Text style={{ fontSize: 25,  flex: 10, fontWeight: 'bold', color: 'white' }}>{this.props.select}</Text>
                                <TouchableOpacity style={{ fontSize: 30,  flex: 1 }} onPress={this.props.onPressClose}><Icon name="close" size={25} color='white' type='material-community'/></TouchableOpacity>
                            </View>                    
                            <View style={{ backgroundColor: 'white', height: height * 0.5, width: width * 0.9, borderRadius: 10, elevation: 2, overflow: 'hidden' }}>    
                                <Item text="Prospection" onPress={()=>this.props.onPressOption('Propspection')} />
                                <Item text="Qualification" onPress={()=>this.props.onPressOption('Qualification')} />
                                <Item text="Proposition" onPress={()=>this.props.onPressOption('Proposition')}/>
                                <Item text="Négociation" onPress={()=>this.props.onPressOption('Négociation')}/>
                                <Item text="Gagnée"  onPress={()=>this.props.onPressOption('Gagnée')}/>
                                <Item text="Perdue"  onPress={()=>this.props.onPressOption('Perdue')}/>            
                            </View>
                    </View>
                  </View>
            </Modal>
        )}}