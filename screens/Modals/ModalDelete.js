import React from 'react';

import {TouchableOpacity,Text, View, Modal,Dimensions} from 'react-native';
import { TextInput} from 'react-native-paper';//component library
import {Icon} from 'react-native-elements';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
import { Button } from 'react-native';

export default class ModalDelete extends React.Component{
    render(){
        return(
            <Overlay isVisible={this.state.modalVisibleDelete} >
                <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                  <Text>Do you want to delete?</Text>

                </View>
                <View style={{flexDirection:'row-reverse'}}>
                  <Button title="yes" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}} onPress={()=>this.setState({modalVisibleDelete:false})}/>
                  <Button title="no" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}}  onPress={()=>this.setState({modalVisibleDelete:false})}/>
                </View>
              </Overlay>
        )}}