import React from 'react';
import {ScrollView,SafeAreaView,TouchableOpacity,Text, View, Modal,Dimensions,Image} from 'react-native';
import { TextInput} from 'react-native-paper';//component library
import {Icon} from 'react-native-elements';
import DateTimePickerModal from "react-native-modal-datetime-picker";
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import { format } from "date-fns";
import styles from '../styles';
import AsyncStorage from '@react-native-async-storage/async-storage';
var date=new Date()
export default class ModalMilestone extends React.Component{
    constructor(props){
        super(props);
        this.state={
            visibleDateDebut:false,
            loadingShow:false,
            projectmilestonename:'',
            datedebut:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
            userId:'',
            videName:false,
            videProject:false,
            session:'',
            
        }
    }
    initialiserValues=()=>{
        this.setState({
            visibleDateDebut:false,
            loadingShow:true,
            projectmilestonename:'',
            datedebut:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
            videName:false,
            videProject:false,
        })
    }
    postFunc=async()=>{
        this.setState({videName:false,videProject:false})   
        if(this.state.projectmilestonename=='' ){console.log('inside vide ');this.setState({videName:true})}
        else if (this.props.idProject=='' ){console.log('inside vide conpte');this.setState({videProject:true})}
        else{
        this.setState({loadingShow:true,videName:false,videProject:false})    
        try{
        await AsyncStorage.multiGet(['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]});console.log(result)});

        var data={"projectid":this.props.idProject,"projectmilestonename":this.state.projectmilestonename,
        "projectmilestonedate":this.state.datedebut,"projectmilestonetype":this.props.type,"assigned_user_id":this.state.userId}
        await fetch('https://portail.crm4you.ma/webservice.php', {
              method: 'POST',
              headers: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
             body:'operation=create&sessionName='+this.state.session+'&element='+JSON.stringify(data)+'&elementType=ProjectMilestone'})
        .then((responses)  => responses.json()) 
        .then((json)=> this.setState({loadingShow:false})   )
        .then(this.props.onPressSave)  
        .then(this.initialiserValues)
        .catch((error) => console.error(error));
        this.props.onPressCancel();
       
            }catch(r){}}
    }
    render(){
        return(
           
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleMilestone} >
                  <View style={styles.align_center_view}>
                      <SafeAreaView style={{height : '80%',elevation : 5,borderRadius : 10,overflow : 'hidden' }}>
                          <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}>
                              <Text style={{ backgroundColor: 'white', color: '#495057', marginTop: 13, fontSize: 23, width:'100%', height: 40, marginBottom: 9}}>
                                  Créer nouveau Jalon de Projet
                              </Text>
                              {this.state.videName ?
                            <Text style={{  color:'red'}} >
                                Le champs Nom du Jalon est vide
                            </Text>
                        : null}
                        {this.state.videProject?
                            <Text style={{  color:'red'}} >
                                Le champs Relatif à est vide
                            </Text>
                        : null}
                              <Text style={{ backgroundColor: 'white', width: width * 0.8, color: '#53aefe',marginLeft:8 }}>
                                  Détail du projet
                              </Text>
                              <TextInput label="Nom du projet Milestone*"value={this.state.projectmilestonename} style={{ backgroundColor: 'white', width: width * 0.8 }} onChangeText={(change)=>this.setState({projectmilestonename:change})}/>
                              <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                  Relatif à
                              </Text>
                              <View style={styles.view_Check}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.props.onPressProject()}>
                                      <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.projectname}</Text>
                                      <View style={styles.align_center_view}>
                                          <Icon name="chevron-down" size={20} type='material-community'/>
                                      </View>
                                  </TouchableOpacity>
                                 
                              </View>


                              <Text style={{ backgroundColor: 'white', width: width * 0.8, marginTop: 20, color: '#53aefe',marginLeft:8 }}>
                                 Date de Milestone
                              </Text>
                               <View style={styles.option}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.setState({visibleDateDebut:true})}>
                                      <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.state.datedebut}</Text>
                                      <View style={styles.align_center_view}>
                                          <Icon name="calendar" size={20} type='material-community'/>
                                      </View>
                                      <DateTimePickerModal
                                            isVisible={this.state.visibleDateDebut}
                                            mode="date"                           
                                            onConfirm={(x) => {this.setState({datedebut:x.getFullYear()+'-'+x.getMonth()+'-'+x.getDate(),visibleDateDebut:false})}}
                                            onCancel={() => {}}
                                        />
                                  </TouchableOpacity>
                              </View>
                              <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                        Type
                                    </Text>
                                    <View style={styles.option}>
                                        <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.props.onPressType()}>
                                            <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.type}</Text>
                                            <View style={styles.align_center_view}>
                                                <Icon name="chevron-down" size={20} type='material-community'/>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                              <View style={{ width: width * 0.8, marginBottom : 20, height: 60, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', marginTop: 20, marginBottom: 10 }}>
                                    <TouchableOpacity onPress={this.postFunc}style={{ backgroundColor: '#53aefe', height: 50, width: 0.35 * width, borderRadius: 10, marginTop: 20, marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}>
                                      <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>Save</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>{this.props.onPressCancel();this.initialiserValues()}} style={{ backgroundColor: '#53aefe', height: 50, width: 0.35 * width, borderRadius: 10, marginTop: 20, alignItems: 'center', elevation: 5, justifyContent: 'center' }}  >
                                      <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >Cancel</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ width: width * 0.8, height: 10,}}> 
                                </View>
                          </ScrollView>
                      </SafeAreaView>
                  </View>

              </Modal>
      
        )}}