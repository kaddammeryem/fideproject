import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import { ScrollView } from 'react-native';
import { StyleSheet,Modal,View ,Text ,TouchableOpacity,Dimensions,Image} from 'react-native';
import {  TextInput } from 'react-native-paper';
import {Icon } from 'react-native-elements';//icons library
import { format } from "date-fns";
import DateTimePickerModal from "react-native-modal-datetime-picker";//DatePicker library
import AsyncStorage from '@react-native-async-storage/async-storage';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
var date=new Date();
export default class ModalActivities extends Component {
   constructor(props){
     super(props);
    console.log(this.props.result+' '+this.props.idCompte)
     this.state={
        loadingShow:true,
        enable:false,
       sujet:'',
       startdate:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
       enddate:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
       starttime:date.getHours()+':'+date.getMinutes(),
       endtime:date.getHours()+':'+(date.getMinutes()+5),
       description:'',
       solution:'',
       session:'',
       userId:'',
       idCompte:this.props.idCompte,
       visibleEndDate:false,
       visibleEndTime:false,
       visibleStartDate:false,
       visibleStartTime:false,
       videSujet:false,
       videCompte:false,
     }
   }
   initaliserValues=()=>{
       this.setState({
        sujet:'',
       startdate:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
       enddate:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
       starttime:date.getHours()+':'+date.getMinutes(),
       endtime:date.getHours()+':'+date.getMinutes(),
       description:'',
       solution:'',})

   }
   postFunc=async()=>{
    this.setState({videSujet:false,videCompte:false})   


       if(this.state.sujet=='' ){this.setState({videSujet:true})}
       else if (this.props.idCompte==undefined || this.props.idCompte=='' ){this.setState({videCompte:true})}
       else{
    this.setState({videSujet:false,videCompte:false,loadingShow:true,enable:true})
    try{
    await AsyncStorage.multiGet(['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]})});
   

    var data={"subject":this.state.sujet,"contact_id":this.props.idContact,
    "parent_id":this.state.idCompte,"description":this.state.description,
    "time_end":this.state.endtime,"activitytype":this.props.selectPriority,
    "time_start":this.state.starttime,"eventstatus":this.props.selectStatut,
    "due_date":this.state.enddate,"taskpriority":this.props.priority,
    "duration_hours":"5",
    "date_start":this.state.startdate,"assigned_user_id":this.state.userId}
    await fetch('https://portail.crm4you.ma/webservice.php', {
          method: 'POST',
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
         body:'operation=create&sessionName='+this.state.session+'&element='+JSON.stringify(data)+'&elementType=Events'})
    .then((responses)  => {responses.json()}) 
    .then((json)=> {this.setState({loadingShow:false,enable:false})})
    .then(this.props.onPressSave) 
    .then(this.initaliserValues)
    .then(this.props.onPressCancel)   
    .catch((error) => console.log(error));
        }
    
        catch(t){
            console.log(t);
        }}
}
    render() { 
        return (
            
            <Modal
            animationType="none"
            transparent={true}
            visible={this.props.modalVisibleActivities} 
            >
            <View style={styles.align_center_view}>
              <SafeAreaView style={{height : '71%',elevation : 5,borderRadius : 10,overflow : 'hidden', backgroundColor : 'white' , alignItems : 'center'}}>
                    <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}>
                        <Text style={{ backgroundColor: 'white', color: '#495057', marginTop: 13, fontSize: 25, width: width * 0.8, height: 40, marginBottom: 9 }}>
                        Créer nouvelle Activité
                        </Text>
                        {this.state.videSujet ?
                            <Text style={{  color:'red'}} >
                                Le champs Sujet est vide
                            </Text>
                        : null}
                        {this.state.videCompte?
                            <Text style={{  color:'red'}} >
                                Le champs Nom du compte est vide
                            </Text>
                        : null}
                        <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',color: '#53aefe' }}>
                            Détails de l'événement
                        </Text>
                        <TextInput label="*Sujet" value={this.state.sujet}onChangeText={(change)=>this.setState({sujet:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />  
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            *Type
                            </Text>
                            <View style={styles.option}>
                                <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressType}>
                                    <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.selectType}</Text>
                                    <View style={styles.align_center_view}>
                                        <Icon name="menu-down" size={20} type='material-community'/>
                                    </View>
                                </TouchableOpacity>
                            </View>   
                            <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            *Statut
                        </Text>
                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressStatut}>
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.selectStatut}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="menu-down" size={20} type='material-community'/>
                                </View>
                            </TouchableOpacity>
                        </View> 
                        <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',marginTop: 20, color: '#53aefe' }}>
                                  Date de début
                        </Text>
                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.setState({visibleStartDate:true})} >
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.state.startdate}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="calendar" size={20} type='material-community'/>
                                </View>
                                <DateTimePickerModal
                                    isVisible={this.state.visibleStartDate}
                                    mode="date"                           
                                    onConfirm={(x) => {this.setState({startdate:x.getFullYear()+'-'+x.getMonth()+'-'+x.getDate(),visibleStartDate:false})}}
                                    onCancel={() => {}}
                                />
                            </TouchableOpacity>
                        </View>
                        <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',marginTop: 20, color: '#53aefe' }}>
                                  Heure de début
                        </Text>
                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.setState({visibleStartTime:true})} >
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.state.starttime}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="clock-time-four-outline" size={20} type='material-community'/>
                                </View>
                                <DateTimePickerModal
                                    isVisible={this.state.visibleStartTime}
                                    mode="time"                           
                                    onConfirm={(x) => 
                                       { 
                                        if(x.getMinutes()<10){
                                           
                                            this.setState({starttime:x.getHours()+':0'+x.getMinutes(), visibleStartTime:false})
                                        }
                                        else{
                                            this.setState({starttime:x.getHours()+':'+x.getMinutes(), visibleStartTime:false})}}
                                    }
                                    onCancel={() => {}}
                                />
                            </TouchableOpacity>
                        </View> 
                        <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',marginTop: 20, color: '#53aefe' }}>
                                  Date de fin
                        </Text>
                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.setState({visibleEndDate:true})} >
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.state.enddate}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="calendar" size={20} type='material-community'/>
                                </View>
                                <DateTimePickerModal
                                    isVisible={this.state.visibleEndDate}
                                    mode="date"                           
                                    onConfirm={(x) => 
                                        
                                        {this.setState({enddate:x.getFullYear()+'-'+x.getMonth()+'-'+x.getDate(),visibleEndDate:false})}}
                                    onCancel={() => {}}
                                />
                            </TouchableOpacity>
                        </View>
                        
                       
                        <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',marginTop: 20, color: '#53aefe' }}>
                                  Heure de fin
                        </Text>

                        <View style={styles.option}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.setState({visibleEndTime:true})} >
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.state.endtime}</Text>
                                <View style={styles.align_center_view}>
                                    <Icon name="clock-time-four-outline" size={20} type='material-community'/>
                                </View>
                                <DateTimePickerModal
                                    isVisible={this.state.visibleEndTime}
                                    mode="time"                           
                                    onConfirm={(x) => {
                                   
                                    if(x.getMinutes()<10){
                                        
                                        this.setState({endtime:x.getHours()+':0'+x.getMinutes(),visibleEndTime:false})
                                    }
                                    else{
                                    this.setState({endtime:x.getHours()+':'+x.getMinutes(),visibleEndTime:false})}}}
                                    onCancel={() => {}}
                                />
                            </TouchableOpacity>
                        </View>  
                       
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                           *Nom du Compte 
                        </Text>
                        <View style={styles.view_Check}>
                          <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressCompte}>
                            <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.accountname} </Text>
                            <View style={styles.align_center_view}>
                              <Icon name="menu-down" size={20} type='material-community'/>
                            </View>
                          </TouchableOpacity>                        
                        </View>
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            Nom du Contact
                        </Text>
                        <View style={styles.view_Check}>
                          <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressItemContact}>
                            <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.lastname}</Text>
                            <View style={styles.align_center_view}>
                              <Icon name="menu-down" size={20} type='material-community'/>
                            </View>
                          </TouchableOpacity>
                          
                        </View>
                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                          Information sur la description
                        </Text>
                        <TextInput label="Description" value={this.state.description}onChangeText={(change)=>this.setState({description:change})} style={{ backgroundColor: 'white', width: width * 0.8 }} />

                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                            *Priorité
                            </Text>
                            <View style={styles.option}>
                                <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressPriority}>
                                    <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.selectPriority}</Text>
                                    <View style={styles.align_center_view}>
                                        <Icon name="menu-down" size={20} type='material-community'/>
                                    </View>
                                </TouchableOpacity>
                            </View>    
                    </ScrollView>
                    <View style={{borderTopWidth :1 ,borderTopColor : '#53aefe', width: width * 0.8, height: 60, justifyContent: 'space-evenly' ,flexDirection: 'row', alignItems: 'center' }}>
                      <TouchableOpacity disabled={this.state.enable}onPress={()=>{this.postFunc()}}style={{position : 'relative', backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>Save</Text></TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.props.onPressCancel();this.setState({videSujet:false,videCompte:false});this.initaliserValues() }}style={{ backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  alignItems: 'center', elevation: 5, justifyContent: 'center' }} ><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >Cancel</Text></TouchableOpacity>
                    </View>
                    
                </SafeAreaView>
                
            </View>

        </Modal>
  
      
        );
    }
}
 





const styles =StyleSheet.create({
    view_Check:{
      marginTop : 10,
      flexDirection : 'row',
      height : 40 ,
      width : width*0.8,
      borderRadius : 5,
      borderWidth : 1,
      borderColor : 'blue'
    },
    option:{
      marginTop : 10,
      flexDirection : 'row',
      height : 40 ,
      width : width*0.8,
      borderRadius : 5,
      borderWidth : 1,
      borderColor : 'gray'
    },
    align_center_view:{
      flex:1,
      justifyContent:'center',
      alignItems:'center',
    },
    scrollView: {
      backgroundColor: 'white',
      padding : 10
    },
    text: {
      fontSize: 42,
    },
})