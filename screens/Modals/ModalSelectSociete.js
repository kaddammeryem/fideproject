import React from 'react';
import {TouchableOpacity,Text, View, Modal,Dimensions,FlatList} from 'react-native';
import {Icon,SearchBar} from 'react-native-elements';
import styles from '../styles';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class ModalSelectSociete extends React.Component{
    constructor(props){
        super(props);
        this.state={
            data:[],
            dataComptes2:[],
            searchTextinput :'',
            userId:'',
            sesison:'',
        }
        
    }
   
    fetching=async()=>{
        try{
        await AsyncStorage.multiGet(
            ['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]})})

       
        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+this.state.session+'&query=select * from Accounts ;')
        .then((response) => response.json())
        .then((json)=>{this.setState({data:json.result,dataComptes2:json.result});console.log('compte :'+JSON.stringify(json)) })
        .catch((error)=>
            console.log('error in societe : '+error))
        
         } catch(r){

        }}
    componentDidMount(){
        this.fetching();
    }
    filter = (searchText) => {

        this.setState({
          searchTextinput: searchText,
          data: this.state.dataComptes2.filter(i =>
            i.accountname.toUpperCase().includes(searchText.toUpperCase()))
        })
        
    
      }
    
    render(){
        return(
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleCompte}>
                  <View style={[styles.align_center_view]}>
                      <View style={{ backgroundColor: 'white', height: height * 0.4, width: width * 0.9, borderRadius: 10, elevation: 2, overflow: 'hidden' }}>
                          <View style={styles.titleModal1}>
                              <Text style={{ fontSize: 25, marginRight: 10, flex: 10, fontWeight: 'bold', color: 'white' }}>Comptes</Text>
                              <TouchableOpacity style={{ fontSize: 30, marginRight: 10, flex: 1 }} onPress={this.props.onPressClose}><Icon name="close" color='white' size={30} type='material-community'/></TouchableOpacity>
                          </View> 
                            <View style={[styles.searchbar,{borderWidth: 0.5, backgroundColor: 'white', borderRadius: 30, overflow: 'hidden', flexDirection: 'row', width: '95%', marginTop: 5, alignItems: 'center'} ]}>
                              <TouchableOpacity style={[styles.align_center_view, { flex: 0.5, height: 40, marginEnd: 3 , backgroundColor: 'white'}]}>
                                  <Icon name="chevron-down" size={20} type='material-community'/>
                              </TouchableOpacity>
                              <SearchBar style={{ height: 40, flex: 7 ,backgroundColor:'white'}}   
                              value={this.state.searchTextinput} 
                              inputContainerStyle={{backgroundColor:'white'}}   
                              onChangeText={(text) => this.setState({searchTextinput : text})}  
                              onSubmitEditing={async () =>  this.filter(this.state.searchTextinput)} 
                              containerStyle={{ flex : 7 , height :50,alignItems : 'center',justifyContent : 'center' ,backgroundColor:'white'}}
                              placeholder="Search"
                              onClear={() =>{ this.setState({data : this.state.dataComptes2 });}}
                              />
                         
                          </View>
                          <FlatList
                              data={this.state.data}
                              renderItem={this.props.renderItem}
                              keyExtractor={item => item.id}
                          />
                      </View>
                  </View>
              </Modal>
        )}}