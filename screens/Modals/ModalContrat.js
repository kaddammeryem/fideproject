import React from 'react';
import {Modal,View ,Text ,TouchableOpacity,Dimensions,SafeAreaView, ScrollView,Image } from 'react-native';
import {  TextInput} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';//icons library
import styles from '../styles'
//import Icon from 'react-native-vector-icons/FontAwesome';//icons library
import { format } from "date-fns";
import DateTimePickerModal from "react-native-modal-datetime-picker";//DatePicker library
import AsyncStorage from '@react-native-async-storage/async-storage';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
var date=new Date()
export default class ModalAddContrat extends React.Component {
    
        constructor(props) {
            super(props);
            this.state={
                progress:'',
                subject :'',
                enable:false,
                userId:'',
                isVisibleDate : false,
                Moreinfo : false,
                MoreinfoIcon : "caret-down",
                MoreinfoText : 'Plus',
                date_debut : date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
                unites_totale : '',
                unites_occasion : '',
                due_date : date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),               
                loadingShow:true,
                videSujet:false,
                session:''
            }}
            initialiserValues=()=>{
                this.setState({
                    progress:'',
                    subject :'',
                    Moreinfo : false,
                    MoreinfoIcon : "caret-down",
                    MoreinfoText : 'Plus',
                    date_debut : date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
                    unites_totale : '',
                    unites_occasion : '',
                    due_date : date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(), 
                    loadingShow:false,
                    enable:false, 
                    videSujet:false,       
                    
                })
            }



        filter = async (Text ,searchText , Data , Result,proprety) => {
            console.log(this.state[`${Data}`]);
            await this.setState({
              [`${searchText}`] : Text,
              [`${Result}`] : this.state[`${Data}`].filter((i,n) =>
                 i[`${proprety}`].toUpperCase().includes(Text.toUpperCase()))
            })
        }

        setDate = (date) => {
            let day = date.getUTCDate();
            let year = date.getFullYear() ;
            let month = date.getMonth()+1 ;
            let Date = `${day}-${month}-${year}`
            this.setState({
              [`${this.state.modifiedDate}`] : Date,
              isVisibleDate : false 
            })
      
      
        }
        postFunc=async()=>{
            this.setState({videSujet:false})   

            if(this.state.subject=='' ){console.log('inside vide ');this.setState({videSujet:true})}
            else{
            this.setState({videSujet:false,loadingShow:true,enable:true})  
            try{  
                await AsyncStorage.multiGet(['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]});console.log(result)});

            var data={"subject":this.state.subject,
            "sc_related_to":this.props.idCompte,"contract_status":this.props.statut,
            "contract_type":this.props.type,"contract_priority":this.props.priority,
            "progress":this.state.progress,"start_date":this.props.date_debut,
            "tracking_unit":this.props.unite,"total_units":this.state.total_units,
            "used_units":this.state.unites_occasion,"due_date":this.state.due_date,"assigned_user_id":this.state.userId}
            await fetch('https://portail.crm4you.ma/webservice.php', {
                  method: 'POST',
                  headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                  },
                 body:'operation=create&sessionName='+this.state.session+'&element='+JSON.stringify(data)+'&elementType=ServiceContracts'})
            .then((responses)  => responses.json()) 
            .then(this.props.onPressSave) 
            .then(this.initialiserValues)
            .then(this.props.onPressCancel)   
            .catch((error) => console.error(error));
                }catch(r){}}
        }
    
    render() { 
        return (
           
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleContrat} >
                  <View style={styles.align_center_view}>
                      <SafeAreaView style={{height : '71%',elevation : 5,borderRadius : 10,overflow : 'hidden', backgroundColor : 'white' , alignItems : 'center'}}>
                          <Text style={{ backgroundColor: 'white', color: '#495057', marginTop: 13, fontSize: 20, width: width * 0.8, height: 40, marginBottom: 9 }}>
                              Créer nouveau Contrat de Service
                          </Text>
                          {this.state.videSujet ?
                          <View style={{alignItems:'flex-start',marginRight:"33%"}}>
                            <Text style={{  color:'red'}} >
                            Le champs Sujet est vide
                            </Text>
                            </View>
                        : null}
                          <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}>
                              <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',color: '#53aefe' }}>
                                  Information sur le Contrat de Service
                              </Text>
                              
                              <TextInput  label="*Sujet"onChangeText={(change)=>{this.setState({subject:change})}}style={{ backgroundColor: 'white', width: width * 0.8 }} multiline={true} />
                                <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                    Relatif à la
                                </Text>
                                <View style={styles.view_Check}>
                                <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressCompte}>
                                    <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.accountname} </Text>
                                    <View style={styles.align_center_view}>
                                    <Icon name="caret-down" size={20} />
                                    </View>
                                </TouchableOpacity>                        
                                </View>
                              <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                  Unités de suivi
                              </Text>
                              <View style={styles.option}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressUnite}>
                                      <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.unite}</Text>
                                      <View style={styles.align_center_view}>
                                          <Icon name="caret-down" size={20} type="material-community"/>
                                      </View>
                                  </TouchableOpacity>
                              </View>
                              <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',marginTop: 20, color: '#53aefe' }}>
                                Date de début
                              </Text>
                              <View style={styles.option}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={() => { this.setState({isVisibleDate : true , modifiedDate : 'date_debut'}); }}>
                                      <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.state.date_debut}</Text>
                                      <View style={styles.align_center_view}>
                                          <Icon name="calendar" size={20} type="material-community"/>
                                      </View>
                                  </TouchableOpacity>
                                  <DateTimePickerModal
                                    isVisible={this.state.isVisibleDate} 
                                    mode="date" 
                                    onConfirm={(date) => this.setDate(date)} 
                                    onCancel={() => {this.setState({ isVisibleDate : false}) }}/>
                              </View>
                              <TextInput multiline={true} onChangeText={(change)=>this.setState({total_units:change})}label="Unités totales" style={{ backgroundColor: 'white', width: width * 0.8 }} multiline={true}  />
                              <Text style={{ backgroundColor: 'white', width: width * 0.8, fontWeight : 'bold',marginTop: 20, color: '#53aefe' }}>
                                  Due date
                              </Text>
                              <View style={styles.option}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={() => { this.setState({isVisibleDate : true , modifiedDate : 'due_date'}); }}>
                                      <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.state.due_date}</Text>
                                      <View style={styles.align_center_view}>
                                          <Icon name="calendar" size={20} type="material-community"/>
                                      </View>
                                  </TouchableOpacity>
                                  <DateTimePickerModal
                                    isVisible={this.state.isVisibleDate} 
                                    mode="date" 
                                    onConfirm={(date) => this.setDate(date)} 
                                    onCancel={() => {this.setState({ isVisibleDate : false}) }}/>
                              </View>                             
                              <TextInput multiline={true} onChangeText={(change)=>this.setState({unites_occasion:change})}label="Unités d'occasion" style={{ backgroundColor: 'white', width: width * 0.8 }} multiline={true}  />
                              <TouchableOpacity style={{flexDirection : 'row',marginTop : 10,marginBottom : 10,justifyContent : 'center' }} 
                               onPress={() => { if(this.state.Moreinfo){
                                this.setState({
                                  Moreinfo : false,
                                  MoreinfoIcon : "caret-down",
                                  MoreinfoText : 'Plus'
                                })
                               }else{
                                this.setState({
                                  Moreinfo : true,
                                  MoreinfoIcon : "caret-up",
                                  MoreinfoText : 'Moins'
                                })
                               }
                                 }} >
                                <Text style={{color : 'blue',borderBottomWidth : 1 , borderBottomColor :'blue'}} > {this.state.MoreinfoText} d'information   </Text>
                                <Icon name={this.state.MoreinfoIcon} size={20} type="material-community" color = 'blue' style={{borderBottomWidth : 1, borderBottomColor :'blue'}} />

                              </TouchableOpacity>
                              {this.state.Moreinfo ? (
                                  <View>
                                      <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                      Type
                                      </Text>
                                      <View style={styles.option}>
                                      <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressType}>
                                          <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.type}</Text>
                                          <View style={styles.align_center_view}>
                                              <Icon name="caret-down" size={20} type="material-community"/>
                                          </View>
                                      </TouchableOpacity>
                                      </View>
                                      <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                            Statut
                                        </Text>
                                        <View style={styles.option}>
                                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressStatut}>
                                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.statut}</Text>
                                                <View style={styles.align_center_view}>
                                                    <Icon name="caret-down" type="material-community" size={20} />
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                            Priorité
                                        </Text>
                                        <View style={styles.option}>
                                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressPriority}>
                                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.priority}</Text>
                                                <View style={styles.align_center_view}>
                                                    <Icon name="caret-down" size={20} type="material-community"/>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                            
                                            </View>)  :null}
                                    </ScrollView>
                                    <View style={{borderTopWidth :1 ,borderTopColor : '#53aefe', width: width * 0.8, height: 60, justifyContent: 'space-evenly' ,flexDirection: 'row', alignItems: 'center' }}>
                                            <TouchableOpacity disabled={this.state.enable}onPress={this.postFunc}style={{position : 'relative', backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>Save</Text></TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.props.onPressCancel();this.initialiserValues()}}style={{ backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  alignItems: 'center', elevation: 5, justifyContent: 'center' }} ><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >Cancel</Text></TouchableOpacity>
                                        </View>
                                </SafeAreaView>
                            </View>

              </Modal>

            
           
        );
    }
}