import React from 'react';
import {TouchableOpacity,Text, View, Modal,Dimensions,FlatList} from 'react-native';
import {Icon,SearchBar} from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';

import styles from '../styles';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class ModalSelectContacts extends React.Component{
    constructor(props){
        super(props);
        this.state={
            data:[],
            dataContacts2:[],
            searchTextinput :'',
            session:'',
            userId:'',
         
        }
       
    }
   
    fetching=async()=>{
        try{  
        if(this.props.activitie=='yes'){
            console.log('inside if')
            
                await AsyncStorage.multiGet(['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]});console.log(result)});
            await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+this.state.session+'&query=select * from Contacts where account_id='+this.props.idCompte+';')
            .then((response) =>response.json())
            .then((json)=>{console.log(json);this.setState({data:json.result,dataContacts2:json.result});})
            .catch((error)=>
                console.log(error))
        }
        else{
            await AsyncStorage.multiGet(['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]});console.log(result)});

        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+this.state.session+'&query=select * from Contacts ;')
        .then((response) => response.json())
        .then((json)=>{this.setState({data:json.result,dataContacts2:json.result});console.log(json) })
        .catch((error)=>
            console.log(error))}}
            catch(r){}
        }
       
    componentDidMount(){
        this.fetching();
    }
    filter = (searchText) => {

        this.setState({
          searchTextinput: searchText,
          data: this.state.dataContacts2.filter(i =>
            i.lastname.toUpperCase().includes(searchText.toUpperCase()))
        })
        
    
      }
    
    render(){
        return(
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleContacts}>
                  <View style={[styles.align_center_view]}>
                      <View style={{ backgroundColor: 'white', height: height * 0.4, width: width * 0.9, borderRadius: 10, elevation: 2, overflow: 'hidden' }}>
                          <View style={styles.titleModal1}>
                              <Text style={{ fontSize: 25, marginRight: 10, flex: 10, fontWeight: 'bold', color: 'white' }}>Contacts</Text>
                              <TouchableOpacity style={{ fontSize: 30, marginRight: 10, flex: 1 }} onPress={this.props.onPressClose}><Icon name="close" size={30} type='material-community' color='white'/></TouchableOpacity>
                          </View>
                          <View style={[styles.searchbar,{borderWidth: 0.5, backgroundColor: 'white', borderRadius: 30, overflow: 'hidden', flexDirection: 'row', width: '95%', marginTop: 5, alignItems: 'center'} ]}>
                              <TouchableOpacity style={[styles.align_center_view, { flex: 0.5, height: 40, marginEnd: 3 , backgroundColor: 'white'}]}>
                                  <Icon name="chevron-down" size={20} type='material-community'/>
                              </TouchableOpacity>
                              <SearchBar style={{ height: 40, flex: 7 ,backgroundColor:'white'}}   
                              value={this.state.searchTextinput} 
                              inputContainerStyle={{backgroundColor:'white'}}   
                              onChangeText={(text) => this.setState({searchTextinput : text})}  
                              onSubmitEditing={async () =>  this.filter(this.state.searchTextinput)} 
                              containerStyle={{ flex : 7 , height :50,alignItems : 'center',justifyContent : 'center' ,backgroundColor:'white'}}
                              placeholder="Search"
                              onClear={() =>{ this.setState({data : this.state.dataContacts2 });console.log('on clear '+JSON.stringify(this.state.data))}}
                              />
                          </View>
                          <FlatList
                              data={this.state.data}
                              renderItem={this.props.renderItem}
                              keyExtractor={item => item.id}
                          />
                      </View>
                  </View>
              </Modal>
        )}}