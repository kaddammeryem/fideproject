import React from 'react';
import {ScrollView,SafeAreaView,TouchableOpacity,Text, View, Modal,Dimensions} from 'react-native';
import { TextInput} from 'react-native-paper';//component library
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
export default class ModalDoc extends React.Component{
    render(){
        return(
        <Modal
            animationType="none"
            transparent={true}
            visible={this.props.modalVisibleDocuments} >
            <View style={styles.align_center_view}>
                <SafeAreaView style={{height : '80%',elevation : 5,borderRadius : 10,overflow : 'hidden' }}>
                    <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}>
                        <Text style={{ backgroundColor: 'white', color: '#495057', marginTop: 13, fontSize: 23, width: '100%', height: 40, marginBottom: 9, }}>
                            Create New Documents
                        </Text>
                        <Text style={{ backgroundColor: 'white', width: width * 0.8, color: '#53aefe',marginLeft:8 }}>
                            Informations sur le Document
                        </Text>
                        <TextInput label="Titre*" style={{ backgroundColor: 'white', width: width * 0.8 }} onChangeText={(change)=>this.props.onChangeTextDoc(change)} />                    
                        <Text style={{ backgroundColor: 'white', width: width * 0.8, marginTop: 20, color: '#53aefe',marginLeft:8 }}>
                           *Assigné à
                        </Text>
                        <View style={styles.view_Check}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressVisible1}>
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'black',fontWeight:'400' }}>DEMO</Text>
                            </TouchableOpacity>                                 
                        </View>       
                        <View style={styles.view_Check}>
                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressPick}>                                     
                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'black',fontWeight:'400' }}>{this.props.nameFile}</Text>                                     
                            </TouchableOpacity>                                  
                        </View>
                        <View style={{ width: width * 0.8, height: 60, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 50, marginBottom: 10, borderTopWidth: 0.5 }}>
                            <TouchableOpacity activeOpacity={0.7} style={[styles.buttonPost, { marginRight: 10 }]} onPress={this.props.onPressAdd }>
                              <Text style={{ fontWeight: 'bold', color: 'blue', fontSize: 15, textAlign: 'center' }}>Save</Text>
                              </TouchableOpacity>
                            <TouchableOpacity activeOpacity={0.7} style={[styles.buttonPost, { marginRight: 10 }]} onPress={this.props.onPressVisibleDoc} >
                              <Text>Cancel</Text>
                              </TouchableOpacity>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View>
        </Modal>)}}