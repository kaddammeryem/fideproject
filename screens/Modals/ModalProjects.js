import React from 'react';
import {ScrollView,SafeAreaView,TouchableOpacity,Text, View, Modal,Dimensions,Image} from 'react-native';
import { TextInput} from 'react-native-paper';//component library
import {Icon} from 'react-native-elements';
import DateTimePickerModal from "react-native-modal-datetime-picker";
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import { format } from "date-fns";
import styles from '../styles';
import AsyncStorage from '@react-native-async-storage/async-storage';
var date=new Date()
export default class ModalProjects extends React.Component{
    constructor(props){
        super(props);
        this.state={
            visibleDateDebut:false,
            loadingShow:true,
            visibleDateFin:false,
            projectname:'',
            enable:false,
            targetbudget:'',
            projecturl:'',
            progress:'',
            descr:'',
            Moreinfo: false,
            MoreinfoIcon: "caret-down",
            MoreinfoText: 'Plus',
            datedebut:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
            datefin:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
            userId:'',
            session:'',
            videName:false,
        }
        console.log('idCompte '+this.props.idCompte)
    }
    initialiserValues=()=>{
        this.setState({ visibleDateDebut:false,
            loadingShow:true,
            visibleDateFin:false,
            projectname:'',
            enable:false,
            targetbudget:'',
            projecturl:'',
            progress:'',
            descr:'',
            Moreinfo: false,
            MoreinfoIcon: "caret-down",
            MoreinfoText: 'Plus',
            datedebut:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
            datefin:date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(),
            videName:false,})

    }
    postFunc=async()=>{
        this.setState({videName:false})   

        if(this.state.projectname=='' ){console.log('inside vide ');this.setState({videName:true})}
else{
        this.setState({loadingShow:true,enable:true,videName:false}) 
        try{  
        await AsyncStorage.multiGet(['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]});console.log(result)});

        var data={"linktoaccountscontacts":this.props.idCompte,"projectname":this.state.projectname,
        "startdate":this.state.datedebut,"targetenddate":this.state.datefin,"assigned_user_id":this.state.userId,
        "progress":this.state.progress,"targetbudget":this.state.targetbudget,"projecturl":this.state.projecturl,
        "description":this.state.descr}
        await fetch('https://portail.crm4you.ma/webservice.php', {
              method: 'POST',
              headers: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
             body:'operation=create&sessionName='+this.state.session+'&element='+JSON.stringify(data)+'&elementType=Project'})
        .then((responses)  => responses.json()) 
        .then((json)=> this.setState({loadingShow:false,enable:false})   )
        .then(this.props.onPressSave)
        .then(this.initialiserValues)  
        .catch((error) => console.error(error));
        this.props.onPressCancel();
       
    }catch(r){}}
}
  
    render(){
        return(
           
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleProjects} >
                  <View style={styles.align_center_view}>
                      <SafeAreaView style={{height : '80%',elevation : 5,borderRadius : 10,overflow : 'hidden' }}>
                          <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}>
                              <Text style={{ backgroundColor: 'white', color: '#495057', marginTop: 13, fontSize: 23, width:'100%', height: 40, marginBottom: 9}}>
                                  Créer nouveau Projet
                              </Text>
                              {this.state.videName ?
                          <View >
                            <Text style={{  color:'red'}} >
                            Le champs Nom du projet est vide
                            </Text>
                            </View>
                        : null}
                              <Text style={{ backgroundColor: 'white', width: width * 0.8, color: '#53aefe',marginLeft:8 }}>
                                  Détail du projet
                              </Text>
                              <TextInput label="Nom du projet*" style={{ backgroundColor: 'white', width: width * 0.8 }} onChangeText={(change)=>this.setState({projectname:change})}/>
                              <Text style={{ backgroundColor: 'white', width: width * 0.8, marginTop: 20, color: '#53aefe',marginLeft:8 }}>
                                 Date de début
                              </Text>
                               <View style={styles.option}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.setState({visibleDateDebut:true})}>
                                      <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.state.datedebut}</Text>
                                      <View style={styles.align_center_view}>
                                          <Icon name="calendar" size={20} type='material-community'/>
                                      </View>
                                      <DateTimePickerModal
                                            isVisible={this.state.visibleDateDebut}
                                            mode="date"                           
                                            onConfirm={(x) => {this.setState({datedebut:x.getFullYear()+'-'+x.getMonth()+'-'+x.getDate(),visibleDateDebut:false})}}
                                            onCancel={() => {}}
                                        />
                                  </TouchableOpacity>
                              </View>
                              <Text style={{ backgroundColor: 'white', width: width * 0.8, marginTop: 20, color: '#53aefe',marginLeft:8 }}>
                                 Echéance fin
                              </Text>
                              <View style={styles.option}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={()=>this.setState({visibleDateFin:true})}>
                                      <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.state.datefin}</Text>
                                      <View style={styles.align_center_view}>
                                          <Icon name="calendar" size={20} type='material-community'/>
                                      </View>
                                      <DateTimePickerModal
                                            isVisible={this.state.visibleDateFin}
                                            mode="date"                           
                                            onConfirm={(x) => {this.setState({datefin:x.getFullYear()+'-'+x.getMonth()+'-'+x.getDate(),visibleDateFin:false})}}
                                            onCancel={() => {}}
                                        />
                                  </TouchableOpacity>
                              </View>
                              <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                    Relatif à 
                                </Text>
                                <View style={styles.view_Check}>
                                <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressCompte}>
                                    <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.accountname} </Text>
                                    <View style={styles.align_center_view}>
                                    <Icon name="caret-down" size={20} />
                                    </View>
                                </TouchableOpacity>                        
                                </View>
                              

                              <TouchableOpacity style={{flexDirection : 'row',marginTop : 10,marginBottom : 10,justifyContent : 'center' }} 
                               onPress={() => { if(this.state.Moreinfo){
                                this.setState({
                                  Moreinfo : false,
                                  MoreinfoIcon : "caret-down",
                                  MoreinfoText : 'Plus'
                                })
                               }else{
                                this.setState({
                                  Moreinfo : true,
                                  MoreinfoIcon : "caret-up",
                                  MoreinfoText : 'Moins'
                                })
                               }
                                 }} >
                                <Text style={{color : 'blue',borderBottomWidth : 1 , borderBottomColor :'blue'}} > {this.state.MoreinfoText} d'information   </Text>
                                <Icon name={this.state.MoreinfoIcon} size={20} type="material-community" color = 'blue' style={{borderBottomWidth : 1, borderBottomColor :'blue'}} />

                              </TouchableOpacity>
                              {this.state.Moreinfo ? (
                                  
                                  <View>

                                  

                                      <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                      Type
                                      </Text>
                                      <View style={styles.option}>
                                      <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressType}>
                                          <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.type}</Text>
                                          <View style={styles.align_center_view}>
                                              <Icon name="caret-down" size={20} type="material-community"/>
                                          </View>
                                      </TouchableOpacity>
                                      </View>
                                      <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                            *Statut
                                        </Text>
                                        <View style={styles.option}>
                                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressStatut}>
                                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.statut}</Text>
                                                <View style={styles.align_center_view}>
                                                    <Icon name="caret-down" type="material-community" size={20} />
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                            Priorité
                                        </Text>
                                        <View style={styles.option}>
                                            <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressPriority}>
                                                <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.priority}</Text>
                                                <View style={styles.align_center_view}>
                                                    <Icon name="caret-down" size={20} type="material-community"/>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        <TextInput  value={this.state.targetbudget}label="Budget cible"onChangeText={(change)=>{this.setState({targetbudget:change})}}style={{ backgroundColor: 'white', width: width * 0.8 }} multiline={true} />
                                    <TextInput  value={this.state.projecturl} label="Url du projet"onChangeText={(change)=>{this.setState({projecturl:change})}}style={{ backgroundColor: 'white', width: width * 0.8 }} multiline={true} />
                                    <TextInput  value={this.state.progress} label="progres"onChangeText={(change)=>{this.setState({progress:change})}}style={{ backgroundColor: 'white', width: width * 0.8 }} multiline={true} />
                                    <TextInput  value={this.state.descr} label="Description"onChangeText={(change)=>{this.setState({descr:change})}}style={{ backgroundColor: 'white', width: width * 0.8 }} multiline={true} />
                                            
                                            </View>)  :null}
                                    </ScrollView>
                                    <View style={{borderTopWidth :1 ,borderTopColor : '#53aefe', width: width * 0.8, height: 60, justifyContent: 'space-evenly' ,flexDirection: 'row', alignItems: 'center' }}>
                                            <TouchableOpacity disabled={this.state.enable}onPress={this.postFunc}style={{position : 'relative', backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>Save</Text></TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.props.onPressCancel();this.initialiserValues()}}style={{ backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  alignItems: 'center', elevation: 5, justifyContent: 'center' }} ><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >Cancel</Text></TouchableOpacity>
                                        </View>
                                </SafeAreaView>
                            </View>

              </Modal>
       
        )}}