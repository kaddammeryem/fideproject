import React from 'react';
import { Modal,View ,Text ,TouchableOpacity,Dimensions ,SafeAreaView ,ScrollView,Image} from 'react-native';
import {  Checkbox,TextInput } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';//icons library
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import styles from '../styles';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class ModalContacts extends React.Component{
    constructor(props){
        super(props);
  
            this.state = {
                paramater:'',
                enable:false,
                isVisibleDate : false,              
                firstname : '',
                lastname : '',
                teleBureau : '',
                email : '',
                service : '',
                fonction : '',
                mobile : '',
                assistant : '',
                assistantphone : '',
                secondaryemail:'',
                mailingcity:'',
                homephone : '',
                otherphone : '',
                fax : '',
                userId:'',
                mailingstreet : '',
                otherstreet : '',
                mailingpobox : '',
                otherpobox : '',
                othercity  : '',
                mailingstate : '',
                otherstate : '',
                mailingzip : '',
                otherzip : '',
                mailingcountry : '',
                othercountry : '',
                description : '',
                loadingShow:true,
                videName:false,
                session:''
        }
    }
    filter = async (Text ,searchText , Data , Result,proprety) => {
        await this.setState({
          [`${searchText}`] : Text,
          [`${Result}`] : this.state[`${Data}`].filter((i,n) =>
             i[`${proprety}`].toUpperCase().includes(Text.toUpperCase()))
        })
    }
    initialiserValues=()=>{
        this.setState({ firstname : '',
        lastname : '',
        teleBureau : '',
        email : '', 
        service : '',
        fonction : '',
        mobile : '',
        assistant : '',
        assistantphone : '',
        secondaryemail:'',
        mailingcity:'',
        homephone : '',
        otherphone : '',
        fax : '',
        mailingstreet : '',
        otherstreet : '',
        mailingpobox : '',
        otherpobox : '',
        othercity  : '',
        mailingstate : '',
        otherstate : '',
        mailingzip : '',
        otherzip : '',
        mailingcountry : '',
        othercountry : '',
        description : '',
       })
    }
    postFunc=async()=>{
        this.setState({videName:false})   

        if(this.state.lastname=='' ){console.log('inside vide ');this.setState({videName:true})}
        else{
        this.setState({loadingShow:true,enable:true,videName:false})
        try{
            await AsyncStorage.multiGet(['USERID','SESSION'], (err, result)=>{ this.setState({userId: result[0][1],session:result[1][1]});console.log(result)});

        var data={"lastname":this.state.lastname,"contact_id":this.props.idContact,
        "account_id":this.props.idCompte,"assistant":this.state.assistant,
        "assistantphone":this.state.assistantphone,"mailingstate":this.state.service,
        "description":this.state.description,"email":this.state.email,
        "fax":this.state.fax,"firstname":this.state.firstname,
        "homephone":this.props.homephone,"assigned_user_id":this.state.userId,"mailingcity":this.state.mailingcity,
        "mailingcountry":this.state.mailingcountry,"mailingpobox":this.state.mailingpobox,"mailingstate":this.state.mailingstate
        ,"mailingstreet":this.state.mailingstreet,"mailingzip":this.state.mailingzip,"mobile":this.state.mobile,
        "othercity":this.state.othercity,"othercountry":this.state.othercountry,"otherphone":this.state.otherphone,
        "otherpobox":this.state.otherpobox,"otherstate":this.state.otherstate,"otherstreet":this.state.otherstreet
        ,"mobile":this.state.mobile,"phone":this.state.phone,"secondaryemail":this.state.secondaryemail,"otherzip":this.state.otherzip,
        "title":this.state.fonction}
        await fetch('https://portail.crm4you.ma/webservice.php', {
              method: 'POST',
              headers: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
             body:'operation=create&sessionName='+this.state.session+'&element='+JSON.stringify(data)+'&elementType=Contacts'})
        .then((responses)  => responses.json()) 
       
        .then((json)=>{
            if(this.props.update=='yes'){         
                this.setState({paramater:json.result.id});
                this.props.onPressSave(this.state.paramater); 
                this.initialiserValues();
                this.setState({loadingShow:false,enable:false})          
            }
            else{
                this.props.onPressSave();   this.setState({loadingShow:false,enable:false,videName:false})    
                             
            }
        }) 
        .then(this.props.onPressCancel)   
        .catch((error) => console.error(error));
    }catch(r){}}
    }
    render(){
        return(
           
            <Modal
                  animationType="none"
                  transparent={true}
                  visible={this.props.modalVisibleContacts} >
                  <View style={styles.align_center_view}>
                  <SafeAreaView style={{height : '71%',elevation : 5,borderRadius : 10,overflow : 'hidden', backgroundColor : 'white' , alignItems : 'center'}}>
                          <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}>
                              <Text style={{ backgroundColor: 'white', color: '#495057', marginTop: 13, fontSize: 25, width: width * 0.8, height: 40, marginBottom: 9 }}>
                                  Créer nouveau Contact
                              </Text>
                              {this.state.videName ?
                          <View >
                            <Text style={{  color:'red'}} >
                            Le champs Nom est vide
                            </Text>
                            </View>
                        : null}
                              <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', color: '#53aefe' }}>
                                  Détail du contact
                              </Text>
                              <TextInput label="Prénom" onChangeText={(change)=>this.setState({firstname:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />
                              <TextInput label="*Nom" onChangeText={(change)=>this.setState({lastname:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />
                              <Text style={{ backgroundColor: 'white', width: width * 0.8,fontWeight : 'bold', marginTop: 20, color: '#53aefe' }}>
                                  Nom de compte
                              </Text>
                              <View style={styles.view_Check}>
                                  <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressCompte}>
                                      <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.accountname}</Text>
                                      <View style={styles.align_center_view}>
                                      <Icon name="caret-down" size={20} />
                                      </View>
                                  </TouchableOpacity>
                                  
                              </View>
                              <TextInput label="Téléphone" onChangeText={(change)=>this.setState({mobile:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />
                              <TextInput label="E-mail" onChangeText={(change)=>this.setState({email:change})}style={{ backgroundColor: 'white', width: width * 0.8 }} />                                            
                              
                              <TouchableOpacity style={{flexDirection : 'row',marginTop : 10,justifyContent : 'center' }} 
                               onPress={this.props.onPressInfo} >
                                <Text style={{color : 'blue',borderBottomWidth : 1 , borderBottomColor :'blue'}} > {this.props.MoreInfoText} d'information   </Text>
                                <Icon name={this.props.MoreInfoIcon} size={20} color = 'blue' style={{borderBottomWidth : 1, borderBottomColor :'blue'}} />
                                </TouchableOpacity>
                              {this.props.MoreInfo ? (
                                <View>
                                    <Text style={styles.titleItemStyle}>
                                        Supérieur hiérarchique
                                    </Text>
                                    <View style={styles.view_Check}>
                                        <TouchableOpacity style={{ alignItems: 'center', flexDirection: 'row', flex: 7 }} onPress={this.props.onPressContact}>
                                            <Text style={{ flex: 7, fontSize: 18, marginStart: 10, color: 'gray' }}>{this.props.lastname}</Text>
                                            <View style={styles.align_center_view}>
                                                <Icon name="caret-down" size={20} />
                                            </View>
                                        </TouchableOpacity>
                                       
                                    </View>
                                    <TextInput multiline={true} value={this.state.service} onChangeText={(text) => {this.setState({service : text})}} label="Service/Division" style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} value={this.state.fonction}  onChangeText={(text) => {this.setState({fonction : text})}} label="fonction"         style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} value={this.state.mobile}    onChangeText={(text) => {this.setState({mobile : text})}} label="mobile"         style={{ backgroundColor: 'white', width: width * 0.8 }} />                        
                                    <Text style={styles.titleItemStyle}>
                                        information personnalisé
                                    </Text>
                                    <TextInput multiline={true} value={this.state.assistant} onChangeText={(text) => {this.setState({assistant : text})}} label="assistant" style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} value={this.state.assistantphone} onChangeText={(text) => {this.setState({assistantphone : text})}} label="Téléphone(assistant)" style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} value={this.state.homephone} onChangeText={(text) => {this.setState({homephone : text})}} label="Téléphone(domicile)" style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} value={this.state.otherphone} onChangeText={(text) => {this.setState({otherphone : text})}} label="Téléphone Secondaire" style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} value={this.state.secondaryemail} onChangeText={(text) => {this.setState({secondaryemail: text})}} label="Email secondaire" style={{ backgroundColor: 'white', width: width * 0.8 }} />

                                    <TextInput multiline={true} value={this.state.fax} onChangeText={(text) => {this.setState({fax : text})}} label="fax" style={{ backgroundColor: 'white', width: width * 0.8 }} />                           
                                    <Text style={styles.titleItemStyle}>
                                        Details de l'adresse
                                    </Text>

                                    <TextInput multiline={true} label="Adresse"            value={this.state.mailingstreet} onChangeText={(text) => {this.setState({mailingstreet : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} label="Adresse(alt.)"      value={this.state.otherstreet} onChangeText={(text) => {this.setState({otherstreet : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} label="Case postale"       value={this.state.mailingpobox} onChangeText={(text) => {this.setState({mailingpobox : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} label="Other P.O Box"      value={this.state.otherpobox} onChangeText={(text) => {this.setState({otherpobox : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} label="Ville"      value={this.state.mailingcity} onChangeText={(text) => {this.setState({mailingcity : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} label="Ville (alt.) "      value={this.state.othercity} onChangeText={(text) => {this.setState({othercity : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} label="Departement"       value={this.state.mailingstate} onChangeText={(text) => {this.setState({mailingstate : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} label="Departement(alt.)" value={this.state.otherstate} onChangeText={(text) => {this.setState({otherstate : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} label="Code postal"        value={this.state.mailingzip} onChangeText={(text) => {this.setState({mailingzip : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} label="Code postal(alt.)"  value={this.state.otherzip} onChangeText={(text) => {this.setState({otherzip : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} label="Pays"               value={this.state.mailingcountry} onChangeText={(text) => {this.setState({mailingcountry : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <TextInput multiline={true} label="Pays(alt.)"         value={this.state.othercountry} onChangeText={(text) => {this.setState({othercountry : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />
                                    <Text style={styles.titleItemStyle}>
                                        Information sur la description
                                    </Text>  
                                    <TextInput multiline={true} label="Description" value={this.state.description} onChangeText={(text) => {this.setState({description : text})}} style={{ backgroundColor: 'white', width: width * 0.8 }} />                                                                   
                                </View>                               
                                ):null}
                          </ScrollView>
                          <View style={{borderTopWidth :1 ,borderTopColor : '#53aefe', width: width * 0.8, height: 60, justifyContent: 'space-evenly' ,flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity disabled={this.state.enable}onPress={this.postFunc}style={{position : 'relative', backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  marginRight: 10, elevation: 5, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}>Save</Text></TouchableOpacity>
                                <TouchableOpacity onPress={()=>{this.props.onPressCancel();this.initialiserValues()}}style={{ backgroundColor: '#53aefe', height: 40, width: 0.34 * width, borderRadius: 10,  alignItems: 'center', elevation: 5, justifyContent: 'center' }} ><Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15, textAlign: 'center' }}  >Cancel</Text></TouchableOpacity>
                        </View>
                      </SafeAreaView>
                      
                  </View>

              </Modal>
       
        )}}