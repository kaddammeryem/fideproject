import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createStackNavigator } from "@react-navigation/stack";
//components
import Affaires from "./Components/affaires";
import Comptes from "./Components/comptes";
import Projects from "./Components/projects";
import Home from "./Components/home";
import Contacts from "./Components/contacts";
import Contrats from "./Components/contrats";
import Tickets from "./Components/tickets";
import Prospects from "./Components/prospects";
//details
import DetailsComptes from "./Details/DetailsCompte";
import DetailsAffaires from "./Details/DetailsAffaires";
import DetailsContacts from "./Details/DetailsContacts";
import DetailsTickets from "./Details/DetailsTickets";
import DetailsContrats from "./Details/DetailsContrats";
import DetailsProjects from "./Details/DetailsProjects";
import DetailsTasks from "./Details//DetailsTasks";
import DetailsMilestones from "./Details//DetailsMilestones";
import DetailsProspects from "./Details//DetailsProspects";

//drawer
import MyDrawer from "./myDrawer";
import Sign from "./sign";
import Milestones from "./Components/milestones";
import Tasks from "./Components/tasks";
import Activities from "./Components/activities";
import DetailsActivities from "./Details/DetailsActivities";

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const drawerStyle = (props) => {
  return (
    <Drawer.Navigator
      drawerContent={({ navigation }) => {
        return <MyDrawer navigation={navigation} />;
      }}
    >
      <Stack.Screen name="Home" component={Home} />
      <Drawer.Screen name="Comptes" component={Comptes} />
      <Drawer.Screen name="Affaires" component={Affaires} />
      <Drawer.Screen name="Milestones" component={Milestones} />
      <Drawer.Screen name="Tasks" component={Tasks} />
      <Drawer.Screen name="Activities" component={Activities} />
      <Drawer.Screen name="Contacts" component={Contacts} />
      <Drawer.Screen name="Tickets" component={Tickets} />
      <Drawer.Screen name="Projects" component={Projects} />
      <Drawer.Screen name="Contrats" component={Contrats} />
      <Drawer.Screen name="Prospects" component={Prospects} />
    </Drawer.Navigator>
  );
};

const Container = (props) => {
  return (
    <Stack.Navigator
      initialRouteName="Sign"
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen name="Home" component={drawerStyle} />
      <Stack.Screen name="Sign" component={Sign} />
      <Stack.Screen name="DetailsComptes" component={DetailsComptes} />
      <Stack.Screen name="DetailsTasks" component={DetailsTasks} />
      <Stack.Screen name="DetailsMilestones" component={DetailsMilestones} />
      <Stack.Screen name="DetailsContacts" component={DetailsContacts} />
      <Stack.Screen name="DetailsContrats" component={DetailsContrats} />
      <Stack.Screen name="DetailsProjects" component={DetailsProjects} />
      <Stack.Screen name="DetailsAffaires" component={DetailsAffaires} />
      <Stack.Screen name="DetailsTickets" component={DetailsTickets} />
      <Stack.Screen name="DetailsActivities" component={DetailsActivities} />
      <Stack.Screen name="DetailsProspects" component={DetailsProspects} />
    </Stack.Navigator>
  );
};

export default Container;
