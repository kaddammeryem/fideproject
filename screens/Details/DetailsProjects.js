import React, { Component } from 'react';
import { SafeAreaView, Image, TouchableOpacity, ScrollView, Text, View, Modal, FlatList } from 'react-native';
import { FAB, Portal, Provider } from 'react-native-paper';//component library
//import Icon from 'react-native-vector-icons/FontAwesome';//icons library
import Info from '../ItemsRender/info';
import { Header, Button, Icon, Overlay } from 'react-native-elements';
import * as DocumentPicker from 'expo-document-picker';
import styles from '../styles'
import Comments from '../ItemsRender/comment';
import TabItem from '../ItemsRender/TabItem';
import ItemAffaire from '../ItemsRender/affaireitem';
import ModalTask from '../Modals/ModalTask';
import ModalSelectItemPriority from '../Modals/ModalSelectItemTickets';
import ModalSelectEngagement from '../Modals/ModalSelectEngagement';
import ModalSelectCategorie from '../Modals/ModalSelectCategorie';
import ModalSelectSociete from '../Modals/ModalSelectSociete';
import ModalSelectProject from '../Modals/ModalSelectProject';
import ModalSelectItemType from '../Modals/ModalSelectItemType';
import ModalMilestone from '../Modals/ModalMilestone';
import ItemTicket from '../ItemsRender/ItemTickets';
import ModalSelectContacts from '../Modals/ModalSelectContact';
import AddTicket from '../Modals/Modal_AddTickets';
import ModalInput from '../Modals/ModalsUpdate/Modalnput';
import ModalDate from '../Modals/ModalsUpdate/ModalDate';
import ModalButton from '../Modals/ModalsUpdate/ModalButton';
import ModalSelectItemStatut from '../Modals/ModalSelectItemStatut';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class DetailsProjects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ProjectMilestone: false,
      ProjectTask: false,
      HelpDesk: false,
      Documents: false,
      loadingShow: true,
      valeur:'',
      valeur2:'',
      dataFet: [],
      dataProjects2:[],
      link: [],
      dataMilestones: [],
      type: 'Select Type',
      id: 0,
      projectname: this.props.route.params.projectname,
      idProject: this.props.route.params.id,
      modalVisibleProjects: false,
      modalVisibleMilestone: false,
      dataTasks: [],
      chosen: 'Select item',
      title: '',
      modalVisibleButton: false,
      lastname: 'Select Contact',
      idContact: "",
      modalVisibleDate: false,
      modalVisibleItemPriority: false,
      modalVisibleItemContacts: false,
      modalVisibleInput: false,
      modalVisibleType: false,
      accountname: 'Select Account',
      Moreinfo: false,
      MoreinfoIcon: "chevron-down",
      MoreinfoText: 'Plus',
      statut: 'Ouvert',
      engagement: 'Mineur',
      priority: 'Normale',
      modalVisibleItemStatut: false,
      modalVisibleCategorie: false,
      idCompte: '',
      categorie: 'Petit problème',
      dataDocuments: [],
      dataTickets: [],
      modalVisibleDelete: false,
      modalVisibleTask: false,
      modalVisibleEngagement: false,
      modalVisibleCompte: false,
      modalVisibleTickets: false,
      idToAccountname: '',
      modalVisibleType: false,
      //tabBG
      BGRésumé: '#9ddfd3',
      BGDetails: 'white',
      BGMises: 'white',
      BGTasks: 'white',
      BGMilestones: 'white',
      BGDoc: 'white',
      BGTickets: 'white',
      //scale
      SRésumé: 0.8,
      SDetails: 0.6,
      SMises: 0.6,
      STasks: 0.6,
      SMilestones: 0.6,
      SDoc: 0.6,
      STickets: 0.6,
      //tabVisible
      TabVisible: 'BGRésumé',
      //------------------
      Detail: true,
      BlocSys: true,
      InfoP: true,
      Adress: true,
      Desc: true,
      Portal: true,
      //date
      date: new Date(),
      time: new Date(),
      isDate: false,
      istime: false,
    }

  }
  initaliserStyle = () => {
    this.setState({
      BGRésumé: 'white',
      BGDetails: 'white',
      BGMises: 'white',
      BGTasks: 'white',
      BGMilestones: 'white',
      BGDoc: 'white',
      BGTickets: 'white',
      //scale
      SRésumé: 0.8,
      SDetails: 0.6,
      SMises: 0.6,
      SContacts: 0.6,
      STasks: 0.6,
      SMilestones: 0.6,
      SDoc: 0.6,
      STickets: 0.6,

    })

  }
  initialiserValue=()=>{
    this.setState({
      lastname: 'Select Contact',
      idContact: "",
      accountname: 'Select Account',
      Moreinfo: false,
      MoreinfoIcon: "chevron-down",
      MoreinfoText: 'Plus',
      statut: 'Ouvert',
      engagement: 'Mineur',
      priority: 'Normale',
      idCompte: '',
      categorie: 'Petit problème',
    })
  }



  renderElementMiseaj = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => { console.log("pressed: " + item.title) }}>
        <View style={styles.block}>
          <View style={styles.icon_container}>
            <Icon style={styles.icon}
              name={item.icon}
              color='#225fbf'
              type='font-awesome' />
          </View>
          <View style={styles.details}>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.description} >{item.description}</Text>
          </View>
          <View style={styles.date_container}>
            <Text style={styles.date}>{item.date} ago</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  renderElementTasks = ({ item, index }) => {
    return (
      <View >
        <ItemAffaire Name={item.projecttaskname} onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} onPress={() => this.props.navigation.navigate('DetailsTasks', { title:item.projecttaskname,projectname: item.projecttaskname, id: item.id, data: this.state.dataTasks, index: index, result: this.state.dataFet })} />

      </View>
    )
  }
  renderElementMilestones = ({ item, index }) => {
    return (
      <View >
        <ItemAffaire Name={item.projectmilestonename} onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} onPress={() => this.props.navigation.navigate('DetailsMilestones', {title:item.projectmilestonename, id: item.id, data: this.state.dataMilestones, index: index, result: this.state.dataFet })} />

      </View>
    )
  }

  renderElementTickets = ({ item, index }) => {
    return (
      <View>
        <ItemTicket Name={item.ticket_title} onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} onPress={() => { this.props.navigation.navigate('DetailsTickets', {title:item.ticket_title, id: item.id, data: this.state.dataTickets, index: index, result: this.state.dataFet }) }} />
      </View>)
  }
  changecolor = (a) => {
    this.initaliserStyle();
    this.setState({
      [`BG${a}`]: '#9ddfd3',
      [`S${a}`]: 0.8,
      TabVisible: `BG${a}`
    });
  }
  setmodalVisibleDocuments = (visible) => {
    this.setState({ modalVisibleDocuments: visible });
  }


  filter = async () => {
    await this.setState({
      dataComptes : this.state.dataComptes2.filter((i,n) =>
         i[`${this.state.searchby }`].toUpperCase().includes(this.state.searchTextinput.toUpperCase()))
    })
}

  getFunction = async () => {
    this.setState({ loadingShow: true });
    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    var id = this.props.route.params.id;
    try {
      await AsyncStorage.multiGet(['SESSION', 'CRM_LINK', 'TYPES'], async (err, res) => {
        types = res[2][1].split(',');
        console.log(types);
        if (types.includes('ProjectMilestone')) this.setState({ ProjectMilestone: true });
        if (types.includes('Documents')) this.setState({ Documents: true });
        if (types.includes('HelpDesk')) this.setState({ HelpDesk: true });
        if (types.includes('ProjectTask')) this.setState({ ProjectTask: true });
        var dataFet = res[0][1];
        this.setState({ dataFet: dataFet });
        var link = res[1][1];
        this.setState({ link: link });
        //this fetch is for Docs
        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Documents ;')
          .then((response) => response.json())
          .then(async (json) => {
            this.setState({ dataDocuments: json.result });
            //Accounts
            await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Accounts where id=' + dataRoute.linktoaccountscontacts + ';')
              .then((response) => response.json())
              .then(async (json) => {
                if (json.success == true && json.result != '') 
                      { this.setState({idToAccountname: json.result[0].accountname }) }
                //this fetch is from Ticketss
                await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from HelpDesk;')
                  .then((response) => response.json())
                  .then(async (json) => {
                    this.setState({ dataTickets: json.result });
                    await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from ProjectTask where projectid=' + id + ';')
                      .then((response) => response.json())
                      .then(async (json) => {
                        this.setState({ dataTasks: json.result,dataProjects2:json.result, });
                        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from ProjectMilestone where projectid=' + id + ';')
                          .then((response) => response.json())
                          .then((json) => {this.setState({ loadingShow: false }); this.setState({ dataMilestones: json.result,dataProjects2:json.result, }) })
                          .catch((error) =>
                            console.log(error))
                      })
                      .catch((error) =>
                        console.log(error))
                  })
                  .catch((error) =>
                    console.log(error))
              })
              .catch((error) =>
                console.log(error))
          })
          .catch((error) =>
            console.log(error))
      })
    } catch (e) {
      console.log(e);
    }
  }

  setVisibleTask = () => {
    this.setState({ modalVisibleTask: false })
  }
  onPressPriority = () => {
    this.setState({ modalVisibleItemPriority: true, modalVisibleTask: false, modalVisibleTickets: false })
  }

  onPressOptionPriority = (val) => { this.setState({ modalVisibleButton: true, modalVisibleTask: true, modalVisibleTickets: true, modalVisibleItemPriority: false,valeur:val,valeur2:val }); }
  onPressOptionStatut = (val) => { this.setState({ modalVisibleTask: true, modalVisibleTickets: true, modalVisibleItemStatut: false, statut: val, modalVisibleButton: true, chosen: val, value: val,valeur:val,valeur2:val }); }
  onPressOptionEngagement = (val) => { this.setState({ modalVisibleTask: true, modalVisibleEngagement: false, modalVisibleTickets: true, engagement: val }); }
  onPressOptionCategorie = (val) => { this.setState({ modalVisibleTask: true, modalVisibleCategorie: false, modalVisibleTickets: true, categorie: val }); }

  onPressInfo = () => {
    if (this.state.Moreinfo) {
      this.setState({
        Moreinfo: false,
        MoreinfoIcon: "chevron-down",
        MoreinfoText: 'Plus'
      })
    } else {
      this.setState({
        Moreinfo: true,
        MoreinfoIcon: "chevron-up",
        MoreinfoText: 'Moins'
      })
    }
  }
  onPressOptionType = (x) => { this.setState({ modalVisibleButton: true, modalVisibleMilestone: true, modalVisibleTask: true, modalVisibleType: false, type: x, value: x, chosen: x }); }
  componentDidMount() {
    this.getFunction();
  }
  onPressItemContact = () => {
    this.setState({ modalVisibleItemContacts: true, modalVisibleTickets: false })
  }
  onPressCancelCompte = () => { this.setState({ modalVisibleCompte: false, value: '',  modalVisibleButton: true ,valeur:'',valeur2:''}); }

  renderItemProjects = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({ modalVisibleProjects: false, modalVisibleMilestone: true, idProject: item.id, projectname: item.projectname }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.projectname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.projectname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );

  renderItemContacts = ({ item }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => { this.setState({ modalVisibleTickets: true, modalVisibleItemContacts: false, idContact: item.id, lastname: item.lastname }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.lastname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.lastname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
  renderItemComptes = ({ item, x }) => (

    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({ modalVisibleCompte: false, modalVisibleTickets: true, modalVisibleTask: true, modalVisibleMilestone: true, modalVisibleButton: true, idCompte: item.id, accountname: item.accountname, chosen: item.accountname, value: item.id,valeur:item.accountname,valeur2:item.id }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.accountname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.accountname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
  update = async (value) => {
    this.setState({ loadingShow: true });
    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    dataRoute[this.state.key] = value;
    await fetch('https://portail.crm4you.ma/webservice.php', {
      method: 'POST',
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: 'operation=update&sessionName=' + this.state.dataFet + '&element=' + JSON.stringify(dataRoute)
    })
      .then((responses) => responses.json())
      .then((json) => { this.setState({ loadingShow: false ,valeur:'',valeur2:'',modalVisibleInput:false,modalVisibleDate:false,modalVisibleButton:false,chosen: 'Select Item'}); console.log(json);  })
      .catch((error) => console.error(error));
    this.getFunction();

  }
  TabNavigation = () => {
    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    if (this.state.TabVisible == 'BGRésumé') {
      return (
        <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 1, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Champ De Clé
                </Text>
            </View>
          </View>
          <View style={styles.content_champ_cle}>
            <Info title={'Nom du projet'} modifiable={true} borderBottom={0.6} content={dataRoute.projectname}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.projectname, key: 'projectname', title: 'Nom du projet' }) }} />
            <Info title={'Date de début'} modifiable={true} borderBottom={0.6} content={dataRoute.startdate}
              onPress={() => { this.setState({ key: "startdate", modalVisibleDate: true,valeur: dataRoute.startdate,title: 'Date de début' }) }} />
            <Info title={'Echéance fin'} modifiable={true} borderBottom={0.6} content={dataRoute.targetenddate}
              onPress={() => { this.setState({ modalVisibleDate: true, valeur: dataRoute.targetenddate,key: "targetenddate", title: "Date d'écheance" }) }} />
            <Info title={'Statut'} modifiable={true} borderBottom={0.6} content={dataRoute.projectstatus}
              onPress={() => { this.setState({ modalVisibleButton: true, valeur:dataRoute.projectstatus,valeur2:dataRoute.projectstatus,key: 'projectstatus', title: 'Statut' }) }} />
            <Info title={'Type'} modifiable={true} borderBottom={0.6} content={dataRoute.projecttype}
              onPress={() => { this.setState({ modalVisibleButton: true, valeur:dataRoute.pprojecttype,valeur2:dataRoute.projecttype,key: 'projecttype', title: 'Type' }) }} />



            <View style={{ width: '100%', height: 70, backgroundColor: 'white', marginTop: 10 }}>
              <TouchableOpacity activeOpacity={0.7} style={styles.buttonFullDetails} onPress={() => { this.changecolor('Details') }} >
                <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: 'white' }}>Full Details</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ height: 50 }}></View>
          <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur={this.state.valeur}
          edit={'Edit Projet'}
          onPressCancel={() => this.setState({ modalVisibleButton: false, value: '',valeur:'',valeur2:'' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Statut') {
              this.setState({
                modalVisibleItemStatut: true, modalVisibleButton: false
              })
            }
            if (this.state.title == 'Type') {
              this.setState({
                modalVisibleType: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Relatif') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Priorite') {
              this.setState({
                modalVisibleItemPriority: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalSelectItemStatut
          modalVisibleItemTickets={this.state.modalVisibleItemStatut}
          onPressOption={this.onPressOptionStatut}
          select={'Select Statut'}
          onPressClose={() => this.setState({ modalVisibleItemStatut: false })}

        />
        <ModalSelectItemType
          modalVisibleItem={this.state.modalVisibleType}
          onPressOption={this.onPressOptionType}
          onPressClose={() => this.setState({ modalVisibleType: false })}
          select={'Select Type'}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={this.onPressCancelCompte}
        />
        <ModalSelectItemPriority
          modalVisibleItemTickets={this.state.modalVisibleItemPriority}
          onPressOption={this.onPressOptionPriority}
          priorite={'Select Priorité'}
          onPressClose={() => this.setState({ modalVisibleItemPriority: false })}

        />
        </ScrollView>
      )
    }
    else if (this.state.TabVisible == 'BGDetails') {
      return (
        <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Détail Du Compte
                            </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Detail: !this.state.Detail }) }}>
              {!this.state.Detail ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.Detail ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Nom du projet'} modifiable={true} borderBottom={0.6} content={dataRoute.projectname}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.projectname,key: 'projectname', title: 'Nom du projet' }) }} />
              <Info title={'Numéro du projet'} modifiable={false} borderBottom={0.6} content={dataRoute.project_no}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'project_no', title: 'Numéro du projet' }) }} />
              <Info title={'Date de début'} modifiable={true} borderBottom={0.6} content={dataRoute.startdate}
                onPress={() => { this.setState({ key: "startdate", valeur: dataRoute.startdate,modalVisibleDate: true, title: 'Date de début' }) }} />
              <Info title={'Echéance fin'} modifiable={true} borderBottom={0.6} content={dataRoute.targetenddate}
                onPress={() => { this.setState({ modalVisibleDate: true,valeur: dataRoute.targetenddate, key: "targetenddate", title: "Date d'écheance" }) }} />
              <Info title={'Date effective de fin'} modifiable={true} borderBottom={0.6} content={dataRoute.actualenddate}
                onPress={() => { this.setState({ key: "actualenddate",valeur: dataRoute.actualenddate, modalVisibleDate: true, title: 'Date effective de fin' }) }} />
              <Info title={'Statut'} modifiable={true} borderBottom={0.6} content={dataRoute.projectstatus}
                onPress={() => { this.setState({ modalVisibleButton: true,valeur: dataRoute.projectstatus,valeur2: dataRoute.projectstatus,key: 'projectstatus', title: 'Statut' }) }} />
              <Info title={'Type'} modifiable={true} borderBottom={0.6} content={dataRoute.projecttype}
                onPress={() => { this.setState({ modalVisibleButton: true,valeur: dataRoute.projecttype,valeur2: dataRoute.projecttype, key: 'projecttype', title: 'Type' }) }} />
              <Info title={'Relatif à'} modifiable={true} borderBottom={0.6} content={this.state.idToAccountname}
                onPress={() => { this.setState({ modalVisibleButton: true,valeur: this.state.idToAccountname, valeur2: dataRoute.linktoaccountscontacts,key: 'linktoaccountscontacts', title: 'Relatif' }) }} />
              <Info title={'Source'} modifiable={false} borderBottom={0.6} content={dataRoute.source}/>

            </View>
          ) : null}
          <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Informations personnalisées
                                </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ BlocSys: !this.state.BlocSys }) }}>
              {!this.state.InfoP ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.InfoP ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Budget cible'} modifiable={true} borderBottom={0.6} content={dataRoute.targetbudget}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.targetbudget, key: 'targetbudget', title: 'Budget cible' }) }} />
              <Info title={'URL du projet'} modifiable={true} borderBottom={0.6} content={dataRoute.projecturl}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.projecturl,key: 'projecturl', title: 'URL du projet' }) }} />
              <Info title={'Priorité'} modifiable={true} borderBottom={0.6} content={dataRoute.projectpriority}
                onPress={() => { this.setState({ modalVisibleButton: true,valeur:dataRoute.projectpriority,valeur2:dataRoute.projectpriority, key: 'projectpriority', title: 'Priorite' }) }} />
              <Info title={'Progres'} modifiable={true} borderBottom={0.6} content={dataRoute.progress}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.progress,key: 'progress', title: 'Progres' }) }} />
              <Info title={'Date de création'} modifiable={false} borderBottom={0.6} content={dataRoute.createdtime}
              />{/** pas encore fait?*/}
              <Info title={'Date de modification'} modifiable={false} borderBottom={0.6} content={dataRoute.modifiedtime}
              />{/** pas encore fait?*/}
            </View>
          ) : null}
           <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur={this.state.valeur}
          edit={'Edit Projet'}
          onPressCancel={() => this.setState({ modalVisibleButton: false, value: '',valeur:'',valeur2:'' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Statut') {
              this.setState({
                modalVisibleItemStatut: true, modalVisibleButton: false
              })
            }
            if (this.state.title == 'Type') {
              this.setState({
                modalVisibleType: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Relatif') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Priorite') {
              this.setState({
                modalVisibleItemPriority: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalSelectItemStatut
          modalVisibleItemTickets={this.state.modalVisibleItemStatut}
          onPressOption={this.onPressOptionStatut}
          select={'Select Statut'}
          onPressClose={() => this.setState({ modalVisibleItemStatut: false })}

        />
        <ModalSelectItemType
          modalVisibleItem={this.state.modalVisibleType}
          onPressOption={this.onPressOptionType}
          onPressClose={() => this.setState({ modalVisibleType: false })}
          select={'Select Type'}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={this.onPressCancelCompte}
        />
        <ModalSelectItemPriority
          modalVisibleItemTickets={this.state.modalVisibleItemPriority}
          onPressOption={this.onPressOptionPriority}
          priorite={'Select Priorité'}
          onPressClose={() => this.setState({ modalVisibleItemPriority: false })}

        />
          <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Information Sur la description
                                </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]}
              onPress={() => { this.setState({ Desc: !this.state.Desc }) }}>
              {!this.state.Desc ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.Desc ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Description'} modifiable={true} borderBottom={0.6} content={dataRoute.description}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.description,key: 'description', title: 'description' }) }} />
            </View>
          ) : null}
           <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          edit={'Edit Projet'}
          onPressCancel={() => this.setState({ modalVisibleButton: false, value: '',valeur:'',valeur2:'' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Statut') {
              this.setState({
                modalVisibleItemStatut: true, modalVisibleButton: false
              })
            }
            if (this.state.title == 'Type') {
              this.setState({
                modalVisibleType: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Relatif') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Priorite') {
              this.setState({
                modalVisibleItemPriority: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalSelectItemStatut
          modalVisibleItemTickets={this.state.modalVisibleItemStatut}
          onPressOption={this.onPressOptionStatut}
          select={'Select Statut'}
          onPressClose={() => this.setState({ modalVisibleItemStatut: false })}

        />
        <ModalSelectItemType
          modalVisibleItem={this.state.modalVisibleType}
          onPressOption={this.onPressOptionType}
          onPressClose={() => this.setState({ modalVisibleType: false })}
          select={'Select Type'}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={this.onPressCancelCompte}
        />
        <ModalSelectItemPriority
          modalVisibleItemTickets={this.state.modalVisibleItemPriority}
          onPressOption={this.onPressOptionPriority}
          priorite={'Select Priorité'}
          onPressClose={() => this.setState({ modalVisibleItemPriority: false })}

        />
        </ScrollView>
      )
    }

    else if (this.state.TabVisible == 'BGTasks') {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <View style={{ flex: 9 }}>

              <FlatList
                data={this.state.dataTasks}
                renderItem={this.renderElementTasks}
                extraData={this.state.dataTasks.length}
                keyExtractor={item => item.id}
              />
            </View>
            <View style={{ flex: 1 }}>
              <Provider>
                <Portal>
                  <FAB.Group
                    open={this.state.open}
                    icon={'plus'}
                    color='white'
                    fabStyle={{ backgroundColor: '#53aefe' }}
                    actions={[
                      { icon: 'plus', onPress: () => console.log('Pressed add') },
                    ]}
                    onStateChange={() => { this.setState({ modalVisibleTask: true }) }}
                    onPress={() => { this.setState({ modalVisibleTask: true }) }}
                  />
                </Portal>
              </Provider>
            </View>

          </View>
          <View>
            <ModalTask
              modalVisibleTask={this.state.modalVisibleTask}
              onPressPriority={this.onPressPriority}
              onPressStatut={() => this.setState({ modalVisibleItemStatut: true, modalVisibleTask: false })}
              onPressCompte={() => this.setState({ modalVisibleCompte: true, modalVisibleTask: false })}
              onPressEngagement={() => this.setState({ modalVisibleEngagement: true, modalVisibleTask: false })}
              onPressCategorie={() => this.setState({ modalVisibleCategorie: true, modalVisibleTask: false })}
              accountname={this.state.accountname}
              selectPriority={this.state.priority}
              selectStatut={this.state.statut}
              
              onPressInfo={this.onPressInfo}
              idProject={this.state.idProject}
              MoreinfoText={this.state.MoreinfoText}
              MoreinfoIcon={this.state.MoreinfoIcon}
              Moreinfo={this.state.Moreinfo}
              onPressCancel={() => {this.setState({ modalVisibleTask: false });this.initialiserValue()}}
              onPressSave={()=>{this.getFunction();this.initialiserValue()}}
              idCompte={this.state.idCompte}
              engagement={this.state.engagement}
              categorie={this.state.categorie}
              type={this.state.type}
              onPressType={() => this.setState({ modalVisibleType: true, modalVisibleTask: false })}

            />
            <ModalSelectItemPriority
              modalVisibleItemTickets={this.state.modalVisibleItemPriority}
              onPressOption={this.onPressOptionPriority}
              priorite={'Select Priorité'}
              onPressClose={() => this.setState({ modalVisibleItemPriority: false })}
            />
            <ModalSelectItemStatut
              modalVisibleItemTickets={this.state.modalVisibleItemStatut}
              onPressOption={this.onPressOptionStatut}
              select={'Select Statut'}
              onPressClose={() => this.setState({ modalVisibleItemStatut: false })}
            />
            <ModalSelectEngagement
              modalVisibleEngagement={this.state.modalVisibleEngagement}
              onPressOption={this.onPressOptionEngagement}
              select={'Select Statut'}
              onPressClose={() => this.setState({ modalVisibleEngagement: false })}
            />
            <ModalSelectCategorie
              modalVisibleCategorie={this.state.modalVisibleCategorie}
              onPressOption={this.onPressOptionCategorie}
              select={'Select catégorie'}
              onPressClose={() => this.setState({ modalVisibleCategorie: false })}
            />
            <ModalSelectSociete
              modalVisibleCompte={this.state.modalVisibleCompte}
              
              renderItem={this.renderItemComptes}
              onPressClose={()=>{this.setState({modalVisibleTask:true,modalVisibleCompte:false})}}
            />
            <ModalSelectItemType
              modalVisibleItem={this.state.modalVisibleType}
              onPressOption={this.onPressOptionType}
              onPressClose={() => this.setState({ modalVisibleType: false })}
              select={'Select Item'}
            />
          </View>

        </View>
      )
    }
    else if (this.state.TabVisible == 'BGMilestones') {
      return (<View style={{ flex: 1, padding: 20 }}>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 9 }}>
            <FlatList
              data={this.state.dataMilestones}
              renderItem={this.renderElementMilestones}
              extraData={this.state.dataMilestones.length}
              keyExtractor={item => item.id}
            />
          </View>
          <View style={{ flex: 1 }}>
            <Provider>
              <Portal>
                <FAB.Group
                  open={this.state.open}
                  icon={'plus'}
                  color='white'
                  fabStyle={{ backgroundColor: '#53aefe' }}
                  actions={[
                    { icon: 'plus', onPress: () => console.log('Pressed add') },
                  ]}
                  onStateChange={() => { this.setState({ modalVisibleMilestone: true }) }}
                  onPress={() => {
                  }}
                />
              </Portal>
            </Provider>
          </View>
        </View>
        <ModalMilestone
          modalVisibleMilestone={this.state.modalVisibleMilestone}
          
          onPressSave={()=>{this.getFunction();this.initialiserValue()}}
          onPressCancel={() => { this.setState({ modalVisibleMilestone: false });this.initialiserValue() }}
          idProject={this.state.idProject}
          projectname={this.state.projectname}
          type={this.state.type}
          onPressProject={() => this.setState({ modalVisibleProjects: true, modalVisibleMilestone: false })}
          onPressType={() => this.setState({ modalVisibleType: true, modalVisibleMilestone: false })}
        />
        <ModalSelectItemType
          modalVisibleItem={this.state.modalVisibleType}
          onPressOption={this.onPressOptionType}
          onPressClose={() => this.setState({ modalVisibleType: false, modalVisibleMilestone: true })}
          select={'Select Item'}

        />
        <ModalSelectProject
          modalVisibleProject={this.state.modalVisibleProjects}
          
          renderItemProject={this.renderItemProjects}
          onPressClose={() => this.setState({ modalVisibleProjects: false, modalVisibleMilestone: true })}
        />
      </View>
      )
    }
   
    else if (this.state.TabVisible == 'BGTickets') {
      return (
        <View style={{ flex: 1 }}>
          <FlatList
            data={this.state.dataTickets}
            renderItem={this.renderElementTickets}

            keyExtractor={item => item.id} />
          <View style={{ flex: 1 }}>
            <Provider>
              <Portal>
                <FAB.Group
                  open={this.state.open}
                  icon={'plus'}
                  color='white'
                  fabStyle={{ backgroundColor: '#53aefe' }}
                  actions={[
                    { icon: 'plus', onPress: () => console.log('Pressed add') },
                  ]}
                  onStateChange={() => { this.setState({ modalVisibleTickets: true }) }}
                  onPress={() => { this.setState({ modalVisibleTickets: true }) }}
                />
              </Portal>
            </Provider>
          </View>
          <View>
            <AddTicket
              modalVisibleTickets={this.state.modalVisibleTickets}
              onPressPriority={this.onPressPriority}
              onPressStatut={this.onPressStatut}
              onPressItemContact={() => this.setState({ modalVisibleItemContacts: true, modalVisibleTickets: false })}
              onPressCompte={() => this.setState({ modalVisibleCompte: true, modalVisibleTickets: false })}
              onPressEngagement={() => this.setState({ modalVisibleEngagement: true, modalVisibleTickets: false })}
              onPressCategorie={() => this.setState({ modalVisibleCategorie: true, modalVisibleTickets: false })}
              accountname={this.state.accountname}
              selectPriority={this.state.priority}
              selectStatut={this.state.statut}
              
              onPressInfo={this.onPressInfo}
              MoreinfoText={this.state.MoreinfoText}
              MoreinfoIcon={this.state.MoreinfoIcon}
              Moreinfo={this.state.Moreinfo}
              onPressCancel={() => {this.setState({ modalVisibleTickets: false });this.initialiserValue()}}
              onPressSave={()=>{this.getFunction();this.initialiserValue()}}
              lastname={this.state.lastname}
              idCompte={this.state.idCompte}
              idContact={this.state.idContact}
              engagement={this.state.engagement}
              categorie={this.state.categorie}
            />
            <ModalSelectItemPriority
              modalVisibleItemTickets={this.state.modalVisibleItemPriority}
              onPressOption={this.onPressOptionPriority}
              priorite={'Select Priorité'}
              onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleTickets: true })}

            />
            <ModalSelectItemStatut
              modalVisibleItemTickets={this.state.modalVisibleItemStatut}
              onPressOption={this.onPressOptionStatut}
              select={'Select Statut'}
              onPressClose={() => this.setState({ modalVisibleItemStatut: false, modalVisibleTickets: true })}

            />
            <ModalSelectEngagement
              modalVisibleEngagement={this.state.modalVisibleEngagement}
              onPressOption={this.onPressOptionEngagement}
              select={'Select Statut'}
              onPressClose={() => this.setState({ modalVisibleEngagement: false, modalVisibleTickets: true })}

            />
            <ModalSelectCategorie
              modalVisibleCategorie={this.state.modalVisibleCategorie}
              onPressOption={this.onPressOptionCategorie}
              select={'Select catégorie'}
              onPressClose={() => this.setState({ modalVisibleCategorie: false, modalVisibleTickets: true })}

            />
            <ModalSelectContacts
              modalVisibleContacts={this.state.modalVisibleItemContacts}
              
              renderItem={this.renderItemContacts}
              onPressClose={() => this.setState({ modalVisibleItemContacts: false, modalVisibleTickets: true })}
            />
            <ModalSelectSociete
              modalVisibleCompte={this.state.modalVisibleCompte}
              
              renderItem={this.renderItemComptes}
              onPressClose={()=>{this.setState({modalVisibleCompte:false,modalVisibleTickets:true})}}
            />
          </View>
        </View>)
    }

  }
  setDate = () => {
    this.setState({ isDate: true })
  }

  filter = (searchText) => {

    this.setState({
      searchText: searchText,
      filterSocieteDataSearch: this.state.societeData.filter(i =>
        i.socName.toUpperCase().includes(searchText.toUpperCase()))
    })

  }
  _pickDocument = async () => {
    let result = await DocumentPicker.getDocumentAsync({})
    this.setState({ cheminDoc: result.uri, nameFile: result.name })
    alert(result.uri);
    console.log(result);
  }
  choseEvent_or_task = () => {
    if (this.state.checked === 'task') {
      this.setState({
        TaskModal: true,
        taskevent: false,

      })
    } else {
      this.setState({
        EventModal: true,
        taskevent: false,
      })

    }
  }
  setTime = (date) => {
    this.setState({ time: date, istime: false })
  }
  setDateFunc = (date) => {
    this.setState({ date: date, isDate: false })
  }
  onPressInfoFunc = () => {
    if (this.state.TaskMoreinfo) {
      this.setState({
        TaskMoreinfo: false,
        TaskMoreinfoIcon: "chevron-down",
        TaskMoreinfoText: 'Plus'
      })
    } else {
      this.setState({
        TaskMoreinfo: true,
        TaskMoreinfoIcon: "chevron-up",
        TaskMoreinfoText: 'Moins'
      })
    }
  }
  //functions for modal task
  onPressOptiontask = () => { this.setState({ optionModal: true }); }
  onPressDateTask = () => { this.setState({ isDate: true }) }
  onPressVisible1Task = () => { this.setModalVisible1(true); }
  onPressTaskModal = () => { this.setState({ TaskModal: false }) }
  onPressTimeTask = () => { this.setState({ istime: true }) }
  //functions for modal docs
  onChangeTextDoc = (change) => this.setState({ docTitle: change })
  //functions Modal Projects
  onChangeTextProject = (change) => this.setState({ projectTitle: change })


  renderItem = ({ item }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => { this.ChangeNomSociete(item.socName); this.setModalVisible1(false) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>{`${item.socName}`.substring(0, 1)}</Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.socName}
          </Text>
        </View>
      </TouchableOpacity>

    </View>
  );

  render() {
    return (

      <View style={{ flex: 1, flexDirection: 'column' }}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.loadingShow}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
          <Header
            placement="right"
            leftComponent={
              <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'stretch' }}>
                <Button icon={<Icon name="chevron-left" size={20} color='white' type='material-community' />} onPress={() => this.props.navigation.goBack()} />
                <Text style={{ fontSize: 22, color: 'white', fontWeight: '500', marginTop: 5 }}>Projets : {this.props.route.params.title}</Text>
              </View>}

          />
        </View>
        <View style={{ flex: 1.2 }}>
          {/* <Text>scrollTabNavigation</Text> */}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.scroll}>

            <TabItem title='Résumé' scale={this.state.SRésumé} BackGround={this.state.BGRésumé} onPress={() => this.changecolor('Résumé')} />
            <TabItem title='Details' scale={this.state.SDetails} BackGround={this.state.BGDetails} onPress={() => this.changecolor('Details')} />
            {(this.state.ProjectMilestone === true) && <TabItem title={'Milestones (' + this.state.dataMilestones.length + ')'} scale={this.state.SMilestones} BackGround={this.state.BGMilestones} onPress={() => this.changecolor('Milestones')} />}
            {(this.state.ProjectTask === true) && <TabItem title={'Tasks (' + this.state.dataTasks.length + ')'} scale={this.state.STasks} BackGround={this.state.BGTasks} onPress={() => this.changecolor('Tasks')} />}
          </ScrollView >


        </View>
        <SafeAreaView style={{ flex: 7, flexDirection: 'column' }}>
          {this.TabNavigation()}
        </SafeAreaView>
        <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Projet'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }); }}
          onPressSave={this.update}
        />
        <ModalDate
          modalVisibleDate={this.state.modalVisibleDate}
          title={this.state.title}
          edit={'Edit Projet'}
          valeur={this.state.valeur}
          onPressCancel={() => this.setState({ modalVisibleDate: false, value: '',valeur:'' })}
          onPressSave={this.update}

        />
       
        <Overlay isVisible={this.state.modalVisibleDelete} >
          <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <Text>Voulez-vous supprimer cet objet?</Text>
          </View>
          <View style={{ flexDirection: 'row-reverse' }}>
            <Button title="Oui" titleStyle={{ color: 'black' }} buttonStyle={{ backgroundColor: "white" }} onPress={() => {
              this.setState({ loadingShow: true });
              fetch('https://portail.crm4you.ma/webservice.php', {
                method: 'POST',
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                body: 'operation=delete&sessionName=' + this.state.dataFet + '&id=' + this.state.id
              }); this.getFunction(); this.setState({ modalVisibleDelete: false })
            }}
            />
            <Button title="Non" titleStyle={{ color: 'black' }} buttonStyle={{ backgroundColor: "white" }} onPress={() => this.setState({ modalVisibleDelete: false })} />
          </View>
        </Overlay>


      </View>

    )

  }
}




