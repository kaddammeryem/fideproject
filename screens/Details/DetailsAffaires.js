import React, { Component } from 'react';
import { SafeAreaView, Image, TouchableOpacity, ScrollView, Text, View, Modal, FlatList } from 'react-native';
import { FAB, Portal, Provider, Divider } from 'react-native-paper';//component library
import Info from '../ItemsRender/info';
import { Header, Button, Icon, Overlay } from 'react-native-elements';
import * as DocumentPicker from 'expo-document-picker';
import styles from '../styles'
import Contact from '../ItemsRender/contact';
import ItemAffaire from '../ItemsRender/affaireitem'
import Comments from '../ItemsRender/comment';
import ModalContacts from '../Modals/ModalContact'
import ModalSelectSociete from '../Modals/ModalSelectSociete';
import TabItem from '../ItemsRender/TabItem';
import ModalSelectContacts from '../Modals/ModalSelectContact';
import ModalSelectItem from '../Modals/ModalSelectItem';
import ModalButton from '../Modals/ModalsUpdate/ModalButton';
import ModalDate from '../Modals/ModalsUpdate/ModalDate';
import ModalInput from '../Modals/ModalsUpdate/Modalnput';
import ModalSelectItemType from '../Modals/ModalSelectItemType';
import AsyncStorage from '@react-native-async-storage/async-storage';



export default class DetailsAffaires extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingShow: false,
      Contacts: false,
      Documents: false,
      link: [],
      dataFet: [],
      valeur:'',
      valeur2:'',
      //updates fields 
      chosen: 'Select Item',
      value: '',
      title: '',
      key: '',
      // updates modals
      modalVisibleInput: false,
      modalVisibleDate: false,
      modalVisibleButton: false,
      //create modals
      modalVisibleProjects: false,
      modalVisibleContacts: false,
      modalVisibleCompte: false,
      modalVisibleItemContacts: false,
      modalVisibleContacts: false,
      modalVisibleDocuments: false,
      modalVisibleDelete: false,
      modalVisibleServices: false,
      modalVisiblePhase: false,
      modalVisibleType: false,

      //data
      dataDocuments: [],
      dataComptes: [],
      dataContacts: [],
      idCompte: this.props.idCompte,
      lastname: '',
      idContact: '',
      Moreinfo: false,
      MoreinfoIcon: "chevron-down",
      MoreinfoText: 'Plus',
      accountname: '',
      idToLastname: '',
      idToAccountname: '',
      //tabBG
      BGRésumé: '#9ddfd3',
      BGDetails: 'white',
      BGMises: 'white',
      BGContacts: 'white',
      BGActivitie: 'white',
      BGDoc: 'white',
      BGComnt: 'white',
      //scale
      SRésumé: 0.8,
      SDetails: 0.6,
      SMises: 0.6,
      SContacts: 0.6,
      SActivitie: 0.6,
      SDoc: 0.6,
      SComnt: 0.6,
      //tabVisible
      TabVisible: 'BGRésumé',
      //------------------
      Detail: true,
      BlocSys: true,
      InfoP: true,
      Adress: true,
      Desc: true,
      Portal: true,
      //
      date: new Date(),
      time: new Date(),
      isDate: false,
      istime: false,
    }

  }

  initaliserStyle = () => {
    this.setState({
      BGRésumé: 'white',
      BGDetails: 'white',
      BGMises: 'white',
      BGContacts: 'white',
      BGActivitie: 'white',
      BGDoc: 'white',
      BGComnt: 'white',
      //scale
      SRésumé: 0.8,
      SDetails: 0.6,
      SMises: 0.6,
      SContacts: 0.6,
      SActivitie: 0.6,
      SDoc: 0.6,
      SComnt: 0.6,

    })


  }

  renderElementMiseaj = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => { console.log("pressed: " + item.title) }}
      >

        <View style={styles.block}>

          <View style={styles.icon_container}>
            <Icon style={styles.icon}
              name={item.icon}
              color='#225fbf'
              type='font-awesome' />
          </View>
          <View style={styles.details}>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.description} >{item.description}</Text>
          </View>
          <View style={styles.date_container}>
            <Text style={styles.date}>{item.date} ago</Text>
          </View>
        </View>

      </TouchableOpacity>
    );
  }
  renderElementAffaire = ({ item }) => {
    return (
      <View>
        <ItemAffaire Name={item.subject} />
      </View>);
  }

  changecolor = (a) => {
    this.initaliserStyle();
    this.setState({
      [`BG${a}`]: '#9ddfd3',
      [`S${a}`]: 0.8,
      TabVisible: `BG${a}`
    });
  }
  renderElementContacts = ({ item, index }) => {
    return (
      <View>

        <Contact email={item.phone} Name={item.firstname + ' ' + item.lastname} onPress={() => this.props.navigation.navigate('DetailsContacts', { title:item.lastname+' '+item.firstname,id: item.id, data: this.state.dataContacts, index: index, result: this.state.dataFet,idCompte:item.related_to,account:this.state.idToAccountname,lastname:item.lastname })} />{/*a voir */}
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Divider style={{ width: '95%' }} />
        </View>
      </View>)

  }
  setmodalVisibleContacts = (visible) => {
    this.setState({ modalVisibleContacts: visible });
  }
  setmodalVisibleDocuments = (visible) => {
    this.setState({ modalVisibleDocuments: visible });
  }

  setModalVisible1 = (visible) => {
    this.setState({ modalVisible1: visible });
  }

  ChangeNomSociete = (soc) => {
    this.setState({ nomSoc: soc })
  }
  getFunction = async () => {
    this.setState({ loadingShow: true });
    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    try {
      await AsyncStorage.multiGet(['SESSION', 'CRM_LINK', 'TYPES'], async (err, res) => {
        types = res[2][1].split(',');
        console.log(types);
        if (types.includes('Contacts')) this.setState({ Contacts: true });
        if (types.includes('Documents')) this.setState({ Documents: true });
        var dataFet = res[0][1];
        this.setState({ dataFet: dataFet });
        var link = res[1][1];
        this.setState({ link: link });
        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Accounts ;')
        .then((response) => response.json())
        .then(async (json) => {
            this.setState({ loadingShow: true });
            console.log(json); this.setState({ dataComptes: json.result }); console.log('dataComptes : ' + JSON.stringify(this.state.dataComptes))
            //this fetch is for Accounts
            await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Accounts where id='+dataRoute.related_to+';')
              .then((response) => response.json())
              .then(async (json) => {
                console.log('comptes here ' + JSON.stringify(json))
                if (json.success == true && json.result != '') {
                  console.log('inside not undefined')
                  this.setState({ idToAccountname: json.result[0].accountname })}
                  //this fetch is from contacts
                  await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Contacts where id=' + dataRoute.contact_id + ';')
                    .then((response) => response.json())
                    .then((json) => {
                      if (json.success == true && json.result != '') 
                      { this.setState({ dataContacts: json.result, idToLastname: json.result[0].lastname }) }
                      this.setState({ loadingShow: false });
                    })
                    .catch((error) =>
                      console.log(error))
                
              })
              .catch((error) =>
                console.log(error))
          })
          .catch((error) =>
            console.log(error))


      })
    } catch (e) {
      console.log('error: ', e);
    }

  }


  componentDidMount() {
    this.getFunction();
  }
  renderItemContacts = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({ modalVisibleItemContacts: false, modalVisibleContacts: true, modalVisibleButton: true, idContact: item.id, lastname: item.lastname, chosen: item.lastname, value: item.id,valeur:item.lastname,valeur2:item.id }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.lastname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.lastname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
  renderItemComptes = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({ modalVisibleCompte: false, modalVisibleContacts: true, modalVisibleButton: true, idCompte: item.id, accountname: item.accountname, chosen: item.accountname, value: item.id,valeur:item.accountname,valeur2:item.id }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.accountname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.accountname}
          </Text>
        </View>
      </TouchableOpacity>

    </View>
  );
  onPressInfo = () => {
    if (this.state.Moreinfo) {
      this.setState({
        Moreinfo: false,
        MoreinfoIcon: "chevron-down",
        MoreinfoText: 'Plus'
      })
    } else {
      this.setState({
        Moreinfo: true,
        MoreinfoIcon: "chevron-up",
        MoreinfoText: 'Moins'
      })
    }
  }
  updateContacts = async (x) => {
    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    dataRoute['contact_id'] = x;
    this.setState({ loadingShow: true });
    await fetch('https://portail.crm4you.ma/webservice.php', {
      method: 'POST',
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: 'operation=update&sessionName=' + this.state.dataFet + '&element=' + JSON.stringify(dataRoute)
    })
      .then((responses) => responses.json())
      .then(()=>{this.getFunction();this.setState({modalVisibleButton:false,modalVisibleInput:false,modalVisibleDate:false})})
      .catch((error) => console.error(error));


  }

  onPressCancelContact = () => this.setState({ modalVisibleItemContacts: false, modalVisibleButton: true })
  onPressCancelCompte = () => this.setState({ modalVisibleCompte: false,  modalVisibleButton: true, })

  update = async (value) => {

    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    dataRoute[this.state.key] = value;
    console.log(dataRoute);
    this.setState({ loadingShow: true });
    await fetch(this.state.link, {
      method: 'POST',
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: 'operation=update&sessionName=' + this.state.dataFet + '&element=' + JSON.stringify(dataRoute)
    })
      .then((responses) => responses.json())
      .then((json) => { this.setState({ loadingShow: false,chosen: 'Select Item',valeur:'' }); console.log(json);  })
      .catch((error) => console.error(error));
    this.getFunction();


  }

  TabNavigation = () => {
    var dataRoute = this.props.route.params.data[this.props.route.params.index]


    if (this.state.TabVisible == 'BGRésumé') {
      return (
        <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 1, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Champ De Clé
                </Text>
            </View>
          </View>
          <View style={styles.content_champ_cle}>
            <Info title={"Nom de l'affaire"} modifiable={true} borderBottom={0.6} content={dataRoute.potentialname}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.potentialname,key: 'potentialname', title: "Nom de l'affaire" }) }} />
            <Info title={'Nom du contact'} modifiable={true} borderBottom={0.6} content={this.state.idToLastname}
              onPress={() => { this.setState({ modalVisibleButton: true, valeur2:dataRoute.contact_id,valeur:this.state.idToLastname,key: 'contact_id', title: 'Nom du contact' }) }} />
            <Info title={'Date de fermeture attendue'} modifiable={true} borderBottom={0.6} content={dataRoute.closingdate}
              onPress={() => { this.setState({ modalVisibleDate: true, valeur:dataRoute.closingdate,key: 'closingdate', title: 'Date de fermeture attendue' }) }} />
            

            <View style={{ width: '100%', height: 70, backgroundColor: 'white', marginTop: 10 }}>
              <TouchableOpacity activeOpacity={0.7} style={styles.buttonFullDetails} onPress={() => { this.changecolor('Details') }} >
                <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: 'white' }}>Full Details</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ height: 50 }}></View>
          <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          edit={'Edit Affaires'}
          onPressCancel={() => this.setState({ modalVisibleButton: false, value: '',chosen:'Select Item',valeur:'',valeur2:'' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Phase de vente') {
              this.setState({
                modalVisiblePhase: true, modalVisibleButton: false
              })
            }
            if (this.state.title == 'Type') {
              this.setState({
                modalVisibleType: true, modalVisibleButton: false
              })
            }
            if (this.state.title === "Nom de l'organisation") {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
            if (this.state.title === "Nom du contact") {
              this.setState({
                modalVisibleItemContacts: true, modalVisibleButton: false
              })
            }
          }

          }
        />

        <ModalSelectItem
          modalVisibleItem={this.state.modalVisiblePhase}
          onPressOption={this.onPressOption}
          select={'Select Item'}
          onPressClose={() => this.setState({ modalVisiblePhase: false, modalVisibleButton: true })}
        />

        <ModalSelectItemType
          modalVisibleItem={this.state.modalVisibleType}
          onPressOption={this.onPressOptionType}
          onPressClose={() => this.setState({ modalVisibleType: false, modalVisibleButton: true })}
          select={'Select Type'}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={this.onPressCancelCompte}
        />
        <ModalSelectContacts
          modalVisibleContacts={this.state.modalVisibleItemContacts}
          
          renderItem={this.renderItemContacts}
          onPressClose={this.onPressCancelContact}
        />

        </ScrollView>
      )
    }
    else if (this.state.TabVisible == 'BGDetails') {
      return (
        <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Détail Du Compte
                            </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Detail: !this.state.Detail }) }}>
              {!this.state.Detail ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.Detail ? (
            <View style={styles.content_champ_cle}>
              <Info title={"Nom de l'affaire"} modifiable={true} borderBottom={0.6} content={dataRoute.potentialname}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.potentialname,key: 'potentialname', title: "Nom de l'affaire" }) }} />
              <Info title={'Affaire N°'} modifiable={false} borderBottom={0.6} content={dataRoute.potential_no}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'potential_no', title: 'Affaire N°' }) }} />
              <Info title={"Nom de l'organisation"} modifiable={true} borderBottom={0.6} content={this.state.idToAccountname}
                onPress={() => { this.setState({ modalVisibleButton: true, valeur:this.state.idToAccountname,valeur2:dataRoute.related_to,key: 'related_to', title: "Nom de l'organisation" }) }} />
              <Info title={"Nom du contact"} modifiable={true} borderBottom={0.6} content={this.state.idToLastname}
                onPress={() => { this.setState({ modalVisibleButton: true,valeur:this.state.idToLastname,valeur2:dataRoute.contact_id, key: 'contact_id', title: 'Nom du contact' }) }} />
              <Info title={'Montant'} modifiable={true} borderBottom={0.6} content={dataRoute.amount}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.amount,key: 'amount', title: 'Montant' }) }} />
              <Info title={'Type'} modifiable={true} borderBottom={0.6} content={dataRoute.opportunity_type}
                onPress={() => { this.setState({ modalVisibleButton: true,valeur:dataRoute.opportunity_type,valeur2:dataRoute.opportunity_type, key: 'opportunity_type', title: 'Type' }) }} />
              <Info title={'Date de fermeture attendue'} modifiable={true} borderBottom={0.6} content={dataRoute.closingdate}
                onPress={() => { this.setState({ modalVisibleDate: true, valeur:dataRoute.closingdate,key: 'closingdate', title: 'Date de fermeture attendue' }) }} />
              <Info title={'Source du prospect'} modifiable={false} borderBottom={0.6} content={dataRoute.leadsource} />
              <Info title={'Suivant'} modifiable={true} borderBottom={0.6} content={dataRoute.nextstep}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.nextstep, key: 'nextstep', title: 'Suivant' }) }} />
              <Info title={'Phase de vente'} modifiable={true} borderBottom={0.6} content={dataRoute.sales_stage}
                onPress={() => { this.setState({ modalVisibleButton: true, valeur2:dataRoute.sales_stage,valeur:dataRoute.sales_stage,key: 'sales_stage', title: 'Phase de vente' }) }} />
              <Info title={'Probabilité'} modifiable={true} borderBottom={0.6} content={dataRoute.probability}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.probability, key: 'probability', title: 'Probabilité' }) }} />
              <Info title={'Date de modification'} modifiable={false} borderBottom={0.6} content={dataRoute.modifiedtime} />
              <Info title={'Date de création'} modifiable={false} borderBottom={0.6} content={dataRoute.createdtime} />
              <Info title={'Revenu pondéré'} modifiable={false} borderBottom={0.6} content={dataRoute.forecast_amount} />
              <Info title={'Source'} modifiable={false} borderBottom={0.6} content={dataRoute.source} />

            </View>
          ) : null}
           <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          value={this.state.value}
          edit={'Edit Affaires'}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false, value: '',chosen:'Select Item',valeur:'',valeur2:'' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Phase de vente') {
              this.setState({
                modalVisiblePhase: true, modalVisibleButton: false
              })
            }
            if (this.state.title == 'Type') {
              this.setState({
                modalVisibleType: true, modalVisibleButton: false
              })
            }
            if (this.state.title === "Nom de l'organisation") {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
            if (this.state.title === "Nom du contact") {
              this.setState({
                modalVisibleItemContacts: true, modalVisibleButton: false
              })
            }
          }

          }
        />

        <ModalSelectItem
          modalVisibleItem={this.state.modalVisiblePhase}
          onPressOption={this.onPressOption}
          select={'Select Item'}
          onPressClose={() => this.setState({ modalVisiblePhase: false, modalVisibleButton: true })}
        />

        <ModalSelectItemType
          modalVisibleItem={this.state.modalVisibleType}
          onPressOption={this.onPressOptionType}
          onPressClose={() => this.setState({ modalVisibleType: false, modalVisibleButton: true })}
          select={'Select Type'}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={this.onPressCancelCompte}
        />
        <ModalSelectContacts
          modalVisibleContacts={this.state.modalVisibleItemContacts}
          
          renderItem={this.renderItemContacts}
          onPressClose={this.onPressCancelContact}
        />

          <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Information Sur la description
                                </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Desc: !this.state.Desc }) }}>
              {!this.state.Desc ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.Desc ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Description'} modifiable={true} borderBottom={0.6} content={dataRoute.description}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.description,key: 'description', title: 'Description' }) }} />
            </View>
          ) : null}
           <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          value={this.state.value}
          edit={'Edit Affaires'}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false, value: '',chosen:'Select Item',valeur2:'',valeur:'' })}
          onPressSave={()=>this.update(this.state.value)}
          onPressSelect={() => {
            if (this.state.title == 'Phase de vente') {
              this.setState({
                modalVisiblePhase: true, modalVisibleButton: false
              })
            }
            if (this.state.title == 'Type') {
              this.setState({
                modalVisibleType: true, modalVisibleButton: false
              })
            }
            if (this.state.title === "Nom de l'organisation") {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
            if (this.state.title === "Nom du contact") {
              this.setState({
                modalVisibleItemContacts: true, modalVisibleButton: false
              })
            }
          }

          }
        />

        <ModalSelectItem
          modalVisibleItem={this.state.modalVisiblePhase}
          onPressOption={this.onPressOption}
          select={'Select Item'}
          onPressClose={() => this.setState({ modalVisiblePhase: false, modalVisibleButton: true })}
        />

        <ModalSelectItemType
          modalVisibleItem={this.state.modalVisibleType}
          onPressOption={this.onPressOptionType}
          onPressClose={() => this.setState({ modalVisibleType: false, modalVisibleButton: true })}
          select={'Select Type'}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={this.onPressCancelCompte}
        />
        <ModalSelectContacts
          modalVisibleContacts={this.state.modalVisibleItemContacts}
          
          renderItem={this.renderItemContacts}
          onPressClose={this.onPressCancelContact}
        />

        </ScrollView>
      )
    }
    else if (this.state.TabVisible == 'BGContacts') {
      return (
        <View style={{ flex: 1, flexDirection: 'column', padding: 20 }}>
          <FlatList
            data={this.state.dataContacts}
            renderItem={this.renderElementContacts}
            keyExtractor={item => item.id}
          />
          <View style={{ flex: 2 }}>
            <Provider>
              <Portal>
                <FAB.Group
                  open={this.state.open}
                  icon={'plus'}
                  color='white'
                  fabStyle={{ backgroundColor: '#53aefe' }}
                  actions={[
                    { icon: 'plus', onPress: () => console.log('Pressed add') },
                  ]}
                  onStateChange={() => { this.setmodalVisibleContacts(true) }}
                  onPress={() => {
                    this.setmodalVisibleContacts(true)
                  }}
                />
              </Portal>
            </Provider>
          </View>
          <ModalContacts
            modalVisibleContacts={this.state.modalVisibleContacts}
            onPressCompte={() => this.setState({ modalVisibleCompte: true, modalVisibleContacts: false })}
            onPressContact={() => this.setState({ modalVisibleItemContacts: true, modalVisibleContacts: false })}
            accountname={this.state.accountname}
            idCompte={this.state.idCompte}
            update='yes'
            lastname={this.state.lastname}
            idContact={this.state.idContact}
            MoreInfoText={this.state.MoreinfoText}
            MoreInfoIcon={this.state.MoreinfoIcon}
            MoreInfo={this.state.Moreinfo}
            onPressInfo={this.onPressInfo}
            onPressCancel={() => this.setState({ modalVisibleContacts: false })}
            onPressSave={this.updateContacts}
            

          />
          <ModalSelectSociete
            modalVisibleCompte={this.state.modalVisibleCompte}
            
            renderItem={this.renderItemComptes}
            onPressClose={()=>{this.setState({modalVisibleCompte:false,modalVisibleContacts:true})}}
          />
          <ModalSelectContacts
            modalVisibleContacts={this.state.modalVisibleItemContacts}
            
            renderItem={this.renderItemContacts}
            onPressClose={()=>{this.setState({modalVisibleItemContacts:false,modalVisibleContacts:true})}}
          />
        </View>)
    }

  }
  setDate = () => {
    this.setState({ isDate: true })
  }

  filter = (searchText) => {

    this.setState({
      searchText: searchText,
      filterSocieteDataSearch: this.state.societeData.filter(i =>
        i.socName.toUpperCase().includes(searchText.toUpperCase()))
    })

  }
  _pickDocument = async () => {
    let result = await DocumentPicker.getDocumentAsync({})
    this.setState({ cheminDoc: result.uri, nameFile: result.name })

    alert(result.uri);
    console.log(result);
  }
  choseEvent_or_task = () => {
    if (this.state.checked === 'task') {
      this.setState({
        TaskModal: true,
        taskevent: false,

      })
    } else {
      this.setState({
        EventModal: true,
        taskevent: false,
      })

    }
  }
  setTime = (date) => {
    this.setState({ time: date, istime: false })
  }
  setDateFunc = (date) => {
    this.setState({ date: date, isDate: false })
  }
  onPressInfoFunc = () => {
    if (this.state.TaskMoreinfo) {
      this.setState({
        TaskMoreinfo: false,
        TaskMoreinfoIcon: "chevron-down",
        TaskMoreinfoText: 'Plus'
      })
    } else {
      this.setState({
        TaskMoreinfo: true,
        TaskMoreinfoIcon: "chevron-up",
        TaskMoreinfoText: 'Moins'
      })
    }
  }

  onPressOption = (x) => { this.setState({ modalVisibleButton: true, modalVisiblePhase: false, phase: x, chosen: x, value: x,valeur2:x,valeur:x }); }
  onPressOptionType = (x) => { this.setState({ modalVisibleButton: true, modalVisibleType: false, type: x, chosen: x, value: x,valeur2:x,valeur:x }); }
  render() {
    return (

      <View style={{ flex: 1, flexDirection: 'column' }}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.loadingShow}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
          <Header
            placement="right"
            leftComponent={
              <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'stretch' }}>
                <Button icon={<Icon name="chevron-left" size={20} color='white' type='material-community' />} onPress={() => this.props.navigation.goBack()} />
                <Text style={{ fontSize: 22, color: 'white', fontWeight: '500', marginTop: 5 }}>Affaires : {this.props.route.params.title}</Text>
              </View>}

          />
        </View>

        <View style={{ flex: 1.2 }}>
          {/* <Text>scrollTabNavigation</Text> */}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.scroll}>
            <TabItem title='Résumé' scale={this.state.SRésumé} BackGround={this.state.BGRésumé} onPress={() => this.changecolor('Résumé')} />
            <TabItem title='Details' scale={this.state.SDetails} BackGround={this.state.BGDetails} onPress={() => this.changecolor('Details')} />
            {(this.state.Contacts === true) && <TabItem title={'Contacts ('+this.state.dataContacts.length+')'} scale={this.state.SContacts} BackGround={this.state.BGContacts} onPress={() => this.changecolor('Contacts')} />}
          </ScrollView >

        </View>
        <SafeAreaView style={{ flex: 7, flexDirection: 'column' }}>
          {this.TabNavigation()}
        </SafeAreaView>
        {/*Nom de l'organisation,Nom du contact,Type;Phase de vente*/}
        <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Affaires'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }); }}
          onPressSave={this.update}
        />
        <ModalDate
          modalVisibleDate={this.state.modalVisibleDate}
          title={this.state.title}
          edit={'Edit Affaires'}
          valeur={this.state.valeur}
          onPressCancel={() => this.setState({ modalVisibleDate: false, value: '',valeur:'' })}
          onPressSave={this.update}

        />

       

      </View>

    )

  }
}




