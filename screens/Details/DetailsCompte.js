
import React, { Component } from 'react';
import { SafeAreaView, Image, TouchableOpacity, ScrollView, Text, View, Modal, FlatList,Linking } from 'react-native';
import { FAB, Portal, Provider, Divider } from 'react-native-paper';
import Info from '../ItemsRender/info';
import { Header, Button, Icon, Overlay } from 'react-native-elements';
import styles from '../styles'
import Contact from '../ItemsRender/contact'
import ItemAffaire from '../ItemsRender/affaireitem'
import Comments from '../ItemsRender/comment';
import ModalSelectItem from '../Modals/ModalSelectItem';
import ModalSelectSociete from '../Modals/ModalSelectSociete';
import ModalAffaires from '../Modals/ModalAffaires';
import TabItem from '../ItemsRender/TabItem';
import ModalProjects from '../Modals/ModalProjects';
import AddTicket from '../Modals/Modal_AddTickets';
import ModalSelectType from '../Modals/ModalSelectType';
import ModalSelectContacts from '../Modals/ModalSelectContact';
import ModalSelectItemPriority from '../Modals/ModalSelectItemTickets';
import ModalSelectEngagement from '../Modals/ModalSelectEngagement';
import ModalSelectCategorie from '../Modals/ModalSelectCategorie';
import ModalContacts from '../Modals/ModalContact';
import ModalAddContrat from '../Modals/ModalContrat';
import ModalSelectUnite from '../Modals/ModalSelectUnite';
import ModalSelectItemType from '../Modals/ModalSelectItemType';
import ModalSelectSecteur from '../Modals/ModalSelectSecteur';
import ModalButton from '../Modals/ModalsUpdate/ModalButton';
import ModalInput from '../Modals/ModalsUpdate/Modalnput';
import ModalTypeUpdate from '../Modals/ModalTypeUpdate';
import ModalSelectItemStatut from '../Modals/ModalSelectItemStatut';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalActivities from '../Modals/ModalActivitie';
import ModalVisibilite from '../Modals/ModalVisibilite';
import ModalSelectStatutActivitie from '../Modals/ModalSelectStatutActvitie';
import ModalTypeActivities from '../Modals/ModalTypeActivities';
import ItemAct from '../ItemsRender/itemActivities';
import ItemRender from '../ItemsRender/itemrender';

export default class DetailsComptes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingShow: true,
      Contacts: false,
      HelpDesk: false,
      Potentials: false,
      Documents: false,
      ServiceContracts: false,
      valeur:'',
      valeur2:'',
      Project: false,
      //updates fields 
      chosen: 'Select Item',
      value: '',
      title: '',
      key: '',
      // updates modals
      modalVisibleInput: false,
      modalVisibleDate: false,
      modalVisibleButton: false,
      modalVisibleSecteur: false,
      modalVisibleTypeUpdate: false,
      modalVisibleType:false,


      //create modals
      modalVisibleAffaires: false,
      modalVisibleVisibilite:false,
      modalVisibleStatutActivitie:false,
      modalVisibleActivities:false,
      modalVisibleCategorie: false,
      modalVisibleUnite: false,
      modalVisibleCompte: false,
      modalVisibleItemPriority: false,
      modalVisibleItemStatut: false,
      modalVisibleContrat: false,
      modalVisibleContacts: false,
      modalVisibleItemContacts: false,
      modalVisibleTickets: false,
      modalVisibleItemType: false,
      modalVisibleDelete: false,
      modalVisibleItem: false,
      modalVisibleProjects: false,
      //create field
      statutTickets: 'Ouvert',
      statutActivitie: 'Plannifiée',
      statutContrat: 'Ouvert',
      statutProject: 'Ouvert',

      priority: 'Normale',
      unite: 'Jours',
      categorie: 'Petit problème',
      modalVisibleEngagement: false,
      engagement: 'Mineur',
      accountname: this.props.route.params.accountname,
      lastname: 'Select Contact',
      type: ' Rendez-vous',
      phase: 'Proposition',
      idDelete: '',
      visibilite:'Select visibilité',
      idCompte: this.props.route.params.idCompte,
      idContact: '',
      idTickets: '',
      idProject: '',
      idToAccountname: '',
      //data
      dataProjects: [],
      dataFields: [],
      dataTickets: [],
      dataActivities: [],
      dataAffaires: [],
      dataDocuments: [],
      dataContrats: [],
      
      dataMiseaj: [{
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
        description: 'Kamal commented on your document',
        date: '3days',
        icon: 'address-book-o'
      },
      {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
        description: 'Aya shared your document with DEMO',
        date: '3days',
        icon: 'cubes'
      },
      {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
        description: 'Ahmed added a new contact',
        date: '3days',
        icon: 'bell-o'
      },],
      dataContacts: [],
      //tabBG
      BGRésumé: '#9ddfd3',
      BGDetails: 'white',
      BGMises: 'white',
      BGContacts: 'white',
      BGAffaires: 'white',
      BGDevis: 'white',
      BGFactures: 'white',
      BGSalesorders: 'white',
      BGTickets: 'white',
      BGActivitie: 'white',
      BGEmails: 'white',
      BGDoc: 'white',
      BGProduit: 'white',
      BGProjects: 'white',
      BGServ: 'white',
      BGComnt: 'white',
      //scale
      SRésumé: 0.8,
      SDetails: 0.6,
      SMises: 0.6,
      SContacts: 0.6,
      SAffaires: 0.6,
      STickets: 0.6,
      SActivitie: 0.6,
      SProjects: 0.6,
   
      //tabVisible
      TabVisible: 'BGRésumé',
      //------------------
      Detail: true,
      BlocSys: true,
      InfoP: true,
      Adress: true,
      Desc: true,
      //add affaires
      AffaireModal: 'false',
      Moreinfo: false,
      MoreinfoIcon: "chevron-down",
      MoreinfoText: 'Plus',
      //
      date: new Date(),
      time: new Date(),
      isDate: false,
      istime: false,
      // TYPES
      Campaigns: false,
      Vendors: false,
      Faq: false,
      Quotes: false,
      PurchaseOrder: false,
      SalesOrder: false,
      Invoice: false,
      Calendar: false,
      Leads: false,
      Accounts: false,
      Contacts: false,
      Potentials: false,
      Products: false,
      Documents: false,
      Emails: false,
      HelpDesk: false,
      Events: false,
      ServiceContracts: false,
      Services: false,
      Assets: false,
      ModComments: false,
      ProjectMilestone: false,
      ProjectTask: false,
      Project: false,
      CTAttendance: false,
      Groups: false,
      Currency: false,
      DocumentFolders: false,
      CompanyDetails: false,
      LineItem: false,
      Tax: false,
      ProductTaxes: false,
      //
      session: [],
      link: [],
      isLoded: false,
    }
  }
  inialiserValues=()=>{
    this.setState({
      statutTickets: 'Ouvert',
      statutActivitie: 'Plannifiée',
      statutContrat: 'Ouvert',
      statutProject: 'Ouvert',
      priority: 'Normale',
      unite: 'Jours',
      categorie: 'Petit problème',
      engagement: 'Mineur',
      accountname: this.props.route.params.accountname,
      lastname: 'Select Contact',
      type: ' Rendez-vous',
      phase: 'Proposition',
      idDelete: '',
      visibilite:'Select visibilité',
      idCompte: this.props.route.params.idCompte,
      idContact: '',
      idTickets: '',
      idProject: '',
      idToAccountname: '',
    })
  }
  async componentDidMount() {
    this.setState({ isLoded: false });
   
    this.getFunction();
  }
  loaded = (val) => {
    this.setState({ isLoded: val });
 
  };
  
  getFunction=async() => {
    console.log('insid fet fucntion')
    this.setState({ loadingShow: true });
    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    console.log('insid fet fucntion again')
    try {
      console.log('insid fet fucntion again again')
      await AsyncStorage.multiGet(['SESSION', 'CRM_LINK', 'TYPES'], async (err, res) => {
        types = res[2][1].split(',');
        if (types.includes('Contacts')) this.setState({ Contacts: true });
        if (types.includes('Documents')) this.setState({ Documents: true });
        if (types.includes('HelpDesk')) this.setState({ HelpDesk: true });
        if (types.includes('Potentials')) this.setState({ Potentials: true });
        if (types.includes('ServiceContracts')) this.setState({ ServiceContracts: true });
        if (types.includes('Project')) this.setState({ Project: true });
        var dataFet = res[0][1];
        this.setState({ dataFet: dataFet });
        var link = res[1][1];
        this.setState({ link: link });
        //this fetch is for contacts
        console.log('insid fet fucntion again 3')
        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Contacts where account_id=' + dataRoute.id + ';')
          .then((response) => response.json())
          .then(async (json) => { this.setState({ dataContacts: json.result }); 
          //this fetch is from projects
          console.log('insid fet fucntion again 4')
          await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Project where linktoaccountscontacts=' + dataRoute.id + ';')
            .then((response) => response.json())
            .then(async (json) => { this.setState({ dataProjects: json.result })
            //this fetch is from contrats services
            await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from ServiceContracts where sc_related_to=' + dataRoute.id + ';')
              .then((response) => response.json())
              .then(async (json) => { this.setState({ dataContrats: json.result }); 
              //Events
              await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Events where parent_id=' + dataRoute.id + ';')
                .then((response) => response.json())
                .then(async(json) => { this.setState({ dataActivities: json.result });         
                
                //Affaires
                await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Potentials where related_to=' + dataRoute.id + ';')
                  .then((response) => response.json())
                  .then(async(json) => { this.setState({ dataAffaires: json.result }) 
                    //Tickets
                    await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from HelpDesk where parent_id=' + dataRoute.id + ';')
                      .then((response) => response.json())
                      .then(async(json) => { this.setState({ dataTickets: json.result }) 
                     
                        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Accounts where id=' + dataRoute.account_id + ';')
                          .then((response) => response.json())
                          .then((json) => {this.setState({ loadingShow: false }); 
                          if (json.success == true && json.result != '') {
                       
                            this.setState({ idToAccountname: json.result[0].accountname })}
                        })
                          .catch((error) => {
                           
                            console.log(error)
                          });
                        this.loaded(true);
                       })
                        .catch((error) =>
                          console.log(error))
                    })
                    .catch((error) =>
                      console.log(error))
                })
                  .catch((error) =>
                    console.log(error))
            
            
            })
              .catch((error) =>
                console.log(error))
              })
              .catch((error) =>
                console.log(error))
          })
            .catch((error) =>
              console.log(error))
        })
         
    } catch (e) {
      console.log(e);
    }
  }



  initaliserStyle = () => {
    this.setState({
      BGRésumé: 'white',
      BGDetails: 'white',
      BGMises: 'white',
      BGContacts: 'white',
      BGAffaires: 'white',
      BGDevis: 'white',
      BGFactures: 'white',
      BGSalesorders: 'white',
      BGActivitie: 'white',
      BGEmails: 'white',
      BGTickets: 'white',
      BGDoc: 'white',
      BGProduit: 'white',
      BGProjects: 'white',
      BGServ: 'white',
      BGComnt: 'white',
      //scale
      SRésumé: 0.8,
      SDetails:0.6,
      SMises:0.6,
      SContacts: 0.6,
      SAffaires: 0.6,
      SDevis:0.6,
      STickets: 0.6,
      SFactures: 0.9,
      SSalesorders: 0.9,
      SActivitie: 0.6,
      SEmails: 0.9,
      SDoc: 0.9,
      SProduit: 0.9,
      SProduit: 0.9,
      SProjects: 0.6,
      SServ: 0.9,
      SComnt: 0.9,


    })

  }
  renderElement = ({ item }) => {
    return (
      <View style={{ flex: 9 }}>
        <ScrollView>
          <ItemAffaire Name={item.potentialname} onPress={() => { this.setState({ image: true }) }} onDelete={() => this.setState({ modalVisibleDelete: true })} />
        </ScrollView>
      </View>)
  }
  renderElementServices = ({ item, index }) => {
    return (
      <View style={{ flex: 9 }}>
        <ItemAffaire Name={item.subject} onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} 
        onPress={() => this.props.navigation.navigate('DetailsContrats', { title:item.subject,id: item.id, data: this.state.dataContrats, index: index, result: this.state.dataFet })} />
      </View>)

  }
  renderElementProjects = ({ item, index }) => {
    return (
      <View style={{ flex: 9 }}>
        <ItemAffaire Name={item.projectname} onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }}
          onPress={() => this.props.navigation.navigate('DetailsProjects', { title:item.projectname,id: item.id, data: this.state.dataProjects, index: index, result: this.state.dataFet })} />
      </View>)

  }



  renderElementMiseaj = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => { console.log("pressed: " + item.title) }}
      >

        <View style={styles.block}>

          <View style={styles.icon_container}>
            <Icon style={styles.icon}
              name={item.icon}
              color='#225fbf'
              type='font-awesome' />
          </View>
          <View style={styles.details}>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.description} >{item.description}</Text>
          </View>
          <View style={styles.date_container}>
            <Text style={styles.date}>{item.date} ago</Text>
          </View>
        </View>

      </TouchableOpacity>
    );
  }
  renderElementAffaire = ({ item, index }) => {
    return (
      <View>
        <ItemAffaire Name={item.potentialname} onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} 
        onPress={() => this.props.navigation.navigate('DetailsAffaires', { title:item.potentialname,idCompte: this.props.route.params.idCompte, id: item.id, data: this.state.dataAffaires, index: index, result: this.state.dataFet })} />
      </View>);
  }
  renderElementActivities = ({ item, index }) => {
    return (
      <View>
        <ItemAct Name={item.subject} status={item.eventstatus} date={item.date_start+' - '+item.time_start}onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} 
        onPress={() => this.props.navigation.navigate('DetailsActivities', { title:item.subject,idCompte: this.props.route.params.idCompte, id: item.id, data: this.state.dataActivities, index: index, result: this.state.dataFet })} />
      </View>);
  }

  changecolor = (a) => {
    this.initaliserStyle();
    this.setState({
      [`BG${a}`]: '#9ddfd3',
      [`S${a}`]: 0.8,
      TabVisible: `BG${a}`
    });
  }
  makeCall = (phone) => {

    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = `tel:${phone}`;
    } else {
      phoneNumber = `telprompt:${phone}`;
    }

    Linking.openURL(phoneNumber);
  };
  onPressEmail=(email) => {Linking.openURL(`mailto:${email}`) }

  renderElementContacts = ({ item, index }) => {
    return (
      <View>
        <Contact  Name={item.firstname+' '+item.lastname} onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }}
         email={item.phone} onPressPhone={()=>this.makeCall(item.phone)} onPress={() => this.props.navigation.navigate('DetailsContacts', { title:item.firstname+' '+item.lastname,lastname: item.lastname, id: item.id, data: this.state.dataContacts, index: index, result: this.state.dataFet,idCompte:item.account_id ,account:this.state.idToAccountname})} />{/*a voir */}
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Divider style={{ width: '95%' }} />
        </View>
      </View>)

  }
  onPressInfo = () => {
    if (this.state.Moreinfo) {
      this.setState({
        Moreinfo: false,
        MoreinfoIcon: "chevron-down",
        MoreinfoText: 'Plus'
      })
    } else {
      this.setState({
        Moreinfo: true,
        MoreinfoIcon: "chevron-up",
        MoreinfoText: 'Moins'
      })
    }
  }




  ChangeNomSociete = (soc) => {
    this.setState({ nomSoc: soc })
  }
  renderElementTickets = ({ item, index }) => {
    return (
      <View>
          <ItemRender Name={item.ticket_title} val={item.ticketstatus}
            onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }}
          onPress={() => { this.props.navigation.navigate('DetailsTickets', { title:item.ticket_title,id: item.id, data: this.state.dataTickets, index: index, result: this.state.dataFet }) }} />{/*a voir */}
      </View>
    )
  }
  setVisibleAffaires = () => {
    this.setState({ modalVisibleAffaires: false })
  }
  setVisibleProjects = () => {
    this.setState({ modalVisibleProjects: false });
  }

  onPressPriority = () => {
    this.setState({ modalVisibleItemPriority: true, modalVisibleTickets: false })
  }
  onPressStatut = () => {
    this.setState({ modalVisibleItemStatut: true, modalVisibleTickets: false,modalVisibleProjects:false,modalVisibleContrat :false,})
  }
  setVisibleTickets = () => {
    this.setState({ modalVisibleTickets: false })
  }
  onPressItemContact = () => {
    this.setState({ modalVisibleItemContacts: true, modalVisibleTickets: false })
  }



  onPressCancelContrat = () => {
    this.setState({ modalVisibleContrat: false })
  }
  update = async (value) => {
    this.setState({ loadingShow: true });
    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    
    dataRoute[this.state.key] = value;
    await fetch('https://portail.crm4you.ma/webservice.php', {
      method: 'POST',
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: 'operation=update&sessionName=' + this.state.dataFet + '&element=' + JSON.stringify(dataRoute)
    })
      .then((responses) => responses.json())
      .then((json) => {this.setState({ loadingShow: false ,modalVisibleInput:false,modalVisibleDate:false,modalVisibleButton:false,valeur:'',chosen: 'Select Item'}); console.log(json)  })
      .then(this.getFunction)
      .catch((error) => console.error(error));
    
  }
  onPressCloseStatut=()=>{
    this.setState({modalVisibleItemStatut:false,modalVisibleProjects:true})
  }
  makeCall = (phone) => {

    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = `tel:${phone}`;
    } else {
      phoneNumber = `telprompt:${phone}`;
    }

    Linking.openURL(phoneNumber);
  };




  TabNavigation = () => {
    var dataRoute = this.props.route.params.data[this.props.route.params.index]

    if (this.state.TabVisible == 'BGRésumé') {
      return (
        <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 1, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Champ De Clé
                </Text>
            </View>
          </View>
          <View style={styles.content_champ_cle}>
            <Info title={'Nom compte'} modifiable={true} borderBottom={0.6} content={dataRoute.accountname}
              onPress={() => { this.setState({ valeur:dataRoute.accountname ,modalVisibleInput: true, key: 'accountname', title: 'Nom compte' }) }} />
            <Info title={'Ville de Facturation'} modifiable={true} borderBottom={0.6} content={dataRoute.bill_city}
              onPress={() => { this.setState({ valeur:dataRoute.bill_city,modalVisibleInput: true, key: 'bill_city', title: 'Ville de Facturation' }) }} />
            <Info title={'Pays de Facturation'} modifiable={true} borderBottom={0.6} content={dataRoute.bill_country}
              onPress={() => { this.setState({ valeur:dataRoute.bill_country,modalVisibleInput: true, key: 'bill_country', title: 'Pays de Facturation' }) }} />
                   <Info title={'Téléphone Principal'} modifiable={true} borderBottom={0.6} content={dataRoute.phone}
                onPressContent={()=>this.makeCall(dataRoute.phone)} onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.phone ,key: 'phone', title: 'Téléphone Principal' }) }} />
                <Info title={'E-Mail Principale'} modifiable={true} borderBottom={0.6} content={dataRoute.email1}
              onPress={() => { this.setState({ modalVisibleInput: true, key: 'email1', title: 'E-Mail Principale', valeur:dataRoute.email1 })}} onPressContent={()=>this.onPressEmail(dataRoute.email1)}/>
            <View style={{ width: '100%', height: 70, backgroundColor: 'white', marginTop: 10 }}>
              <TouchableOpacity activeOpacity={0.7} style={styles.buttonFullDetails} onPress={() => { this.changecolor('Details') }} >
                <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: 'white' }}>Full Details</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ height: 50 }}></View>
          <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Compte'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }) }}
          onPressSave={this.update}
        />
          <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Compte'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false,value:'',chosen:'Select Item' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Type') {
              this.setState({
                modalVisibleTypeUpdate: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Secteur') {
              this.setState({
                modalVisibleSecteur: true, modalVisibleButton: false
              })
            }

            if (this.state.title === 'Filiale de') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalSelectSecteur
          modalVisibleSecteur={this.state.modalVisibleSecteur}
          onPressOption={this.onPressOptionSecteur}
          onPressClose={() => this.setState({ modalVisibleSecteur: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
        <ModalTypeUpdate
          modalVisibleTypeUpdate={this.state.modalVisibleTypeUpdate}
          onPressOption={this.onPressOptionTypeUpdate}
          onPressClose={() => this.setState({ modalVisibleTypeUpdate: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={this.onPressCancelCompte}
        />
        </ScrollView>
      )
    }
    else if (this.state.TabVisible == 'BGDetails') {
      return (
        <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Détail Du Compte
              </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Detail: !this.state.Detail }) }}>
              {!this.state.Detail ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.Detail ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Nom compte'} modifiable={true} borderBottom={0.6} content={dataRoute.accountname}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.accountname, key: 'accountname', title: 'Nom compte' }) }} />
              <Info title={'Filiale de'} modifiable={true} borderBottom={0.6} content={this.state.idToAccountname}
                onPress={() => { this.setState({ modalVisibleButton: true, key: 'account_id', valeur:this.state.idToAccountname,valeur2:dataRoute.account_id,title: 'Filiale de' }) }} />
              <Info title={'Secteur'} modifiable={true} borderBottom={0.6} content={dataRoute.industry}
                onPress={() => { this.setState({ modalVisibleButton: true, key: 'industry', title: 'Secteur',valeur:dataRoute.industry,valeur2:dataRoute.industry, }) }} />
              <Info title={'Type'} modifiable={true} borderBottom={0.6} content={dataRoute.accounttype}
                onPress={() => { this.setState({ modalVisibleButton: true, key: 'accounttype', title: 'Type',valeur:dataRoute.accounttype,valeur2:dataRoute.accounttype, }) }} />
              <Info title={'Téléphone Principal'} modifiable={true} borderBottom={0.6} content={dataRoute.phone}
                onPressContent={()=>this.makeCall(dataRoute.phone)} onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.phone,key: 'phone', title: 'Téléphone Principal' }) }} />
              <Info title={'Téléphone Secondaire'} modifiable={true} borderBottom={0.6} content={dataRoute.otherphone}
                onPressContent={()=>this.makeCall(dataRoute.otherphone)} onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.otherphone,key: 'otherphone', title: 'Téléphone Secondaire' }) }} />
              <Info title={'E-Mail Principale'} modifiable={true} borderBottom={0.6} content={dataRoute.email1}
              onPressContent={()=>this.onPressEmail(dataRoute.email1)} onPress={() => { this.setState({ modalVisibleInput: true, key: 'email1',valeur:dataRoute.email1, title: 'E-Mail Principale' }) }}/>
              <Info title={'Autre Email'} modifiable={true} borderBottom={0.6} content={dataRoute.email2}
                onPressContent={()=>this.onPressEmail(dataRoute.email2)} onPress={() => { this.setState({ modalVisibleInput: true, key: 'email2', valeur:dataRoute.email2,title: 'Autre Email' }) }} />
              <Info title={'Fax'} modifiable={true} borderBottom={0.6} content={dataRoute.fax}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.fax, key: 'fax', title: 'Fax' }) }} />
              <Info title={'ICE'} modifiable={true} borderBottom={0.6} content={dataRoute.cf_932}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.cf_932,key: 'cf_932', title: 'ICE' }) }} />
              <Info title={'Site Web'} modifiable={true} borderBottom={0.6} content={dataRoute.website}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'website', valeur:dataRoute.website,title: 'Site Web' }) }} />
              <Info title={'Effectif'} modifiable={true} borderBottom={0.6} content={dataRoute.employees}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'employees',valeur:dataRoute.employees, title: 'Effectif' }) }} />
              <Info title={'Revenu Annuel'} modifiable={true} borderBottom={0.6} content={dataRoute.annual_revenue}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'annual_revenue',valeur:dataRoute.annual_revenue, title: 'Revenu Annuel' }) }} />
              <Info title={'Email Opt Out'} modifiable={true} borderBottom={0} content={dataRoute.emailoptout}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'emailoptout', valeur:dataRoute.emailoptout,title: 'Email Opt Out' }) }} />
            </View>
          ) : null}
          <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Compte'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }) }}
          onPressSave={this.update}
        />
           <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Compte'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false,value:'',chosen:'Select Item',valeur:'',valeur2:'' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Type') {
              this.setState({
                modalVisibleTypeUpdate: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Secteur') {
              this.setState({
                modalVisibleSecteur: true, modalVisibleButton: false
              })
            }

            if (this.state.title === 'Filiale de') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalSelectSecteur
          modalVisibleSecteur={this.state.modalVisibleSecteur}
          onPressOption={this.onPressOptionSecteur}
          onPressClose={() => this.setState({ modalVisibleSecteur: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
        <ModalTypeUpdate
          modalVisibleTypeUpdate={this.state.modalVisibleTypeUpdate}
          onPressOption={this.onPressOptionTypeUpdate}
          onPressClose={() => this.setState({ modalVisibleTypeUpdate: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={this.onPressCancelCompte}
        />
          <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Bloc Systeme
              </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ BlocSys: !this.state.BlocSys }) }}>
              {!this.state.BlocSys ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>

          {this.state.BlocSys ? (

            <View style={styles.content_champ_cle}>
              <Info title={'Date de modification'} borderBottom={0.6} content={dataRoute.modifiedtime} modifiable={false}
              />
              <Info title={'Date de création'} borderBottom={0.6} content={dataRoute.createdtime}modifiable={false}
              />
              <Info title={'Source'} borderBottom={0.6} content={dataRoute.source} modifiable={false}/>
              <Info title={'Compte N°'} borderBottom={0.6} content={dataRoute.account_no} modifiable={false}/>
            </View>
          ) : null}

          <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Information Personnalisée
              </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ InfoP: !this.state.InfoP }) }}>
              {!this.state.InfoP ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>

          <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Compte'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }) }}
          onPressSave={this.update}
        />
           <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Compte'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false,value:'',chosen:'Select Item',valeur:'',valeur2:'' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Type') {
              this.setState({
                modalVisibleTypeUpdate: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Secteur') {
              this.setState({
                modalVisibleSecteur: true, modalVisibleButton: false
              })
            }

            if (this.state.title === 'Filiale de') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalSelectSecteur
          modalVisibleSecteur={this.state.modalVisibleSecteur}
          onPressOption={this.onPressOptionSecteur}
          onPressClose={() => this.setState({ modalVisibleSecteur: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
        <ModalTypeUpdate
          modalVisibleTypeUpdate={this.state.modalVisibleTypeUpdate}
          onPressOption={this.onPressOptionTypeUpdate}
          onPressClose={() => this.setState({ modalVisibleTypeUpdate: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={this.onPressCancelCompte}
        />
          <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Détail de l'Adresse
              </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Adress: !this.state.Adress }) }}>
              {!this.state.Adress ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>

          {this.state.Adress ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Adresse de Facturation'} modifiable={true} borderBottom={0.6} content={dataRoute.bill_street}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.bill_street,key: 'bill_street', title: 'Adresse de Facturation' }) }} />
              <Info title={'Adresse de Livraison'} modifiable={true} borderBottom={0.6} content={dataRoute.ship_street}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.ship_street, key: 'ship_street', title: 'Adresse de Livraison' }) }} />
              <Info title={'Boite Postale de Facturation'} modifiable={true} borderBottom={0.6} content={dataRoute.bill_pobox}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'bill_pobox', valeur:dataRoute.bill_pobox,title: 'Boite Postale de Facturation' }) }} />
              <Info title={'Boite Postale de Livraison'} modifiable={true} borderBottom={0.6} content={dataRoute.ship_pobox}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'ship_pobox',valeur:dataRoute.ship_pobox, title: 'Boite Postale de Livraison' }) }} />
              <Info title={'Ville de Facturation'} modifiable={true} borderBottom={0.6} content={dataRoute.bill_city}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'bill_city',valeur:dataRoute.bill_city, title: 'Ville de Facturation' }) }} />
              <Info title={'Ville de Livraison'} modifiable={true} borderBottom={0.6} content={dataRoute.ship_city}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'ship_city',valeur:dataRoute.ship_city, title: 'Ville de Livraison' }) }} />
              <Info title={'Etat de la Facturation '} modifiable={true} borderBottom={0.6} content={dataRoute.bill_state}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'bill_state',valeur:dataRoute.bill_state, title: 'Etat de la Facturation ' }) }} />
              <Info title={'Code Postale de Facturation'} modifiable={true} borderBottom={0.6} content={dataRoute.bill_code}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'bill_code',valeur:dataRoute.bill_code, title: 'Code Postale de Facturation' }) }} />
              <Info title={'Code Postale de Livraison'} modifiable={true} borderBottom={0.6} content={dataRoute.ship_code}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'ship_code', valeur:dataRoute.ship_code,title: 'Code Postale de Livraison' }) }} />
              <Info title={'Pays de Facturation'} modifiable={true} borderBottom={0.6} content={dataRoute.bill_country}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'bill_country',valeur:dataRoute.bill_country, title: 'Pays de Facturation' }) }} />
              <Info title={'Pays de Livraison'} modifiable={true} borderBottom={0} content={dataRoute.ship_country}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.ship_country, key: 'ship_countryo', title: 'Pays de Livraison' }) }} />
            </View>
          ) : null}
          <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Compte'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }) }}
          onPressSave={this.update}
        />
           <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Compte'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false ,value:'',chosen:'Select Item',valeur:'',valeur2:''})}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Type') {
              this.setState({
                modalVisibleTypeUpdate: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Secteur') {
              this.setState({
                modalVisibleSecteur: true, modalVisibleButton: false
              })
            }

            if (this.state.title === 'Filiale de') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalSelectSecteur
          modalVisibleSecteur={this.state.modalVisibleSecteur}
          onPressOption={this.onPressOptionSecteur}
          onPressClose={() => this.setState({ modalVisibleSecteur: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
        <ModalTypeUpdate
          modalVisibleTypeUpdate={this.state.modalVisibleTypeUpdate}
          onPressOption={this.onPressOptionTypeUpdate}
          onPressClose={() => this.setState({ modalVisibleTypeUpdate: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={this.onPressCancelCompte}
        />
          <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Information Sur la description
              </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Desc: !this.state.Desc }) }}>
              {!this.state.Desc ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>

          {this.state.Desc ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Description'} modifiable={true} borderBottom={0} content={dataRoute.description}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'description', title: 'Description' }) }} />
            </View>
          ) : null}
          <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Compte'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }) }}
          onPressSave={this.update}
        />
           <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Compte'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false,value:'',chosen:'Select Item',valeur:'',valeur2:'' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Type') {
              this.setState({
                modalVisibleTypeUpdate: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Secteur') {
              this.setState({
                modalVisibleSecteur: true, modalVisibleButton: false
              })
            }

            if (this.state.title === 'Filiale de') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalSelectSecteur
          modalVisibleSecteur={this.state.modalVisibleSecteur}
          onPressOption={this.onPressOptionSecteur}
          onPressClose={() => this.setState({ modalVisibleSecteur: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
        <ModalTypeUpdate
          modalVisibleTypeUpdate={this.state.modalVisibleTypeUpdate}
          onPressOption={this.onPressOptionTypeUpdate}
          onPressClose={() => this.setState({ modalVisibleTypeUpdate: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={this.onPressCancelCompte}
        />

          <View style={{ marginTop: 40 }}></View>
        </ScrollView>
      )
    }
    else if (this.state.TabVisible == 'BGTickets') {
      return (
        <View style={{ flex: 1 }}>
          <FlatList
            data={this.state.dataTickets}
            renderItem={this.renderElementTickets}
            extraData={this.state.dataTickets.length}
            keyExtractor={item => item.id} />
          <View style={{ flex: 2 }}>
            <Provider>
              <Portal>
                <FAB.Group
                  open={this.state.open}
                  icon={'plus'}
                  color='white'
                  fabStyle={{ backgroundColor: '#53aefe' }}
                  actions={[
                    { icon: 'plus', onPress: () => console.log('Pressed add') },
                  ]}
                  onStateChange={() => this.setState({ modalVisibleTickets: true })}
                  onPress={() => {
                    this.setState({ modalVisibleTickets: true })
                  }}
                />
              </Portal>
            </Provider>
          </View>
          <AddTicket
            modalVisibleTickets={this.state.modalVisibleTickets}
            onPressPriority={this.onPressPriority}
            onPressStatut={this.onPressStatut}
            onPressItemContact={this.onPressItemContact}
            onPressCompte={() => this.setState({ modalVisibleCompte: true, modalVisibleTickets: false })}
            onPressEngagement={() => this.setState({ modalVisibleEngagement: true, modalVisibleTickets: false })}
            onPressCategorie={() => this.setState({ modalVisibleCategorie: true, modalVisibleTickets: false })}
            accountname={this.state.accountname}
            selectPriority={this.state.priority}
            selectStatut={this.state.statutTickets}
            
            onPressInfo={this.onPressInfo}
            MoreinfoText={this.state.MoreinfoText}
            MoreinfoIcon={this.state.MoreinfoIcon}
            Moreinfo={this.state.Moreinfo}
            onPressCancel={()=>{this.setVisibleTickets();this.inialiserValues()}}
            onPressSave={()=>{this.getFunction();this.inialiserValues()}}
            lastname={this.state.lastname}
            idCompte={this.state.idCompte}
            idContact={this.state.idContact}
            engagement={this.state.engagement}
            categorie={this.state.categorie}
          />
          <ModalSelectItemPriority
            modalVisibleItemTickets={this.state.modalVisibleItemPriority}
            onPressOption={this.onPressOptionPriority}
            priorite={'Select Priorité'}
            onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleTickets: true })}
          />
          <ModalSelectItemStatut
            modalVisibleItemTickets={this.state.modalVisibleItemStatut}
            onPressOption={this.onPressOptionStatut}
            select={'Select Statut'}
            onPressClose={this.onPressCloseStatut}
          />
          <ModalSelectEngagement
            modalVisibleEngagement={this.state.modalVisibleEngagement}
            onPressOption={this.onPressOptionEngagement}
            select={'Select Statut'}
            onPressClose={() => this.setState({ modalVisibleEngagement: false, modalVisibleTickets: true })}

          />
          <ModalSelectCategorie
            modalVisibleCategorie={this.state.modalVisibleCategorie}
            onPressOption={this.onPressOptionCategorie}
            select={'Select catégorie'}
            onPressClose={() => this.setState({ modalVisibleCategorie: false, modalVisibleTickets: true })}

          />
          <ModalSelectContacts
            modalVisibleContacts={this.state.modalVisibleItemContacts}
            
            renderItem={this.renderItemContacts}
            onPressClose={()=>{this.setState({modalVisibleItemContacts:false,modalVisibleTickets:true})}}
          />
          <ModalSelectSociete
            modalVisibleCompte={this.state.modalVisibleCompte}
            
            renderItem={this.renderItemComptes}
            onPressClose={()=>{this.setState({modalVisibleCompte:false,modalVisibleTickets:true})}}
          />
        </View>

      )
    }
   
    else if (this.state.TabVisible == 'BGContacts') {
      return (
        <View style={{ flex: 1, flexDirection: 'column', padding: 20 }}>
          <FlatList
            data={this.state.dataContacts}
            renderItem={this.renderElementContacts}
            keyExtractor={item => item.id}
          />

          <View style={{ flex: 2 }}>
            <Provider>
              <Portal>
                <FAB.Group
                  
                  icon={'plus'}
                  color='white'
                  fabStyle={{ backgroundColor: '#53aefe' }}
                  actions={[
                    { icon: 'plus', onPress: () => console.log('Pressed add') },
                  ]}
                  onStateChange={() => { this.setState({ modalVisibleContacts: true }) }}
                  onPress={() => { this.setState({ modalVisibleContacts: true }) }}
                />
              </Portal>
            </Provider>
          </View>
          <ModalContacts
            modalVisibleContacts={this.state.modalVisibleContacts}
            onPressCompte={() => this.setState({ modalVisibleCompte: true, modalVisibleContacts: false })}
            onPressContact={() => this.setState({ modalVisibleItemContacts: true, modalVisibleContacts: false })}
            accountname={this.state.accountname}
            idCompte={this.state.idCompte}
            lastname={this.state.lastname}
            idContact={this.state.idContact}
            MoreInfoText={this.state.MoreinfoText}
            MoreInfoIcon={this.state.MoreinfoIcon}
            MoreInfo={this.state.Moreinfo}
            onPressInfo={this.onPressInfo}
            onPressCancel={() => {this.setState({ modalVisibleContacts: false });this.inialiserValues()}}
            onPressSave={()=>{this.getFunction();this.inialiserValues()}}
            

          />
          <ModalSelectSociete
            modalVisibleCompte={this.state.modalVisibleCompte}
            
            renderItem={this.renderItemComptes}
            onPressClose={()=>{this.setState({ modalVisibleContacts: true,modalVisibleCompte:false})}}
          />
          <ModalSelectContacts
            modalVisibleContacts={this.state.modalVisibleItemContacts}
            
            renderItem={this.renderItemContacts}
            onPressClose={()=>{this.setState({ modalVisibleContacts: true,modalVisibleItemContacts:false})}}
          />
        </View>)
    }
    else if (this.state.TabVisible == 'BGActivitie') {
      return (
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 9 }}>

            <FlatList
              data={this.state.dataActivities}
              renderItem={this.renderElementActivities}
              extraData={this.state.dataActivities.length}
              keyExtractor={item => item.id}
            />


          </View>
          <View style={{ flex: 1 }}>
            <Provider>
              <Portal>
                <FAB.Group
                  open={this.state.open}
                  icon={'plus'}
                  color='white'
                  fabStyle={{ backgroundColor: '#53aefe' }}
                  actions={[
                    { icon: 'plus', onPress: () => console.log('Pressed add') },
                  ]}
                  onStateChange={() => { this.setState({ modalVisibleActivities: true }) }}
                  onPress={() => { this.setState({ modalVisibleActivities: true }) }}
                />
              </Portal>
            </Provider>
          </View>
          <ModalActivities
                            modalVisibleActivities={this.state.modalVisibleActivities}
                            onPressPriority={() => this.setState({ modalVisibleItemPriority: true, modalVisibleActivities: false })}
                            onPressStatut={() => this.setState({ modalVisibleStatutActivitie: true, modalVisibleActivities: false })}
                            onPressItemContact={() => this.setState({ modalVisibleItemContacts: true, modalVisibleActivities: false })}
                            onPressCompte={() => this.setState({ modalVisibleCompte: true, modalVisibleActivities: false })}
                            onPressType={() => this.setState({ modalVisibleItemType: true, modalVisibleActivities: false })}
                            onPressCategorie={() => this.setState({ modalVisibleCategorie: true, modalVisibleActivities: false })}
                            onPressVisibilite={()=>this.setState({modalVisibleActivities:false,modalVisibleVisibilite:true})}
                            accountname={this.state.accountname}
                            selectPriority={this.state.priority}
                            selectStatut={this.state.statutActivitie}
                            selectType={this.state.type}
                            
                            onPressInfo={this.onPressInfo}
                            MoreinfoText={this.state.MoreinfoText}
                            MoreinfoIcon={this.state.MoreinfoIcon}
                            Moreinfo={this.state.Moreinfo}
                            onPressCancel={()=>{this.setState({modalVisibleActivities:false});this.inialiserValues()}}
                            onPressSave={()=>{this.getFunction();this.inialiserValues()}}
                            lastname={this.state.lastname}
                            idCompte={this.state.idCompte}
                            idContact={this.state.idContact}
                            selectVisibilite={this.state.visibilite}
                        />
                        <ModalSelectItemPriority
                            modalVisibleItemTickets={this.state.modalVisibleItemPriority}
                            onPressOption={this.onPressOptionPriority}
                            priorite={'Select Priorité'}
                            onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleActivities: true })}
                        />
                        <ModalSelectStatutActivitie
                        modalVisibleStatutActivitie={this.state.modalVisibleStatutActivitie}
                        onPressOption={this.onPressOptionStatut}
                        select={'Select Statut'}
                        onPressClose={() => this.setState({ modalVisibleStatutActivitie: false, modalVisibleActivities: true })}
                        />
                         <ModalVisibilite
                        modalVisibleVisibilite={this.state.modalVisibleVisibilite}
                        onPressOption={this.onPressOptionVisibilite}
                        select={'Select Visibilité'}
                        onPressClose={() => this.setState({ modalVisibleVisibilite: false, modalVisibleActivities: true })}
                        />
                        <ModalTypeActivities
                        modalVisibleTypeActivities={this.state.modalVisibleItemType}
                        onPressOption={this.onPressOptionType}
                        select={'Select Type'}
                        onPressClose={() => this.setState({ modalVisibleItemType: false, modalVisibleActivities: true })}
                        />
                       
                        <ModalSelectContacts
                        modalVisibleContacts={this.state.modalVisibleItemContacts}
                        
                        renderItem={this.renderItemContacts}
                        onPressClose={()=>{this.setState({modalVisibleActivities:true,modalVisibleItemContacts:false})}}
                        activitie={'yes'}
                        idCompte={this.state.idCompte}
                        />
                        <ModalSelectSociete
                        modalVisibleCompte={this.state.modalVisibleCompte}
                        
                        renderItem={this.renderItemComptes}
                        onPressClose={()=>{this.setState({modalVisibleCompte:false,modalVisibleActivities:true})}}
                        />     
        </View>)
    }

    else if (this.state.TabVisible == 'BGAffaires') {
      return (
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 9 }}>

            <FlatList
              data={this.state.dataAffaires}
              renderItem={this.renderElementAffaire}
              extraData={this.state.dataAffaires.length}
              keyExtractor={item => item.id}
            />


          </View>
          <View style={{ flex: 1 }}>
            <Provider>
              <Portal>
                <FAB.Group
                  open={this.state.open}
                  icon={'plus'}
                  color='white'
                  fabStyle={{ backgroundColor: '#53aefe' }}
                  actions={[
                    { icon: 'plus', onPress: () => console.log('Pressed add') },
                  ]}
                  onStateChange={() => { this.setState({ modalVisibleAffaires: true }) }}
                  onPress={() => { this.setState({ modalVisibleAffaires: true }) }}
                />
              </Portal>
            </Provider>
            <ModalAffaires
              AffaireModal={this.state.modalVisibleAffaires}
              onPressInfo={this.onPressInfo}
              MoreinfoText={this.state.MoreinfoText}
              MoreinfoIcon={this.state.MoreinfoIcon}
              Moreinfo={this.state.Moreinfo}
              onPressCancel={()=>{this.setVisibleAffaires();this.inialiserValues()}}
              onPressSave={()=>{this.getFunction();this.inialiserValues()}}
              
              onPressCompte={() => this.setState({ modalVisibleCompte: true, modalVisibleAffaires: false })}
              accountname={this.state.accountname}
              lastname={this.state.lastname}
              type={this.state.type}
              idCompte={this.state.idCompte}
              idContact={this.state.idContact}
              onPressPhase={() => this.setState({ modalVisibleItem: true, modalVisibleAffaires: false })}
              phase={this.state.phase}
              onPressContact={() => this.setState({ modalVisibleItemContacts: true, modalVisibleAffaires: false })}
              onPressType={() => this.setState({ modalVisibleType: true, modalVisibleAffaires: false })}

            />
            <ModalSelectSociete
              modalVisibleCompte={this.state.modalVisibleCompte}
              
              renderItem={this.renderItemComptes}
              onPressClose={() => this.setState({ modalVisibleCompte: true, modalVisibleAffaires: false })}
            />
            <ModalSelectContacts
              modalVisibleContacts={this.state.modalVisibleItemContacts}
              
              renderItem={this.renderItemContacts}
              onPressClose={() => this.setState({ modalVisibleItemContacts: true, modalVisibleAffaires: false })}
            />
            <ModalSelectItem
              modalVisibleItem={this.state.modalVisibleItem}
              onPressOption={this.onPressOption}
              select={'Select Item'}
              onPressClose={() => this.setState({ modalVisibleItem: false, modalVisibleAffaires: true })}

            />
            <ModalSelectType
              modalVisibleItem={this.state.modalVisibleType}
              onPressOption={this.onPressOptionType}
              onPressClose={() => this.setState({ modalVisibleType: false, modalVisibleAffaires: true })}
              select={'Select Item'}

            />

          </View>
        </View>
      )
    }

  
    else if (this.state.TabVisible == 'BGProjects') {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <FlatList
            data={this.state.dataProjects}
            renderItem={this.renderElementProjects}
            extraData={this.state.dataProjects.length}
            keyExtractor={item => item.id} />
          <FAB
            style={styles.fab}
            icon="plus"
            color='white'
            onStateChange={() => { this.setState({ modalVisibleProjects: true }) }}
            onPress={() => { this.setState({ modalVisibleProjects: true }) }} />
          <ModalProjects
            modalVisibleProjects={this.state.modalVisibleProjects}
            
            onPressSave={()=>{this.getFunction();this.inialiserValues()}}
            onPressCancel={()=>{this.setVisibleProjects();this.inialiserValues()}}
            accountname={this.state.accountname}
            idCompte={this.state.idCompte}
            lastname={this.state.lastname}
            type={this.state.type}
            statut={this.state.statutProject}
            priority={this.state.priority}
            onPressCompte={() => { this.setState({ modalVisibleCompte: true, modalVisibleProjects: false });  }}
            onPressType={() => this.setState({ modalVisibleItemType: true, modalVisibleProjects: false })}
            onPressStatut={this.onPressStatut}
            onPressPriority={() => this.setState({ modalVisibleItemPriority: true, modalVisibleProjects: false })}
          />
          <ModalSelectItemPriority
            modalVisibleItemTickets={this.state.modalVisibleItemPriority}
            onPressOption={this.onPressOptionPriority}
            select={'Select Priorité'}
            onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleProjects: true })}
          />
          <ModalSelectItemType
            modalVisibleItem={this.state.modalVisibleItemType}
            onPressOption={this.onPressOptionType}
            select={'Select Type'}
            onPressClose={() => this.setState({ modalVisibleItemType: false, modalVisibleProjects: true })}
          />
          <ModalSelectItemStatut
            modalVisibleItemTickets={this.state.modalVisibleItemStatut}
            onPressOption={this.onPressOptionStatut}
            select={'Select Statut'}
            onPressClose={() => this.setState({ modalVisibleItemStatut: false, modalVisibleProjects: true })}
          />
          <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={() => this.setState({ modalVisibleCompte: true, modalVisibleProjects: false })}
          />
        </View>
      )
    }
    else if (this.state.TabVisible == 'BGServ') {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <FlatList
            data={this.state.dataContrats}
            renderItem={this.renderElementServices}
            extraData={this.state.dataContrats.length}
            keyExtractor={item => item.id} />
          <FAB
            style={styles.fab}
            icon="plus"
            color='white'
            onPress={() => this.setState({ modalVisibleContrat: true })} />
          <ModalAddContrat
            modalVisibleContrat={this.state.modalVisibleContrat}
            accountname={this.state.accountname}
            idCompte={this.state.idCompte}
            lastname={this.state.lastname}
            idContact={this.state.idContact}
            unite={this.state.unite}
            type={this.state.type}
            statut={this.state.statutContrat}
            priority={this.state.priority}
            onPressUnite={() => this.setState({ modalVisibleUnite: true, modalVisibleContrat: false })}
            onPressInfo={this.onPressInfo}
            MoreinfoText={this.state.MoreinfoText}
            MoreinfoIcon={this.state.MoreinfoIcon}
            Moreinfo={this.state.Moreinfo}
            onPressSave={()=>{this.getFunction();this.inialiserValues()}}
            
            onPressCancel={()=>{this.onPressCancelContrat();this.inialiserValues()}}
            onPressType={() => this.setState({ modalVisibleType: true, modalVisibleContrat: false })}
            onPressUnite={() => this.setState({ modalVisibleUnite: true, modalVisibleContrat: false })}
            onPressStatut={this.onPressStatut}
            onPressPriority={() => this.setState({ modalVisibleItemPriority: true, modalVisibleContrat: false })}
            onPressCompte={() => { this.setState({ modalVisibleCompte: true, modalVisibleContrat: false });  }}
            onPressContact={() => this.setState({ modalVisibleItemContacts: true, modalVisibleContrat: false })}
          />
          <ModalSelectSociete
            modalVisibleCompte={this.state.modalVisibleCompte}
            
            renderItem={this.renderItemComptes}
            onPressClose={()=>{this.setState({modalVisibleCompte:false,modalVisibleContrat:true})}}
          />
          <ModalSelectContacts
            modalVisibleContacts={this.state.modalVisibleItemContacts}
            
            renderItem={this.renderItemContacts}
            onPressClose={()=>{this.setState({modalVisibleItemContacts:false,modalVisibleContrat:true})}}
          />
          <ModalSelectUnite
            modalVisibleUnite={this.state.modalVisibleUnite}
            onPressOption={this.onPressOptionUnite}
            onPressClose={() => this.setState({ modalVisibleUnite: false, modalVisibleContrat: true })}
            select={'Select Item'}
          />
          <ModalSelectItemPriority
            modalVisibleItemTickets={this.state.modalVisibleItemPriority}
            onPressOption={this.onPressOptionPriority}
            select={'Select Priorité'}
            onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleContrat: true })}

          />
          <ModalSelectItemType
            modalVisibleItem={this.state.modalVisibleItemType}
            onPressOption={this.onPressOptionType}
            select={'Select Type'}
            onPressClose={() => this.setState({ modalVisibleItemType: false, modalVisibleContrat: true })}
          />
          <ModalSelectItemStatut
            modalVisibleItemTickets={this.state.modalVisibleItemStatut}
            onPressOption={this.onPressOptionStatut}
            select={'Select Statut'}
            onPressClose={() => this.setState({ modalVisibleItemStatut: false, modalVisibleContrat: true })}

          />

        </View>

      )
    }
 
  }
  setDate = () => {
    this.setState({ isDate: true })
  }



  setTime = (date) => {
    this.setState({ time: date, istime: false })
  }
  setDateFunc = (date) => {
    this.setState({ date: date, isDate: false })
  }
  onPressInfoFunc = () => {
    if (this.state.TaskMoreinfo) {
      this.setState({
        TaskMoreinfo: false,
        TaskMoreinfoIcon: "chevron-down",
        TaskMoreinfoText: 'Plus'
      })
    } else {
      this.setState({
        TaskMoreinfo: true,
        TaskMoreinfoIcon: "chevron-up",
        TaskMoreinfoText: 'Moins'
      })
    }
  }


  renderItemComptes = ({ item, x }) => (

    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => { this.setState({ modalVisibleCompte: false, modalVisibleTickets: true, modalVisibleActivities:true,modalVisibleProjects: true,modalVisibleButton: true, modalVisibleAffaires: true, idCompte: item.id, accountname: item.accountname, chosen: item.accountname, value: item.id,valeur:item.accountname,valeur2:item.id }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.accountname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.accountname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );


  renderItemContacts = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {this.setState({ modalVisibleItemContacts: false, modalVisibleContacts: true, modalVisibleActivities:true,modalVisibleContrat: true, modalVisibleAffaires: true, modalVisibleTickets: true, idContact: item.id, lastname: item.lastname }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.lastname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.lastname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
  onPressOptionVisibilite = (val) => { this.setState({ modalVisibleActivities: true,  modalVisibleVisibilite: false, visibilite: val }); }

  onPressOption = (x) => { this.setState({ modalVisibleAffaires: true, modalVisibleItem: false, phase: x }); }
  onPressOptionType = (x) => { this.setState({ modalVisibleAffaires: true, modalVisibleProjects: true,modalVisibleContrat: true,modalVisibleActivities:true, modalVisibleItemType: false, type: x }); }
  onPressOptionPriority = (val) => { this.setState({ modalVisibleTickets: true,modalVisibleProjects: true,modalVisibleActivities: true, modalVisibleContrat: true, modalVisibleItemPriority: false, priority: val  }); }
  onPressOptionStatut = (val) => { this.setState({ modalVisibleTickets: true, modalVisibleStatutActivitie:false,modalVisibleContrat: true,modalVisibleProjects: true,modalVisibleActivities: true, modalVisibleItemStatut: false, statutTickets: val,statutActivitie: val,statutContrat:val,statutProject:val }); }
  onPressOptionEngagement = (val) => { this.setState({ modalVisibleTickets: true, modalVisibleEngagement: false, engagement: val }); }
  onPressOptionCategorie = (val) => { this.setState({ modalVisibleTickets: true, modalVisibleCategorie: false, categorie: val }); }
  onPressOptionUnite = (x) => { this.setState({ modalVisibleContrat: true, modalVisibleUnite: false, unite: x, }); }
  onPressCancelCompte = () => this.setState({ modalVisibleCompte: false, modalVisibleButton: true })
  onPressCancelContact = () => this.setState({ modalVisibleItemContacts: false, modalVisibleButton: true,  })
  onPressOptionSecteur = (val) => { this.setState({ modalVisibleSecteur: false, modalVisibleButton: true, chosen: val, value: val,valeur:val,valeur2:val }); }
  onPressOptionTypeUpdate = (x) => { this.setState({ modalVisibleTypeUpdate: false, modalVisibleButton: true, chosen: x, value: x,valeur:x,valeur2:x }); }



  render() {
    return (

      <View style={{ flex: 1, flexDirection: 'column' }}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.loadingShow}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
          <Header
            placement="right"
            leftComponent={
              <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'stretch' }}>
                <Button icon={<Icon name="chevron-left" size={20} color='white' type='material-community' />} onPress={()=>this.props.navigation.goBack()}/>
                <Text style={{ fontSize: 22, color: 'white', fontWeight: '500', marginTop: 5 }}>Comptes: {this.props.route.params.accountname}</Text>
              </View>}

          />
        </View>

        <View style={{ flex: 1.2 }}>
          {/* <Text>scrollTabNavigation</Text> */}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.scroll}>

            <TabItem title='Résumé' scale={this.state.SRésumé} BackGround={this.state.BGRésumé} onPress={() => this.changecolor('Résumé')} />
            <TabItem title='Details' scale={this.state.SDetails} BackGround={this.state.BGDetails} onPress={() => this.changecolor('Details')} />
            <TabItem title={'Activities (' + this.state.dataActivities.length + ')'} scale={this.state.SActivitie} BackGround={this.state.BGActivitie} onPress={() => { this.changecolor('Activitie') }} />
            {(this.state.Contacts === true) && <TabItem title={'Contacts (' + this.state.dataContacts.length + ')'} scale={this.state.SContacts} BackGround={this.state.BGContacts} onPress={() => this.changecolor('Contacts')} />}
            {(this.state.HelpDesk === true) && <TabItem title={'Tickets (' + this.state.dataTickets.length + ')'} scale={this.state.STickets} BackGround={this.state.BGTickets} onPress={() => this.changecolor('Tickets')} />}
            {(this.state.Potentials === true) && <TabItem title={'Affaires (' + this.state.dataAffaires.length + ')'} scale={this.state.SAffaires} BackGround={this.state.BGAffaires} onPress={() => this.changecolor('Affaires')} />}
            {(this.state.ServiceContracts === true) && <TabItem title={'Contrats (' + this.state.dataContrats.length + ')'} scale={this.state.SServ} BackGround={this.state.BGServ} onPress={() => { this.changecolor('Serv') }} />}

            {(this.state.Project === true) && <TabItem title={'Projets (' + this.state.dataProjects.length + ')'} scale={this.state.SProjects} BackGround={this.state.BGProjects} onPress={() => this.changecolor('Projects')} />}
          </ScrollView >

        </View>
        <SafeAreaView style={{ flex: 7, flexDirection: 'column' }}>
          {this.TabNavigation()}
        </SafeAreaView>
        
       
        <Overlay isVisible={this.state.modalVisibleDelete} >
          <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <Text>Voulez-vous supprimer cet objet?</Text>
          </View>
          <View style={{ flexDirection: 'row-reverse' }}>
            <Button title="Oui" titleStyle={{ color: 'black' }} buttonStyle={{ backgroundColor: "white" }} onPress={() => {
              this.setState({ loadingShow: true });
              fetch('https://portail.crm4you.ma/webservice.php', {
                method: 'POST',
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                body: 'operation=delete&sessionName=' + this.state.dataFet + '&id=' + this.state.id
              }); this.getFunction(); this.setState({ modalVisibleDelete: false })
            }}
            />
            <Button title="Non" titleStyle={{ color: 'black' }} buttonStyle={{ backgroundColor: "white" }} onPress={() => this.setState({ modalVisibleDelete: false })} />
          </View>
        </Overlay>




      </View>

    )

  }
}




