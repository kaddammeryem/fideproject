import React, { Component } from 'react';
import { SafeAreaView, Image, TouchableOpacity, ScrollView, Text, View, Modal, FlatList } from 'react-native';
import { FAB, Portal, Provider,} from 'react-native-paper';//component library
import Info from '../ItemsRender/info';
import { Header, Button, Icon,Overlay } from 'react-native-elements';
import * as DocumentPicker from 'expo-document-picker';
import styles from '../styles'
import ItemAffaire from '../ItemsRender/affaireitem'
import ModalSelectSociete from '../Modals/ModalSelectSociete';
import TabItem from '../ItemsRender/TabItem';
import ModalSelectContacts from '../Modals/ModalSelectContact';
import ModalButton from '../Modals/ModalsUpdate/ModalButton';
import ModalDate from '../Modals/ModalsUpdate/ModalDate';
import ModalInput from '../Modals/ModalsUpdate/Modalnput';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalStatutProspect from '../Modals/ModalStatutProspect';
import ModalSourceProspect from '../Modals/ModalSourceProspect';
import ModalSelectNote from '../Modals/ModalSelectNote';
import ModalSelectSecteur from '../Modals/ModalSelectSecteur';
import ItemAct from '../ItemsRender/itemActivities';
import ModalActivities from '../Modals/ModalActivitie';
import ModalSelectItemPriority from '../Modals/ModalSelectItemTickets';
import ModalSelectStatutActivitie from '../Modals/ModalSelectStatutActvitie';
import ModalTypeActivities from '../Modals/ModalTypeActivities';



export default class DetailsPropspects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingShow: false,
      Contacts: false,
      Documents: false,
      link: [],
      dataFet: '',
      valeur:'',
      valeur2:'',
      //updates fields 
      chosen: 'Select Item',
      value: '',
      title: '',
      key: '',
      // updates modals
      modalVisibleInput: false,
      modalVisibleDate: false,
      modalVisibleButton: false,
      //create modals
      modalVisibleProjects: false,
      modalVisibleContacts: false,
      modalVisibleCompte: false,
      modalVisibleItemContacts: false,
      modalVisibleContacts: false,
      modalVisibleDocuments: false,
      modalVisibleDelete: false,
      modalVisibleServices: false,
      modalVisiblePhase: false,
      modalVisibleType: false,
      modalVisibleNote:false,
      modalVisibleStatut:false,
      modalVisibleSource:false,
      modalVisibleSecteur:false,
      modalVisibleStatutActivitie:false,
      modalVisibleActivities:false,
      modalVisibleCategorie: false,
      modalVisibleUnite: false,
      modalVisibleCompte: false,
      modalVisibleItemPriority: false,
      modalVisibleItemStatut: false,
      modalVisibleItemType:false,

      //data
      dataDocuments: [],
      dataComptes: [],
      dataActivities: [],
      modalVisibleActivities:false,
      lastname: 'Select Compte',
      statut: 'Plannifiée',
      idContact: '',
      idCompte: this.props.route.params.idCompte,
      idContact: '',
      Moreinfo: false,
      MoreinfoIcon: "chevron-down",
      MoreinfoText: 'Plus',
      idToLastname: '',
      idToAccountname: '',
      priority: 'Normale',
      unite: 'Jours',
      categorie: 'Petit problème',
      modalVisibleEngagement: false,
      engagement: 'Mineur',
      accountname: this.props.route.params.accountname,
      lastname: 'Select Contact',
      type: ' Rendez-vous',
      phase: 'Proposition',
      //tabBG
      BGRésumé: '#9ddfd3',
      BGDetails: 'white',
      BGMises: 'white',
      BGContacts: 'white',
      BGActivitie: 'white',
      BGDoc: 'white',
      BGComnt: 'white',
      //scale
      SRésumé: 0.8,
      SDetails: 0.6,
      SMises: 0.6,
      SContacts: 0.6,
      SActivitie: 0.6,
      SDoc: 0.6,
      SComnt: 0.6,
      //tabVisible
      TabVisible: 'BGRésumé',
      //------------------
      Detail: true,
      BlocSys: true,
      InfoP: true,
      Adress: true,
      Desc: true,
      Portal: true,
      //
      date: new Date(),
      time: new Date(),
      isDate: false,
      istime: false,
    }

  }

  initaliserStyle = () => {
    this.setState({
      BGRésumé: 'white',
      BGDetails: 'white',
      BGMises: 'white',
      BGContacts: 'white',
      BGActivitie: 'white',
      BGDoc: 'white',
      BGComnt: 'white',
      //scale
      SRésumé: 0.6,
      SDetails: 0.6,
      SMises: 0.6,
      SContacts: 0.6,
      SActivitie: 0.6,
      SDoc: 0.6,
      SComnt: 0.6,

    })


  }
  onPressOptionTypeActivitie = (x) => { this.setState({ modalVisibleActivities:true, modalVisibleItemType: false, type: x }); }
  onPressOptionPriorityActivitie = (val) => { this.setState({ modalVisibleActivities: true,  modalVisibleItemPriority: false, priority: val  }); }
  onPressOptionStatutActivitie= (val) => { this.setState({  modalVisibleStatutActivitie:false,modalVisibleActivities: true,  statut: val,statutContrat:val }); }

  renderElementMiseaj = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => { console.log("pressed: " + item.title) }}
      >

        <View style={styles.block}>

          <View style={styles.icon_container}>
            <Icon style={styles.icon}
              name={item.icon}
              color='#225fbf'
              type='font-awesome' />
          </View>
          <View style={styles.details}>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.description} >{item.description}</Text>
          </View>
          <View style={styles.date_container}>
            <Text style={styles.date}>{item.date} ago</Text>
          </View>
        </View>

      </TouchableOpacity>
    );
  }
  renderElementAffaire = ({ item }) => {
    return (
      <View>
        <ItemAffaire Name={item.subject} />
      </View>);
  }

  changecolor = (a) => {
    this.initaliserStyle();
    this.setState({
      [`BG${a}`]: '#9ddfd3',
      [`S${a}`]: 0.8,
      TabVisible: `BG${a}`
    });
  }
  renderElementActivities= ({ item, index }) => {
    return (
      <View>

          <ItemAct Name={item.subject} status={item.eventstatus} date={item.date_start+' - '+item.time_start}
                onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} 
                onPress={() => this.props.navigation.navigate('DetailsActivities', {title:item.subject, id: item.id, data: this.state.dataActivities, index: index, result: session })} /> 
          
       
      </View>)

  }
  setmodalVisibleContacts = (visible) => {
    this.setState({ modalVisibleContacts: visible });
  }
  setmodalVisibleDocuments = (visible) => {
    this.setState({ modalVisibleDocuments: visible });
  }

  setModalVisible1 = (visible) => {
    this.setState({ modalVisible1: visible });
  }

  ChangeNomSociete = (soc) => {
    this.setState({ nomSoc: soc })
  }
  getFunction = async () => {
    this.setState({ loadingShow: true });
    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    try {
      await AsyncStorage.multiGet(['SESSION', 'CRM_LINK', 'TYPES'], async (err, res) => {
        types = res[2][1].split(',');
      
        if (types.includes('Contacts')) this.setState({ Contacts: true });
        if (types.includes('Documents')) this.setState({ Documents: true });
        var dataFet = res[0][1];
        this.setState({ dataFet: dataFet });
        var link = res[1][1];
        this.setState({ link: link });
        this.setState({ loadingShow: false });
        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select * from Events where parent_id='+dataRoute.id+';')
              .then((response) => response.json())
              .then((json)=>{
                this.setState({dataActivities:json.result})
               
              
              })
              .catch((error)=>
                console.log(error))  
            })
          
      
    } catch (e) {
      console.log('error: ', e);
    }

  }


  componentDidMount() {
    this.getFunction();
  }
  renderItemContacts = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({ modalVisibleItemContacts: false, modalVisibleContacts: true,modalVisibleActivities:true, modalVisibleButton: true, idContact: item.id, lastname: item.lastname, chosen: item.lastname, value: item.id,valeur:item.lastname,valeur2:item.id }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.lastname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.lastname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
  renderItemComptes = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({ modalVisibleCompte: false, modalVisibleContacts: true, modalVisibleButton: true, modalVisibleActivities:true,idCompte: item.id, accountname: item.accountname, chosen: item.accountname, value: item.id,valeur:item.accountname,valeur2:item.id }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.accountname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.accountname}
          </Text>
        </View>
      </TouchableOpacity>

    </View>
  );
  onPressInfo = () => {
    if (this.state.Moreinfo) {
      this.setState({
        Moreinfo: false,
        MoreinfoIcon: "chevron-down",
        MoreinfoText: 'Plus'
      })
    } else {
      this.setState({
        Moreinfo: true,
        MoreinfoIcon: "chevron-up",
        MoreinfoText: 'Moins'
      })
    }
  }
 

  onPressCancelContact = () => this.setState({ modalVisibleItemContacts: false, modalVisibleButton: true })
  onPressCancelCompte = () => this.setState({ modalVisibleCompte: false,  modalVisibleButton: true, })

  update = async (value) => {

    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    dataRoute[this.state.key] = value;
  
    this.setState({ loadingShow: true });
    await fetch('https://portail.crm4you.ma/webservice.php', {
      method: 'POST',
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: 'operation=update&sessionName=' + this.state.dataFet + '&element=' + JSON.stringify(dataRoute)
    })
      .then((responses) => responses.json())
      .then((json) => { this.setState({ loadingShow: false,chosen: 'Select Item',valeur:'' ,modalVisibleInput:false});  })
      .catch((error) => console.error(error));
   


  }

  TabNavigation = () => {
    var dataRoute = this.props.route.params.data[this.props.route.params.index]


    if (this.state.TabVisible == 'BGRésumé') {
      return (
        <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 1, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Champ De Clé
                </Text>
            </View>
          </View>
          <View style={styles.content_champ_cle}>
            <Info title={"Nom du Propect"} modifiable={true} borderBottom={0.6} content={dataRoute.lastname}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.lastname,key: 'lastname', title: "Nom du Propect" }) }} />
            <Info title={'Prénom du Propect'} modifiable={true} borderBottom={0.6} content={dataRoute.firstname}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.firstname,key: 'firstname', title: 'Prénom du Propect' }) }} />
            <Info title={'Téléphone Principal'} modifiable={true} borderBottom={0.6} content={dataRoute.phone}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.phone,key: 'phone', title: 'Téléphone Principal' }) }} />
             <Info title={'E-Mail Principale'} modifiable={true} borderBottom={0.6} content={dataRoute.email}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.email,key: 'email', title: 'E-Mail Principale' }) }} />
            <Info title={'Ville'} modifiable={true} borderBottom={0.6} content={dataRoute.city}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.city,key: 'city', title: 'Ville' }) }} />
             <Info title={'Société'} modifiable={true} borderBottom={0.6} content={dataRoute.company}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.company,key: 'company', title: 'Société' }) }} />
            

            <View style={{ width: '100%', height: 70, backgroundColor: 'white', marginTop: 10 }}>
              <TouchableOpacity activeOpacity={0.7} style={styles.buttonFullDetails} onPress={() => { this.changecolor('Details') }} >
                <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: 'white' }}>Full Details</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ height: 50 }}></View>
        </ScrollView>
      )
    }
    else if (this.state.TabVisible == 'BGDetails') {
      return (
        <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Détail Du Compte
                            </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Detail: !this.state.Detail }) }}>
              {!this.state.Detail ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.Detail ? (
            <View style={styles.content_champ_cle}>
               <Info title={"Nom du Propect"} modifiable={true} borderBottom={0.6} content={dataRoute.lastname}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.lastname,key: 'lastname', title: "Nom du Propect" }) }} />
            <Info title={'Prénom du Propect'} modifiable={true} borderBottom={0.6} content={dataRoute.firstname}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.firstname,key: 'firstname', title: 'Prénom du Propect' }) }} />
            <Info title={'Téléphone Principal'} modifiable={true} borderBottom={0.6} content={dataRoute.phone}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.phone,key: 'phone', title: 'Téléphone Principal' }) }} />
             <Info title={'E-Mail Principale'} modifiable={true} borderBottom={0.6} content={dataRoute.email}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.email,key: 'email', title: 'E-Mail Principale' }) }} />
            <Info title={'Ville'} modifiable={true} borderBottom={0.6} content={dataRoute.city}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.city,key: 'city', title: 'Ville' }) }} />
             <Info title={'Société'} modifiable={true} borderBottom={0.6} content={dataRoute.company}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.company,valeur2:dataRoute.company,key: 'company', title: 'Société' }) }} />
               <Info title={'Titre'} modifiable={true} borderBottom={0.6} content={dataRoute.designation}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.designation,key: 'designation', title: 'Titre' }) }} />
              <Info title={'Secteur'} modifiable={true} borderBottom={0.6} content={dataRoute.industry}
              onPress={() => { this.setState({ modalVisibleButton: true, valeur:dataRoute.industry,valeur2:dataRoute.industry,key: 'industry', title: 'Secteur' }) }} />
               <Info title={'Note'} modifiable={true} borderBottom={0.6} content={dataRoute.rating}
              onPress={() => { this.setState({ modalVisibleButton: true, valeur:dataRoute.rating,valeur2:dataRoute.rating,key: 'rating', title: 'Note' }) }} />
              <Info title={'Statut'} modifiable={true} borderBottom={0.6} content={dataRoute.leadstatus}
              onPress={() => { this.setState({ modalVisibleButton: true, valeur:dataRoute.leadstatus,valeur2:dataRoute.leadstatus,key: 'leadstatus', title: 'Statut' }) }} />
              <Info title={'Source du prospect'} modifiable={true} borderBottom={0.6} content={dataRoute.leadsource}
              onPress={() => { this.setState({ modalVisibleButton: true, valeur:dataRoute.leadsource,valeur2:dataRoute.leadsource,key: 'leadsource', title: 'Source du prospect' }) }} />              
             <Info title={'Revenu Annuel'} modifiable={true} borderBottom={0.6} content={dataRoute.annualrevenue}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.annualrevenue,key: 'annualrevenue', title: 'Revenu Annuel' }) }} />
             <Info title={'Effectif'} modifiable={true} borderBottom={0.6} content={dataRoute.noofemployees}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.noofemployees,key: 'noofemployees', title: 'Effectif' }) }} />
             <Info title={'E-Mail Secondaire'} modifiable={true} borderBottom={0.6} content={dataRoute.secondaryemail}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.secondaryemail,key: 'secondaryemail', title: 'E-Mail Secondaire' }) }} />
               <Info title={'Mobile'} modifiable={true} borderBottom={0.6} content={dataRoute.mobile}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.mobile,key: 'mobile', title: 'Mobile' }) }} />
             <Info title={'Prospect N°'} modifiable={false} borderBottom={0.6} content={dataRoute.lead_no}/>
             <Info title={'Date de création'} modifiable={false} borderBottom={0.6} content={dataRoute.createdtime}/>
             <Info title={'Date de modification'} modifiable={false} borderBottom={0.6} content={dataRoute.modifiedtime}/>
             <Info title={'Siteweb'} modifiable={true} borderBottom={0.6} content={dataRoute.website}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.website,key: 'website', title: 'Siteweb' }) }} />
            </View>
          ) : null}
          <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Prospect'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false,value:'',chosen:'Select Item',valeur:'',valeur2:'' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Source du prospect') {
              this.setState({
                modalVisibleSource: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Secteur') {
              this.setState({
                modalVisibleSecteur: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Note') {
              this.setState({
                modalVisibleNote: true, modalVisibleButton: false
              })
            }

            if (this.state.title === 'Statut') {
              this.setState({
                modalVisibleStatut: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalSelectSecteur
          modalVisibleSecteur={this.state.modalVisibleSecteur}
          onPressOption={this.onPressOptionSecteur}
          onPressClose={() => this.setState({ modalVisibleSecteur: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
        <ModalStatutProspect
          modalVisibleItem={this.state.modalVisibleStatut}
          onPressOption={this.onPressOptionStatut}
          onPressClose={() => this.setState({ modalVisibleStatut: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
       <ModalSourceProspect
          modalVisibleItem={this.state.modalVisibleSource}
          onPressOption={this.onPressOptionSource}
          onPressClose={() => this.setState({ modalVisibleSource: false, modalVisibleButton: true })}
          select={'Select Item'}
        />
        <ModalSelectNote
        modalVisibleItem={this.state.modalVisibleNote}
        onPressOption={this.onPressOptionNote}
        onPressClose={() => this.setState({ modalVisibleNote: false, modalVisibleButton: true })}
        select={'Select Item'}
      />
        
         <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Détail de l'Adresse
                                </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Adress: !this.state.Adress }) }}>
              {!this.state.Adress ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>

          {this.state.Adress ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Rue'} modifiable={true} borderBottom={0.6} content={dataRoute.lane}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.lane, key: 'lane', title: 'Rue' }) }} />
              <Info title={'Code Postal'} modifiable={true} borderBottom={0.6} content={dataRoute.code}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.code,key: 'code', title: 'Code Postal' }) }} />
              <Info title={'Pays'} modifiable={true} borderBottom={0.6} content={dataRoute.country}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.country,key: 'country', title: 'Pays' }) }} />
              <Info title={'Boite Postale'} modifiable={true} borderBottom={0.6} content={dataRoute.pobox}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.pobox,key: 'pobox', title: 'Boite Postale' }) }} />
              <Info title={'Département'} modifiable={true} borderBottom={0.6} content={dataRoute.state}
                onPress={() => { this.setState({ modalVisibleInput: true,  valeur:dataRoute.state,key: 'state', title: 'Département' }) }} />
              
            </View>
          ) : null}
          <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Contact'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }) }}
          onPressSave={this.update}
        />
        <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Contact'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false ,value: '',chosen:'Select Item',valeur:'',valeur2:''})}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Nom de compte') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Superieur hiérarchique') {
              this.setState({
                modalVisibleItemContacts: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalDate
          modalVisibleDate={this.state.modalVisibleDate}
          title={this.state.title}
          edit={'Edit Contact'}
          valeur={this.state.valeur}
          onPressCancel={() => this.setState({ modalVisibleDate: false, value: '' })}
          onPressSave={this.update}

        />
        <ModalSelectContacts
          modalVisibleContacts={this.state.modalVisibleItemContacts}
          
          renderItem={this.renderItemContactsUpdate}
          onPressClose={() => this.setState({ modalVisibleItemContacts: false })}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptesUpdate}
          onPressClose={() => this.setState({ modalVisibleCompte: false })}
        />


          <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Information Sur la description
              </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Desc: !this.state.Desc }) }}>
              {!this.state.Desc ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.Desc ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Description'} modifiable={true} borderBottom={0.6} content={dataRoute.description}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'description', title: 'Description' }) }} />
            </View>
          ) : null}
          <View style={{ marginTop: 40 }}></View>


        </ScrollView>
      )
    }
    else if (this.state.TabVisible == 'BGActivitie') {
      return (
        <View style={{ flex: 1, flexDirection: 'column', padding: 20 }}>
          <FlatList
            data={this.state.dataActivities}
            renderItem={this.renderElementActivities}
            keyExtractor={item => item.id}
          />
          <View style={{ flex: 2 }}>
            <Provider>
              <Portal>
                <FAB.Group
                  open={this.state.open}
                  icon={'plus'}
                  color='white'
                  fabStyle={{ backgroundColor: '#53aefe' }}
                  actions={[
                    { icon: 'plus', onPress: () => console.log('Pressed add') },
                  ]}
                  onStateChange={() => { this.setState({modalVisibleActivities:true}) }}
                  onPress={() => {
                    this.setState({modalVisibleActivities:true})
                  }}
                />
              </Portal>
            </Provider>
          </View>
          <ModalActivities
                            modalVisibleActivities={this.state.modalVisibleActivities}
                            onPressPriority={() => this.setState({ modalVisibleItemPriority: true, modalVisibleActivities: false })}
                            onPressStatut={() => this.setState({ modalVisibleStatutActivitie: true, modalVisibleActivities: false })}
                            onPressItemContact={() => this.setState({ modalVisibleItemContacts: true, modalVisibleActivities: false })}
                            onPressCompte={() => this.setState({ modalVisibleCompte: true, modalVisibleActivities: false })}
                            onPressType={() => this.setState({ modalVisibleItemType: true, modalVisibleActivities: false })}
                            onPressCategorie={() => this.setState({ modalVisibleCategorie: true, modalVisibleActivities: false })}
                            onPressVisibilite={()=>this.setState({modalVisibleActivities:false,modalVisibleVisibilite:true})}
                            accountname={this.state.accountname}
                            selectPriority={this.state.priority}
                            selectStatut={this.state.statut}
                            selectType={this.state.type}                         
                            onPressInfo={this.onPressInfo}
                            MoreinfoText={this.state.MoreinfoText}
                            MoreinfoIcon={this.state.MoreinfoIcon}
                            Moreinfo={this.state.Moreinfo}
                            onPressCancel={()=>{this.setState({modalVisibleActivities:false});}}
                            onPressSave={()=>{this.getFunction();}}
                            lastname={this.state.lastname}
                            idCompte={this.state.idCompte}
                            idContact={this.state.idContact}
                            selectVisibilite={this.state.visibilite}
                        />
                        <ModalSelectItemPriority
                            modalVisibleItemTickets={this.state.modalVisibleItemPriority}
                            onPressOption={this.onPressOptionPriorityActivitie}
                            priorite={'Select Priorité'}
                            onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleActivities: true })}
                        />
                        <ModalSelectStatutActivitie
                        modalVisibleStatutActivitie={this.state.modalVisibleStatutActivitie}
                        onPressOption={this.onPressOptionStatutActivitie}
                        select={'Select Statut'}
                        onPressClose={() => this.setState({ modalVisibleStatutActivitie: false, modalVisibleActivities: true })}
                        />
                        
                        <ModalTypeActivities
                        modalVisibleTypeActivities={this.state.modalVisibleItemType}
                        onPressOption={this.onPressOptionTypeActivitie}
                        select={'Select Type'}
                        onPressClose={() => this.setState({ modalVisibleItemType: false, modalVisibleActivities: true })}
                        />
                       
                        <ModalSelectContacts
                        modalVisibleContacts={this.state.modalVisibleItemContacts}  
                        renderItem={this.renderItemContacts}
                        onPressClose={()=>{this.setState({modalVisibleActivities:true,modalVisibleItemContacts:false})}}
                        activitie={'yes'}
                        idCompte={this.state.idCompte}
                        />
                        <ModalSelectSociete
                        modalVisibleCompte={this.state.modalVisibleCompte}        
                        renderItem={this.renderItemComptes}
                        onPressClose={()=>{this.setState({modalVisibleCompte:false,modalVisibleActivities:true})}}
                        />   
         
        </View>)
    }

  }
  setDate = () => {
    this.setState({ isDate: true })
  }

  filter = (searchText) => {

    this.setState({
      searchText: searchText,
      filterSocieteDataSearch: this.state.societeData.filter(i =>
        i.socName.toUpperCase().includes(searchText.toUpperCase()))
    })

  }
  _pickDocument = async () => {
    let result = await DocumentPicker.getDocumentAsync({})
    this.setState({ cheminDoc: result.uri, nameFile: result.name })

    alert(result.uri);
   
  }
  choseEvent_or_task = () => {
    if (this.state.checked === 'task') {
      this.setState({
        TaskModal: true,
        taskevent: false,

      })
    } else {
      this.setState({
        EventModal: true,
        taskevent: false,
      })

    }
  }
  setTime = (date) => {
    this.setState({ time: date, istime: false })
  }
  setDateFunc = (date) => {
    this.setState({ date: date, isDate: false })
  }
  onPressInfoFunc = () => {
    if (this.state.TaskMoreinfo) {
      this.setState({
        TaskMoreinfo: false,
        TaskMoreinfoIcon: "chevron-down",
        TaskMoreinfoText: 'Plus'
      })
    } else {
      this.setState({
        TaskMoreinfo: true,
        TaskMoreinfoIcon: "chevron-up",
        TaskMoreinfoText: 'Moins'
      })
    }
  }

  onPressOption = (x) => { this.setState({ modalVisibleButton: true, modalVisiblePhase: false, phase: x, chosen: x, value: x,valeur2:x,valeur:x }); }
  onPressOptionType = (x) => { this.setState({ modalVisibleButton: true, modalVisibleType: false, type: x, chosen: x, value: x,valeur2:x,valeur:x }); }
  onPressOptionStatut = (x) => { this.setState({ modalVisibleButton: true, modalVisibleStatut: false, statut: x, chosen: x, value: x,valeur2:x,valeur:x }); }
  onPressOptionSource = (x) => { this.setState({ modalVisibleButton: true, modalVisibleSource: false, source: x, chosen: x, value: x,valeur2:x,valeur:x }); }
  onPressOptionNote = (x) => { this.setState({ modalVisibleButton: true, modalVisibleNote: false, note: x, chosen: x, value: x,valeur2:x,valeur:x }); }
  onPressOptionSecteur = (x) => { this.setState({ modalVisibleButton: true, modalVisibleSecteur: false, secteur: x, chosen: x, value: x,valeur2:x,valeur:x }); }

  render() {
    return (

      <View style={{ flex: 1, flexDirection: 'column' }}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.loadingShow}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
          <Header
            placement="right"
            leftComponent={
              <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'stretch' }}>
                <Button icon={<Icon name="chevron-left" size={20} color='white' type='material-community' />} onPress={() => this.props.navigation.goBack()} />
                <Text style={{ fontSize: 22, color: 'white', fontWeight: '500', marginTop: 5 }}>Prospects : {this.props.route.params.title}</Text>
              </View>}

          />
        </View>

        <View style={{ flex: 1.2 }}>
          {/* <Text>scrollTabNavigation</Text> */}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.scroll}>
            <TabItem title='Résumé' scale={this.state.SRésumé} BackGround={this.state.BGRésumé} onPress={() => this.changecolor('Résumé')} />
            <TabItem title='Details' scale={this.state.SDetails} BackGround={this.state.BGDetails} onPress={() => this.changecolor('Details')} />
            {(this.state.Contacts === true) && <TabItem title={'Activities ('+this.state.dataActivities.length+')'} scale={this.state.SActivitie} BackGround={this.state.BGActivitie} onPress={() => this.changecolor('Activitie')} />}
          </ScrollView >

        </View>
        <SafeAreaView style={{ flex: 7, flexDirection: 'column' }}>
          {this.TabNavigation()}
        </SafeAreaView>
        {/*Nom de l'organisation,Nom du contact,Type;Phase de vente*/}
        <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Prospect'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }); }}
          onPressSave={this.update}
        />
         <Overlay isVisible={this.state.modalVisibleDelete} >
          <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <Text>Voulez-vous supprimer cet objet?</Text>
          </View>
          <View style={{ flexDirection: 'row-reverse' }}>
            <Button title="Oui" titleStyle={{ color: 'black' }} buttonStyle={{ backgroundColor: "white" }} onPress={() => {
              this.setState({ loadingShow: true });
              fetch('https://portail.crm4you.ma/webservice.php', {
                method: 'POST',
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                body: 'operation=delete&sessionName=' + this.state.dataFet + '&id=' + this.state.id
              }); this.getFunction(); this.setState({ modalVisibleDelete: false })
            }}
            />
            <Button title="Non" titleStyle={{ color: 'black' }} buttonStyle={{ backgroundColor: "white" }} onPress={() => this.setState({ modalVisibleDelete: false })} />
          </View>
        </Overlay>
        
       

      </View>

    )

  }
}




