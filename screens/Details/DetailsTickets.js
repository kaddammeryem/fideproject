import React, { Component } from 'react';
import { SafeAreaView, Image, TouchableOpacity, ScrollView, Text, View, Modal, FlatList } from 'react-native';
import { FAB, Portal, Provider } from 'react-native-paper';//component library
//import Icon from 'react-native-vector-icons/FontAwesome';//icons library
import Info from '../ItemsRender/info';
import { Header, Button, Icon, Overlay } from 'react-native-elements';
import * as DocumentPicker from 'expo-document-picker';
import styles from '../styles'
import ItemAffaire from '../ItemsRender/affaireitem'
import Comments from '../ItemsRender/comment';
import TabItem from '../ItemsRender/TabItem';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalInput from '../Modals/ModalsUpdate/Modalnput';
import ModalButton from '../Modals/ModalsUpdate/ModalButton';
import ModalSelectSociete from '../Modals/ModalSelectSociete';
import ModalSelectContacts from '../Modals/ModalSelectContact';
import ModalDate from '../Modals/ModalsUpdate/ModalDate';
import ModalSelectItemPriority from '../Modals/ModalSelectItemTickets';
import ModalSelectItemStatut from '../Modals/ModalSelectItemStatut';
import ModalSelectEngagement from '../Modals/ModalSelectEngagement';
import ModalSelectCategorie from '../Modals/ModalSelectCategorie';

export default class DetailsTickets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Documents: false,
      ServiceContracts: false,
      Project: false,
      loadingShow: true,
      dataFet: [],
      link: [],
      valeur:'',
      valeur2:'',
      modalVisibleDate:false,
      modalVisibleProjects: false,
      dataProjects: [],
      dataDocuments: [],
      dataContrats: [],
      modalVisibleItemStatut:false,
      modalVisibleCategorie:false,
      modalVisibleContacts: false,
      modalVisibleItemPriority:false,
      modalVisibleEngagement:false,
      modalVisibleDocuments: false,
      modalVisibleDelete: false,
      modalVisibleServices: false,
      modalVisibleItemContacts:false,
      modalVisibleButton:false,
      modalVisibleInput:false,
      modalVisibleCompte:false,
      chosen:'Select Item',
      value:'',
      idToLastname: '',
      idToAccountname: '',
      categorie:'Select Catégorie',
      priorite:"Select Priorité",
      statut:'Select Statut',
      engagement:'Select Engagement',
      idCompte:'',
      idContact:'',
      lastname:'',
      accountname:'',
      //tabBG
      BGRésumé: '#9ddfd3',
      BGDetails: 'white',
      BGMises: 'white',
      BGActivitie: 'white',
      BGDoc: 'white',
      BGProjects: 'white',
      BGServ: 'white',
      BGComnt: 'white',
      //scale
      SRésumé: 0.8 ,
      SDetails: 0.6 ,
      SMises: 0.6 ,
      SActivitie: 0.6 ,
      SDoc: 0.6 ,
      SProjects: 0.6 ,
      SServ: 0.6 ,
      SComnt: 0.6 ,
      //tabVisible
      TabVisible: 'BGRésumé',
      //------------------
      Detail: true,
      BlocSys: true,
      InfoP: true,
      Adress: true,
      Desc: true,
      Portal: true,
      date: new Date(),
      time: new Date(),
      isDate: false,
      istime: false,
      userId:'',
    }

  }

  initaliserStyle = () => {
    this.setState({
      BGRésumé: 'white',
      BGDetails: 'white',
      BGMises: 'white',
      BGDoc: 'white',
      BGProjects: 'white',
      BGServ: 'white',
      BGComnt: 'white',
      //scale
      SRésumé: 0.8 ,
      SDetails: 0.6 ,
      SMises: 0.6 ,
      SDoc: 0.6 ,
      SProjects: 0.6 ,
      SServ: 0.6 ,
      SComnt: 0.6 ,

    })

  }

  renderElementServices = ({ item, index }) => {
    return (
      <View style={{ flex: 9 }}>
        <ItemAffaire Name={item.subject} onDelete={() => this.setState({ modalVisibleDelete: true })} onPress={() => this.props.navigation.navigate('DetailsContrats', { id: item.id, data: this.state.dataContrats, index: index, result: this.state.dataFet })} />
      </View>)

  }
  renderElementProjects = ({ item, index }) => {
    return (
      <View style={{ flex: 9 }}>
        <ItemAffaire Name={item.projectname} onDelete={() => this.setState({ modalVisibleDelete: true })} onPress={() => this.props.navigation.navigate('DetailsProjects', { id: item.id, data: this.state.dataProjects, index: index, result: this.state.dataFet })} />
      </View>)

  }

  renderElementMiseaj = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => { console.log("pressed: " + item.title) }}
      >

        <View style={styles.block}>

          <View style={styles.icon_container}>
            <Icon style={styles.icon}
              name={item.icon}
              color='#225fbf'
              type='font-awesome' />
          </View>
          <View style={styles.details}>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.description} >{item.description}</Text>
          </View>
          <View style={styles.date_container}>
            <Text style={styles.date}>{item.date} ago</Text>
          </View>
        </View>

      </TouchableOpacity>
    );
  }

  changecolor = (a) => {
    this.initaliserStyle();
    this.setState({
      [`BG${a}`]: '#9ddfd3',
      [`S${a}`]: 0.8,
      TabVisible: `BG${a}`
    });
  }

  upload = async () => {

    await AsyncStorage.getItem('USERID', (err, result)=>{ this.setState({userId: result})});
    var filename=[
      {"name":"screen.jpg",}
    ]
    this.setState({ loadingShow: true });
    var link = 'https://scontent.frba1-1.fna.fbcdn.net/v/t1.0-9/89787150_3585678251502759_827809608629223424_o.jpg?_nc_cat=111&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGTEs78cXv1dbkYC8gYB1QJ3C_TMiqcnobcL9MyKpyehrYJYL8nhF9vOxzlPbqIo0q3iXkMZlOc6THxcIg76dBk&_nc_ohc=LfeIGBGg4BQAX_u9Oaj&_nc_ht=scontent.frba1-1.fna&oh=690deb3f58acad097f532cf6a32d21b4&oe=60492708'
    var dataRoute = {
     "filelocationtype": 'I', "filestatus": "1",
     "filetype": "multipart/form-data", "fileversion": "", "folderid": "22x1", "assigned_user_id": this.state.id,
      "notes_title": "Image 6","filesize":"357648","filename":'crm_icon.png'
    }
    await fetch('https://portail.crm4you.ma/webservice.php', {
      method: 'POST',
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: 'operation=create&sessionName=' + this.state.dataFet +
        '&element=' + JSON.stringify(dataRoute) + '&elementType=Documents&filename=./crm_icon.png'
    })
      .then((responses) => responses.json())
      .then((json) => { this.setState({ loadingShow: false });console.log(json) })
      .catch((error) => console.error(error));

  }


  setmodalVisibleDocuments = (visible) => {
    this.setState({ modalVisibleDocuments: visible });
  }

  setModalVisible1 = (visible) => {
    this.setState({ modalVisible1: visible });
  }

  ChangeNomSociete = (soc) => {
    this.setState({ nomSoc: soc })
  }
  getFunction = async () => {
    this.setState({ loadingShow: true });
    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    console.log(dataRoute)
    var id = this.props.route.params.id;
    try {
      await AsyncStorage.multiGet ( ['SESSION', 'CRM_LINK', 'TYPES'], async (err, res) => {
        types = res[2][1].split(',');
        console.log(types);
        if (types.includes('Documents')) this.setState({ Documents: true });
        if (types.includes('ServiceContracts')) this.setState({ ServiceContracts: true });
        if (types.includes('Project')) this.setState({ Project: true });

        var dataFet = res[0][1];
        this.setState({ dataFet: dataFet });
        var link = res[1][1];
        this.setState({ link: link });
        if(dataRoute.parent_id!=''){
                await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Accounts where id='+dataRoute.parent_id+';')
                .then((response) => response.json())
                .then(async (json) => {
                  
                  if (json.success == true && json.result != '') 
                      { this.setState({  idToAccountname: json.result[0].accountname}) }
  
      })}
      if(dataRoute.contact_id!=''){
      await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Contacts where id=' + dataRoute.contact_id + ';')
                    .then((response) => response.json())
                    .then((json) => {
                      console.log('entering fetching contact');
                      
                      if (json.success == true && json.result != '') 
                      { this.setState({  idToLastname: json.result[0].lastname })  }
                    })
                    .catch((error) =>
                      console.log(error))}

                 
    
       })} catch (e) {
      console.log(e);
    }
    this.setState({ loadingShow: false });
  }
/* await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Accounts where id='+dataRoute.parent_id+';')
                .then((response) => response.json())
                .then(async (json) => {
                  
                  if (json.success == true && json.result != '') 
                      { this.setState({  idToAccountname: json.result[0].accountname}) 
                  await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Contacts where id=' + dataRoute.contact_id + ';')
                    .then((response) => response.json())
                    .then((json) => {
                      console.log('entering fetching contact');
                      
                      if (json.success == true && json.result != '') 
                      { this.setState({  idToLastname: json.result[0].lastname })  }
                    })
                    .catch((error) =>
                      console.log(error))
                 };this.setState({ loadingShow: false });
                })
                .catch((error) =>
                  console.log(error))
         })*/


  componentDidMount() {
    this.getFunction();
  }

  TabNavigation = () => {
    var dataRoute = this.props.route.params.data[this.props.route.params.index]

    if (this.state.TabVisible == 'BGRésumé') {
      return (
        <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 1, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Champ De Clé
                </Text>
            </View>
          </View>
          <View style={styles.content_champ_cle}>
            <Info title={'Titre'} modifiable={true} borderBottom={0.6} content={dataRoute.ticket_title} 
            onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.ticket_title, key:'ticket_title',title:'Titre'}) }}/>
            <Info title={'Relatif à'} modifiable={true} borderBottom={0.6} content={this.state.idToAccountname} 
            onPress={() => { this.setState({modalVisibleButton : true,valeur:this.state.idToAccountname,valeur2:dataRoute.parent_id,key:'parent_id',title:'Relatif à'}) }}/> 
            <Info title={'Nom du contact'} modifiable={true} borderBottom={0.6} content={this.state.idToLastname} 
            onPress={() => { this.setState({modalVisibleButton : true,valeur:this.state.idToLastname,valeur2:dataRoute.contact_id,key:'contact_id',title:'Nom du contact'}) }}/>  
            <Info title={'Statut'} modifiable={true} borderBottom={0.6} content={dataRoute.ticketstatus} 
            onPress={() => { this.setState({modalVisibleButton : true,valeur2:dataRoute.ticketstatus,valeur:dataRoute.ticketstatus,key:'ticketstatus',title:'Statut'}) }}/> 
            <Info title={'Engagement'} modifiable={true} borderBottom={0.6} content={dataRoute.ticketseverities} 
            onPress={() => { this.setState({modalVisibleButton : true,valeur2:dataRoute.ticketseverities,valeur:dataRoute.ticketseverities,key:'ticketseverities',title:'Engagement'}) }}/> 
            <Info title={'Ticket N°'} modifiable={false} borderBottom={0.6} content={dataRoute.ticket_no} modifiable={false} />
            <Info title={'Description'} modifiable={true} borderBottom={0.6} content={dataRoute.description} 
            onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.description,key:'description',title:'Description'}) }}/> 
            <View style={{ width: '100%', height: 70, backgroundColor: 'white', marginTop: 10 }}>
              <TouchableOpacity activeOpacity={0.7} style={styles.buttonFullDetails} onPress={() => { this.changecolor('Details') }} >
                <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: 'white' }}>Full Details</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ height: 50 }}></View>
        </ScrollView>
      )
    }
    else if (this.state.TabVisible == 'BGDetails') {
      return (
        <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Détail Du Ticket
                            </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Detail: !this.state.Detail }) }}>
              {!this.state.Detail ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.Detail ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Titre'} modifiable={true} borderBottom={0.6} content={dataRoute.ticket_title} 
              onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.ticket_title,key:'ticket_title',title:'Titre'}) }}/> 
              <Info title={'Relatif à'} modifiable={true} borderBottom={0.6} content={this.state.idToAccountname} 
              onPress={() => { this.setState({modalVisibleButton : true,valeur2:dataRoute.parent_id,valeur:this.state.idToAccountname,key:'parent_id',title:'Relatif à'}) }}/> 
              <Info title={'Nom du contact'} modifiable={true} borderBottom={0.6} content={this.state.idToLastname} 
              onPress={() => { this.setState({modalVisibleButton : true,valeur2:dataRoute.contact_id,valeur:this.state.idToLastname,key:'contact_id',title:'Nom du contact'}) }}/> 
              <Info title={'Priorité'} modifiable={true} borderBottom={0.6} content={dataRoute.ticketpriorities} 
              onPress={() => { this.setState({modalVisibleButton : true,valeur2:dataRoute.ticketpriorities,valeur:dataRoute.ticketpriorities,key:'ticketpriorities',title:'Priorité'}) }}/> 
              <Info title={'Statut'} modifiable={true} borderBottom={0.6} content={dataRoute.ticketstatus} 
              onPress={() => { this.setState({modalVisibleButton : true,valeur2:dataRoute.ticketstatus,valeur:dataRoute.ticketstatus,key:'ticketstatus',title:'Statut'}) }}/> 
              <Info title={'Engagement'} modifiable={true} borderBottom={0.6} content={dataRoute.ticketseverities} 
              onPress={() => { this.setState({modalVisibleButton: true,valeur2:dataRoute.ticketseverities,valeur:dataRoute.ticketseverities,key:'ticketseverities',title:'Engagement'}) }}/> 
              <Info title={'Date de création'} modifiable={false} borderBottom={0.6} content={dataRoute.createdtime} /> 
              <Info title={'Heures'} modifiable={true} borderBottom={0.6} content={dataRoute.hours} 
              onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.hours,key:'hours',title:'Heures'}) }}/> 
              <Info title={'Jours'} modifiable={true} borderBottom={0.6} content={dataRoute.days}
               onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.days,key:'days',title:'Jours'}) }}/> 
              <Info title={'Catégorie'} modifiable={true} borderBottom={0.6} content={dataRoute.ticketcategories} 
              onPress={() => { this.setState({modalVisibleButton : true,valeur2:dataRoute.ticketcategories,valeur:dataRoute.ticketcategories,key:'ticketcategories',title:'Catégorie'}) }}/> 
              <Info title={'Date de modification'} modifiable={false} borderBottom={0.6} content={dataRoute.modifiedtime} /> 
              <Info title={'Ticket N°'} modifiable={false} borderBottom={0.6} content={dataRoute.ticket_no} />
              <Info title={'Source'} modifiable={false} borderBottom={0.6} content={dataRoute.source} /> 


            </View>
          ) : null}
          <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Informations sur la description
                                </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} 
            onPress={() => { this.setState({ BlocSys: !this.state.BlocSys }) }}>
              {!this.state.InfoP ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community'/>
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community'/>
              }
            </TouchableOpacity>
          </View>
          {this.state.InfoP ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Description'} borderBottom={0.6} content={dataRoute.description}  modaifiable={true}
                onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.description,key:'description',title:'Description'}) }}/> 
              </View>
          ) : null}
          <View style={{ marginTop: 40 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Details de la solution
                                </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ BlocSys: !this.state.BlocSys }) }}>
              {!this.state.Portal ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community'/>
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community'/>
              }
            </TouchableOpacity>
          </View>
          {this.state.Portal ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Solution'} borderBottom={0.6} content={dataRoute.solution} modaifiable={true}
                onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.solution,key:'solution',title:'Solution'}) }}/> 
                </View>
          ) : null}

        </ScrollView>
      )


    }



   
  }
  setDate = () => {
    this.setState({ isDate: true })
  }

 
  onPressInfoFunc = () => {
    if (this.state.TaskMoreinfo) {
      this.setState({
        TaskMoreinfo: false,
        TaskMoreinfoIcon: "chevron-down",
        TaskMoreinfoText: 'Plus'
      })
    } else {
      this.setState({
        TaskMoreinfo: true,
        TaskMoreinfoIcon: "chevron-up",
        TaskMoreinfoText: 'Moins'
      })
    }
  }
 

  renderItem = ({ item }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => { this.ChangeNomSociete(item.socName); this.setModalVisible1(false) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>{`${item.socName}`.substring(0, 1)}</Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.socName}
          </Text>
        </View>
      </TouchableOpacity>

    </View>
  );
  renderItemContacts = ({ item}) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {this.setState({modalVisibleButton:true,modalVisibleItemContacts:false,idContact:item.id,lastname:item.lastname,chosen:item.lastname,value:item.id,valeur:item.lastname,valeur2:item.id}) }}>
        <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
          <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>
          {String(item.lastname).slice(0,1)}            
          </Text>
        </View>
        <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
          <Text style={{fontSize : 18}}>
          {item.lastname }
          </Text>
        </View>
      </TouchableOpacity> 
    </View>
  );
  renderItemComptes = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {this.setState({ modalVisibleCompte: false, modalVisibleButton: true,  idCompte: item.id, accountname: item.accountname,chosen:item.accountname,value:item.id }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.accountname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.accountname}
          </Text>
        </View>
      </TouchableOpacity>

    </View>
  );

  update=async(value)=>{
    console.log('ehre update')
    this.setState({ loadingShow: true });
    var dataRoute=this.props.route.params.data[this.props.route.params.index]
      dataRoute[this.state.key]=value;
      await fetch(this.state.link, {
            method: 'POST',
            headers: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
           body:'operation=update&sessionName='+this.state.dataFet+'&element='+JSON.stringify(dataRoute)})
      .then((responses)  => responses.json()) 
      .then((json)=>{this.setState({ loadingShow: false ,valeur:'',modalVisibleInput:false,modalVisibleDate:false,modalVisibleButton:false,chosen:'Select Item'});console.log('here json '+JSON.stringify(json));})
      .catch((error) => console.error(error));
      this.getFunction();
      
     
    }
    onPressOptionPriority = (val) => { this.setState({ modalVisibleButton: true, modalVisibleItemPriority: false, priority: val,chosen:val,value:val ,valeur:val,valeur2:val}); }
    onPressOptionStatut = (val) => { this.setState({ modalVisibleButton: true,  modalVisibleItemStatut: false, statut: val,chosen:val,value:val,valeur:val,valeur2:val }); }
    onPressOptionEngagement = (val) => { this.setState({ modalVisibleButton: true, modalVisibleEngagement: false, engagement: val ,chosen:val,value:val,valeur:val,valeur2:val}); }
    onPressOptionCategorie = (val) => { this.setState({ modalVisibleButton: true, modalVisibleCategorie: false, categorie: val,chosen:val,value:val,valeur:val,valeur2:val }); }

  render() {
    return (

      <View style={{ flex: 1, flexDirection: 'column' }}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.loadingShow}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
          <Header
            placement="right"
            leftComponent={
              <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'stretch' }}>
                <Button icon={<Icon name="chevron-left" size={20} color='white' type='material-community' />} onPress={()=>this.props.navigation.goBack()}/>
                <Text style={{ fontSize: 22, color: 'white', fontWeight: '500', marginTop: 5 }}>Tickets : {this.props.route.params.title}</Text>
              </View>}

          />
        </View>
        <View style={{ flex: 1.2 }}>
          {/* <Text>scrollTabNavigation</Text> */}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.scroll}>
            <TabItem title='Résumé' scale={this.state.SRésumé} BackGround={this.state.BGRésumé} onPress={() => this.changecolor('Résumé')} />
            <TabItem title='Details' scale={this.state.SDetails} BackGround={this.state.BGDetails} onPress={() => this.changecolor('Details')} />
          </ScrollView >

        </View>
        <SafeAreaView style={{ flex: 7, flexDirection: 'column' }}>
          {this.TabNavigation()}
        </SafeAreaView>
        <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Tickets'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }) }}
          onPressSave={this.update}
        />
        <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Tickets'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Statut') {
              this.setState({
                modalVisibleItemStatut: true, modalVisibleButton: false
              })
            }
            if (this.state.title == 'Catégorie') {
              this.setState({
                modalVisibleCategorie: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Engagement') {
              this.setState({
                modalVisibleEngagement: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Priorité') {
              this.setState({
                modalVisibleItemPriority: true, modalVisibleButton: false
              })
            }

            if (this.state.title === 'Nom du contact') {
              this.setState({
                modalVisibleItemContacts: true, modalVisibleButton: false
              })
            }

            if (this.state.title === 'Relatif à') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
          }

          }
        />
         <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptes}
          onPressClose={()=>{this.setState({modalVisibleButton:true,modalVisibleCompte:false})}}
        />
        <ModalSelectContacts 
          modalVisibleContacts={this.state.modalVisibleItemContacts}                           
          
          renderItem={this.renderItemContacts}
          onPressClose={()=>this.setState({modalVisibleItemContacts:false,modalVisibleButton:true})}
        />
        <ModalDate 
          modalVisibleDate={this.state.modalVisibleDate}  
          title={this.state.title} 
          edit={'Edit Tickets'}
          valeur={this.state.valeur}
          onPressCancel={()=>this.setState({modalVisibleDate:false,value:''})} 
          onPressSave={this.update}
                  
        />
        <ModalSelectItemPriority
            modalVisibleItemTickets={this.state.modalVisibleItemPriority}
            onPressOption={this.onPressOptionPriority}
            priorite={'Select Priorité'}
            onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleTickets: true })}

          />
          <ModalSelectItemStatut
            modalVisibleItemTickets={this.state.modalVisibleItemStatut}
            onPressOption={this.onPressOptionStatut}
            select={'Select Statut'}
            onPressClose={() => this.setState({ modalVisibleItemStatut: false, modalVisibleTickets: true })}

          />
          <ModalSelectEngagement
            modalVisibleEngagement={this.state.modalVisibleEngagement}
            onPressOption={this.onPressOptionEngagement}
            select={'Select Statut'}
            onPressClose={() => this.setState({ modalVisibleEngagement: false, modalVisibleTickets: true })}

          />
          <ModalSelectCategorie
            modalVisibleCategorie={this.state.modalVisibleCategorie}
            onPressOption={this.onPressOptionCategorie}
            select={'Select catégorie'}
            onPressClose={() => this.setState({ modalVisibleCategorie: false, modalVisibleTickets: true })}

          />






      </View>

    )

  }
}




