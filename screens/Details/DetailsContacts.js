import React, { Component } from 'react';
import { SafeAreaView, Image, TouchableOpacity, ScrollView, Text, View, Modal, FlatList } from 'react-native';
import { FAB, Portal, Provider } from 'react-native-paper';//component library
//import Icon from 'react-native-vector-icons/FontAwesome';//icons library
import Info from '../ItemsRender/info';
import { Header, Button, Icon, Overlay } from 'react-native-elements';
import * as DocumentPicker from 'expo-document-picker';
import styles from '../styles';
import ItemAffaire from '../ItemsRender/affaireitem';
import Comments from '../ItemsRender/comment';
import ModalSelectSociete from '../Modals/ModalSelectSociete';
import TabItem from '../ItemsRender/TabItem';
import ItemTicket from '../ItemsRender/ItemTickets';
import AddTicket from '../Modals/Modal_AddTickets';
import ModalSelectItemPriority from '../Modals/ModalSelectItemTickets';
import ModalSelectItemStatut from '../Modals/ModalSelectItemStatut';
import ModalSelectEngagement from '../Modals/ModalSelectEngagement';
import ModalSelectCategorie from '../Modals/ModalSelectCategorie';
import ModalSelectContacts from '../Modals/ModalSelectContact';
import ModalAffaires from '../Modals/ModalAffaires';
import ModalSelectItem from '../Modals/ModalSelectItem';
import ModalSelectType from '../Modals/ModalSelectType';
import ModalAddContrat from '../Modals/ModalContrat';
import ModalSelectUnite from '../Modals/ModalSelectUnite';
import ModalSelectItemType from '../Modals/ModalSelectItemType';
import ModalProjects from '../Modals/ModalProjects';
import ModalButton from '../Modals/ModalsUpdate/ModalButton';
import ModalInput from '../Modals/ModalsUpdate/Modalnput';
import ModalDate from '../Modals/ModalsUpdate/ModalDate';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Linking } from 'react-native';
import ModalTypeActivities from '../Modals/ModalTypeActivities';
import ModalVisibilite from '../Modals/ModalVisibilite';
import ModalSelectStatutActivitie from '../Modals/ModalSelectStatutActvitie';
import ModalActivities from '../Modals/ModalActivitie';
import Contact from '../ItemsRender/contact'
import ItemAct from '../ItemsRender/itemActivities';
import ItemRender from '../ItemsRender/itemrender';

export default class DetailsContacts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Potentials: false,
      Documents: false,
      HelpDesk: false,
      ServiceContracts: false,
      Project: false,
      loadingShow: true,
      dataFet: [],
      link: [],
      valeur:'',
      //updates fields 
      chosen: 'Select Item',
      value: '',
      title: '',
      key: '',
      // updates modals
      modalVisibleInput: false,
      modalVisibleDate: false,
      modalVisibleButton: false,
      //Create modals
      modalVisibleAffaires: false,
      modalVisibleProjects: false,
      modalVisibleActivities:false,
      modalVisibleItem: false,
      modalVisibleType: false,
      modalVisibleContrat: false,
      modalVisibleUnite: false,
      modalVisibleItemPriority: false,
      modalVisibleVisibilite:false,
      modalVisibleItemStatut: false,
      modalVisibleStatutActivitie: false,
      modalVisibleItemContacts: false,
      modalVisibleEngagement: false,
      modalVisibleCategorie: false,
      modalVisibleCompte: false,
      modalVisibleContacts: false,
      modalVisibleTickets: false,
      modalVisibleDocuments: false,
      modalVisibleItemType:false,
      modalVisibleDelete: false,
      modalVisibleServices: false,
      //create fields
      unite: 'Select unité',
      id: 0,
      accountname: '',
      selectPriority: '',
      priority:'Normale',
      selectStatut: '',
      idCompte: '',
      lastname: this.props.route.params.lastname,
      idContact: this.props.route.params.id,
      engagement: ' Mineur',
      categorie: 'Petit problème',
      statutTickets:'Ouvert',
      statutContrat:'Ouvert',
      statutActivitie:' Plannifiée ',
      select: '',
      type: ' Rendez-vous ',
      phase: 'Proposition',
      idToAccountname: '',
      idToLastname: '',
      //data 
      dataProjects: [],
      dataActivities: [],
      dataDocuments: [],
      dataContrats: [],
      dataAffaires: [],
      dataTickets: [],
      dataMiseaj: [{
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
        description: 'Kamal commented on your document',
        date: '3days',
        icon: 'address-book-o'
      },
      {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
        description: 'Aya shared your document with DEMO',
        date: '3days',
        icon: 'cubes'
      },
      {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
        description: 'Ahmed added a new contact',
        date: '3days',
        icon: 'bell-o'
      },],


      //tabBG
      BGRésumé: '#9ddfd3',
      BGDetails: 'white',
      BGMises: 'white',
      BGAffaires: 'white',
      BGActivitie: 'white',
      BGDoc: 'white',
      BGProjects: 'white',
      BGServ: 'white',
      BGComnt: 'white',
      BGTickets: 'white',
      //scale
      SRésumé: 0.8,
      SDetails: 0.6,
      SMises:0.6,
      SContacts: 0.6,
      SAffaires: 0.6,
      SActivitie: 0.6,
      SDoc: 0.6,
      SProjects: 0.6,
      SServ: 0.6,
      SComnt:0.6,
      STickets: 0.6,

      //tabVisible
      TabVisible: 'BGRésumé',
      //------------------
      Detail: true,
      BlocSys: true,
      InfoP: true,
      Adress: true,
      Desc: true,
      Portal: true,
      //       
      Moreinfo: false,
      MoreinfoIcon: "chevron-down",
      MoreinfoText: 'Plus',
      //
      date: new Date(),
      time: new Date(),
      isDate: false,
      istime: false,
    }

  }

  initialiserValue=()=>{
    this.setState({
      unite: 'Select unité',
      id: 0,
      accountname: '',
      selectPriority: '',
      selectStatut: '',
      idCompte: '',
      lastname: this.props.route.params.lastname,
      idContact: this.props.route.params.id,
      engagement: ' Mineur',
      categorie: 'Petit problème',
      statutTickets:'Ouvert',
      statutContrat:'Ouvert',
      statutActivitie:' Plannifiée ',
      select: '',
      type: ' Rendez-vous',
      phase: 'Proposition',
      idToAccountname: '',
      idToLastname: '',
    })
  }
  

  initaliserStyle = () => {
    this.setState({
      BGRésumé: 'white',
      BGDetails: 'white',
      BGMises: 'white',
      BGAffaires: 'white',
      BGActivitie: 'white',
      BGDoc: 'white',
      BGProjects: 'white',
      BGServ: 'white',
      BGComnt: 'white',
      BGTickets: 'white',
      //scale
      SRésumé: 0.8,
      SDetails: 0.6,
      SMises: 0.6,
      SContacts: 0.6,
      SAffaires:0.6,
      SActivitie: 0.6,
      SDoc: 0.6,
      SProjects: 0.6,
      SServ: 0.6,
      SComnt: 0.6,
      STickets: 0.6,

    })

  }
  renderElementTickets = ({ item, index }) => {
    return (
      <View>
        <ItemRender Name={item.ticket_title} val={item.ticketstatus} onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} onPress={() => { this.props.navigation.navigate('DetailsTickets', { title:item.ticket_title,id: item.id, data: this.state.dataTickets, index: index, result: this.state.dataFet }) }} />
      </View>)
  }
  renderElementServices = ({ item, index }) => {
    return (
      <View style={{ flex: 9 }}>
        <ItemAffaire Name={item.subject} onDelete={() => this.setState({ modalVisibleDelete: true })} onPress={() => { this.props.navigation.navigate('DetailsContrats', {title:item.subject, id: item.id, data: this.state.dataContrats, index: index, result: this.state.dataFet }) }} />
      </View>)

  }
  renderElementProjects = ({ item, index }) => {
    return (
      <View style={{ flex: 9 }}>
        <ItemAffaire Name={item.projectname} onDelete={() => this.setState({ modalVisibleDelete: true })} onPress={() => { this.props.navigation.navigate('DetailsProjects', { title:item.projectname,id: item.id, data: this.state.dataProjects, index: index, result: this.state.dataFet }) }} />
      </View>)

  }
  renderElementActivities = ({ item, index }) => {
    return (
      <View>
        <ItemAct Name={item.subject} status={item.eventstatus} date={item.date_start+' - '+item.time_start}onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} 
        onPress={() => this.props.navigation.navigate('DetailsActivities', { title:item.subject,idCompte: this.props.route.params.idCompte, id: item.id, data: this.state.dataActivities, index: index, result: this.state.dataFet })} />
      </View>);
  }


  renderElementMiseaj = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => { console.log("pressed: " + item.title) }}
      >

        <View style={styles.block}>

          <View style={styles.icon_container}>
            <Icon style={styles.icon}
              name={item.icon}
              color='#225fbf'
              type='font-awesome' />
          </View>
          <View style={styles.details}>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.description} >{item.description}</Text>
          </View>
          <View style={styles.date_container}>
            <Text style={styles.date}>{item.date} ago</Text>
          </View>
        </View>

      </TouchableOpacity>
    );
  }
  renderElementAffaire = ({ item, index }) => {
    return (
      <View>
        <ItemAffaire Name={item.potentialname} onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} onPress={() => { this.props.navigation.navigate('DetailsAffaires', { title:item.potentialname,id: item.id, data: this.state.dataAffaires, index: index, result: this.state.dataFet }) }} />
      </View>);
  }

  changecolor = (a) => {
    this.initaliserStyle();
    this.setState({
      [`BG${a}`]: '#9ddfd3',
      [`S${a}`]: 0.8,
      TabVisible: `BG${a}`
    });
  }

  getFunction = async () => {
    this.setState({ loadingShow: true });
    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    var id = this.props.route.params.id;
    try {
      await AsyncStorage.multiGet(['SESSION', 'CRM_LINK', 'TYPES'], async (err, res) => {
        types = res[2][1].split(',');
       
        console.log(types);
        if (types.includes('Documents')) this.setState({ Documents: true });
        if (types.includes('HelpDesk')) this.setState({ HelpDesk: true });
        if (types.includes('Potentials')) this.setState({ Potentials: true });
        if (types.includes('ServiceContracts')) this.setState({ ServiceContracts: true });
        if (types.includes('Project')) this.setState({ Project: true });

        var dataFet = res[0][1];
        console.log(dataFet)
        this.setState({ dataFet: dataFet });
        var link = res[1][1];
        this.setState({ link: link });
        //this fetch is for Affaires
        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Potentials where contact_id=' + id + ';')
          .then((response) => response.json())
          .then(async (json) => {
            this.setState({ dataAffaires: json.result });
            //this fetch is for Activities
            await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Events where contact_id=' + id + ';')
              .then((response) => response.json())
              .then(async (json) => {
                this.setState({ dataActivities: json.result });
                //this fetch is from contrats services
                await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from ServiceContracts where sc_related_to=' + id + ';')
                  .then((response) => response.json())
                  .then(async (json) => {
                    this.setState({ dataContrats: json.result });
                    //this fetch is from contrats services
                    await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Project where linktoaccountscontacts=' + id + ';')
                      .then((response) => response.json())
                      .then(async (json) => {
                        this.setState({ dataProjects: json.result }); console.log('dataProjects : ' + JSON.stringify(this.state.dataProjects));
                        //this fetch is from contrats Tickets
                        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from HelpDesk where contact_id=' + id + ';')
                          .then((response) => response.json())
                          .then(async (json) => {
                            this.setState({ dataTickets: json.result });
                            await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Accounts where id=' + dataRoute.account_id + ';')
                              .then((response) => response.json())
                              .then(async (json) => {
                                if (json.success == true && json.result != '') {
                                  console.log('inside not undefined')
                                  this.setState({ idToAccountname: json.result[0].accountname })}
                                await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName=' + dataFet + '&query=select * from Contacts where id=' + dataRoute.contact_id + ';')
                                  .then((response) => response.json())
                                  .then(async (json) => { this.setState({ loadingShow: false }); 
                                  if (json.success == true && json.result != '') 
                                  { this.setState({ dataContacts: json.result, idToLastname: json.result[0].lastname }) }
                                  })
                                  .catch((error) =>
                                    console.log(error))
                              })
                              .catch((error) =>
                                console.log(error))
                          })
                          .catch((error) =>
                            console.log(error))
                      })
                      .catch((error) =>
                        console.log(error))
                  })
                  .catch((error) =>
                    console.log(error))
              })
              .catch((error) =>
                console.log(error))
          })
          .catch((error) =>
            console.log(error))
      })
    } catch (e) {
      console.log(e);
    }
  }
  componentDidMount() {
    this.getFunction();
  }

  onPressPriority = () => {
    this.setState({ modalVisibleItemPriority: true, modalVisibleTickets: false })
  }
  setVisibleAffaires = () => {
    this.setState({ modalVisibleAffaires: false })
  }
  onPressStatut = () => {
    this.setState({ modalVisibleItemStatut: true, modalVisibleTickets: false })
  }
  setVisibleTickets = () => {
    this.setState({ modalVisibleTickets: false })
  }
  onPressItemContact = () => {
    this.setState({ modalVisibleItemContacts: true, modalVisibleTickets: false })
  }
  onPressInfo = () => {
    if (this.state.Moreinfo) {
      this.setState({
        Moreinfo: false,
        MoreinfoIcon: "chevron-down",
        MoreinfoText: 'Plus'
      })
    } else {
      this.setState({
        Moreinfo: true,
        MoreinfoIcon: "chevron-up",
        MoreinfoText: 'Moins'
      })
    }
  }
  onPressEmail=(email) => {Linking.openURL(`mailto:${email}`) }

  onPressOptionPriority = (val) => { this.setState({ modalVisibleActivities:true,modalVisibleTickets: true, modalVisibleContrat: true, modalVisibleItemPriority: false, priority: val }); }
  onPressOptionStatut = (val) => { this.setState({ modalVisibleStatutActivitie:false,modalVisibleActivities:true,modalVisibleTickets: true, modalVisibleContrat: true, modalVisibleItemStatut: false, statutTickets: val , statutActivitie: val, statutContrat: val}); }
  onPressOptionEngagement = (val) => { this.setState({ modalVisibleActivities:true,modalVisibleTickets: true, modalVisibleEngagement: false, engagement: val }); }
  onPressOptionCategorie = (val) => { this.setState({modalVisibleActivities:true, modalVisibleTickets: true, modalVisibleCategorie: false, categorie: val }); }
  onPressOptionVisibilite = (val) => { this.setState({ modalVisibleActivities: true,  modalVisibleVisibilite: false, visibilite: val }); }
onPressCloseContact=()=>{this.setState({modalVisibleActivities:true,modalVisibleItemContacts:false})}
  renderItemComptes = ({ item, x }) => (

    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {this.setState({ modalVisibleCompte: false, modalVisibleAffaires: true, modalVisibleActivities: true,modalVisibleContrat: true, modalVisibleTickets: true, idCompte: item.id, accountname: item.accountname, chosen: item.accountname, value: item.id }); console.log(this.state.idCompte) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.accountname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.accountname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
  renderItemComptesUpdate = ({ item, x }) => (

    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {this.setState({ modalVisibleCompte: false,   modalVisibleButton: true, idCompte: item.id, accountname: item.accountname, chosen: item.accountname, value: item.id,valeur:item.accountname,valeur2:item.id }); console.log(this.state.idCompte) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.accountname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.accountname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
  
  renderItemContactsUpdate = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => { this.setState({ modalVisibleItemContacts: false, modalVisibleButton: true, idContact: item.id, lastname: item.lastname, chosen: item.lastname, value: item.id }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.lastname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.lastname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
  renderItemContacts = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => { this.setState({ modalVisibleItemContacts: false, modalVisibleActivities: true,modalVisibleTickets: true,  modalVisibleAffaires: true, modalVisibleContrat: true, idContact: item.id, lastname: item.lastname, chosen: item.lastname, value: item.id }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.lastname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.lastname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
  makeCall = (phone) => {

    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = `tel:${phone}`;
    } else {
      phoneNumber = `telprompt:${phone}`;
    }

    Linking.openURL(phoneNumber);
  };
  onPressOptionType = (x) => { this.setState({ modalVisibleAffaires: true, modalVisibleContrat: true, modalVisibleType: false,modalVisibleItemType: false,modalVisibleActivities:true, type: x }); }
  onPressOption = (x) => { this.setState({ modalVisibleAffaires: true, modalVisibleItem: false, phase: x }); }


  onPressOptionUnite = (x) => { this.setState({ modalVisibleContrat: true, modalVisibleUnite: false, unite: x }); }
  setVisibleProjects = () => {
    this.setState({ modalVisibleProjects: false });
  }
  update = async (value) => {
    this.setState({ loadingShow: true });
    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    dataRoute[this.state.key] = value;
    console.log(this.state.valeur);
    await fetch('https://portail.crm4you.ma/webservice.php', {
      method: 'POST',
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: 'operation=update&sessionName=' + this.state.dataFet + '&element=' + JSON.stringify(dataRoute)
    })
      .then((responses) => responses.json())
      .then((json) => {console.log('inside json contact update ');this.setState({ loadingShow: false,valeur:'',valeur2:'',modalVisibleInput:false,modalVisibleDate:false,modalVisibleButton:false,chosen:'Select Item' });console.log(json);  })
      .catch((error) => console.error(error));
  }
  TabNavigation = () => {
    var dataRoute = this.props.route.params.data[this.props.route.params.index]
    if (this.state.TabVisible == 'BGRésumé') {
      return (
        <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 1, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Champ De Clé
                </Text>
            </View>
          </View>
          <View style={styles.content_champ_cle}>
            <Info title={'Prénom'} modifiable={true} borderBottom={0.6} content={dataRoute.firstname}
              onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.firstname, key: 'firstname', title: 'Prénom' }) }} />
            <Info title={'Nom'} modifiable={true} borderBottom={0.6} content={dataRoute.lastname}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.lastname,key: 'lastname}', title: 'Nom' }) }} />
            <Info title={'Nom de compte'} modifiable={true} borderBottom={0.6} content={this.state.idToAccountname}
              onPress={() => { this.setState({ modalVisibleButton: true,valeur:this.state.idToAccountname,valeur2:dataRoute.account_id, key: 'account_id', title: 'Nom de compte' }) }} />
            <Info title={'Fonction'} modifiable={true} borderBottom={0.6} content={dataRoute.department}
              onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.department,key: 'department', title: 'Fonction' }) }} />
            <Info title={'Téléphone (bureau)'} modifiable={true} borderBottom={0.6} content={dataRoute.phone} onPressContent={()=>this.makeCall(dataRoute.phone)}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.phone, key: 'phone', title: 'Téléphone (bureau)' }) }} />
            <Info title={'E-Mail Principale'} modifiable={true} borderBottom={0.6} content={dataRoute.email}
                onPressContent={()=>this.onPressEmail(dataRoute.email)} onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.email,key: 'email', title: 'E-Mail Principale' }) }} />
            <View style={{ width: '100%', height: 70, backgroundColor: 'white', marginTop: 10 }}>
              <TouchableOpacity activeOpacity={0.7} style={styles.buttonFullDetails} onPress={() => { this.changecolor('Details') }} >
                <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: 'white' }}>Full Details</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ height: 50 }}></View>
          <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Contact'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }) }}
          onPressSave={this.update}
        />
       
        <ModalDate
          modalVisibleDate={this.state.modalVisibleDate}
          title={this.state.title}
          edit={'Edit Contact'}
          valeur={this.state.valeur}
          onPressCancel={() => this.setState({ modalVisibleDate: false, value: '', })}
          onPressSave={this.update}

        />
         <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Contact'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false,value: '',chosen:'Select Item',valeur:'',valeur2:'' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Nom de compte') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Superieur hiérarchique') {
              this.setState({
                modalVisibleItemContacts: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalSelectContacts
          modalVisibleContacts={this.state.modalVisibleItemContacts}
          
          renderItem={this.renderItemContactsUpdate}
          onPressClose={() => this.setState({ modalVisibleItemContacts: false })}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptesUpdate}
          onPressClose={() => this.setState({ modalVisibleCompte: false })}
        />


         
        </ScrollView>
      )
    }
    else if (this.state.TabVisible == 'BGDetails') {
      return (
        <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Détail Du Compte
                            </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Detail: !this.state.Detail }) }}>
              {!this.state.Detail ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.Detail ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Prénom'} modifiable={true} borderBottom={0.6} content={dataRoute.firstname}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.firstname,key: 'firstname', title: 'Prénom' }) }} />
              <Info title={'Nom'} modifiable={true} borderBottom={0.6} content={dataRoute.lastname}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.lastname, key: 'lastname', title: 'Nom' }) }} />
              <Info title={'Nom compte'} modifiable={true} borderBottom={0.6} content={this.state.idToAccountname}
                onPress={() => { this.setState({ modalVisibleButton: true,valeur:this.state.idToAccountname,valeur2:dataRoute.account_id, key: 'account_id', title: 'Nom compte' }) }} />
              <Info title={'Superieur hiérarchique'} modifiable={true} borderBottom={0.6} content={this.state.idToLastname}
                onPress={() => { this.setState({ modalVisibleButton: true, key: 'contact_id',valeur:this.state.idToLastname,valeur2:dataRoute.contact_id ,title: 'Superieur hiérarchique' }) }} />
              <Info title={'Division'} modifiable={true} borderBottom={0.6} content={dataRoute.department}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.department, key: 'department', title: 'Division' }) }} />
              <Info title={'Fonction'} modifiable={true} borderBottom={0.6} content={dataRoute.title}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.title,key: 'otitle', title: 'Fonction' }) }} />
              <Info title={'Téléphone (bureau)'} modifiable={true} borderBottom={0.6} content={dataRoute.phone} onPressContent={()=>this.makeCall(dataRoute.phone)}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.phone,key: 'phone', title: 'Téléphone (bureau)' }) }} />
              <Info title={'Mobile'} modifiable={true} borderBottom={0.6} content={dataRoute.mobile} onPressContent={()=>this.makeCall(dataRoute.mobile)}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.otherphone, key: 'otherphone', title: 'Mobile' }) }} />
              <Info title={'E-Mail Principale'} modifiable={true} borderBottom={0.6} content={dataRoute.email}
                onPressContent={()=>this.onPressEmail(dataRoute.email)} onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.email, key: 'email', title: 'E-Mail Principale' }) }} />
              <Info title={'Source du prospect'} modifiable={false} borderBottom={0.6} content={dataRoute.leadsource}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.leadsource,key: 'leadsource', title: 'Source du prospect' }) }} />
             
            </View>
          ) : null}
          <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Informations personnalisées
                                </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ BlocSys: !this.state.BlocSys }) }}>
              {!this.state.InfoP ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.InfoP ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Assistant'} borderBottom={0.6} content={dataRoute.assistant}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.assistant,key: 'assistant', title: 'Assistant' }) }} />
              <Info title={'Téléphone (assistant)'} modifiable={true} borderBottom={0.6} content={dataRoute.assistantphone}
                onPressContent={()=>this.makeCall(dataRoute.assistantphone)} onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.assistantphone, key: 'assistantphone', title: 'Téléphone (assistant)' }) }} />
              <Info title={'Téléphone (domicile)'} borderBottom={0.6} content={dataRoute.homephone}
                 onPressContent={()=>this.makeCall(dataRoute.homephone)} onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.homephone,key: 'homephone', title: 'Téléphone (domicile)' }) }} />
              <Info title={'Téléphone secondaire'} borderBottom={0.6} content={dataRoute.otherphone}
                onPressContent={()=>this.makeCall(dataRoute.otherphone)}  onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.otherphone,key: 'otherphone', title: 'Téléphone Secondaire' }) }} />
              <Info title={'Fax'} borderBottom={0.6} content={dataRoute.fax}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'fax', title: 'Fax' }) }} />
              <Info title={'Email secondaire'} borderBottom={0.6} content={dataRoute.secondaryemail}
                onPressContent={()=>this.onPressEmail(dataRoute.secondaryemail)} onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.fax,key: 'secondaryemail', title: 'Email secondaire' }) }} />
             
            </View>
          ) : null}
          <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Contact'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }) }}
          onPressSave={this.update}
        />
        <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Contact'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false,value: '',chosen:'Select Item',valeur:'',valeur2:'' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Nom de compte') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Superieur hiérarchique') {
              this.setState({
                modalVisibleItemContacts: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalDate
          modalVisibleDate={this.state.modalVisibleDate}
          title={this.state.title}
          edit={'Edit Contact'}
          valeur={this.state.valeur}
          onPressCancel={() => this.setState({ modalVisibleDate: false, value: '' })}
          onPressSave={this.update}

        />
        <ModalSelectContacts
          modalVisibleContacts={this.state.modalVisibleItemContacts}
          
          renderItem={this.renderItemContactsUpdate}
          onPressClose={() => this.setState({ modalVisibleItemContacts: false })}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptesUpdate}
          onPressClose={() => this.setState({ modalVisibleCompte: false })}
        />



          <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Détail de l'Adresse
                                </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Adress: !this.state.Adress }) }}>
              {!this.state.Adress ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>

          {this.state.Adress ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Adresse'} modifiable={true} borderBottom={0.6} content={dataRoute.mailingstreet}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.mailingstreet, key: 'mailingstreet', title: 'Adresse' }) }} />
              <Info title={'Adresse (alt)'} modifiable={true} borderBottom={0.6} content={dataRoute.otherstreet}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.otherstreet,key: 'otherstreet', title: 'Adresse (alt)' }) }} />
              <Info title={'Case Postale '} modifiable={true} borderBottom={0.6} content={dataRoute.mailingpobox}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.mailingpobox,key: 'mailingpobox', title: 'Case Postale' }) }} />
              <Info title={'Other P.O.Box'} modifiable={true} borderBottom={0.6} content={dataRoute.otherpobox}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.otherpobox,key: 'otherpobox', title: 'Other P.O.Box' }) }} />
              <Info title={'Ville'} modifiable={true} borderBottom={0.6} content={dataRoute.mailingcity}
                onPress={() => { this.setState({ modalVisibleInput: true,  valeur:dataRoute.mailingcity,key: 'mailingcity', title: 'Ville' }) }} />
              <Info title={'Ville (alt)'} modifiable={true} borderBottom={0.6} content={dataRoute.othercity}
                onPress={() => { this.setState({ modalVisibleInput: true,  valeur:dataRoute.othercity,key: 'othercity', title: 'Ville (alt)' }) }} />
              <Info title={'Departement '} modifiable={true} borderBottom={0.6} content={dataRoute.mailingstate}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.mailingstate, key: 'mailingstate', title: 'Departement' }) }} />
              <Info title={'Departement (alt)'} modifiable={true} borderBottom={0.6} content={dataRoute.otherstate}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.otherstate, key: 'otherstate', title: 'Departement (alt)' }) }} />
              <Info title={'Code Postal'} modifiable={true} borderBottom={0.6} content={dataRoute.mailingzip}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.mailingzip, key: 'mailingzip', title: 'Code Postal' }) }} />
              <Info title={'Code Postal (alt)'} modifiable={true} borderBottom={0.6} content={dataRoute.otherzip}
                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.otherzip,key: 'otherzip', title: 'Code Postal (alt)' }) }} />
              <Info title={'Pays '} modifiable={true} borderBottom={0} content={dataRoute.mailingcountry}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.mailingcountry, key: 'mailingcountry', title: 'Pays' }) }} />
              <Info title={'Pays (alt)'} modifiable={true} borderBottom={0.6} content={dataRoute.othercountry}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.othercountry, key: 'Pays', title: 'Pays (alt)' }) }} />

            </View>
          ) : null}
          <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Contact'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }) }}
          onPressSave={this.update}
        />
        <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Contact'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false ,value: '',chosen:'Select Item',valeur:'',valeur2:''})}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Nom de compte') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Superieur hiérarchique') {
              this.setState({
                modalVisibleItemContacts: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalDate
          modalVisibleDate={this.state.modalVisibleDate}
          title={this.state.title}
          edit={'Edit Contact'}
          valeur={this.state.valeur}
          onPressCancel={() => this.setState({ modalVisibleDate: false, value: '' })}
          onPressSave={this.update}

        />
        <ModalSelectContacts
          modalVisibleContacts={this.state.modalVisibleItemContacts}
          
          renderItem={this.renderItemContactsUpdate}
          onPressClose={() => this.setState({ modalVisibleItemContacts: false })}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptesUpdate}
          onPressClose={() => this.setState({ modalVisibleCompte: false })}
        />


          <View style={{ height: 70 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Information Sur la description
              </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Desc: !this.state.Desc }) }}>
              {!this.state.Desc ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.Desc ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Description'} modifiable={true} borderBottom={0.6} content={dataRoute.description}
                onPress={() => { this.setState({ modalVisibleInput: true, key: 'description', title: 'Description' }) }} />
            </View>
          ) : null}
          <View style={{ marginTop: 40 }}></View>

          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Bloc Systeme
                                </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Desc: !this.state.Desc }) }}>
              {!this.state.BlocSys ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.BlocSys ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Contact ID'} modifiable={false} borderBottom={0.6} content={dataRoute.contact_no} />
              <Info title={'Source'} modifiable={false} borderBottom={0.6} content={dataRoute.source} />
              <Info title={'Email Opt Out'} modifiable={true} borderBottom={0.6} content={dataRoute.emailoptout}
                onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.emailoptout, key: 'emailoptout', title: 'Email Opt Out' }) }} />
              <Info title={'Date de modification'} modifiable={false} borderBottom={0.6} content={dataRoute.modifiedtime} />
              <Info title={'Date de création'} modifiable={false} borderBottom={0.6} content={dataRoute.createdtime} />

            </View>
          ) : null}
          <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Contact'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }) }}
          onPressSave={this.update}
        />
        <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Contact'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false ,value: '',chosen:'Select Item',valeur2:'',valeur:''})}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Nom de compte') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Superieur hiérarchique') {
              this.setState({
                modalVisibleItemContacts: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalDate
          modalVisibleDate={this.state.modalVisibleDate}
          title={this.state.title}
          edit={'Edit Contact'}
          valeur={this.state.valeur}
          onPressCancel={() => this.setState({ modalVisibleDate: false, value: '' })}
          onPressSave={this.update}

        />
        <ModalSelectContacts
          modalVisibleContacts={this.state.modalVisibleItemContacts}
          
          renderItem={this.renderItemContactsUpdate}
          onPressClose={() => this.setState({ modalVisibleItemContacts: false })}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptesUpdate}
          onPressClose={() => this.setState({ modalVisibleCompte: false })}
        />


          <View style={{ marginTop: 40 }}></View>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Portail de service client
                                </Text>
            </View>
            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ BlocSys: !this.state.BlocSys }) }}>
              {!this.state.Portal ?
                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                :
                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
              }
            </TouchableOpacity>
          </View>
          {this.state.Portal ? (
            <View style={styles.content_champ_cle}>
              <Info title={'Soutien Date de debut'} modifiable={true} borderBottom={0.6} content={dataRoute.support_start_date}
                onPress={() => { this.setState({ modalVisibleDate: true, valeur:dataRoute.support_start_date,key: 'support_start_date', title: 'Soutien Date de debut' }) }} />
              <Info title={'Soutien date de fin'} borderBottom={0.6} content={dataRoute.support_end_date}
                onPress={() => { this.setState({ modalVisibleDate: true, valeur:dataRoute.support_end_date,key: 'support_end_date', title: 'Soutien date de fin' }) }} />
            </View>
          ) : null}
          <ModalInput
          modalVisibleInput={this.state.modalVisibleInput}
          title={this.state.title}
          edit={'Edit Contact'}
          valeur={this.state.valeur}
          onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'' }) }}
          onPressSave={this.update}
        />
        <ModalButton
          modalVisibleButton={this.state.modalVisibleButton}
          title={this.state.title}
          chosen={this.state.chosen}
          edit={'Edit Contact'}
          value={this.state.value}
          valeur={this.state.valeur}
          valeur2={this.state.valeur2}
          onPressCancel={() => this.setState({ modalVisibleButton: false,value: '',chosen:'Select Item',valeur:'',valeur2:'' })}
          onPressSave={this.update}
          onPressSelect={() => {
            if (this.state.title == 'Nom de compte') {
              this.setState({
                modalVisibleCompte: true, modalVisibleButton: false
              })
            }
            if (this.state.title === 'Superieur hiérarchique') {
              this.setState({
                modalVisibleItemContacts: true, modalVisibleButton: false
              })
            }
          }

          }
        />
        <ModalDate
          modalVisibleDate={this.state.modalVisibleDate}
          title={this.state.title}
          edit={'Edit Contact'}
          valeur={this.state.valeur}
          onPressCancel={() => this.setState({ modalVisibleDate: false, value: '' })}
          onPressSave={this.update}

        />
        <ModalSelectContacts
          modalVisibleContacts={this.state.modalVisibleItemContacts}
          
          renderItem={this.renderItemContactsUpdate}
          onPressClose={() => this.setState({ modalVisibleItemContacts: false })}
        />
        <ModalSelectSociete
          modalVisibleCompte={this.state.modalVisibleCompte}
          
          renderItem={this.renderItemComptesUpdate}
          onPressClose={() => this.setState({ modalVisibleCompte: false })}
        />


        </ScrollView>
      )


    }
    else if (this.state.TabVisible == 'BGTickets') {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <FlatList
            data={this.state.dataTickets}
            renderItem={this.renderElementTickets}
            extraData={this.state.dataTickets.length}
            keyExtractor={item => item.id} />
          <View style={{ flex: 2 }}>
            <Provider>
              <Portal>
                <FAB.Group
                  open={this.state.open}
                  icon={'plus'}
                  color='white'
                  fabStyle={{ backgroundColor: '#53aefe' }}
                  actions={[
                    { icon: 'plus', onPress: () => console.log('Pressed add') },
                  ]}
                  onStateChange={() => this.setState({ modalVisibleTickets: true })}
                  onPress={() => {
                    this.setState({ modalVisibleTickets: true })
                  }}
                />
              </Portal>
            </Provider>
          </View>
          <AddTicket
            modalVisibleTickets={this.state.modalVisibleTickets}
            onPressPriority={this.onPressPriority}
            onPressStatut={this.onPressStatut}
            onPressItemContact={this.onPressItemContact}
            onPressCompte={() => this.setState({ modalVisibleCompte: true, modalVisibleTickets: false })}
            onPressEngagement={() => this.setState({ modalVisibleEngagement: true, modalVisibleTickets: false })}
            onPressCategorie={() => this.setState({ modalVisibleCategorie: true, modalVisibleTickets: false })}
            accountname={this.state.accountname}
            selectPriority={this.state.priority}
            selectStatut={this.state.statutTickets}
            
            onPressInfo={this.onPressInfo}
            MoreinfoText={this.state.MoreinfoText}
            MoreinfoIcon={this.state.MoreinfoIcon}
            Moreinfo={this.state.Moreinfo}
            onPressCancel={()=>{this.setVisibleTickets();this.initialiserValue()}}
            onPressSave={()=>{this.getFunction();this.initialiserValue()}}
            lastname={this.state.lastname}
            idCompte={this.state.idCompte}
            idContact={this.state.idContact}
            engagement={this.state.engagement}
            categorie={this.state.categorie}
          />
          <ModalSelectItemPriority
            modalVisibleItemTickets={this.state.modalVisibleItemPriority}
            onPressOption={this.onPressOptionPriority}
            priorite={'Select Priorité'}
            onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleTickets: true })}

          />
          <ModalSelectItemStatut
            modalVisibleItemTickets={this.state.modalVisibleItemStatut}
            onPressOption={this.onPressOptionStatut}
            select={'Select Statut'}
            onPressClose={() => this.setState({ modalVisibleItemStatut: false, modalVisibleTickets: true })}

          />
          <ModalSelectEngagement
            modalVisibleEngagement={this.state.modalVisibleEngagement}
            onPressOption={this.onPressOptionEngagement}
            select={'Select Statut'}
            onPressClose={() => this.setState({ modalVisibleEngagement: false, modalVisibleTickets: true })}

          />
          <ModalSelectCategorie
            modalVisibleCategorie={this.state.modalVisibleCategorie}
            onPressOption={this.onPressOptionCategorie}
            select={'Select catégorie'}
            onPressClose={() => this.setState({ modalVisibleCategorie: false, modalVisibleTickets: true })}

          />
          <ModalSelectContacts
            modalVisibleContacts={this.state.modalVisibleItemContacts}
            
            renderItem={this.renderItemContacts}
            onPressClose={() => this.setState({ modalVisibleItemContacts: false, modalVisibleTickets: true })}
          />
          <ModalSelectSociete
            modalVisibleCompte={this.state.modalVisibleCompte}
            
            renderItem={this.renderItemComptes}
            onPressClose={() => this.setState({ modalVisibleCompte: false, modalVisibleTickets: true })}
          />

        </View>

      )
    }

    else if (this.state.TabVisible == 'BGAffaires') {
      return (
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 9 }}>
            <FlatList
              data={this.state.dataAffaires}
              renderItem={this.renderElementAffaire}
              extraData={this.state.dataAffaires.length}
              keyExtractor={item => item.id}
            />
          </View>
          <View style={{ flex: 1 }}>
            <Provider>
              <Portal>
                <FAB.Group
                  open={this.state.open}
                  icon={'plus'}
                  color='white'
                  fabStyle={{ backgroundColor: '#53aefe' }}
                  actions={[
                    { icon: 'plus', onPress: () => console.log('Pressed add') },
                  ]}
                  onStateChange={() => { this.setState({ modalVisibleAffaires: true }) }}
                  onPress={() => { this.setState({ modalVisibleAffaires: true }) }}
                />
              </Portal>
            </Provider>
            <ModalAffaires
              AffaireModal={this.state.modalVisibleAffaires}
              onPressInfo={this.onPressInfo}
              MoreinfoText={this.state.MoreinfoText}
              MoreinfoIcon={this.state.MoreinfoIcon}
              Moreinfo={this.state.Moreinfo}
              onPressCancel={()=>{this.setVisibleAffaires();this.initialiserValue()}}
              onPressSave={()=>{this.getFunction();this.initialiserValue()}}
              
              onPressCompte={() => this.setState({ modalVisibleCompte: true, modalVisibleAffaires: false })}
              accountname={this.state.accountname}
              lastname={this.state.lastname}
              type={this.state.type}
              idCompte={this.state.idCompte}
              idContact={this.state.idContact}
              onPressPhase={() => this.setState({ modalVisibleItem: true, modalVisibleAffaires: false })}
              phase={this.state.phase}
              onPressContact={() => this.setState({ modalVisibleItemContacts: true, modalVisibleAffaires: false })}
              onPressType={() => this.setState({ modalVisibleType: true, modalVisibleAffaires: false })}
            />
            <ModalSelectSociete
              modalVisibleCompte={this.state.modalVisibleCompte}
              
              renderItem={this.renderItemComptes}
              onPressClose={() => this.setState({ modalVisibleCompte: false, modalVisibleAffaires: true })}
            />
            <ModalSelectContacts
              modalVisibleContacts={this.state.modalVisibleItemContacts}
              
              renderItem={this.renderItemContacts}
              onPressClose={() => this.setState({ modalVisibleItemContacts: false, modalVisibleAffaires: true })}
            />
            <ModalSelectItem
              modalVisibleItem={this.state.modalVisibleItem}
              onPressOption={this.onPressOption}
              select={'Select Item'}
              onPressClose={() => this.setState({ modalVisibleItem: false, modalVisibleAffaires: true })}

            />
            <ModalSelectType
              modalVisibleItem={this.state.modalVisibleType}
              onPressOption={this.onPressOptionType}
              onPressClose={() => this.setState({ modalVisibleType: false, modalVisibleAffaires: true })}
              select={'Select Item'}

            />
          </View>
        </View>)
    }
    else if (this.state.TabVisible == 'BGActivitie') {
      return (
        <View style={{ flex: 1, flexDirection: 'column', padding: 20 }}>
          <FlatList
            data={this.state.dataActivities}
            renderItem={this.renderElementActivities}
            keyExtractor={item => item.id}
          />

          <View style={{ flex: 2 }}>
            <Provider>
              <Portal>
                <FAB.Group
                  
                  icon={'plus'}
                  color='white'
                  fabStyle={{ backgroundColor: '#53aefe' }}
                  actions={[
                    { icon: 'plus', onPress: () => console.log('Pressed add') },
                  ]}
                  onStateChange={() => { this.setState({ modalVisibleActivities: true }) }}
                  onPress={() => { this.setState({ modalVisibleActivities: true }) }}
                />
              </Portal>
            </Provider>
          </View>
          <ModalActivities
                            modalVisibleActivities={this.state.modalVisibleActivities}
                            onPressPriority={() => this.setState({ modalVisibleItemPriority: true, modalVisibleActivities: false })}
                            onPressStatut={() => this.setState({ modalVisibleStatutActivitie: true, modalVisibleActivities: false })}
                            onPressItemContact={() => this.setState({ modalVisibleItemContacts: true, modalVisibleActivities: false })}
                            onPressCompte={() => this.setState({  modalVisibleActivities: true })}
                            onPressType={() => this.setState({ modalVisibleItemType: true, modalVisibleActivities: false })}
                            onPressCategorie={() => this.setState({ modalVisibleCategorie: true, modalVisibleActivities: false })}
                            onPressVisibilite={()=>this.setState({modalVisibleActivities:false,modalVisibleVisibilite:true})}
                            accountname={this.props.route.params.account}
                            selectPriority={this.state.priority}
                            selectStatut={this.state.statutActivitie}
                            selectType={this.state.type}
                            
                            onPressInfo={this.onPressInfo}
                            MoreinfoText={this.state.MoreinfoText}
                            MoreinfoIcon={this.state.MoreinfoIcon}
                            Moreinfo={this.state.Moreinfo}
                            onPressCancel={()=>{this.setState({modalVisibleActivities:false});this.initialiserValue()}}
                            onPressSave={()=>{this.getFunction();this.initialiserValue()}}
                            lastname={this.state.lastname}
                            idCompte={this.props.route.params.idCompte}
                            idContact={this.state.idContact}
                        />
                        <ModalSelectItemPriority
                            modalVisibleItemTickets={this.state.modalVisibleItemPriority}
                            onPressOption={this.onPressOptionPriority}
                            priorite={'Select Priorité'}
                            onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleActivities: true })}
                        />
                        <ModalSelectStatutActivitie
                        modalVisibleStatutActivitie={this.state.modalVisibleStatutActivitie}
                        onPressOption={this.onPressOptionStatut}
                        select={'Select Statut'}
                        onPressClose={() => this.setState({ modalVisibleStatutActivitie: false, modalVisibleActivities: true })}
                        />
                         <ModalVisibilite
                        modalVisibleVisibilite={this.state.modalVisibleVisibilite}
                        onPressOption={this.onPressOptionVisibilite}
                        select={'Select Visibilité'}
                        onPressClose={() => this.setState({ modalVisibleVisibilite: false, modalVisibleActivities: true })}
                        />
                        <ModalTypeActivities
                        modalVisibleTypeActivities={this.state.modalVisibleItemType}
                        onPressOption={this.onPressOptionType}
                        select={'Select Type'}
                        onPressClose={() => this.setState({ modalVisibleItemType: false, modalVisibleActivities: true })}
                        />
                       
                        <ModalSelectContacts
                        modalVisibleContacts={this.state.modalVisibleItemContacts}
                        
                        renderItem={this.renderItemContacts}
                        onPressClose={() => this.setState({ modalVisibleItemContacts: false, modalVisibleActivities: true })}
                        />
                        <ModalSelectSociete
                        modalVisibleCompte={this.state.modalVisibleCompte}
                        renderItem={this.renderItemComptes}
                        onPressClose={()=>{this.setState({modalVisibleCompte:false,modalVisibleActivities:true})}}
                        />     
        </View>)
    }
    else if (this.state.TabVisible == 'BGProjects') {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <FlatList
            data={this.state.dataProjects}
            renderItem={this.renderElementProjects}

            keyExtractor={item => item.socName} />
          <FAB
            style={styles.fab}
            icon="plus"
            color='white'
            onPress={() => this.setState({ modalVisibleProjects: true })}
          />
          <ModalProjects
            modalVisibleProjects={this.state.modalVisibleProjects}
            
            onPressSave={()=>{this.getFunction();this.initialiserValue()}}
            onPressCancel={()=>{this.setVisibleProjects();this.initialiserValue()}}
            idCompte={this.state.idCompte}
          />
        </View>

      )
    }
    else if (this.state.TabVisible == 'BGServ') {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <FlatList
            data={this.state.dataContrats}
            renderItem={this.renderElementServices}
            keyExtractor={item => item.socName}
          />
          <FAB
            style={styles.fab}
            icon="plus"
            color='white'
            onPress={() => this.setState({ modalVisibleContrat: true })}
          />
          <ModalAddContrat
            modalVisibleContrat={this.state.modalVisibleContrat}
            accountname={this.state.accountname}
            idCompte={this.state.idCompte}
            lastname={this.state.lastname}
            idContact={this.state.idContact}
            unite={this.state.unite}
            type={this.state.type}
            statut={this.state.statutContrat}
            priority={this.state.priority}
            onPressInfo={this.onPressInfo}
            MoreinfoText={this.state.MoreinfoText}
            MoreinfoIcon={this.state.MoreinfoIcon}
            Moreinfo={this.state.Moreinfo}
            onPressCancel={() => {this.setState({ modalVisibleContrat: false });this.initialiserValue()}}
            onPressSave={()=>{this.getFunction();this.initialiserValue()}}
            
            onPressType={() => this.setState({ modalVisibleType: true, modalVisibleContrat: false })}
            onPressUnite={() => this.setState({ modalVisibleUnite: true, modalVisibleContrat: false })}
            onPressStatut={() => this.setState({ modalVisibleItemStatut: true, modalVisibleContrat: false })}
            onPressPriority={() => this.setState({ modalVisibleItemPriority: true, modalVisibleContrat: false })}
            onPressCompte={() => { this.setState({ modalVisibleCompte: true, modalVisibleContrat: false }); console.log("pressed") }}
            onPressContact={() => this.setState({ modalVisibleItemContacts: true, modalVisibleContrat: false })}
          />
          <ModalSelectSociete
            modalVisibleCompte={this.state.modalVisibleCompte}
            
            renderItem={this.renderItemComptes}
            onPressClose={() => this.setState({ modalVisibleCompte: false, modalVisibleContrat: true })}
          />
          <ModalSelectContacts
            modalVisibleContacts={this.state.modalVisibleItemContacts}
            
            renderItem={this.renderItemContacts}
            onPressClose={() => this.setState({ modalVisibleItemContacts: false, modalVisibleContrat: true })}
          />
          <ModalSelectUnite
            modalVisibleUnite={this.state.modalVisibleUnite}
            onPressOption={this.onPressOptionUnite}
            onPressClose={() => this.setState({ modalVisibleUnite: false, modalVisibleContrat: true })}
            select={'Select Item'} />
          <ModalSelectItemPriority
            modalVisibleItemTickets={this.state.modalVisibleItemPriority}
            onPressOption={this.onPressOptionPriority}
            select={'Select Priorité'}
            onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleContrat: true })}

          />
          <ModalSelectItemType
            modalVisibleItem={this.state.modalVisibleType}
            onPressOption={this.onPressOptionType}
            select={'Select Type'}
            onPressClose={() => this.setState({ modalVisibleType: false, modalVisibleContrat: true })}

          />
          <ModalSelectItemStatut
            modalVisibleItemTickets={this.state.modalVisibleItemStatut}
            onPressOption={this.onPressOptionStatut}
            select={'Select Statut'}
            onPressClose={() => this.setState({ modalVisibleItemStatut: false, modalVisibleContrat: true })}

          />
        </View>
      )
    }
  
  }
  setDate = () => {
    this.setState({ isDate: true })
  }

  filter = (searchText) => {

    this.setState({
      searchText: searchText,
      filterSocieteDataSearch: this.state.societeData.filter(i =>
        i.socName.toUpperCase().includes(searchText.toUpperCase()))
    })

  }
  _pickDocument = async () => {
    let result = await DocumentPicker.getDocumentAsync({})
    this.setState({ cheminDoc: result.uri, nameFile: result.name })

    alert(result.uri);
    console.log(result);
  }
  choseEvent_or_task = () => {
    if (this.state.checked === 'task') {
      this.setState({
        TaskModal: true,
        taskevent: false,

      })
    } else {
      this.setState({
        EventModal: true,
        taskevent: false,
      })

    }
  }
  setTime = (date) => {
    this.setState({ time: date, istime: false })
  }
  setDateFunc = (date) => {
    this.setState({ date: date, isDate: false })
  }
  onPressInfoFunc = () => {
    if (this.state.TaskMoreinfo) {
      this.setState({
        TaskMoreinfo: false,
        TaskMoreinfoIcon: "chevron-down",
        TaskMoreinfoText: 'Plus'
      })
    } else {
      this.setState({
        TaskMoreinfo: true,
        TaskMoreinfoIcon: "chevron-up",
        TaskMoreinfoText: 'Moins'
      })
    }
  }
  //functions for modal task
  onPressOptiontask = () => { this.setState({ optionModal: true }); }
  onPressDateTask = () => { this.setState({ isDate: true }) }
  onPressVisible1Task = () => { this.setModalVisible1(true); }
  onPressTaskModal = () => { this.setState({ TaskModal: false }) }
  onPressTimeTask = () => { this.setState({ istime: true }) }
  //functions for modal docs
  onChangeTextDoc = (change) => this.setState({ docTitle: change })
  //functions Modal Projects
  onChangeTextProject = (change) => this.setState({ projectTitle: change })
  onPressAddProject = () => { this.addElementProjects(); this.setState({ modalVisibleProjects: false }) }



  render() {
    var lengthAffaires = 0;
    if (this.state.dataAffaires == undefined) {
      lengthAffaires = 0;
    }
    else {
      lengthAffaires = this.state.dataAffaires.length
    }
    return (

      <View style={{ flex: 1, flexDirection: 'column' }}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.loadingShow}
          onRequestClose={() => {
            setModalVisible(!this.state.loadingShow);
          }}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
          <Header
            placement="right"
            leftComponent={
              <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'stretch' }}>
                <Button icon={<Icon name="chevron-left" size={20} color='white' type='material-community' />} onPress={() => this.props.navigation.goBack()} />
                <Text style={{ fontSize: 22, color: 'white', fontWeight: '500', marginTop: 5 }}>Contacts:{this.props.route.params.title} </Text>
              </View>}

          />
        </View>
        <View style={{ flex: 1.2 }}>
          {/* <Text>scrollTabNavigation</Text> */}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.scroll}>

            <TabItem title='Résumé' scale={this.state.SRésumé} BackGround={this.state.BGRésumé} onPress={() => this.changecolor('Résumé')} />
            <TabItem title='Details' scale={this.state.SDetails} BackGround={this.state.BGDetails} onPress={() => this.changecolor('Details')} />
            <TabItem title={'Activities (' + this.state.dataActivities.length + ')'} scale={this.state.SActivitie} BackGround={this.state.BGActivitie} onPress={() => { this.changecolor('Activitie') }} />
            {(this.state.HelpDesk === true) && <TabItem title={'Tickets (' + this.state.dataTickets.length + ')'} scale={this.state.STickets} BackGround={this.state.BGTickets} onPress={() => this.changecolor('Tickets')} />}
            {(this.state.Potentials === true) && <TabItem title={'Affaires (' + this.state.dataAffaires.length + ')'} scale={this.state.SAffaires} BackGround={this.state.BGAffaires} onPress={() => this.changecolor('Affaires')} />}
            {(this.state.ServiceContracts === true) && <TabItem title={'Contrats (' + this.state.dataContrats.length + ')'} scale={this.state.SServ} BackGround={this.state.BGServ} onPress={() => { this.changecolor('Serv') }} />}

            {(this.state.Project === true) && <TabItem title={'Projects (' + this.state.dataProjects.length + ')'} scale={this.state.SProjects} BackGround={this.state.BGProjects} onPress={() => this.changecolor('Projects')} />}
          </ScrollView >

        </View>
        <SafeAreaView style={{ flex: 7, flexDirection: 'column' }}>
          {this.TabNavigation()}
        </SafeAreaView>
        

        <Overlay isVisible={this.state.modalVisibleDelete} >
          <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <Text>Voulez-vous supprimer cet objet?</Text>
          </View>
          <View style={{ flexDirection: 'row-reverse' }}>
            <Button title="Oui" titleStyle={{ color: 'black' }} buttonStyle={{ backgroundColor: "white" }} onPress={() => {
              this.setState({ loadingShow: true });
              fetch('https://portail.crm4you.ma/webservice.php', {
                method: 'POST',
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                body: 'operation=delete&sessionName=' + this.state.dataFet + '&id=' + this.state.id
              }); this.getFunction(); this.setState({ modalVisibleDelete: false })
            }}
            />
            <Button title="Non" titleStyle={{ color: 'black' }} buttonStyle={{ backgroundColor: "white" }} onPress={() => this.setState({ modalVisibleDelete: false })} />
          </View>
        </Overlay>
      </View>

    )

  }
}




