import React, { Component } from 'react';
import {SafeAreaView,Image,  TouchableOpacity, ScrollView,Text, View, Modal, FlatList} from 'react-native';
import Info from '../ItemsRender/info';
import {Header,Button,Icon ,Overlay}from 'react-native-elements';
import styles from '../styles'
import TabItem from '../ItemsRender/TabItem';
import ItemAffaire from '../ItemsRender/affaireitem';
import ModalInput from '../Modals/ModalsUpdate/Modalnput';
import ModalButton from '../Modals/ModalsUpdate/ModalButton';
import ModalDate from '../Modals/ModalsUpdate/ModalDate';
import ModalSelectProject from '../Modals/ModalSelectProject';
import ModalSelectItemType from '../Modals/ModalSelectItemType';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class DetailsMilestones extends Component {
    constructor(props) {
        super(props);
        this.state = {
          loadingShow: true,
          dataFet: [],
          link:[],
          //updates fields 
          chosen:'Select item',
          value:'',
          title:'',
          key:'',
          valeur:'',
          valeur2:'',
          //modals fiels
          priority:'Select priorite',
          statut:'',
          categorie:'',
          // updates modals
          modalVisibleInput:false,
          modalVisibleCategorie:false,
          modalVisibleDate:false,
          modalVisibleButton:false,
          modalVisibleItemPriority:false,
          modalVisibleType:false,
          modalVisibleStatut:false,
          modalVisibleProjects:false,
          //data   
          dataMilestones:[],
          open : false,
          //tabBG
          BGRésumé    : '#9ddfd3' , 
          BGDetails   : 'white' ,
          //scale
          SRésumé    : 0.8,
          SDetails   : 0.6,
          //tabVisible
          TabVisible: 'BGRésumé',
          //------------------
          Detail  : true ,
          BlocSys : true ,
          InfoP   : true ,
          Adress  : true ,
          Desc    : true ,
          Portal   : true ,
          //
          date : new Date(),
          time : new Date(),
          isDate : false ,
          istime : false ,
        }     
    } 
    initaliserStyle = () => {
      this.setState({
        BGRésumé       : 'white' , 
        BGDetails      : 'white' ,
        //scale
        SRésumé       : 0.8,
        SDetails      : 0.6,

      })
    }
    
    
   
    addElement=()=>{
       
      this.setState( (prevState)=>{prevState.data2.push({name:this.state.docTitle})}); 
      this.setState({modalVisibleDocuments:false})
      console.log(this.state.data2) ;
    }
    renderElementMiseaj = ({ item }) => {
      return(
      <TouchableOpacity
          onPress={() => {console.log("pressed: " + item.title)}}
        >
  
      <View style={styles.block}>
        
      <View style={styles.icon_container}>
        <Icon style={styles.icon}
          name= {item.icon}
          color= '#225fbf'
          type='font-awesome'/>
      </View>
      <View style={styles.details}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.description} >{item.description}</Text>
      </View>
      <View style={styles.date_container}>
        <Text style={styles.date}>{item.date} ago</Text>
      </View>
    </View>
    
      </TouchableOpacity>
    );}
    renderElementTasks=({item,index})=>{
      return(
      <View >
        <ItemAffaire  Name={item.projecttaskname} onPress={()=>this.props.navigation.navigate('DetailsTasks',{projectname:item.projecttaskname,id:item.id,data:this.state.dataTasks,index:index,result:this.state.dataFet})}/>
           
      </View>
   )}
   renderElementMilestones=({item,index})=>{
    return(
    <View >
      <ItemAffaire  Name={item.projectmilestonename} onPress={()=>this.props.navigation.navigate('DetailsTasks',{projectname:item.projecttaskname,id:item.id,data:this.state.dataTasks,index:index,result:this.state.dataFet})}/>
         
    </View>
 )}
   
   renderElementTickets=({item,index})=>{
    return(
     <View>
       <ItemAffaire Name={item.ticket_title}  onPress={()=>{this.props.navigation.navigate('DetailsTickets',{id:item.id,data:this.state.dataTickets,index:index,result:this.state.dataFet})}}/>
     </View>)
  }
    changecolor = (a) =>{
      this.initaliserStyle();
      this.setState({
        [`BG${a}`] : '#9ddfd3',
        [`S${a}`] : 0.8,
        TabVisible :`BG${a}` 
      }) ;
      }
    setmodalVisibleDocuments = (visible) => {
      this.setState({ modalVisibleDocuments: visible });
    }

    setModalVisible1 = (visible) => {
      this.setState({ modalVisible1: visible });
    }

    ChangeNomSociete = (soc) => {
        this.setState({nomSoc : soc})
    }
    TabNavigation = () => {
      var dataRoute=this.props.route.params.data[this.props.route.params.index]
   
    
      if(this.state.TabVisible == 'BGRésumé')     {
        return (
          <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
          <View style={styles.View_Champ_cle}>
            <View style={{ flex: 1, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
              <Text style={styles.textTitle}>
                Champ De Clé
                </Text>
            </View>
          </View>
            <View style={styles.content_champ_cle}>
                <Info title={'Nom du projet Milestone'} modifiable={true} borderBottom={0.6} content={dataRoute.projectmilestonename}  
                onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.projectmilestonename,key:'projectmilestonename',title:'Nom du projet Milestone'}) }}/>
                <Info title={'Date de Milestone'} modifiable={true} borderBottom={0.6} content={dataRoute.projectmilestonedate} 
                onPress={() => { this.setState({modalVisibleDate : true,valeur:dataRoute.projectmilestonedate,key:'projectmilestonedate',title:'Date de Milestone'}) }}/>
                <Info title={'Type'} modifiable={true} borderBottom={0.6} content={dataRoute.projectmilestonetype} 
               onPress={() => { this.setState({modalVisibleButton : true,valeur:dataRoute.projectmilestonetype,valeur2:dataRoute.projectmilestonetype,key:'projectmilestonetype',title:'Type'}) }}/>
              <View style={{ width: '100%', height: 70, backgroundColor: 'white', marginTop: 10 }}>
                <TouchableOpacity activeOpacity={0.7} style={styles.buttonFullDetails} onPress  ={() => {this.changecolor('Details')}} >
                  <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: 'white' }}>Full Details</Text>
                </TouchableOpacity>
              </View>
            </View>            
        </ScrollView>
        )
      } 
      else if(this.state.TabVisible == 'BGDetails') {
                return (
                <ScrollView contentContainerStyle={{alignItems: 'center', marginTop: 20 }}>
                    <View style={styles.View_Champ_cle}>
                        <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
                            <Text style={styles.textTitle}>
                            Détail Du Compte
                            </Text>
                        </View>
                        <TouchableOpacity style={[styles.align_center_view]} onPress={() => {this.setState({Detail : !this.state.Detail})}}>
                            {!this.state.Detail ?
                                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community'/>
                                :
                                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community'/>
                                }
                        </TouchableOpacity>
                    </View>
                    {this.state.Detail ?(
                        <View style={styles.content_champ_cle}>
                           <Info title={'Nom du projet Milestone'} modifiable={true} borderBottom={0.6} content={dataRoute.projectmilestonename} 
                           onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.projectmilestonename,key:'projectmilestonename',title:'Nom du projet Milestone'}) }}/>
                            <Info title={'Numéro du projet Milestone'} modifiable={false} borderBottom={0.6} content={dataRoute.projectmilestone_no}  />
                            <Info title={'Relatif à'} modifiable={true} borderBottom={0.6} content={this.state.idToName} 
                            onPress={() => { this.setState({modalVisibleButton : true,valeur:this.state.idToName,valeur2:dataRoute.projectid,key:'projectid',title:'Relatif'}) }}/>
                            <Info title={'Date de Milestone'} modifiable={true} borderBottom={0.6} content={dataRoute.projectmilestonedate} 
                           onPress={() => { this.setState({modalVisibleDate : true,valeur:dataRoute.projectmilestonedate,key:'projectmilestonedate',title:'Date de Milestone'}) }}/>
                            <Info title={'Type'} modifiable={true} borderBottom={0.6} content={dataRoute.projecttype} 
                            onPress={() => { this.setState({modalVisibleButton : true,valeur:dataRoute.projecttype,valeur2:dataRoute.projecttype,key:'projecttype',title:'Type'}) }}/>
                            <Info title={'Source'} modifiable={false} borderBottom={0.6} content={dataRoute.source} 
                            />
                            
                        </View>
                    ) :null}
                    <View style={{height :70}}></View>
                        <View style={styles.View_Champ_cle}>
                            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
                                <Text style={styles.textTitle}>
                                Informations personnalisées
                                </Text>
                            </View>
                            <TouchableOpacity style={[styles.align_center_view]} onPress={() => {this.setState({BlocSys : !this.state.BlocSys})}}>
                                {!this.state.InfoP ?
                                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community'/>
                                :
                                <Icon name="chevron-down" size={20} color='#1089ff'type='material-community' />
                                }
                            </TouchableOpacity>
                        </View>
                        {this.state.InfoP ? (
                        <View style={styles.content_champ_cle}>
                            <Info title={'Date de création'} modifiable={false} borderBottom={0} content={dataRoute.createdtime} onPress={() => { this.setState({modalModifier : true}) }}  />{/** pas encore fait?*/}
                            <Info title={'Date de modification'} modifiable={false} borderBottom={0} content={dataRoute.modifiedtime} onPress={() => { this.setState({modalModifier : true}) }}  />{/** pas encore fait?*/}
                        </View>
                        ):null}

                      
                        <View style={{height :70}}></View>
                        <View style={styles.View_Champ_cle}>
                            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
                                <Text style={styles.textTitle}>
                                Information Sur la description
                                </Text>
                            </View>
                            <TouchableOpacity style={[styles.align_center_view]} onPress={() => {this.setState({Desc : !this.state.Desc})}}>
                                {!this.state.Desc ?
                                <Icon name="chevron-up" size={20} color='#1089ff'type='material-community' />
                                :
                                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community'/>
                                }
                            </TouchableOpacity>
                        </View>
                        {this.state.Desc ? (
                        <View style={styles.content_champ_cle}>
                            <Info title={'Description'} modifiable={true} borderBottom={0.6} content={dataRoute.description} 
                               onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.description,key:'description',title:'Description'}) }}/>
                        </View>
                        ): null}
                </ScrollView>
                )    
                } 
                
      
  }
  
  getFunction = async () => { 
    this.setState({ loadingShow: true });
    var dataRoute=this.props.route.params.data[this.props.route.params.index]
    try{
      await AsyncStorage.multiGet(['SESSION', 'CRM_LINK'], async (err, res)=>{ 

        var dataFet= res[0][1];
        this.setState({dataFet: dataFet});
        var link = res[1][1];
        this.setState({link: link});
        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select * from Project where id='+dataRoute.projectid+' ;')
        .then((response) => response.json())
        .then((json)=>{this.setState({ loadingShow: false });this.setState({idToName:json.result[0].projectname}); })
        .catch((error)=>
        console.log(error))
      })
    }catch(e){
      console.log(e);
    }
  }

  componentDidMount(){
    this.getFunction();
  }

  renderItemProjects = ({ item,x }) => (             
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({modalVisibleProjects:false,modalVisibleButton:true,idProject:item.id,projectname:item.projectname,chosen:item.projectname,value:item.id,valeur:item.projectname,valeur2:item.id}) }}>
        <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
          <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>
              {String(item.projectname).slice(0,1)}
          </Text>
        </View>
        <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
          <Text style={{fontSize : 18}}>
          {item.projectname} 
          </Text>
        </View>
      </TouchableOpacity> 
    </View>
  );
  update=async(value)=>{ 
    this.setState({ loadingShow: true });
    var dataRoute=this.props.route.params.data[this.props.route.params.index]
    console.log('dataRoute '+JSON.stringify(dataRoute));
      dataRoute[this.state.key]=value;
      await fetch('https://portail.crm4you.ma/webservice.php', {
            method: 'POST',
            headers: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
           body:'operation=update&sessionName='+this.state.dataFet+'&element='+JSON.stringify(dataRoute)})
      .then((responses)  => responses.json()) 
      .then((json)=>{this.setState({ loadingShow: false ,valeur:'',valeur2:'',modalVisibleInput:false,modalVisibleDate:false,modalVisibleButton:false,chosen:'Select Item'});console.log(json);})
      .catch((error) => console.error(error)); 
      this.getFunction();     
    }
 
  onPressOptionType=(x)=>   
  {this.setState({modalVisibleButton:true,modalVisibleType:false,type:x,chosen:x,value:x,valeur:x,valeur2:x});}
    render(){
        return (

            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.loadingShow}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>
              <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={
                                        <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'stretch'}}>  
                                               <Button icon={ <Icon name="chevron-left" size={20} color='white' type='material-community'  />} onPress={()=>this.props.navigation.goBack()}/>                                                    
                                                <Text style={{fontSize:22,color:'white',fontWeight:'500',marginTop:5}}>Jalon du projet : {this.props.route.params.title}</Text>
                                        </View>}
                                       
                                />
                        </View>
                        <View style={{ flex: 1.2}}>
                          {/* <Text>scrollTabNavigation</Text> */}
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.scroll}>              
                              <TabItem   title='Résumé'       scale={this.state.SRésumé}    BackGround={this.state.BGRésumé}      onPress={()=>this.changecolor('Résumé')}/>
                              <TabItem   title='Details'      scale={this.state.SDetails}   BackGround={this.state.BGDetails}     onPress={()=>this.changecolor('Details')}/>
                            </ScrollView >      
                       
                    </View>
              <SafeAreaView style={{ flex: 7, flexDirection: 'column' }}>
                  {this.TabNavigation()}
              </SafeAreaView>
              <ModalInput 
                  modalVisibleInput={this.state.modalVisibleInput} 
                  title={this.state.title}
                  edit={'Edit Milestone'}
                  valeur={this.state.valeur}
                  onPressCancel={()=>{this.setState({modalVisibleInput:false,value:'',valeur:''})} }
                  onPressSave={this.update}
                />
                <ModalButton
                  modalVisibleButton={this.state.modalVisibleButton}  
                  title={this.state.title}
                  chosen={this.state.chosen} 
                  edit={'Edit Milestone'}
                  value={this.state.value}  
                  valeur={this.state.valeur} 
                  valeur2={this.state.valeur2}                 
                  onPressCancel={()=>this.setState({modalVisibleButton:false})} 
                  onPressSave={this.update}
                  onPressSelect={()=>{                
                    if(this.state.title==='Relatif')
                      {this.setState({
                        modalVisibleProjects:true,modalVisibleButton:false})}
                    if(this.state.title==='Type')
                      {this.setState({
                        modalVisibleType:true,modalVisibleButton:false})}
                   
                    }      
                                
                  }
                /> 
                <ModalDate 
                  modalVisibleDate={this.state.modalVisibleDate}  
                  title={this.state.title}
                  edit={'Edit Milestone'} 
                  valeur={this.state.valeur}
                  onPressCancel={()=>this.setState({modalVisibleDate:false,value:''})} 
                  onPressSave={this.update}
                  
                />
                <ModalSelectProject 
                  modalVisibleProject={this.state.modalVisibleProjects}                           
                  
                  renderItemProject={this.renderItemProjects}
                  onPressClose={()=>this.setState({modalVisibleProjects:false,modalVisibleButton:true})}
                /> 
                <ModalSelectItemType
                  modalVisibleItem={this.state.modalVisibleType}
                  onPressOption={this.onPressOptionType}
                  onPressClose={()=>this.setState({modalVisibleType:false,modalVisibleButton:true})}
                  select={'Select Item'}
                />

           
          </View>

        )
    
     }
    }


    

