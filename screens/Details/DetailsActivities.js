import React from 'react';
import { SafeAreaView } from 'react-native';
import { StyleSheet, View ,ScrollView,Text,TouchableOpacity,Modal,Image} from 'react-native';
import { Icon,Header,Button,Divider} from 'react-native-elements';
import Info from '../ItemsRender/info';
import ModalSelectContacts from '../Modals/ModalSelectContact';
import ModalSelectItemPriority from '../Modals/ModalSelectItemTickets';
import ModalSelectSociete from '../Modals/ModalSelectSociete';
import ModalSelectStatutActivitie from '../Modals/ModalSelectStatutActvitie';
import ModalButton from '../Modals/ModalsUpdate/ModalButton';
import ModalDate from '../Modals/ModalsUpdate/ModalDate';
import ModalInput from '../Modals/ModalsUpdate/Modalnput';
import ModalTypeActivities from '../Modals/ModalTypeActivities';
import ModalVisibilite from '../Modals/ModalVisibilite';
import ModalTime from '../Modals/ModalsUpdate/ModalTime';
import styles from '../styles';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class DetailsActivities extends React.Component{
    constructor(props){
        super(props);
        this.state={ 
            dataActivitie:[],
            Detail  : true ,
            BlocSys : true ,
            InfoP   : true ,
            Adress  : true ,
            Desc    : true ,
            link:'',
            dataFet:'',
            valeur:'',
            valeur2:'',
            loadingShow:true,
            modalVisibleItemPriority:false,
            modalVisibleStatutActivitie:false,
            modalVisibleVisibilite:false,
            modalVisibleType:false,
            modalVisibleCompte:false,
            modalVisibleItemContacts:false,
            modalVisibleTime:false,
            idToAccountname:'',
            idToLastname:'',
             //updates fields 
            chosen: 'Select Item',
            value: '',
            title: '',
            key: '',
            // updates modals
            modalVisibleInput: false,
            modalVisibleDate: false,
            modalVisibleButton: false,
     

      }}
      update = async (value) => {
        
        this.setState({ loadingShow: true });
        var dataRoute = this.props.route.params.data[this.props.route.params.index]
        dataRoute[this.state.key] = value;
        await fetch(this.state.link, {
          method: 'POST',
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          body: 'operation=update&sessionName=' + this.state.dataFet+ '&element=' + JSON.stringify(dataRoute)
        })
          .then((responses) => responses.json())
          .then((json) => { this.setState({ valeur:'',modalVisibleInput:false,modalVisibleDate:false,modalVisibleButton:false,modalVisibleTime:false,chosen: 'Select Item'  }); console.log(json)  })
          .then(this.getFunction)
          .catch((error) => console.error(error));
    
      }
             
      onPressOptionVisibilite = (val) => { this.setState({ modalVisibleActivities: true,  modalVisibleVisibilite: false, modalVisibleButton:true,visibilite: val,chosen:val,value:val,valeur:val,valeur2:val }); }
      onPressOptionType = (x) => { this.setState({ modalVisibleActivities: true, modalVisibleType: false, modalVisibleButton:true, type: x ,chosen:x,value:x,valeur2:x,valeur:x}); }
      onPressOptionPriority = (val) => { this.setState({ modalVisibleActivities: true, modalVisibleButton:true, modalVisibleItemPriority: false, priority: val ,chosen:val,value:val,valeur:val,valeur2:val}); }
      onPressOptionStatut = (val) => { this.setState({ modalVisibleActivities: true, modalVisibleButton:true,  modalVisibleStatutActivitie: false, statut: val ,chosen:val,value:val,valeur:val,valeur2:val}); }
      onPressCancelCompte = () => this.setState({ modalVisibleCompte: false, modalVisibleButton:true,   })
      onPressCancelContact = () => this.setState({ modalVisibleItemContacts: false, modalVisibleButton:true})
      getFunction = async () => {
        this.setState({ loadingShow: true });
        var dataRoute = this.props.route.params.data[this.props.route.params.index]
        var id = this.props.route.params.id;
        try {
          await AsyncStorage.multiGet(['SESSION', 'CRM_LINK', 'TYPES'], async (err, res) => {     
            var dataFet = res[0][1];
            this.setState({ dataFet: dataFet });
            var link = res[1][1];
           
            this.setState({ link: link });
            //this fetch is for Docs
            await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select * from Events ;')
            .then((response) => response.json())
              .then(async (json) => {
                this.setState({ dataActivitie: json.result });
                //Accounts
                if(dataRoute.parent_id !='')
                await fetch(this.state.link + '?operation=query&sessionName=' + dataFet + '&query=select * from Accounts where id=' + dataRoute.parent_id + ';')
                  .then((response) => response.json())
                  .then(async (json) => {

                    console.log('account after '+dataRoute['accountname'])
                    if (json.success == true && json.result != '') 
                          { this.setState({idToAccountname: json.result[0].accountname }) }
                    //this fetch is from Ticketss
                    if(dataRoute.contact_id!='')
                    {await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+ this.state.dataFet+ '&query=select * from Contacts where id='+dataRoute.contact_id+';')
                    .then((response) => response.json())
                    .then(async (json) => {
                      
                        if (json.success ==true && json.result != '') 
                              { this.setState({idToLastname: json.result[0].lastname }) ;}
                              
                      })
                  
                     
                      
                      .catch((error) =>
                      
                        console.log('erreur 1'))
                  }})
                  .catch((error) =>
                    console.log('erreur 2'))
              })
              .catch((error) =>
                console.log('erreur 3'))
          })
         this.setState({loadingShow:false})
        } catch (e) {
          console.log(e);
        }
        
      }
    
     
    renderItemComptes = ({ item, x }) => (

        <View style={styles.flatView}>
          <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({ modalVisibleCompte: false, modalVisibleButton: true,  idCompte: item.id, accountname: item.accountname, chosen: item.accountname, value: item.id,valeur:item.accountname,valeur2:item.id }) }}>
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
              <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
                {String(item.accountname).slice(0, 1)}
              </Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
              <Text style={{ fontSize: 18 }}>
                {item.accountname}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      );
      renderItemContacts = ({ item }) => (
        <View style={styles.flatView}>
          <TouchableOpacity style={styles.company} onPress={() => { this.setState({ modalVisibleButton: true, modalVisibleItemContacts: false, idContact: item.id, lastname: item.lastname,chosen:item.lastname,value:item.id,valeur:item.lastname,valeur2:item.id }) }}>
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
              <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
                {String(item.lastname).slice(0, 1)}
              </Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
              <Text style={{ fontSize: 18 }}>
                {item.lastname}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      );


    componentDidMount(){
        this.getFunction();
    }
        render(){
            var dataRoute=this.props.route.params.data[this.props.route.params.index]
            return(
                <View>
                   <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.loadingShow}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>
                    <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                            <Header
                                    placement="right"
                                    leftComponent={
                                    <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'stretch'}}>  
                                        <Button icon={ <Icon name="chevron-left" size={20} color='white' type='material-community'  />} onPress={()=>this.props.navigation.goBack()}/>                                                    
                                            <Text style={{fontSize:22,color:'white',fontWeight:'500',marginTop:5}}>Activities</Text>
                                    </View>}                         
                            />
                    </View>
                                             
           
                    <ScrollView contentContainerStyle={{ alignItems: 'center', marginTop: 20 }}>
                    <SafeAreaView style={{ flex: 7, flexDirection: 'column' }}>
                        <View style={styles.View_Champ_cle}>
                        <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
                                <Text style={styles.textTitle}>
                                Détail De l'Activitie
                                </Text>
                            </View>
                            <TouchableOpacity style={[styles.align_center_view]} onPress={() => {this.setState({Detail : !this.state.Detail})}}>
                                {!this.state.Detail ?
                                    <Icon name="chevron-up" size={20} color='#1089ff' type='material-community'/>
                                    :
                                    <Icon name="chevron-down" size={20} color='#1089ff' type='material-community'/>
                                    }
                            </TouchableOpacity>
                        </View>
                    {this.state.Detail ?(
                        <View style={styles.content_champ_cle}>
                            <Info title={"Sujet"} modifiable={true} borderBottom={0.6} content={dataRoute.subject} 
                            onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.subject,key: 'subject', title: 'Sujet' }) }} />
                            <Info title={"Type d'activité"} modifiable={true} borderBottom={0.6} content={dataRoute.recurringtype} 
                            onPress={() => { this.setState({ modalVisibleButton: true, valeur:dataRoute.recurringtype,valeur2:dataRoute.recurringtype, key: 'recurringtype', title: "Type d'activité" }) }} />
                              <Info title={'Statut'} modifiable={true} borderBottom={0.6} content={dataRoute.eventstatus} 
                            onPress={() => { this.setState({ modalVisibleButton: true, valeur:dataRoute.eventstatus,valeur2:dataRoute.eventstatus,key: 'eventstatus', title: 'Statut' }) }} />
                            <Info title={"Date  de début"} modifiable={true} borderBottom={0.6} content={dataRoute.date_start} 
                            onPress={() => { this.setState({ modalVisibleDate: true,valeur:dataRoute.date_start,key: 'date_start', title: 'Date  de début' }) }} />
                              <Info title={"Heure de début"} modifiable={true} borderBottom={0.6} content={dataRoute.time_start} 
                            onPress={() => { this.setState({ modalVisibleTime: true,valeur:dataRoute.time_start,key: 'time_start', title: 'Heure de début' }) }} />
                            <Info title={'Date de fin'} modifiable={true} borderBottom={0.6} content={dataRoute.due_date} 
                            onPress={() => { this.setState({ modalVisibleDate: true,valeur:dataRoute.due_date, key: 'due_date', title: 'Date de fin' }) }} />
                          
                            <Info title={'Heure de fin'} modifiable={true} borderBottom={0.6} content={dataRoute.time_end} 
                            onPress={() => { this.setState({ modalVisibleTime: true, valeur:dataRoute.time_end,key: 'time_end}', title: 'Heure de fin' }) }} /> 
                            <Info title={'Localisation'} modifiable={true} borderBottom={0.6} content={dataRoute.location} 
                            onPress={() => { this.setState({ modalVisibleInput: true,valeur:dataRoute.location, key: 'location', title: 'Localisation' }) }} />
                            <Info title={'Date de création'} modifiable={false} borderBottom={0.6} content={dataRoute.createdtime} 
                            />
                            <Info title={'Date de modification'} modifiable={false} borderBottom={0.6} content={dataRoute.modifiedtime} 
                             />
                            <Info title={'Priorité'} modifiable={true} borderBottom={0.6} content={dataRoute.taskpriority} 
                            onPress={() => { this.setState({ modalVisibleButton: true, valeur:dataRoute.taskpriority,valeur2:dataRoute.taskpriority,key: 'taskpriority', title: 'Priorité' }) }} />
                            <Info title={'Source'} modifiable={false} borderBottom={0.6} content={dataRoute.source} 
                            />
                        </View>):null}
                        <View style={{height :40}}></View>
                        <View style={styles.View_Champ_cle}>
                            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
                                <Text style={styles.textTitle}>
                                Lié à
                                </Text>
                            </View>
                            <TouchableOpacity style={[styles.align_center_view]} onPress={() => {this.setState({Desc : !this.state.Desc})}}>
                                {!this.state.Desc ?
                                <Icon name="chevron-up" size={20} color='#1089ff'type='material-community' />
                                :
                                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community'/>
                                }
                            </TouchableOpacity>
                        </View>
                        {this.state.Desc ? (
                        <View style={styles.content_champ_cle}>
                            <Info title={'Nom du contact'} modifiable={true} borderBottom={0.6} content={this.state.idToLastname} 
                            onPress={() => { this.setState({ modalVisibleButton: true, valeur:this.state.idToLastname,valeur2:dataRoute.contact_id,key: 'contact_id', title: 'Nom du contact' }) }} />
                            <Info title={'Relatif à'} modifiable={true} borderBottom={0.6} content={this.state.idToAccountname} 
                            onPress={() => { this.setState({ modalVisibleButton: true, valeur:this.state.idToAccountname,valeur2:dataRoute.parent_id,key: 'parent_id', title: 'Relatif à' }) }} />
                        </View>
                        ): null}
                        <View style={{height :70}}></View>
                        <View style={styles.View_Champ_cle}>
                            <View style={{ flex: 8, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
                            <Text style={styles.textTitle}>
                                Information Sur la description
                            </Text>
                            </View>
                            <TouchableOpacity style={[styles.align_center_view]} onPress={() => { this.setState({ Desc: !this.state.Desc }) }}>
                            {!this.state.Desc ?
                                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community' />
                                :
                                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community' />
                            }
                            </TouchableOpacity>
                        </View>

                        {this.state.Desc ? (
                            <View style={styles.content_champ_cle}>
                            <Info title={'Description'} modifiable={true} borderBottom={0} content={dataRoute.description}
                                onPress={() => { this.setState({ modalVisibleInput: true, valeur:dataRoute.description,key: 'description', title: 'Description' }) }} />
                            </View>
                        ) : null}

                        <View style={{ marginTop: 40 }}></View>
                       </SafeAreaView>
                       <ModalInput
                        modalVisibleInput={this.state.modalVisibleInput}
                        title={this.state.title}
                        edit={'Edit Activitie'}
                        valeur={this.state.valeur}
                        onPressCancel={() => { this.setState({ modalVisibleInput: false, value: '',valeur:'', }); }}
                        onPressSave={this.update}
                        />
                        <ModalDate
                        modalVisibleDate={this.state.modalVisibleDate}
                        title={this.state.title}
                        edit={'Edit Activitie'}
                        valeur={this.state.valeur}
                        onPressCancel={() => this.setState({ modalVisibleDate: false, value: '' })}
                        onPressSave={this.update}

                        />
                        <ModalTime
                        modalVisibleTime={this.state.modalVisibleTime}
                        title={this.state.title}
                        edit={'Edit Activitie'}
                        valeur={this.state.valeur}
                        onPressCancel={() => this.setState({ modalVisibleTime: false, value: '',valeur:'' })}
                        onPressSave={this.update}

                        />
                        <ModalButton
                        modalVisibleButton={this.state.modalVisibleButton}
                        title={this.state.title}
                        chosen={this.state.chosen}
                        edit={'Edit Activitie'}
                        value={this.state.value}
                        valeur={this.state.valeur}
                        valeur2={this.state.valeur2}
                        onPressCancel={() => this.setState({ modalVisibleButton: false, value: '',chosen:'Select Item',valeur2:'',valeur:'' })}
                        onPressSave={this.update}
                        onPressSelect={() => {
                            if (this.state.title == 'Statut') {
                            this.setState({
                                modalVisibleStatutActivitie: true, modalVisibleButton: false
                            })
                            }
                            if (this.state.title == 'Nom du contact') {
                                this.setState({
                                    modalVisibleItemContacts: true, modalVisibleButton: false
                                })
                                }
                            if (this.state.title == "Type d'activité") {
                            this.setState({
                                modalVisibleType: true, modalVisibleButton: false
                            })
                            }
                            if (this.state.title === 'Relatif à') {
                            this.setState({
                                modalVisibleCompte: true, modalVisibleButton: false
                            })
                            }
                            if (this.state.title === 'Visibilité') {
                                this.setState({
                                    modalVisibleVisibilite: true, modalVisibleButton: false
                                })
                                }
                            if (this.state.title === 'Priorité') {
                            this.setState({
                                modalVisibleItemPriority: true, modalVisibleButton: false
                            })
                            }
                        }

                        }
                        />
                        <ModalSelectItemPriority
                            modalVisibleItemTickets={this.state.modalVisibleItemPriority}
                            onPressOption={this.onPressOptionPriority}
                            priorite={'Normale'}
                            onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleActivities: true })}
                        />
                        <ModalSelectStatutActivitie
                        modalVisibleStatutActivitie={this.state.modalVisibleStatutActivitie}
                        onPressOption={this.onPressOptionStatut}
                        select={'Plannifiée'}
                        onPressClose={() => this.setState({ modalVisibleStatutActivitie: false, modalVisibleActivities: true })}
                        />
                         
                        <ModalTypeActivities
                        modalVisibleTypeActivities={this.state.modalVisibleType}
                        onPressOption={this.onPressOptionType}
                        select={'Mobile call'}
                        onPressClose={() => this.setState({ modalVisibleType: false, modalVisibleActivities: true })}
                        />
                       
                        <ModalSelectContacts
                        modalVisibleContacts={this.state.modalVisibleItemContacts}
                        
                        renderItem={this.renderItemContacts}
                        onPressClose={()=>{this.setState({modalVisibleItemContacts:false,modalVisibleActivities:true})}}
                        />
                        <ModalSelectSociete
                        modalVisibleCompte={this.state.modalVisibleCompte}
                        
                        renderItem={this.renderItemComptes}
                        onPressClose={()=>{this.setState({modalVisibleCompte:false,modalVisibleActivities:true})}}
                        />    
                        </ScrollView>
                        
                
            
                </View>
                      
            )
        }
}


  