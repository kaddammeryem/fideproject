import React, { Component } from 'react';
import {SafeAreaView,Image,  TouchableOpacity, ScrollView,Text, View, Modal, FlatList} from 'react-native';
import { FAB,} from 'react-native-paper';//component library
//import Icon from 'react-native-vector-icons/FontAwesome';//icons library
import Info from '../ItemsRender/info';
import {Header,Button,Icon ,Overlay}from 'react-native-elements';
import * as DocumentPicker from 'expo-document-picker';
import styles from '../styles';
import TabItem from '../ItemsRender/TabItem';
import ModalSelectItemType from '../Modals/ModalSelectItemType';
import ModalSelectItemStatut from '../Modals/ModalSelectItemStatut';
import ModalButton from '../Modals/ModalsUpdate/ModalButton';
import ModalDate from '../Modals/ModalsUpdate/ModalDate';
import ModalInput from '../Modals/ModalsUpdate/Modalnput';
import ModalSelectItemPriority from '../Modals/ModalSelectItemTickets';
import ModalSelectContacts from '../Modals/ModalSelectContact';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class DetailsContrats extends Component {
    constructor(props) {
        super(props);
        this.state = {
      Documents: false,
          loadingShow: true,
          dataFet:[],
          link: [],
          valeur:'' ,
          valeur2:'',
          //update fileds
          chosen:'Select Item',
          key:'',
          title:'',
          value:'',
          //modalsupdate

          modalVisibleItemPriority:false,
          modalVisibleItemStatut:false,
          modalVisibleCompte:false,
          modalVisibleContacts:false,
          modalVisibleItemContacts:false,
          modalVisibleType:false,
          modalVisibleProjects:false,   
          modalVisibleButton:false,
          modalVisibleInput:false,
          modalVisibleDate:false,  
          //data   
          dataProjects:[],
          dataDocuments:[],
          dataContrats:[],
          dataMiseaj:[{
            id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
            title: 'First Item',
            description: 'Kamal commented on your document',
            date: '3days',
            icon: 'address-book-o'
          },
          {
            id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
            title: 'Second Item',
            description: 'Aya shared your document with DEMO',
            date: '3days',
            icon: 'cubes'
          },
          {
            id: '58694a0f-3da1-471f-bd96-145571e29d72',
            title: 'Third Item',
            description: 'Ahmed added a new contact',
            date: '3days',
            icon: 'bell-o'
          },],
          dataContacts:[],
          //modal create
          modalVisibleDocuments:false,
          modalVisibleDelete:false,
          modalVisibleServices:false,
          idToLastname:'',
          //tabBG
          BGDetails   : '#9ddfd3' ,
          BGMises     : 'white' ,
          BGDoc       : 'white' ,
          //scale
          SDetails   : 0.8, 
          SMises     : 0.6,
          SDoc       : 0.6,
          //tabVisible
          TabVisible: 'BGDetails',
          //------------------
          Detail  : true ,
          BlocSys : true ,
          InfoP   : true ,
          Adress  : true ,
          Desc    : true ,
          Portal   : true ,
          //
         
        }
    }
 
    initaliserStyle = () => {
      this.setState({
        BGDetails      : 'white' ,
        BGMises        : 'white' ,
        BGDoc          : 'white' ,
        //scale
        SDetails      : 0.8,
        SMises        : 0.6,
        SDoc          : 0.6,
      })

    }  
    renderElementMiseaj = ({ item }) => {
      return(
      <TouchableOpacity
          onPress={() => {console.log("pressed: " + item.title)}}
        >
  
      <View style={styles.block}>
        
      <View style={styles.icon_container}>
        <Icon style={styles.icon}
          name= {item.icon}
          color= '#225fbf'
          type='font-awesome'/>
      </View>
      <View style={styles.details}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.description} >{item.description}</Text>
      </View>
      <View style={styles.date_container}>
        <Text style={styles.date}>{item.date} ago</Text>
      </View>
    </View>
    
      </TouchableOpacity>
    );}
    changecolor = (a) =>{
      this.initaliserStyle();
      this.setState({
        [`BG${a}`] : '#9ddfd3',
        [`S${a}`] : 0.8,
        TabVisible :`BG${a}` 
      }) ;
      }    

    getFunction = async () => {
      this.setState({ loadingShow: true }); 
        var dataRoute=this.props.route.params.data[this.props.route.params.index]
        //this fetch is for Contacts
        try{
          await AsyncStorage.multiGet(['SESSION', 'CRM_LINK', 'TYPES'], async (err, res) => {
            types = res[2][1].split(',');
            console.log(types);
            if (types.includes('Documents')) this.setState({ Documents: true });
            var dataFet= res[0][1];
            this.setState({dataFet: dataFet});
            var link = res[1][1];
            this.setState({link: link});
            await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select * from Contacts where id='+dataRoute.sc_related_to+' ;')
            .then((response) => response.json())
            .then((json)=>{
              if (json.success == true && json.result != '') 
              { this.setState({ idToLastname: json.result[0].lastname }) } ;this.setState({loadingShow:false})

            })
           
              .catch((error)=>
              console.log(error))
            })
          }catch(e){
            console.log(e);
          }
              
        }
    async componentDidMount(){
        this.getFunction();
    }
    update=async(value)=>{
      console.log('ehre update')
      this.setState({ loadingShow: true });
      var dataRoute=this.props.route.params.data[this.props.route.params.index]
        dataRoute[this.state.key]=value;
        await fetch('https://portail.crm4you.ma/webservice.php', {
              method: 'POST',
              headers: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
             body:'operation=update&sessionName='+this.state.dataFet+'&element='+JSON.stringify(dataRoute)})
        .then((responses)  => responses.json()) 
        .then((json)=>{this.setState({ loadingShow: false,valeur:'',valeur2:'',modalVisibleInput:false,modalVisibleDate:false,modalVisibleButton:false });console.log('here json '+JSON.stringify(json));this.setState({chosen:'Select Item'})})
        .catch((error) => console.error(error));
        this.getFunction();
        
       
      }

    TabNavigation = () => {
      var dataRoute=this.props.route.params.data[this.props.route.params.index]
    
   if(this.state.TabVisible == 'BGDetails') {
                return (
                <ScrollView contentContainerStyle={{alignItems: 'center', marginTop: 20 }}>
                    <View style={styles.View_Champ_cle}>
                        <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
                            <Text style={styles.textTitle}>
                            Informations sur le contrat de service
                            </Text>
                        </View>
                        <TouchableOpacity style={[styles.align_center_view]} onPress={() => {this.setState({Detail : !this.state.Detail})}}>
                            {!this.state.Detail ?
                                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community'/>
                                :
                                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community'/>
                                }
                        </TouchableOpacity>
                    </View>
                    {this.state.Detail ?(
                        <View style={styles.content_champ_cle}>
                            <Info title={'Sujet'} modifiable={true} borderBottom={0.6} content={dataRoute.subject} 
                            onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.subject,key:'subject',title:'Sujet'}) }}/>
                            <Info title={'Contrat N°'} modifiable={false} borderBottom={0.6} content={dataRoute.contract_no} />
                            <Info title={'Relatif à'} modifiable={true} borderBottom={0.6} content={this.state.idToLastname} 
                            onPress={() => { this.setState({modalVisibleButton : true,valeur:this.state.idToLastname,valeur2:dataRoute.sc_related_to,key:'sc_related_to',title:'Relatif'}) }}/>
                            <Info title={'Type'} modifiable={true} borderBottom={0.6} content={dataRoute.contract_type} 
                            onPress={() => { this.setState({modalVisibleButton : true,valeur2:dataRoute.contract_type,valeur2:dataRoute.contract_type,key:'contract_type',title:'Type'}) }}/>
                            <Info title={'Unités de suivi'} modifiable={true} borderBottom={0.6} content={dataRoute.tracking_unit} 
                            onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.tracking_unit,key:'tracking_unit',title:'Unités de suivi'}) }}/>
                            <Info title={'Date de debut'} modifiable={true} borderBottom={0.6} content={dataRoute.start_date} 
                            onPress={() => { this.setState({modalVisibleDate : true,valeur:dataRoute.start_date,key:'start_date',title:'Date de debut'}) }}/>
                            <Info title={'Unités totales'} modifiable={true} borderBottom={0.6} content={dataRoute.total_units} 
                            onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.total_units,key:'total_units',title:'Unités totales'}) }}/>
                            <Info title={'Due date'} modifiable={true} borderBottom={0.6} content={dataRoute.due_date} 
                            onPress={() => { this.setState({modalVisibleDate : true,valeur:dataRoute.due_date,key:'due_date',title:'Due date'}) }}/>
                            <Info title={"Unités d'occasion"} modifiable={true} borderBottom={0.6} content={dataRoute.used_units} 
                            onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.used_units,key:'used_units',title:"Unités d'occasion"}) }}/>
                            <Info title={'Date de fin'} modifiable={true} borderBottom={0.6} content={dataRoute.end_date} 
                            onPress={() => { this.setState({modalVisibleDate : true,valeur:dataRoute.end_date,key:'end_date',title:'Date de fin'}) }}/>
                            <Info title={'Statut'} modifiable={true} borderBottom={0.6} content={dataRoute.contract_status} 
                            onPress={() => { this.setState({modalVisibleButton : true,valeur:dataRoute.contract_status,valeur2:dataRoute.contract_status,key:'contract_status',title:'Statut'}) }}/>
                            <Info title={'Durée estimée(en jours)'} modifiable={true} borderBottom={0.6} content={dataRoute.planned_duration} 
                            onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.planned_duration,key:'planned_duration',title:'Durée estimée(en jours)'}) }}/>
                            <Info title={'Priorité'} modifiable={true} borderBottom={0.6} content={dataRoute.contract_priority} 
                            onPress={() => { this.setState({modalVisibleButton : true,valeur:dataRoute.contract_priority,valeur2:dataRoute.contract_priority,key:'contract_priority',title:'Priorite'}) }}/>
                            <Info title={'Durée actuelle(en jours)'} modifiable={true} borderBottom={0.6} content={dataRoute.actual_duration} 
                            onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.actual_duration,key:'actual_duration',title:'Durée actuelle(en jours)'}) }}/>
                            <Info title={'Progression(en %)'} modifiable={true} borderBottom={0.6} content={dataRoute.progress} 
                            onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.progress,key:'progress',title:'Progression(en %)'}) }}/>
                            <Info title={'Date de création'} modifiable={false} borderBottom={0.6} content={dataRoute.createdtime} />
                            <Info title={'Date de modification'} modifiable={false} borderBottom={0.6} content={dataRoute.modifiedtime} />
                            <Info title={'Source'} modifiable={false} borderBottom={0.6} content={dataRoute.source} />

                        </View>
                    ) :null}
                     <ModalButton 
                  modalVisibleButton={this.state.modalVisibleButton}  
                  title={this.state.title}
                  chosen={this.state.chosen} 
                  edit={'Edit Contrats'}
                  value={this.state.value}  
                  valeur={this.state.valeur}       
                  valeur2={this.state.valeur2}             
                  onPressCancel={()=>this.setState({modalVisibleButton:false,value:'',chosen:'Select Item',valeur:'',valeur2:''})} 
                  onPressSave={this.update}
                  onPressSelect={()=>{
                    if(this.state.title=='Statut')
                    {this.setState({
                    modalVisibleItemStatut:true,modalVisibleButton:false})
                    }
                    if(this.state.title=='Type')
                      {this.setState({
                        modalVisibleType:true,modalVisibleButton:false})
                    }
                    if(this.state.title==='Relatif')
                      {this.setState({
                      modalVisibleItemContacts:true,modalVisibleButton:false})}
                    if(this.state.title==='Priorite')
                      {this.setState({
                      modalVisibleItemPriority:true,modalVisibleButton:false})}
                    }      
                                
                  }
                />              
                <ModalSelectItemStatut
                  modalVisibleItemTickets={this.state.modalVisibleItemStatut}
                  onPressOption={this.onPressOptionStatut}
                  select={'Select Statut'}
                  onPressClose={()=>this.setState({modalVisibleItemStatut:false,modalVisibleButton:true})}

                />
                 <ModalSelectItemType
                  modalVisibleItem={this.state.modalVisibleType}
                  onPressOption={this.onPressOptionType}
                  onPressClose={()=>this.setState({modalVisibleType:false,modalVisibleButton:true})}
                  select={'Select Type'}
                />      
                <ModalSelectContacts 
                  modalVisibleContacts={this.state.modalVisibleItemContacts}                           
                  
                  renderItem={this.renderItemContacts}
                  onPressClose={()=>this.setState({modalVisibleItemContacts:false,modalVisibleButton:true})}
                />
                <ModalSelectItemPriority
                  modalVisibleItemTickets={this.state.modalVisibleItemPriority}
                  onPressOption={this.onPressOptionPriority}
                  priorite={'Select Priorité'}
                  onPressClose={()=>this.setState({modalVisibleItemPriority:false,modalVisibleButton:true})}
                />
                </ScrollView>
                )               
                } 

  }
  renderItemContacts = ({ item}) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {this.setState({modalVisibleButton:true,modalVisibleItemContacts:false,idContact:item.id,lastname:item.lastname,chosen:item.lastname,value:item.id,valeur:item.lastname,valeur2:item.id}) }}>
        <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
          <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>
          {String(item.lastname).slice(0,1)}            
          </Text>
        </View>
        <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
          <Text style={{fontSize : 18}}>
          {item.lastname }
          </Text>
        </View>
      </TouchableOpacity> 
    </View>
  );
    
  onPressOptionPriority=(val)=>   
  {this.setState({modalVisibleButton:true,modalVisibleItemPriority:false,chosen:val,value:val,valeur:val,valeur2:val});}
  onPressOptionStatut=(val)=>   
  {this.setState({modalVisibleItemStatut:false,modalVisibleButton:true,chosen:val,value:val,valeur:val,valeur2:val});}
  onPressOptionType=(x)=>   
  {this.setState({modalVisibleButton:true,modalVisibleType:false,type:x,value:x,chosen:x,valeur:x,valeur2:x});}

    render(){
        return (

            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.loadingShow}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>
              <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={
                                        <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'stretch'}}>  
                                               <Button icon={ <Icon name="chevron-left" size={20} color='white' type='material-community'  />} onPress={()=>this.props.navigation.goBack()}/>                                                    
                                                <Text style={{fontSize:22,color:'white',fontWeight:'500',marginTop:5}}>Contrats de Services : {this.props.route.params.title}</Text>
                                        </View>}
                                       
                                />
                        </View>
                <View style={{ flex: 1.2 }}>
                <View style={{ flex: 1.2}}>
                          {/* <Text>scrollTabNavigation</Text> */}
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.scroll}>              
                              <TabItem   title='Details'      scale={this.state.SDetails}   BackGround={this.state.BGDetails}     onPress={()=>this.changecolor('Details')}/>
                            </ScrollView >      
                       
                    </View>
              </View>
              <SafeAreaView style={{ flex: 7, flexDirection: 'column' }}>
                  {this.TabNavigation()}
              </SafeAreaView>
              <ModalInput 
                  modalVisibleInput={this.state.modalVisibleInput} 
                  title={this.state.title}
                  edit={'Edit Contrats'}
                  valeur={this.state.valeur}
                  onPressCancel={()=>{this.setState({modalVisibleInput:false,value:'',valeur:''}); } }
                  onPressSave={this.update}
                />
                <ModalDate 
                  modalVisibleDate={this.state.modalVisibleDate}  
                  title={this.state.title} 
                  edit={'Edit Contrats'}
                  valeur={this.state.valeur}
                  onPressCancel={()=>this.setState({modalVisibleDate:false,value:''})} 
                  onPressSave={this.update}
                  
                />
               
                      

          </View>

        )
    
     }
    }


    

