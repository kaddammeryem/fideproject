import React, { Component } from 'react';
import {SafeAreaView, TouchableOpacity, ScrollView,Text, View, FlatList, Modal, Image} from 'react-native';
import { FAB} from 'react-native-paper';//component library
//import Icon from 'react-native-vector-icons/FontAwesome';//icons library
import Info from '../ItemsRender/info';
import {Header,Button,Icon}from 'react-native-elements';
import * as DocumentPicker from 'expo-document-picker';
import styles from '../styles'
import Comments from '../ItemsRender/comment';
import TabItem from '../ItemsRender/TabItem';
import ModalSelectItemPriority from '../Modals/ModalSelectItemTickets';
import ModalSelectProject from '../Modals/ModalSelectProject';
import ModalDate from '../Modals/ModalsUpdate/ModalDate';
import ModalButton from '../Modals/ModalsUpdate/ModalButton';
import ModalInput from '../Modals/ModalsUpdate/Modalnput';
import ModalSelectItemStatut from '../Modals/ModalSelectItemStatut';
import ModalSelectCategorie from '../Modals/ModalSelectCategorie';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class DetailsTasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
          Documents: false, 
          loadingShow: true,
          dataFet:[],
          link:[],
          //updates fields 
          chosen:'Select item',
          value:'',
          title:'',
          key:'',
          valeur:'',
          valeur2:'',
          //modals fiels
          priority:'Select priorite',
          statut:'',
          categorie:'',
          idToName:'',
          // updates modals
          modalVisibleInput:false,
          modalVisibleCategorie:false,
          modalVisibleDate:false,
          modalVisibleButton:false,
          modalVisibleItemPriority:false,
          modalVisibleType:false,
          modalVisibleStatut:false,
          modalVisibleProjects:false,
          //data     
          dataDocuments:[],
          //tabBG 
          BGDetails   : '#9ddfd3' ,
          BGMises     : 'white' ,
          BGTasks : 'white' ,
          BGDoc       : 'white' ,
          BGComnt     : 'white',
          BGTickets     : 'white',
          //scale
          SDetails   : 0.8 ,
          SMises     : 0.6 ,
          STasks : 0.6 ,
          SDoc       : 0.6 ,
          SComnt     : 0.6 ,
          STickets     : 0.6 ,
          //tabVisible
          TabVisible: 'BGDetails',
          //------------------
          Detail  : true ,
          BlocSys : true ,
          InfoP   : true ,
          Adress  : true ,
          Desc    : true ,
          Portal   : true ,
        }
      
    }
 
    initaliserStyle = () => {
      this.setState({
        BGDetails      : 'white' ,
        BGMises        : 'white' ,
        BGTasks       : 'white' ,
        BGDoc          : 'white' ,
        BGComnt        : 'white' ,
        BGTickets       : 'white' ,
        //scale
        SDetails      : 0.8 ,
        SMises        : 0.6 ,
        SContacts     : 0.6 ,
        STasks    : 0.6 ,
        SDoc          : 0.6 ,
        SComnt        : 0.6 ,
        STickets       : 0.6 ,

      })

    }
    renderElementMiseaj = ({ item }) => {
      return(
      <TouchableOpacity
          onPress={() => {console.log("pressed: " + item.title)}}
        >
  
      <View style={styles.block}>
        
      <View style={styles.icon_container}>
        <Icon style={styles.icon}
          name= {item.icon}
          color= '#225fbf'
          type='font-awesome'/>
      </View>
      <View style={styles.details}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.description} >{item.description}</Text>
      </View>
      <View style={styles.date_container}>
        <Text style={styles.date}>{item.date} ago</Text>
      </View>
    </View>
    
      </TouchableOpacity>
    );}
    changecolor = (a) =>{
      this.initaliserStyle();
      this.setState({
        [`BG${a}`] : '#9ddfd3',
        [`S${a}`] : 0.8,
        TabVisible :`BG${a}` 
      }) ;
    }
    update=async(value)=>{ 
      this.setState({ loadingShow: true });
      var dataRoute=this.props.route.params.data[this.props.route.params.index]
      console.log('dataRoute '+JSON.stringify(dataRoute));
      dataRoute[this.state.key]=value;
        await fetch('https://portail.crm4you.ma/webservice.php', {
              method: 'POST',
              headers: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
             body:'operation=update&sessionName='+this.state.dataFet+'&element='+JSON.stringify(dataRoute)})
        .then((responses)  => responses.json()) 
        .then((json)=>{this.setState({ loadingShow: false,valeur:'',valeur2:'',modalVisibleInput:false,modalVisibleDate:false,modalVisibleButton:false ,chosen:'Select Item'});console.log(json);t})
        .catch((error) => console.error(error)); 
        this.getFunction();     
      }

      getFunction = async () => { 
        this.setState({ loadingShow: true });
        var dataRoute=this.props.route.params.data[this.props.route.params.index]
        try{
          await AsyncStorage.multiGet(['SESSION', 'CRM_LINK', 'TYPES'], async (err, res) => {
            types = res[2][1].split(',');
            console.log(types);
            if (types.includes('Documents')) this.setState({ Documents: true });
            
            var dataFet= res[0][1];
            this.setState({dataFet: dataFet});
            var link = res[1][1];
            this.setState({link: link});
            await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select * from Project where id='+dataRoute.projectid+' ;')
            .then((response) => response.json())
            .then((json)=>{this.setState({ loadingShow: false });this.setState({idToName:json.result[0].projectname}); })
            .catch((error)=>
            console.log(error))
          })
        }catch(e){
          console.log(e);
        }
      }
            
            componentDidMount(){
        this.getFunction();
      }
   
    TabNavigation = () => {
      var dataRoute=this.props.route.params.data[this.props.route.params.index]
        if(this.state.TabVisible == 'BGDetails') {
                return (
                <ScrollView contentContainerStyle={{alignItems: 'center', marginTop: 20 }}>
                    <View style={styles.View_Champ_cle}>
                        <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
                            <Text style={styles.textTitle}>
                            Détail Du Compte
                            </Text>
                        </View>
                        <TouchableOpacity style={[styles.align_center_view]} onPress={() => {this.setState({Detail : !this.state.Detail})}}>
                            {!this.state.Detail ?
                                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community'/>
                                :
                                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community'/>
                                }
                        </TouchableOpacity>
                    </View>
                    {this.state.Detail ?(
                        <View style={styles.content_champ_cle}>
                          <Info title={'Projet Nom de le tache'} modifiable={true} borderBottom={0.6} content={dataRoute.projecttaskname} 
                           onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.projecttaskname,key:'projecttaskname',title:'Projet Nom de le tache'}) }}/>
                          <Info title={'Projet Tache'} modifiable={false} borderBottom={0.6} content={dataRoute.projecttask_no}  />                          
                          <Info title={'Priorité'} modifiable={true} borderBottom={0.6} content={dataRoute.projecttaskpriority} 
                            onPress={() => { this.setState({modalVisibleButton : true,valeur:dataRoute.projecttaskpriority,valeur2:dataRoute.projecttaskpriority,key:'projecttaskpriority',title:'Priorite'}) }}/>
                          <Info title={'Catégorie'} modifiable={true} borderBottom={0.6} content={dataRoute.projecttasktype} 
                            onPress={() => { this.setState({modalVisibleButton : true,valeur:dataRoute.projecttasktype,valeur2:dataRoute.projecttasktype,key:'projecttasktype',title:'Categorie'}) }}/>
                          <Info title={'Numero du projet de travail'} modifiable={true} borderBottom={0.6} content={dataRoute.targetenddate} 
                            onPress={() => { this.setState({modalVisibleDate : true,valeur:dataRoute.targetenddate,key:'targetenddate',title:'Numero du projet de travail'}) }}/>
                          <Info title={'Relatif à'} modifiable={true} borderBottom={0.6} content={this.state.idToName} 
                            onPress={() => { this.setState({modalVisibleButton : true,valeur:this.state.idToName,valeur2:dataRoute.projectid,key:'projectid',title:'Relatif à'}) }}/>
                         
                          <Info title={'Source'} modifiable={false} borderBottom={0.6} content={dataRoute.source} />
                          <Info title={'Statut'} modifiable={true} borderBottom={0.6} content={dataRoute.projecttaskstatus} 
                            onPress={() => { this.setState({modalVisibleButton : true,valeur:dataRoute.projecttaskstatus,valeur2:dataRoute.projecttaskstatus,key:'projecttaskstatus',title:'Statut'}) }}/>
                        </View>
                    ) :null}
                    <View style={{height :70}}></View>
                        <View style={styles.View_Champ_cle}>
                            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
                                <Text style={styles.textTitle}>
                                Informations personnalisées
                                </Text>
                            </View>
                            <TouchableOpacity style={[styles.align_center_view]} onPress={() => {this.setState({BlocSys : !this.state.BlocSys})}}>
                                {!this.state.InfoP ?
                                <Icon name="chevron-up" size={20} color='#1089ff' type='material-community'/>
                                :
                                <Icon name="chevron-down" size={20} color='#1089ff'type='material-community' />
                                }
                            </TouchableOpacity>
                        </View>
                        {this.state.InfoP ? (
                        <View style={styles.content_champ_cle}>
                            <Info title={'Progress'} modifiable={true} borderBottom={0.6} content={dataRoute.projecttaskprogress} 
                            onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.projecttaskprogress,key:'projecttaskprogress',title:'Progress'}) }}/>
                            <Info title={'Heures travaillées'} modifiable={true} borderBottom={0.6} content={dataRoute.projecttaskhours} 
                            onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.projecttaskhours,key:'projecttaskhours',title:'Heures travaillées'}) }}/>
                            <Info title={'Date de début'} modifiable={true} borderBottom={0.6} content={dataRoute.startdate} 
                            onPress={() => { this.setState({modalVisibleDate : true,valeur:dataRoute.startdate,key:'startdate',title:'Date de début'}) }}/>
                            <Info title={'Date de fin'} modifiable={true} borderBottom={0.6} content={dataRoute.enddate} 
                            onPress={() => { this.setState({modalVisibleDate : true,valeur:dataRoute.enddate,key:'enddate',title:'Date de fin'}) }}/>
                            <Info title={'Heure de création'} modifiable={false} borderBottom={0.6} content={dataRoute.createdtime} />
                            <Info title={'Heure de modification'} modifiable={false} borderBottom={0.6} content={dataRoute.modifiedtime} />
                        </View>
                        ):null}

                      
                        <View style={{height :70}}></View>
                        <View style={styles.View_Champ_cle}>
                            <View style={{ flex: 7, marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>
                                <Text style={styles.textTitle}>
                                Information Sur la description
                                </Text>
                            </View>
                            <TouchableOpacity style={[styles.align_center_view]} onPress={() => {this.setState({Desc : !this.state.Desc})}}>
                                {!this.state.Desc ?
                                <Icon name="chevron-up" size={20} color='#1089ff'type='material-community' />
                                :
                                <Icon name="chevron-down" size={20} color='#1089ff' type='material-community'/>
                                }
                            </TouchableOpacity>
                        </View>
                        {this.state.Desc ? (
                        <View style={styles.content_champ_cle}>
                            <Info title={'Description'} modifiable={true} borderBottom={0.6} content={dataRoute.description} 
                            onPress={() => { this.setState({modalVisibleInput : true,valeur:dataRoute.description, key:'description',title:'Description'}) }}/>
                        </View>
                        ): null}
                </ScrollView>
                )    
                } 
  
  }

  onPressOptionPriority=(val)=>   
{this.setState({modalVisibleButton:true,modalVisibleItemPriority:false,priority:val,chosen:val,value:val,valeur:val,valeur2:val});}
onPressOptionStatut=(val)=>   
{this.setState({modalVisibleButton:true,modalVisibleStatut:false,statut:val,chosen:val,value:val,valeur:val,valeur2:val});}
onPressOptionCategorie=(val)=>   
{this.setState({modalVisibleButton:true,modalVisibleCategorie:false,categorie:val,chosen:val,value:val,valeur:val,valeur2:val});}
renderItemProjects = ({ item,x }) => (             
  <View style={styles.flatView}>
    <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({modalVisibleProjects:false,modalVisibleButton:true,idProject:item.id,projectname:item.projectname,chosen:item.projectname,value:item.id,valeur:item.projectname,valeur2:item.id}) }}>
      <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
        <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>
            {String(item.projectname).slice(0,1)}
        </Text>
      </View>
      <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
        <Text style={{fontSize : 18}}>
        {item.projectname} 
        </Text>
      </View>
    </TouchableOpacity> 
  </View>
);


    render(){
        return (

            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.loadingShow}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>
              <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={
                                        <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'stretch'}}>  
                                               <Button icon={ <Icon name="chevron-left" size={20} color='white' type='material-community'  />} onPress={()=>this.props.navigation.goBack()}/>                                                    
                                                <Text style={{fontSize:22,color:'white',fontWeight:'500',marginTop:5}}>Projets : {this.props.route.params.projectname}</Text>
                                        </View>}
                                       
                                />
                        </View>
                        <View style={{ flex: 1.2}}>
                          {/* <Text>scrollTabNavigation</Text> */}
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.scroll}>             
                              <TabItem   title='Details'      scale={this.state.SDetails}   BackGround={this.state.BGDetails}     onPress={()=>this.changecolor('Details')}/>
                            </ScrollView >      
                       
                    </View>
              <SafeAreaView style={{ flex: 7, flexDirection: 'column' }}>
                  {this.TabNavigation()}
              </SafeAreaView>
              <ModalInput 
                  modalVisibleInput={this.state.modalVisibleInput} 
                  title={this.state.title}
                  edit={'Edit Tasks'}
                  valeur={this.state.valeur}
                  onPressCancel={()=>{this.setState({modalVisibleInput:false,value:'',valeur:''})} }
                  onPressSave={this.update}
                />
                <ModalButton 
                  modalVisibleButton={this.state.modalVisibleButton}  
                  title={this.state.title}
                  chosen={this.state.chosen} 
                  edit={'Edit Tasks'}
                  value={this.state.value} 
                  valeur={this.state.valeur}     
                  valeur2={this.state.valeur2}                
                  onPressCancel={()=>this.setState({modalVisibleButton:false})} 
                  onPressSave={this.update}
                  onPressSelect={()=>{
                    if(this.state.title=='Statut')
                    {this.setState({
                    modalVisibleStatut:true,modalVisibleButton:false})
                    }                   
                    if(this.state.title==='Relatif à')
                      {this.setState({
                        modalVisibleProjects:true,modalVisibleButton:false})}
                    if(this.state.title==='Categorie')
                      {this.setState({
                        modalVisibleCategorie:true,modalVisibleButton:false})}
                    if(this.state.title==='Priorite')
                      {this.setState({
                      modalVisibleItemPriority:true,modalVisibleButton:false})}
                    }      
                                
                  }
                /> 
                <ModalDate 
                  modalVisibleDate={this.state.modalVisibleDate}  
                  title={this.state.title} 
                  edit={'Edit Tasks'}
                  valeur={this.state.valeur}
                  onPressCancel={()=>this.setState({modalVisibleDate:false,value:'',valeur:''})} 
                  onPressSave={this.update}
                  
                />
                <ModalSelectProject 
                  modalVisibleProject={this.state.modalVisibleProjects}                           
                  
                  renderItemProject={this.renderItemProjects}
                  onPressClose={()=>this.setState({modalVisibleProjects:false,modalVisibleButton:true})}
              /> 
                <ModalSelectItemPriority
                  modalVisibleItemTickets={this.state.modalVisibleItemPriority}
                  onPressOption={this.onPressOptionPriority}
                  priorite={'Select Priorité'}
                  onPressClose={()=>this.setState({modalVisibleItemPriority:false,modalVisibleButton:true})}
                />
                <ModalSelectItemStatut
                  modalVisibleItemTickets={this.state.modalVisibleStatut}
                  onPressOption={this.onPressOptionStatut}
                  select={'Select Statut'}
                  onPressClose={()=>this.setState({modalVisibleStatut:false,modalVisibleButton:true})}
                />
                <ModalSelectCategorie
                  modalVisibleCategorie={this.state.modalVisibleCategorie}
                  onPressOption={this.onPressOptionCategorie}
                  select={'Select catégorie'}
                  onPressClose={()=>this.setState({modalVisibleCategorie:false,modalVisibleButton:true})}
                />
                

           
          </View>

        )
    
     }
    }
