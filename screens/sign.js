import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  Modal,
  Pressable,
  Alert,
} from "react-native";
import { Button, Input, CheckBox } from "react-native-elements";
import AsyncStorage from "@react-native-async-storage/async-storage";
import md5 from "md5";

/*==================================================================================================================*/

const Sign = (props) => {
  const [formData, setFormData] = useState({
    userName: "",
    userAccessKey: "",
  });

  const [loginError, setLoginError] = useState(false);
  const [showForm, setShowForm] = useState(false); // TOREMOVE : we don't need this
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    console.log("enter");
    
      try {
        //check if user is logged in in storage
        AsyncStorage.multiGet(["USER_NAME", "USER_ACCESS_KEY"])
          .then((response) => {
            if (response[0][1] && response[1][1]) {
              loginUsingStorage(response[0][1], response[1][1]);
            } else {
              setShowForm(true);
            }
          })
          .catch((e) => {
            setShowForm(true);
          });
      } catch (e) {
        setShowForm(true);
      }

    return () => {
      console.log("quitting sign in");
    };
  }, []);

  const Login = (userName, userAccessKey, callback) => {
    fetch(
      "https://portail.crm4you.ma/webservice.php?operation=getchallenge&username=" +
        userName,
      { mode: "no-cors" }
    )
      .then((res) => res.json())
      .then((resJson) => {
        if (resJson.success == true) {
          var key = md5(resJson.result.token + userAccessKey);

          var form = new FormData();
          form.append("operation", "login");
          form.append("username", userName);
          form.append("accessKey", key);

          //log in
          fetch("https://portail.crm4you.ma/webservice.php", {
            method: "POST",
            body: form,
          })
            .then((res) => res.json())
            .then((resJson) => {
              if (resJson.success == true) {
                const userInfo = resJson.result;
                const session = userInfo.sessionName;
                fetch(
                  "https://portail.crm4you.ma/webservice.php?operation=listtypes&sessionName=" +
                    session
                )
                  .then((res) => res.json())
                  .then((resJson) => {
                    callback(userAccessKey, userInfo, resJson);
                  });
              } else {
                // setState({ loadingShow: false, cred: true });
                Alert.alert("Mot de passe incorrect");
              }
            })
            .catch((error) => console.error(error));
        } else {
          setLoginError(true);
        }
      })
      .catch((error) => console.error(error));
  };

  const loginUsingForm = () => {
    Login(
      formData.userName,
      formData.userAccessKey,
      (userAccessKey, userInfo, resJson) => {
        if (resJson.success) {
          let storage = [
            ["USER_NAME", userInfo.username || ""],
            ["USER_ACCESS_KEY", userAccessKey || ""],
            ["USERID", userInfo.userId || ""],
            ["EMAIL", userInfo.email || ""],
            ["SESSION", userInfo.sessionName || ""],
            ["LINK", "https://portail.crm4you.ma"],
            ["IS_LOGGED", "true"],
            ["TYPES", resJson.result.types.toString() || ""],
          ];

          AsyncStorage.multiSet(storage)
            .then(() => {
              props.navigation.navigate("Home");
            })
            .catch((err) => {
              console.log(err);
            });
        } else {
          Alert.alert("Mot de passe incorrect");
        }
      }
    );
  };

  const loginUsingStorage = (userName, userAccessKey) => {
    console.log("doing");
    Login(userName, userAccessKey, (userAccessKey, userInfo, resJson) => {
      let storage = [
        ["SESSION", userInfo.sessionName || ""],
        ["TYPES", resJson.result.types.toString() || ""],
      ];
      AsyncStorage.multiSet(storage)
        .then(() => {
          props.navigation.navigate("Home");
          // setState({ loadingShow: false });
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

  return (
    <View style={{ flex: 1 }}>
      {/* this is for loading */}
      <Modal
        animationType="fade"
        transparent={true}
        visible={isLoading}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(14,14,14,0.6)",
          }}
        >
          <View>
            <Image
              style={{
                width: 100,
                height: 100,
              }}
              source={require("../assets/crm_icon.png")}
            />
            <Image
              style={{
                width: 100,
                height: 100,
              }}
              source={require("../assets/Spinner.gif")}
            />
          </View>
        </View>
      </Modal>

      <Image
        style={styles.backgroundImage}
        source={require("../assets/back2.png")}
      />

      <View style={{ justifyContent: "space-around", height: "80%" }}>
        <View
          style={{
            height: "50%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Image
            style={{
              width: 200,
              height: 200,
            }}
            source={require("../assets/crm_icon.png")}
          />
        </View>
        <View style={{ flex: 1, flexDirection: "column" }}>
          <Input
            placeholder="Username"
            leftIcon={{
              type: "ionicon",
              name: "person-outline",
              color: "white",
            }}
            placeholderTextColor="white"
            onChangeText={(value) =>
              setFormData({ ...formData, userName: value })
            }
          />
          <Input
            placeholder="Access key"
            placeholderTextColor="white"
            secureTextEntry={true}
            type="password"
            leftIcon={{
              type: "ionicon",
              name: "lock-closed-outline",
              color: "white",
            }}
            onChangeText={(value) =>
              setFormData({ ...formData, userAccessKey: value })
            }
          />
          {loginError ? (
            <Text style={{ color: "red" }}>
              Access key ou username incorrect
            </Text>
          ) : null}
        </View>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            height: "20%",
          }}
        >
          <Pressable
            style={{
              backgroundColor: "royalblue",
              borderRadius: 5,
              alignSelf: "center",
              alignItems: "center",
              justifyContent: "center",
              width: 200,
              height: 40,
              marginTop: 100,
            }}
            onPress={() => loginUsingForm()}
          >
            <Text
              style={{
                fontSize: 18,
                color: "white",
              }}
            >
              Login
            </Text>
          </Pressable>
          {/*  <Text style={{ fontWeight: '500', color: 'white', fontSize: 16 }}>Forgot password?</Text>*/}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  backgroundImage: {
    position: "absolute",
    overlayColor: "blue",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    opacity: 0.5,
  },
  text: {
    color: "white",
    fontSize: 42,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#000000a0",
  },
});

export default Sign;
