import React from 'react';
import { StyleSheet, View ,FlatList,Text,TouchableOpacity,Modal,Image} from 'react-native';
//Reservez.js 
import { SearchBar,Icon,Header,Button, Divider,Overlay} from 'react-native-elements';
import { FAB } from 'react-native-paper';
import ItemAffaire from '../ItemsRender/affaireitem';
import ModalAddContrat from '../Modals/ModalContrat.js';
import ModalSearchBy from '../Modals/ModalSearchBy';
import ModalSelectContacts from '../Modals/ModalSelectContact.js';
import ModalSelectItemStatut from '../Modals/ModalSelectItemStatut';
import ModalSelectItemPriority from '../Modals/ModalSelectItemTickets.js';
import ModalSelectItemType from '../Modals/ModalSelectItemType.js';
import ModalSelectSociete from '../Modals/ModalSelectSociete.js';
import ModalSelectUnite from '../Modals/ModalSelectUnite.js';
import AsyncStorage from '@react-native-async-storage/async-storage';





export default class Contrats extends React.Component{
    constructor(props){
        super(props);
        this.state={ 
          loadingShow:true,
          link:'',
          recordN: 0 ,
          pagesN : 0 ,
          ActuallPage: 0,
          search : false ,
          dataFet:'',
          dataContrats :[],
          dataContrats2 :[],
          accountname:'Select Compte',
          idCompte:'',
          lastname:'Select Contact',
          idContact:'',
          unite:"",
          type:'Opératoire',
          statut:'Ouvert',
          priority:'Normale',
          Moreinfo : false,
          MoreinfoIcon : "chevron-down",
          MoreinfoText : 'Plus',
          modalVisibleContrat:false,
          modalVisibleCompte:false,
          modalVisibleUnite:false,
          modalVisibleItemContacts:false,
          modalVisibleItemPriority:false,
          modalVisibleType:false,
          modalVisibleItemStatut:false,
          modalVisibleDelete:false,
          id:'',
          colorobject : {
            BG1 : '#53aefe',
            BG2 : 'gray',
            BG3 : 'gray',
          },
          searchby : 'subject', 
          searchbytitle : 'Sujet', 
          searchTextinput : '',
          MSearchVisibility : false,
      }}
      initialiserValue=()=>{
        this.setState({
          accountname:'Select Compte',
          idCompte:'',
          lastname:'Select Contact',
          idContact:'',
          unite:"",
          type:'Opératoire',
          statut:'Ouvert',
          priority:'Normale',
          Moreinfo : false,
          MoreinfoIcon : "chevron-down",
          MoreinfoText : 'Plus',
        })
      }
      onPressInfo=() => { if(this.state.Moreinfo){
        this.setState({
          Moreinfo : false,
          MoreinfoIcon : "chevron-down",
          MoreinfoText : 'Plus'
        })
       }else{
        this.setState({
          Moreinfo : true,
          MoreinfoIcon : "chevron-up",
          MoreinfoText : 'Moins'
        })
       }
         }
   
        _keyExtractor = (item, index) => item.key;
        renderSeperator=()=> {
          return(
          <View style={{justifyContent:'center',alignItems:'center'}}>
              <Divider style={{width:'95%'}}/>
          </View>
        )}

        fetching=async()=>{
        
          this.setState({loadingShow:true})
          try{
            await AsyncStorage.multiGet(['SESSION', 'CRM_LINK'], async (err, res)=>{ 
  
              var dataFet= res[0][1];
              this.setState({dataFet: dataFet});
              var link = res[1][1];
              this.setState({link: link});
          await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select * from ServiceContracts ;')
          .then((response) => response.json())
          .then((json)=>{this.setState({dataContrats:json.result,dataContrats2:json.result,});console.log(json.result);this.setState({loadingShow:false}) })
          .catch((error)=>
            console.log(error))
          })
              
        }catch(e){
          console.log('error: ', e);
        }}

        componentDidMount(){
            this.fetching();
        }

        renderElements =({item,index})=>{
                return(
                      <View style={{flex:1}}>
                          <ItemAffaire Name={item.subject} onPressDelete={()=> {this.setState({id:item.id,modalVisibleDelete:true})}} onPress={()=>this.props.navigation.navigate('DetailsContrats',{title:item.subject,subject:item.subject,id:item.id,index:index,data:this.state.dataContrats,result:this.props.route.params.result})}/>
                      </View>)}
         renderItemComptesContrat = ({ item,x }) => (            
          <View style={styles.flatView}>
          <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({modalVisibleCompte:false,modalVisibleContrat:true,idCompte:item.id,accountname:item.accountname}) }}>
            <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
              <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>
               {String(item.accountname).slice(0,1)}               
              </Text>
            </View>
            <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
              <Text style={{fontSize : 18}}>          
               {item.accountname}
              </Text>
            </View>
          </TouchableOpacity> 
    
          </View>
        );
        renderItemContactsContrats = ({ item,x }) => (
          <View style={styles.flatView}>
            <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({modalVisibleItemContacts:false,modalVisibleContrat:true,idContact:item.id,lastname:item.lastname}) }}>
              <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
                <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>
                {String(item.lastname).slice(0,1)}            
                </Text>
              </View>
              <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
                <Text style={{fontSize : 18}}>
                {item.lastname }
                </Text>
              </View>
            </TouchableOpacity> 
          </View>
        );
        onPressOptionPriority=(val)=>   
        {this.setState({modalVisibleContrat:true,modalVisibleItemPriority:false,priority:val});}
        onPressOptionStatut=(val)=>   
        {this.setState({modalVisibleContrat:true,modalVisibleItemStatut:false,statut:val});}
        onPressOptionType=(x)=>   
        {this.setState({modalVisibleContrat:true,modalVisibleType:false,type:x});}
        onPressOptionUnite=(x)=>   
        {this.setState({modalVisibleContrat:true,modalVisibleUnite:false,unite:x});}
          
        

        SubmitItem = (s , d , cc) => {
          let c = {[`${s}`] : '#53aefe'}
          let initial = {BG1 : 'gray',BG2 : 'gray',BG3 : 'gray',}
          let obj =  Object.assign(this.state.colorobject,initial,c)
          this.setState({
            colorobject : obj,
            searchby : d,
            searchbytitle : cc
  
          })
    
        }
  
  
        filter = async () => {
          if( this.state.searchTextinput)
          await this.setState({
            search : true,
            dataContrats : this.state.dataContrats2.filter((i,n) =>
               i[`${this.state.searchby }`].toUpperCase().includes(this.state.searchTextinput.toUpperCase()))
          })
      }

      getRecord = async (page) => {
        
        let offset = ( (page-1) * 50 ) +1
        this.setState({loadingShow:true})
        try{
          await AsyncStorage.multiGet(['SESSION', 'CRM_LINK'], async (err, res)=>{ 
            var dataFet= res[0][1];
            this.setState({dataFet: dataFet});
            var link = res[1][1];
            this.setState({link: link});
            await fetch( 'https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select *  from ServiceContracts limit ' + offset + ' ,' +50+' ;')
            .then((response) => response.json())
            .then(
              async (json) => {               
              await this.setState({
                dataContrats : this.state.dataContrats.concat(json.result),
                dataContrats2 : this.state.dataContrats2.concat(json.result),
                ActuallPage : page ,
                loadingShow:false              
              })
            
            }
            )})
      .catch((error)=>
        console.log(error))
      }catch(e){
        console.log('error: ', e);
      }}
    // 

    componentDidMount(){
      this.getRecordsNumber();
      this.getRecord(1);
    }

    getFunction = async () => {
      this.setState({
        dataContrats : [],
        dataContrats2 : [],
        recordN: 0 ,
        pagesN : 0 ,
        ActuallPage: 0,
        loadingShow: true ,
      })
      this.getRecordsNumber();
      this.getRecord(1);}

      getRecordsNumber = async () => {
        var dataFet=this.props.route.params.result;
        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select COUNT(*)  from ServiceContracts ;')
        .then((response) => response.json())
        .then((json)=>{
          console.log(json.result[0].count);
          this.setState({ 
            recordN : json.result[0].count ,
            pagesN : Math.ceil(parseFloat(json.result[0].count) /50),
          })
        
        })
        .catch((error)=>
          console.log(error))  
      }
      // 

        // add this
        LoadMore = (test) =>  {
          if(this.state.ActuallPage + 1 <= this.state.pagesN && !this.state.search) {
            console.log('yeeeeeeeeeeeeeeees ', test  );
            this.getRecord(this.state.ActuallPage + 1);
          }
          else this.setState({ loadingShow : false})
        }

    render(){
        return(
          <View style={{flex:1}}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.loadingShow}
          >
            <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>      
                <ModalSearchBy
                          MSearchVisibility={this.state.MSearchVisibility}
                          Items={[
                            { title: 'Sujet',  value: 'subject', colorname: 'BG1' },
                            { title: 'Contrat N°'   ,  value: 'contract_no', colorname: 'BG2' },
                            { title: 'Type',   value: 'contract_type', colorname: 'BG3' }
                          ]}
                          colorObject={this.state.colorobject}
                          searchby={this.state.searchby}
                          onpresst={(a, b, c) => { this.SubmitItem(a, b, c) }}
                          onCancel={() => this.setState({ MSearchVisibility: false })}
                        />   
                <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                    <Header
                            placement="right"
                            leftComponent={
                            <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'stretch'}}>  
                                    <Button onPress={()=> {this.props.navigation.toggleDrawer();}} icon={<Icon name='menu' color='white'/>} 
                                            />
                                    <Text style={{fontSize:22,color:'white',fontWeight:'500',marginTop:8}}>Contrats</Text>
                            </View>}
                           
                    />
                </View>
                <View style={{flex:3,justifyContent:'space-evenly',alignItems:'stretch'}}>

                  
                            
                <View style={{ alignItems: 'center' }}>
                            <View style={{ borderWidth: 0.5, backgroundColor: 'white', borderRadius: 30, overflow: 'hidden', flexDirection: 'row', width: '95%', marginTop: 5, alignItems: 'center' }}>
                            <SearchBar
                                placeholder={`Search by ${this.state.searchbytitle}`}
                                platform='android'
                                value={this.state.searchTextinput}
                                onChangeText={(text) => this.setState({searchTextinput : text})}
                                containerStyle={{ flex : 7 , height :50,alignItems : 'center',justifyContent : 'center' }}
                                onSubmitEditing={async () =>  this.filter()}
                                placeholderTextColor={'#g5g5g5'}
                                onClear={() => this.setState({dataContrats : this.state.dataContrats2 ,search : false , loadingShow : true })}

                              />
                              <View style={{flex : 2,flexDirection : 'row',justifyContent : 'space-evenly' }} >
                              
                              
                              <Icon
                                name='caret-down'
                                color='#00aced'
                                type='font-awesome'
                                size={27}
                                onPress={() => this.setState({MSearchVisibility : true})}
                              />
                              
                              <Icon
                                name='filter'
                                color='#00aced'
                                type='font-awesome'
                                size={23}
                                onPress={this.filter}
                              />
                             
                              </View>
                              
                            </View>
                          </View>



                  <FlatList
                   data={this.state.dataContrats}
                   renderItem={this.renderElements}
                   numColumns={1}
                   keyExtractor={item => item.id}
                   ItemSeparatorComponent={this.renderSeperator}
                   onEndReached={this.LoadMore}
                   onEndReachedThreshold={0.0001}
                  />
                  <FAB.Group
                    open={this.state.open}
                    icon={ 'plus'}
                    color='white'
                    fabStyle={{backgroundColor : '#53aefe'}}
                    actions={[
                    { icon: 'plus',  },]}
                    onPress={()=>this.setState({modalVisibleContrat:true})}
                    onStateChange={()=>this.setState({modalVisibleContrat:true})}
                  />
                    <ModalAddContrat 
                      modalVisibleContrat={this.state.modalVisibleContrat}
                      accountname={this.state.accountname}
                      idCompte={this.state.idCompte}
                      lastname={this.state.lastname}
                      idContact={this.state.idContact}
                      unite={this.state.unite}
                      type={this.state.type}
                      statut={this.state.statut}
                      priority={this.state.priority}
                      onPressUnite={()=>this.setState({modalVisibleUnite:true,modalVisibleContrat:false})}
                      onPressInfo={this.onPressInfo}      
                      MoreinfoText={this.state.MoreinfoText}
                      MoreinfoIcon={this.state.MoreinfoIcon}
                      Moreinfo={this.state.Moreinfo}
                      onPressCancel={()=>{this.setState({modalVisibleContrat:false});this.initialiserValue()} }
                      onPressSave={()=>{this.fetching();this.initialiserValue()}}
                      
                      onPressType={()=>this.setState({modalVisibleType:true,modalVisibleContrat:false})}
                      onPressUnite={()=>this.setState({modalVisibleUnite:true,modalVisibleContrat:false})}
                      onPressStatut={()=>this.setState({modalVisibleItemStatut:true,modalVisibleContrat:false})}
                      onPressPriority={()=>this.setState({modalVisibleItemPriority:true,modalVisibleContrat:false})}
                      onPressCompte={()=>{this.setState({modalVisibleCompte:true,modalVisibleContrat:false});console.log("pressed")}}
                      onPressContact={()=>this.setState({modalVisibleItemContacts:true,modalVisibleContrat:false})}
                    />
                    <ModalSelectSociete   
                      modalVisibleCompte={this.state.modalVisibleCompte}                           
                      
                      renderItem={this.renderItemComptesContrat}
                      onPressClose={()=>this.setState({modalVisibleCompte:false,modalVisibleContrat:true}) }
                    /> 
                    <ModalSelectContacts 
                      modalVisibleContacts={this.state.modalVisibleItemContacts}                           
                      
                      renderItem={this.renderItemContactsContrats}
                      onPressClose={()=>this.setState({modalVisibleItemContacts:false,modalVisibleContrat:true}) }
                    />    
                    <ModalSelectUnite 
                    modalVisibleUnite={this.state.modalVisibleUnite}
                    onPressOption={this.onPressOptionUnite}
                    onPressClose={()=>this.setState({modalVisibleUnite:false,modalVisibleContrat:true})}
                    select={'Select Item'}  /> 
                    <ModalSelectItemPriority
                      modalVisibleItemTickets={this.state.modalVisibleItemPriority}
                      onPressOption={this.onPressOptionPriority}
                      select={'Select Priorité'}
                      onPressClose={()=>this.setState({modalVisibleItemPriority:false,modalVisibleContrat:true})}

                    />
                  <ModalSelectItemType
                      modalVisibleItem={this.state.modalVisibleType}
                      onPressOption={this.onPressOptionType}
                      select={'Select Type'}
                      onPressClose={()=>this.setState({modalVisibleType:false,modalVisibleContrat:true})}
                    />
                    <ModalSelectItemStatut
                      modalVisibleItemTickets={this.state.modalVisibleItemStatut}
                      onPressOption={this.onPressOptionStatut}
                      select={'Select Statut'}
                      onPressClose={()=>this.setState({modalVisibleItemStatut:false,modalVisibleContrat:true})}
                    />
                </View>
                <Overlay isVisible={this.state.modalVisibleDelete} >
                <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                  <Text>Voulez-vous supprimer cet objet?</Text>
                </View>
                <View style={{flexDirection:'row-reverse'}}>
                  <Button title="Oui" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}} onPress={()=> {
                    fetch('https://portail.crm4you.ma/webservice.php', {
                    method: 'POST',
                    headers: {
                      "Content-Type": "application/x-www-form-urlencoded"
                    },
                    body:'operation=delete&sessionName='+this.props.route.params.result+'&id='+this.state.id
                    });this.fetching();this.setState({modalVisibleDelete:false})}}
                  />
                  <Button title="Non" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}}  onPress={()=>this.setState({modalVisibleDelete:false})}/>
                 </View>
                </Overlay>
              </View>
        )
    }
}
const styles = StyleSheet.create({
    fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
      backgroundColor:'blue'
    },
  })

  