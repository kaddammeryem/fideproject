import React from 'react';
import { StyleSheet, View ,FlatList,Text,Modal,TouchableOpacity,Image} from 'react-native';
import { SearchBar,Icon,Header,Button,Divider,Overlay} from 'react-native-elements';
import { FAB } from 'react-native-paper';
import Contact from '../ItemsRender/contact';
import styles from '../styles';
import ModalContacts from '../Modals/ModalContact';
import ModalSelectSociete from '../Modals/ModalSelectSociete';
import ModalSelectContacts from '../Modals/ModalSelectContact';
import ModalSearchBy from '../Modals/ModalSearchBy';
import { Linking } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class Contacts extends React.Component{
      constructor(props){
        super(props);
        this.state={ 
          dataContacts :[],
          loadingShow:true,
          recordN: 0 ,
          pagesN : 0 ,
          ActuallPage: 0,
          search : false ,
          dataFet:'',
          dataContacts2 :[],
          modalVisibleCompte:false,
          modalVisibleDelete:false,
          modalVisibleContacts:false,
          modalVisibleItemContacts:false,
          idCompte:'',
          accountname:'Select Compte',
          lastname:'Select Contact',
          idContact:'',
          Moreinfo : false,
          MoreinfoIcon : "chevron-down",
          MoreinfoText : 'Plus',
          colorobject : {
            BG1 : '#53aefe',
            BG2 : 'gray',
            BG3 : 'gray',
          },
          searchby : 'firstname', 
          searchbytitle : 'Prénom', 
          searchTextinput : '',
          MSearchVisibility : false,
      }}
      initaliserValue=()=>{
        this.setState({
          idCompte:'',
          accountname:'Select Compte',
          lastname:'Select Contact',
          idContact:'',
          Moreinfo : false,
          MoreinfoIcon : "chevron-down",
          MoreinfoText : 'Plus',
        })
      }
 
      getRecordsNumber = async () => {
        var dataFet=this.props.route.params.result;
        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select COUNT(*)  from Contacts ;')
        .then((response) => response.json())
        .then((json)=>{
          console.log(json.result[0].count);
          this.setState({ 
            recordN : json.result[0].count ,
            pagesN : Math.ceil(parseFloat(json.result[0].count) /50),
          })
        
        })
        .catch((error)=>
          console.log(error))  
      }
      // 
      LoadMore = (test) =>  {
        if(this.state.ActuallPage + 1 <= this.state.pagesN && !this.state.search) {
          console.log('yeeeeeeeeeeeeeeees ', test  );
          this.getRecord(this.state.ActuallPage + 1);
        }
        else this.setState({ loadingShow : false})
      }

      getRecord = async (page) => {
        
        let offset = ( (page-1) * 50 ) +1
        this.setState({loadingShow:true})
        try{
          await AsyncStorage.multiGet(['SESSION', 'CRM_LINK'], async (err, res)=>{ 
            var dataFet= res[0][1];
            this.setState({dataFet: dataFet});
            var link = res[1][1];
            this.setState({link: link});
            await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select * from Contacts ;')
            .then((response) => response.json())
            .then(async(json)=>{this.setState({dataContacts:json.result,dataContacts2:json.result ,ActuallPage : page   }); 
           for(let val of this.state.dataContacts){
            
            await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select * from Accounts where id='+val.account_id+';')
            .then((response)=>response.json())
            .then((json)=>
    
            {
              if(json.result.length!=0)
              {
             
              val['accountname']=json.result[0].accountname;
              
            }
            });

            
            }; 
            ;this.setState({loadingShow:false})})
            .catch((error)=>
            console.log(error))
    
          })
                
        }catch(e){
          console.log('error: ', e);
        }}
    // 
    componentDidMount(){
      this.getRecordsNumber();
      this.getRecord(1);
    }

    getFunction = async () => {
      this.setState({
        dataAffaires : [],
        dataAffaires2 : [],
        recordN: 0 ,
        pagesN : 0 ,
        ActuallPage: 0,
        loadingShow: true ,
      })
      this.getRecordsNumber();
      this.getRecord(1);}

      renderSeperator=()=> {
        return(
        <View style={{justifyContent:'center',alignItems:'center'}}>
            <Divider style={{width:'95%'}}/>
        </View>
        )}
        makeCall = (phone) => {

          let phoneNumber = '';
      
          if (Platform.OS === 'android') {
            phoneNumber = `tel:${phone}`;
          } else {
            phoneNumber = `telprompt:${phone}`;
          }
      
          Linking.openURL(phoneNumber);
        };
   
      renderElements =({item,index})=>{
            return(
                  <Contact onPressDelete={()=> {this.setState({idContact:item.id,modalVisibleDelete:true})}}
                  compte={item.accountname} Name={item.lastname+' '+item.firstname} email={item.phone} onPressPhone={()=>this.makeCall(item.phone)}
                  onPress={()=>this.props.navigation.navigate('DetailsContacts',{idCompte:item.account_id,account:item.accountname,title:item.firstname+' '+item.lastname,lastname:item.lastname,firstname:item.firstname,data:this.state.dataContacts,index:index,id:item.id,result:this.props.route.params.result})}/>
      )}
      onPressInfo=() => { if(this.state.Moreinfo){
        this.setState({
          Moreinfo : false,
          MoreinfoIcon : "chevron-down",
          MoreinfoText : 'Plus'
        })
       }else{
        this.setState({
          Moreinfo : true,
          MoreinfoIcon : "chevron-up",
          MoreinfoText : 'Moins'
        })
       }
      }
      renderItemComptes = ({ item }) => (            
        <View style={styles.flatView}>
        <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({modalVisibleCompte:false,modalVisibleContacts:true,idCompte:item.id,accountname:item.accountname}) }}>
          <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
            <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>
             {String(item.accountname).slice(0,1)}               
            </Text>
          </View>
          <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
            <Text style={{fontSize : 18}}>          
             {item.accountname}
            </Text>
          </View>
        </TouchableOpacity> 
  
        </View>
      );
      renderItemContacts= ({ item,x }) => (
        <View style={styles.flatView}>
          <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({modalVisibleItemContacts:false,modalVisibleContacts:true,idContact:item.id,lastname:item.lastname}) }}>
            <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
              <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>
              {String(item.lastname).slice(0,1)}            
              </Text>
            </View>
            <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
              <Text style={{fontSize : 18}}>
              {item.lastname }
              </Text>
            </View>
          </TouchableOpacity> 
        </View>
      );

      SubmitItem = (s , d , cc) => {
        let c = {[`${s}`] : '#53aefe'}
        let initial = {BG1 : 'gray',BG2 : 'gray',BG3 : 'gray',}
        let obj =  Object.assign(this.state.colorobject,initial,c)
        this.setState({
          colorobject : obj,
          searchby : d,
          searchbytitle : cc

        })
  
      }


      filter = async () => {
        await this.setState({
          dataContacts : this.state.dataContacts2.filter((i,n) =>
             i[`${this.state.searchby}`].toUpperCase().includes(this.state.searchTextinput.toUpperCase()))
        })
    }

    
     

      render(){
        const dataFet=this.state.dataFet;

            return(
              <View style={{flex:1}}>
              <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.loadingShow}
              >
                <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: 'rgba(14,14,14,0.6)'
                }}>
                <View>
                  <Image style={{
                    width: 100,
                    height: 100
                  }} source={require('../../assets/crm_icon.png')} />
                  <Image style={{
                    width: 100,
                    height: 100
                  }} source={require('../../assets/Spinner.gif')} />
                </View>
              </View>
            </Modal> 

                        <ModalSearchBy
                          MSearchVisibility={this.state.MSearchVisibility}
                          Items={[
                            { title: 'Prénom',  value: 'firstname', colorname: 'BG1' },
                            { title: 'Nom'   ,  value: 'lastname', colorname: 'BG2' },
                            { title: 'email',   value: 'email', colorname: 'BG3' }
                          ]}
                          colorObject={this.state.colorobject}
                          searchby={this.state.searchby}
                          onpresst={(a, b, c) => { this.SubmitItem(a, b, c) }}
                          onCancel={() => this.setState({ MSearchVisibility: false })}
                        />
                        <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={
                                        <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'stretch'}}>  
                                                <Button onPress={()=> {this.props.navigation.toggleDrawer();}} icon={<Icon name='menu' color='white'/>} 
                                                        />
                                                <Text style={{fontSize:22,color:'white',fontWeight:'500',marginTop:8}}>Contacts</Text>
                                        </View>}
                                      
                                />
                        </View>

                        <View style={{flex:3,justifyContent:'space-evenly',alignItems:'stretch'}}>
                        
                            
                        <View style={{ alignItems: 'center' }}>
                            <View style={{ borderWidth: 0.5, borderRadius: 30, overflow: 'hidden', flexDirection: 'row', width: '95%', marginTop: 5, alignItems: 'center' }}>
                            <SearchBar
                                placeholder={`Search by ${this.state.searchbytitle}`}
                                platform='android'
                                value={this.state.searchTextinput}
                                onChangeText={(text) => this.setState({searchTextinput : text})}
                                containerStyle={{ flex : 7 , height :50,alignItems : 'center',justifyContent : 'center' }}
                                onSubmitEditing={async () =>  this.filter()}
                                placeholderTextColor={'#g5g5g5'}
                                onClear={() => this.setState({dataContacts : this.state.dataContacts2 ,search : false , loadingShow : true  })}
                              />
                              <View style={{flex : 2,flexDirection : 'row',justifyContent : 'space-evenly' }} >                 
                                <Icon
                                  name='caret-down'
                                  color='#00aced'
                                  type='font-awesome'
                                  size={27}
                                  onPress={() => this.setState({MSearchVisibility : true})}
                                />                   
                                <Icon
                                  name='filter'
                                  color='#00aced'
                                  type='font-awesome'
                                  size={23}
                                  onPress={this.filter}
                                />                             
                              </View>
                              
                            </View>
                          </View>


                            <FlatList
                    
                              data={this.state.dataContacts}
                              renderItem={this.renderElements}
                              numColumns={1}
                              keyExtractor={item => item.id}
                              ItemSeparatorComponent={this.renderSeperator}
                              onEndReached={this.LoadMore}
                              onEndReachedThreshold={0.0001}
                            />
                            <FAB.Group
                                icon={ 'plus'}
                                color='white'
                                fabStyle={{backgroundColor : '#53aefe'}}
                                actions={[
                                  { icon: 'plus', onPress: () => this.setState({modalVisible:true}) },
                                ]}
                                onStateChange={() => { this.setState({modalVisibleContacts:true}) }}
                                onPress={() => {this.setState({modalVisibleContacts:true})
                                }}
                            />
                            <ModalContacts
                              modalVisibleContacts={this.state.modalVisibleContacts}
                              onPressCompte={()=>this.setState({modalVisibleCompte:true,modalVisibleContacts:false})}
                              onPressContact={()=>this.setState({modalVisibleItemContacts:true,modalVisibleContacts:false})}
                              accountname={this.state.accountname}
                              idCompte={this.state.idCompte}
                              lastname={this.state.lastname}
                              idContact={this.state.idContact}
                              MoreInfoText={this.state.MoreinfoText}
                              MoreInfoIcon={this.state.MoreinfoIcon}
                              MoreInfo={this.state.Moreinfo}
                              onPressInfo={this.onPressInfo}
                              onPressCancel={()=>{this.setState({modalVisibleContacts:false});this.initaliserValue()}}
                              onPressSave={()=>{this.getFunction();this.initaliserValue()}}
                              
                            />
                            <ModalSelectSociete
                              modalVisibleCompte={this.state.modalVisibleCompte}
                              
                              renderItem={this.renderItemComptes}
                              onPressClose={()=>this.setState({modalVisibleCompte:false,modalVisibleContacts:true})}
                            />
                            <ModalSelectContacts 
                              modalVisibleContacts={this.state.modalVisibleItemContacts}                           
                              renderItem={this.renderItemContacts}
                              onPressClose={()=>this.setState({modalVisibleItemContacts:false,modalVisibleContacts:true})}
                            />
                            <Overlay isVisible={this.state.modalVisibleDelete} >
                                <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                  <Text>Voulez-vous supprimer cet objet?</Text>

                                </View>
                                <View style={{flexDirection:'row-reverse'}}>
                                  <Button title="yes" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}} onPress={()=> {console.log(this.state.idContact);
                                      fetch('https://portail.crm4you.ma/webservice.php', {
                                        method: 'POST',
                                        headers: {
                                          "Content-Type": "application/x-www-form-urlencoded"
                                      },
                                      body:'operation=delete&sessionName='+this.props.route.params.result+'&id='+this.state.idContact
                                      });this.getFunction();this.setState({modalVisibleDelete:false})}}
                                  />
                                  <Button title="no" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}}  onPress={()=>this.setState({modalVisibleDelete:false})}/>
                                </View>
                            </Overlay>
                        </View>
                   </View>    
        )
    }
}
