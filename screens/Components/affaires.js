import React from 'react';
import {  View ,FlatList,Text,TouchableOpacity,Modal,Image} from 'react-native';
import { SearchBar,Icon,Header,Button,Divider,Overlay} from 'react-native-elements';
import { FAB } from 'react-native-paper';
import ItemAffaire from '../ItemsRender/affaireitem';
import styles from '../styles';
import ModalSelectSociete from '../Modals/ModalSelectSociete';
import ModalSelectItem from '../Modals/ModalSelectItem';
import ModalAffaires from '../Modals/ModalAffaires';
import ModalSelectContacts from '../Modals/ModalSelectContact';
import ModalSelectType from '../Modals/ModalSelectType';
import ModalSearchBy from '../Modals/ModalSearchBy';
import AsyncStorage from '@react-native-async-storage/async-storage';

//affaires qui est dans drawer
export default class Affaires extends React.Component{
    constructor(props){
        super(props);
        this.state={ 
          loadingShow:true,
          recordN: 0 ,
          pagesN : 0 ,
          ActuallPage: 0,
          search : false ,
          link:'',
          dataFet:'',
          dataAffaires :[],
          dataAffaires2 :[],
          dataComptes:[],
          modalVisibleItem:false,
          modalVisibleSociete:false,
          dataUsers:[],
          modalVisibleItem:false,
          modalVisibleType:false,
          modalVisibleContacts:false,
          modalVisibleAffaires : 'false',
          Moreinfo : false,
          MoreinfoIcon : "chevron-down",
          MoreinfoText : 'Plus',
          potentialname:'',
          idAffaire:'',
          idContact:'',
          modalVisibleDelete:false,
          modalVisibleCompte:false,
          accountname:'Select an account',
          lastname:'Select un contact',
          idCompte:'',
          phase:'Proposition',
          type:'Client',

          colorobject : {
            BG1 : '#53aefe',
            BG2 : 'gray',
            BG3 : 'gray',
          },
          searchby : 'potentialname', 
          searchbytitle : 'Nom de l\'affaire', 
          searchTextinput : '',
          MSearchVisibility : false,
      
          

      }}
      initaliserValues=()=>{
        this.setState({
          Moreinfo : false,
          MoreinfoIcon : "chevron-down",
          MoreinfoText : 'Plus',
          potentialname:'',
          idAffaire:'',
          idContact:'',
          accountname:'Select an account',
          lastname:'Select un contact',
          idCompte:'',
          phase:'Proposition',
          type:'Client',
        })
      }
      _keyExtractor = (item, index) => item.key;
      
      renderSeperator=()=> {
        return(
        <View style={{justifyContent:'center',alignItems:'center'}}>
            <Divider style={{width:'95%'}}/>
        </View>
        )}
       


       
            getRecord = async (page) => {
        
              let offset = ( (page-1) * 50 ) +1
              this.setState({loadingShow:true})
              try{
                await AsyncStorage.multiGet(['SESSION', 'CRM_LINK'], async (err, res)=>{ 
                  var dataFet= res[0][1];
                  this.setState({dataFet: dataFet});
                  var link = res[1][1];
                  this.setState({link: link});
                  await fetch( 'https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select *  from Potentials limit ' + offset + ' ,' +50+' ;')
                  .then((response) => response.json())
                  .then(
                    async (json) => {               
                    await this.setState({
                      dataAffaires : this.state.dataAffaires.concat(json.result),
                      dataAffaires2 : this.state.dataAffaires2.concat(json.result),
                      ActuallPage : page ,
                      loadingShow:false              
                    })
                  
                  }
                  )})
            .catch((error)=>
              console.log(error))
            }catch(e){
              console.log('error: ', e);
            }}
          // 

          componentDidMount(){
            this.getRecordsNumber();
            this.getRecord(1);
          }

          getFunction = async () => {
            this.setState({
              dataAffaires : [],
              dataAffaires2 : [],
              recordN: 0 ,
              pagesN : 0 ,
              ActuallPage: 0,
              loadingShow: true ,
            })
            this.getRecordsNumber();
            this.getRecord(1);}

            getRecordsNumber = async () => {
              var dataFet=this.props.route.params.result;
              await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select COUNT(*)  from Potentials ;')
              .then((response) => response.json())
              .then((json)=>{
                console.log(json.result[0].count);
                this.setState({ 
                  recordN : json.result[0].count ,
                  pagesN : Math.ceil(parseFloat(json.result[0].count) /50),
                })
              
              })
              .catch((error)=>
                console.log(error))  
            }
            // 
      
              // add this
              LoadMore = (test) =>  {
                if(this.state.ActuallPage + 1 <= this.state.pagesN && !this.state.search) {
                  console.log('yeeeeeeeeeeeeeeees ', test  );
                  this.getRecord(this.state.ActuallPage + 1);
                }
                else this.setState({ loadingShow : false})
              }
        
        renderElements =({item,index})=>{//onPress ca nous ramene vers parties divisées
                return(
                    <View>
                        <ItemAffaire onPressDelete={()=> {this.setState({idAffaire:item.id,modalVisibleDelete:true})}} Name={item.potentialname} onPress={()=>this.props.navigation.navigate('DetailsAffaires',{title:item.potentialname
                          ,result:this.props.route.params.result,potentialname:item.potentialname,id:item.id,index:index,data:this.state.dataAffaires})}/>                   
                    </View>)}
       
        ChangeNomSociete = (soc) => {
                this.setState({nomCompte: soc})
        }
        renderItemComptes = ({ item,x }) => (
              
                <View style={styles.flatView}>
                <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({modalVisibleCompte:false,modalVisibleAffaires:true,idCompte:item.id,accountname:item.accountname}) }}>
                  <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
                    <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>

 
                     {String(item.accountname).slice(0,1)}
                 
                           
                        </Text>
                  </View>
                  <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
                    <Text style={{fontSize : 18}}>
                    
                     {item.accountname}
                
                    </Text>
                  </View>
                </TouchableOpacity> 
          
                </View>
              );
              renderItemContacts = ({ item,x }) => (
              
                <View style={styles.flatView}>
                <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({modalVisibleContacts:false,modalVisibleAffaires:true,idContact:item.id,lastname:item.lastname}) }}>
                  <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
                    <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>

 
                     {String(item.lastname).slice(0,1)}
                 
                           
                        </Text>
                  </View>
                  <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
                    <Text style={{fontSize : 18}}>
                     {item.lastname }
                    </Text>
                  </View>
                </TouchableOpacity> 
          
                </View>
              );
        onPressOption=(x)=>   
        {this.setState({modalVisibleAffaires:true,modalVisibleItem:false,phase:x});}
        onPressOptionType=(x)=>   
        {this.setState({modalVisibleAffaires:true,modalVisibleType:false,type:x});}
        onPressCancelCompte=()=>this.setState({modalVisibleCompte:false,modalVisibleAffaires:true})
        onPressCancelContact=()=>this.setState({modalVisibleContacts:false,modalVisibleAffaires:true})
        setVisible=()=>{
          this.setState({modalVisibleAffaires:false})
        }

        SubmitItem = (s , d , cc) => {
          let c = {[`${s}`] : '#53aefe'}
          let initial = {BG1 : 'gray',BG2 : 'gray',BG3 : 'gray',}
          let obj =  Object.assign(this.state.colorobject,initial,c)
          this.setState({
            colorobject : obj,
            searchby : d,
            searchbytitle : cc
  
          })
    
        }
  
  // Edit this
        filter = async () => {
          if( this.state.searchTextinput)
          await this.setState({
            search : true,
            dataAffaires : this.state.dataAffaires2.filter((i,n) =>
               i[`${this.state.searchby }`].toUpperCase().includes(this.state.searchTextinput.toUpperCase()))
          })
      }
    
    render(){
        return(
          
          <View style={{flex:1}}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.loadingShow}
            >
              <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: 'rgba(14,14,14,0.6)'
              }}>
              <View>
                <Image style={{
                  width: 100,
                  height: 100
                }} source={require('../../assets/crm_icon.png')} />
                <Image style={{
                  width: 100,
                  height: 100
                }} source={require('../../assets/Spinner.gif')} />
              </View>
            </View>
          </Modal>      

              <ModalSearchBy
                          MSearchVisibility={this.state.MSearchVisibility}
                          Items={[
                            { title: 'Nom de l\'Affaire',  value: 'potentialname', colorname: 'BG1' },
                            { title: 'Type',   value: 'opportunity_type', colorname: 'BG3' },
                            { title: 'Probabilité'   ,  value: 'probability', colorname: 'BG2' },
                          ]}
                          colorObject={this.state.colorobject}
                          searchby={this.state.searchby}
                          onpresst={(a, b, c) => { this.SubmitItem(a, b, c) }}
                          onCancel={() => this.setState({ MSearchVisibility: false })}
                        />  
                <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                        <Header
                                placement="right"
                                leftComponent={
                                <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'stretch'}}>  
                                        <Button onPress={()=> {this.props.navigation.toggleDrawer();}} icon={<Icon name='menu' color='white'/>}/>
                                        <Text style={{fontSize:22,color:'white',fontWeight:'500',marginTop:8}}>Affaires </Text>
                                </View>}
                                
                        />
                </View>

                <View style={{flex:1,justifyContent:'space-evenly',alignItems:'stretch'}}>
                    
                        
                            
                <View style={{ alignItems: 'center' }}>
                            <View style={{ borderWidth: 0.5, backgroundColor: 'white', borderRadius: 30, overflow: 'hidden', flexDirection: 'row', width: '95%', marginTop: 5, alignItems: 'center' }}>
                            <SearchBar
                                placeholder={`Search by ${this.state.searchbytitle}`}
                                platform='android'
                                value={this.state.searchTextinput}
                                onChangeText={(text) => this.setState({searchTextinput : text})}
                                containerStyle={{ flex : 7 , height :50,alignItems : 'center',justifyContent : 'center' }}
                                onSubmitEditing={async () =>  this.filter()}
                                placeholderTextColor={'#g5g5g5'}
                                onClear={() => this.setState({dataAffaires : this.state.dataAffaires2 ,search : false , loadingShow : true })}

                              />
                              <View style={{flex : 2,flexDirection : 'row',justifyContent : 'space-evenly' }} >
                              
                              
                              <Icon
                                name='caret-down'
                                color='#00aced'
                                type='font-awesome'
                                size={27}
                                onPress={() => this.setState({MSearchVisibility : true})}
                              />
                              
                              <Icon
                                name='filter'
                                color='#00aced'
                                type='font-awesome'
                                size={23}
                                onPress={this.filter}
                              />
                             
                              </View>
                              
                            </View>
                          </View>
                        <FlatList
                                data={this.state.dataAffaires}
                                renderItem={this.renderElements}
                                numColumns={1}
                                keyExtractor={item => item.id}
                                ItemSeparatorComponent={this.renderSeperator}
                                onEndReached={this.LoadMore}
                               
                                onEndReachedThreshold={0.0001}
                        />
                        <FAB.Group
                                open={this.state.open}
                                icon={ 'plus'}
                                color='white'
                                fabStyle={{backgroundColor : '#53aefe'}}
                                actions={[
                                { icon: 'plus', onPress: () => console.log('Pressed add') },
                                ]}
                                onStateChange={() => {this.setState({modalVisibleAffaires:true}) }}
                                onPress={() => {this.setState({modalVisibleAffaires:true})
                                }}
                        />
                      
                        {/* <Text>select item</Text> */}
                      
                        <ModalAffaires
                                AffaireModal={this.state.modalVisibleAffaires}                             
                                onPressInfo={() => { if(this.state.Moreinfo){
                                this.setState({
                                Moreinfo : false,
                                MoreinfoIcon : "chevron-down",
                                MoreinfoText : 'Plus'
                                })
                                }else{
                                this.setState({
                                Moreinfo : true,
                                MoreinfoIcon : "chevron-up",
                                MoreinfoText : 'Moins'
                                })
                                }
                                }}                              
                                MoreinfoText={this.state.MoreinfoText}
                                MoreinfoIcon={this.state.MoreinfoIcon}
                                Moreinfo={this.state.Moreinfo}
                                onPressSave={()=>{this.fetching();this.initaliserValues()}} 
                                onPressCancel={()=>{this.setVisible();this.initaliserValues()}}
                               
                                
                                onPressCompte={()=>this.setState({modalVisibleCompte:true,modalVisibleAffaires:false})}
                                accountname={this.state.accountname}
                                lastname={this.state.lastname}
                                type={this.state.type}
                                idCompte={this.state.idCompte}
                                idContact={this.state.idContact}
                                onPressPhase={()=>this.setState({modalVisibleItem:true,modalVisibleAffaires:false})}
                                phase={this.state.phase}
                                onPressContact={()=>this.setState({modalVisibleContacts:true,modalVisibleAffaires:false})}
                                onPressType={()=>this.setState({modalVisibleType:true,modalVisibleAffaires:false})}
                               
                               
                               
                        />
                        {/* <Text>add Task</Text> */}
                        <Overlay isVisible={this.state.modalVisibleDelete} >
                                <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                  <Text>Voulez-vous supprimer cet objet?</Text>

                                </View>
                                <View style={{flexDirection:'row-reverse'}}>
                                  <Button title="Oui" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}} onPress={()=> {
                                      fetch('https://portail.crm4you.ma/webservice.php', {
                                        method: 'POST',
                                        headers: {
                                          "Content-Type": "application/x-www-form-urlencoded"
                                      },
                                      body:'operation=delete&sessionName='+this.props.route.params.result+'&id='+this.state.idAffaire
                                      });this.fetching();this.setState({modalVisibleDelete:false})}}
                                  />
                                  <Button title="Non" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}}  onPress={()=>this.setState({modalVisibleDelete:false})}/>
                                </View>
                            </Overlay>
                        <ModalSelectSociete 
                            modalVisibleCompte={this.state.modalVisibleCompte}                           
                            
                            renderItem={this.renderItemComptes}
                            onPressClose={this.onPressCancelCompte}
                        />
                        <ModalSelectContacts 
                            modalVisibleContacts={this.state.modalVisibleContacts}                           
                            
                            renderItem={this.renderItemContacts}
                            onPressClose={this.onPressCancelContact}
                        />
                        <ModalSelectItem
                          modalVisibleItem={this.state.modalVisibleItem}
                          onPressOption={this.onPressOption}
                          select={'Select Item'}
                          onPressClose={()=>this.setState({modalVisibleItem:false,modalVisibleAffaires:true})}

                        />
                        <ModalSelectType
                          modalVisibleItem={this.state.modalVisibleType}
                          onPressOption={this.onPressOptionType}
                          onPressClose={()=>this.setState({modalVisibleType:false,modalVisibleAffaires:true})}
                        />

        
    
                </View>
     </View>
     
        )
    }
}

  