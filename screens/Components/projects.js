import React from 'react';
import {  View ,FlatList,Text,TouchableOpacity,Modal,Image} from 'react-native';
import { SearchBar,Icon,Header,Button, Divider,Overlay} from 'react-native-elements';
import { FAB } from 'react-native-paper';
import ItemAffaire from '../ItemsRender/affaireitem';
import styles from '../styles';
import ModalProjects from '../Modals/ModalProjects'
import ModalSearchBy from '../Modals/ModalSearchBy';
import ModalSelectItemPriority from '../Modals/ModalSelectItemTickets';
import ModalSelectItemType from '../Modals/ModalSelectItemType';
import ModalSelectItemStatut from '../Modals/ModalSelectItemStatut';
import ModalSelectSociete from '../Modals/ModalSelectSociete';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class Projects extends React.Component{
    constructor(props){
        super(props);
        this.state={ 
          loadingShow:true,
          link:'',
          dataFet:'',
          recordN: 0 ,
          pagesN : 0 ,
          ActuallPage: 0,
          search : false ,
          dataProjects :[],
          dataProjects2:[],
          //modalVisible
          modalVisibleProjects:false,
          modalVisibleDelete:false,
          modalVisibleCompte:false,
          modalVisibleItemType:false,
          modalVisibleItemStatut:false,
          modalVisibleItemPriority:false,
          //data
          priority:'Normale',
          type:'Opératoire',
          statut:'Ouvert',
          idCompte:'',
          accountname:"Select Account",
          
          idProject:'',
          colorobject : {
            BG1 : '#53aefe',
            BG2 : 'gray',
            BG3 : 'gray',
          },
          searchby : 'projectname', 
          searchbytitle : 'Nom de Projet', 
          searchTextinput : '',
          MSearchVisibility : false,

      }}
      initaliserValues=()=>{
        this.setState({
          priority:'Normale',
          type:'Opératoire',
          statut:'Ouvert',
          idCompte:'',
          accountname:"Select Account",
          
        })

      }
    

      getRecordsNumber = async () => {
        this.setState({loadingShow:true})
        try{
          await AsyncStorage.multiGet(['SESSION', 'CRM_LINK'], async (err, res)=>{ 
  
            var dataFet= res[0][1];
            this.setState({dataFet: dataFet});
            var link = res[1][1];
            this.setState({link: link});
        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select COUNT(*)  from Project ;')
        .then((response) => response.json())
        .then((json)=>{
          console.log(json.result[0].count);
          this.setState({ 
            recordN : json.result[0].count ,
            pagesN : Math.ceil(parseFloat(json.result[0].count) /50),
          })
        
        }).catch((error)=>
        console.log(error))
      })
            
    }catch(e){
      console.log('error: ', e);
    }}
      fetching = async () => {
        this.setState({
          dataProjects : [],
          dataProjects2 : [],
          recordN: 0 ,
          pagesN : 0 ,
          ActuallPage: 0,
          loadingShow : true ,
        })
        this.getRecordsNumber();
        this.getRecord(1);
        
        }
      
      
      getRecord = async (page) => {
        
          let offset = ((page-1) * 50) + 1 
          var dataFet=this.props.route.params.result;
              await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select *  from Project limit ' + offset+ ' ,' + 50 +' ;')
              .then((response) => response.json())
              .then(
                (json) => {
                console.log(json);
                this.setState({
                  dataProjects : this.state.dataProjects.concat(json.result),
                  dataProjects2 : this.state.dataProjects2.concat(json.result),
                  ActuallPage : page,
                  loadingShow:false
                  
                })
              }
              )
        .catch((error)=>
          console.log(error))
  }        
  
      LoadMore = () =>  {
        if(this.state.ActuallPage + 1 <= this.state.pagesN && !this.state.search) this.getRecord(this.state.ActuallPage + 1);
        else this.setState({ loadingShow : false})
      }
     
           
      componentDidMount(){
        this.getRecordsNumber();
        this.getRecord(1);
      }
  
    renderSeperator=()=> {
        return(
            <View style={{justifyContent:'center',alignItems:'center'}}>
                <Divider style={{width:'95%'}}/>
            </View>
            )}
    setVisible=()=>{
        this.setState({modalVisibleProjects:false});
   }
       
    renderElements =({item,index})=>{
        return(
            <View>
                <ItemAffaire Name={item.projectname} onPressDelete={()=> {this.setState({idProject:item.id,modalVisibleDelete:true})}} 
                onPress={()=>this.props.navigation.navigate('DetailsProjects',{title:item.projectname,result:this.props.route.params.result,projectname:item.projectname,id:item.id,index:index,data:this.state.dataProjects})}/>
            </View>
        )
    }

    SubmitItem = (s , d , cc) => {
        let c = {[`${s}`] : '#53aefe'}
        let initial = {BG1 : 'gray',BG2 : 'gray',BG3 : 'gray',}
        let obj =  Object.assign(this.state.colorobject,initial,c)
        this.setState({
          colorobject : obj,
          searchby : d,
          searchbytitle : cc

        })
  
      }


      filter = async () => {
        if( this.state.searchTextinput) 
         await this.setState({
          search : true,
            dataProjects : this.state.dataProjects2.filter((i,n) =>
             i[`${this.state.searchby }`].toUpperCase().includes(this.state.searchTextinput.toUpperCase()))
        })
    }
    renderItemComptes = ({ item, x }) => (
      <View style={styles.flatView}>
        <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({ modalVisibleCompte: false, modalVisibleProjects:true, idCompte: item.id, accountname: item.accountname }) }}>
          <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
            <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
              {String(item.accountname).slice(0, 1)}
            </Text>
          </View>
          <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
            <Text style={{ fontSize: 18 }}>
              {item.accountname}
            </Text>
          </View>
        </TouchableOpacity>
  
      </View>
    );

    onPressOptionType = (x) => { this.setState({  modalVisibleProjects: true, modalVisibleItemType: false, type: x }); }
    onPressOptionPriority = (val) => { this.setState({ modalVisibleProjects: true,  modalVisibleItemPriority: false, priority: val }); }
    onPressOptionStatut = (val) => { this.setState({  modalVisibleProjects: true, modalVisibleItemStatut: false, statut: val }); }
    onPressCancelCompte = () => this.setState({ modalVisibleCompte: false, modalVisibleProjects: true,modalVisibleButton: true, })

    render(){
        return(
          <View style={{flex:1}}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.loadingShow}
          >
            <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>  
                        <ModalSearchBy
                          MSearchVisibility={this.state.MSearchVisibility}
                          Items={[
                            { title: 'Nom de projet',  value: 'projectname', colorname: 'BG1' },
                            { title: 'Date de début'   ,  value: 'startdate', colorname: 'BG2' },
                            { title: 'Echéance fin',   value: 'targetenddate', colorname: 'BG3' }
                          ]}
                          colorObject={this.state.colorobject}
                          searchby={this.state.searchby}
                          onpresst={(a, b, c) => { this.SubmitItem(a, b, c) }}
                          onCancel={() => this.setState({ MSearchVisibility: false })}
                        /> 
                        <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={
                                        <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'stretch'}}>  
                                                <Button onPress={()=> {this.props.navigation.toggleDrawer();}} icon={<Icon name='menu' color='white'/>}/>
                                                <Text style={{fontSize:22,color:'white',fontWeight:'500',marginTop:8}}>Projets</Text>
                                        </View>}
                                       
                                />
                        </View>

                        <View style={{flex:3,justifyContent:'space-evenly',alignItems:'stretch'}}>            
                             

                        <View style={{ alignItems: 'center' }}>
                            <View style={{ borderWidth: 0.5, backgroundColor: 'white', borderRadius: 30, overflow: 'hidden', flexDirection: 'row', width: '95%', marginTop: 5, alignItems: 'center' }}>
                            <SearchBar
                                placeholder={`Search by ${this.state.searchbytitle}`}
                                platform='android'
                                value={this.state.searchTextinput}
                                onChangeText={(text) => this.setState({searchTextinput : text})}
                                containerStyle={{ flex : 7 , height :50,alignItems : 'center',justifyContent : 'center' }}
                                onSubmitEditing={async () =>  this.filter()}
                                placeholderTextColor={'#g5g5g5'}
                                onClear={() => this.setState({dataProjects : this.state.dataProjects2,search : false , loadingShow: true })}


                              />
                              <View style={{flex : 2,flexDirection : 'row',justifyContent : 'space-evenly' }} >
                              
                              
                              <Icon
                                name='caret-down'
                                color='#00aced'
                                type='font-awesome'
                                size={27}
                                onPress={() => this.setState({MSearchVisibility : true})}
                              />
                              
                              <Icon
                                name='filter'
                                color='#00aced'
                                type='font-awesome'
                                size={23}
                                onPress={this.filter}
                              />
                             
                              </View>
                              
                            </View>
                          </View>
                            <FlatList
                    
                              data={this.state.dataProjects}
                              renderItem={this.renderElements}
                              numColumns={1}
                              keyExtractor={item => item.id}
                              ItemSeparatorComponent={this.renderSeperator}
                              onEndReached={this.LoadMore}
                              onEndReachedThreshold={0.0001}
                            />
                            <FAB.Group
                                open={this.state.open}
                                icon={ 'plus'}
                                color='white'
                                fabStyle={{backgroundColor : '#53aefe'}}
                                actions={[
                                { icon: 'plus', onPress: () => console.log('Pressed add') },]}
                                onStateChange={() => {this.setState({modalVisibleProjects:true}) }}
                                onPress={() => {this.setState({modalVisibleProjects:true})}}
                                
                            />
                        </View>
                        <ModalProjects
                          modalVisibleProjects={this.state.modalVisibleProjects}
                          
                          onPressSave={()=>{this.fetching();this.initaliserValues()}}
                          onPressCancel={()=>{this.setState({modalVisibleProjects:false});this.initaliserValues()}}
                          accountname={this.state.accountname}
                          idCompte={this.state.idCompte}
                          type={this.state.type}
                          statut={this.state.statut}
                          priority={this.state.priority}
                          onPressCompte={() => { this.setState({ modalVisibleCompte: true, modalVisibleProjects: false }); console.log("pressed") }}
                          onPressType={() => this.setState({ modalVisibleItemType: true, modalVisibleProjects: false })}
                          onPressStatut={()=>this.setState({modalVisibleProjects:false,modalVisibleItemStatut:true})}
                          onPressPriority={() => this.setState({ modalVisibleItemPriority: true, modalVisibleProjects: false })}
                        />
                        <ModalSelectItemPriority
                          modalVisibleItemTickets={this.state.modalVisibleItemPriority}
                          onPressOption={this.onPressOptionPriority}
                          select={'Select Priorité'}
                          onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleProjects: true })}
                        />
                        <ModalSelectItemType
                          modalVisibleItem={this.state.modalVisibleItemType}
                          onPressOption={this.onPressOptionType}
                          select={'Select Type'}
                          onPressClose={() => this.setState({ modalVisibleItemType: false, modalVisibleProjects: true })}
                        />
                        <ModalSelectItemStatut
                          modalVisibleItemTickets={this.state.modalVisibleItemStatut}
                          onPressOption={this.onPressOptionStatut}
                          select={'Select Statut'}
                          onPressClose={() => this.setState({ modalVisibleItemStatut: false, modalVisibleProjects: true })}
                        />
                         <ModalSelectSociete
                          modalVisibleCompte={this.state.modalVisibleCompte}
                          
                          renderItem={this.renderItemComptes}
                          onPressClose={this.onPressCancelCompte}
                        />
                        <Overlay isVisible={this.state.modalVisibleDelete} >
                                <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                  <Text>Voulez-vous supprimer cet objet?</Text>

                                </View>
                                <View style={{flexDirection:'row-reverse'}}>
                                  <Button title="Oui" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}} onPress={()=> {
                                      fetch('https://portail.crm4you.ma/webservice.php', {
                                        method: 'POST',
                                        headers: {
                                          "Content-Type": "application/x-www-form-urlencoded"
                                      },
                                      body:'operation=delete&sessionName='+this.props.route.params.result+'&id='+this.state.idProject
                                      });this.fetching();this.setState({modalVisibleDelete:false})}}
                                  />
                                  <Button title="Non" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}}  onPress={()=>this.setState({modalVisibleDelete:false})}/>
                                </View>
                            </Overlay>
                    </View>
                   
            
        )
    }
}
