import React from 'react';
import {  View ,FlatList,Text,TouchableOpacity,Modal,Image} from 'react-native';
import { SearchBar,Icon,Header,Button,Divider,Overlay} from 'react-native-elements';
import { FAB } from 'react-native-paper';
import ItemAffaire from '../ItemsRender/affaireitem';
import styles from '../styles';
import ModalSearchBy from '../Modals/ModalSearchBy';
import ModalSelectItemType from '../Modals/ModalSelectItemType';
import ModalSelectProject from '../Modals/ModalSelectProject';
import ModalMilestone from '../Modals/ModalMilestone';
import AsyncStorage from '@react-native-async-storage/async-storage';

//affaires qui est dans drawer
export default class Milestones extends React.Component{
    constructor(props){
        super(props);
        this.state={ 
          loadingShow:true,
          link:'',
          dataFet:'',
          recordN: 0 ,
          pagesN : 0 ,
          ActuallPage: 0,
          search : false ,
        dataMilestones: [],
        dataMilestones2: [],
          modalVisibleItem:false,
          modalVisibleSociete:false,
          modalVisibleDelete:false,
          id:'',
          dataUsers:[],
          modalVisibleItem:false,
          modalVisibleType:false,
          modalVisibleContacts:false,
          modalVisibleProjects:false,
          modalVisibleMilestone:false,
          modalVisibleAffaires : 'false',
          Moreinfo : false,
          MoreinfoIcon : "chevron-down",
          MoreinfoText : 'Plus',
          potentialname:'',
          idAffaire:'',
          projectname:'',
          idProject:'',
          idContact:'',
          modalVisibleCompte:false,
          phase:'Select phase de vente',
          type:'Type',

          colorobject : {
            BG1 : '#53aefe',
            BG2 : 'gray',
            BG3 : 'gray',
          },
          searchTextinput : '',
          MSearchVisibility : false,
      
          

      }}
      _keyExtractor = (item, index) => item.key;
      
      renderSeperator=()=> {
        return(
        <View style={{justifyContent:'center',alignItems:'center'}}>
            <Divider style={{width:'95%'}}/>
        </View>
        )}
        getRecordsNumber = async () => {
          this.setState({loadingShow:true})
          try{
            await AsyncStorage.multiGet(['SESSION', 'CRM_LINK'], async (err, res)=>{ 
    
              var dataFet= res[0][1];
              this.setState({dataFet: dataFet});
              var link = res[1][1];
              this.setState({link: link});
          await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select COUNT(*)  from ProjectMilestone ;')
          .then((response) => response.json())
          .then((json)=>{
            console.log(json.result[0].count);
            this.setState({ 
              recordN : json.result[0].count ,
              pagesN : Math.ceil(parseFloat(json.result[0].count) /50),
            })
          
          }).catch((error)=>
          console.log(error))
        })
              
      }catch(e){
        console.log('error: ', e);
      }}
        fetching = async () => {
          this.setState({
            dataMilestones : [],
            dataMilestones2 : [],
            recordN: 0 ,
            pagesN : 0 ,
            ActuallPage: 0,
            loadingShow : true ,
          })
          this.getRecordsNumber();
          this.getRecord(1);
          
          }
        
        
        getRecord = async (page) => {
          this.setState({loadingShow:true})
            let offset = ((page-1) * 50) + 1 
            var dataFet=this.props.route.params.result;
                await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select *  from ProjectMilestone limit ' + offset+ ' ,' + 50 +' ;')
                .then((response) => response.json())
                .then(
                  (json) => {
                  console.log(json);
                  this.setState({
                    dataMilestones : this.state.dataMilestones.concat(json.result),
                    dataMilestones2 : this.state.dataMilestones2.concat(json.result),
                    ActuallPage : page,
                    loadingShow:false
                    
                  })
                }
                )
          .catch((error)=>
            console.log(error))
    }        
    
        LoadMore = () =>  {
          if(this.state.ActuallPage + 1 <= this.state.pagesN && !this.state.search) this.getRecord(this.state.ActuallPage + 1);
          else this.setState({ loadingShow : false})
        }
       
             
        componentDidMount(){
          this.getRecordsNumber();
          this.getRecord(1);
        }
          renderElements = ({ item, index }) => {
            return (
              <View >
                <ItemAffaire Name={item.projectmilestonename} onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} onPress={() => this.props.navigation.navigate('DetailsMilestones', {title:item.projectmilestonename, id: item.id, data: this.state.dataMilestones, index: index, result: this.props.route.params.result })} /> 
              </View>
            )
          }
       
        ChangeNomSociete = (soc) => {
                this.setState({nomCompte: soc})
        }
        renderItemComptes = ({ item,x }) => (
              
                <View style={styles.flatView}>
                <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({modalVisibleCompte:false,modalVisibleAffaires:true,idCompte:item.id,accountname:item.accountname}) }}>
                  <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
                    <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>
                     {String(item.accountname).slice(0,1)}                 
                        </Text>
                  </View>
                  <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
                    <Text style={{fontSize : 18}}>                    
                     {item.accountname}               
                    </Text>
                  </View>
                </TouchableOpacity>          
                </View>
              );
              renderItemContacts = ({ item,x }) => (
              
                <View style={styles.flatView}>
                <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({modalVisibleContacts:false,modalVisibleAffaires:true,idContact:item.id,lastname:item.lastname}) }}>
                  <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
                    <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>
                     {String(item.lastname).slice(0,1)}                          
                        </Text>
                  </View>
                  <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
                    <Text style={{fontSize : 18}}>
                     {item.lastname }
                    </Text>
                  </View>
                </TouchableOpacity>           
                </View>
              );
        onPressOption=(x)=>   
        {this.setState({modalVisibleMilestone:true,modalVisibleItem:false,phase:x});}
        onPressOptionType=(x)=>   
        {this.setState({modalVisibleMilestone:true,modalVisibleType:false,type:x});}
        onPressCancelCompte=()=>this.setState({modalVisibleCompte:false,modalVisibleMilestone:true})
        onPressCancelContact=()=>this.setState({modalVisibleContacts:false,modalVisibleMilestone:true})
        setVisible=()=>{
          this.setState({modalVisibleAffaires:false})
        }

        SubmitItem = (s , d , cc) => {
          let c = {[`${s}`] : '#53aefe'}
          let initial = {BG1 : 'gray',BG2 : 'gray',BG3 : 'gray',}
          let obj =  Object.assign(this.state.colorobject,initial,c)
          this.setState({
            colorobject : obj,
            searchby : d,
            searchbytitle : cc
  
          })
    
        }
        renderItemProjects = ({ item, x }) => (
            <View style={styles.flatView}>
              <TouchableOpacity style={styles.company} onPress={() => { this.setState({ modalVisibleProjects: false, modalVisibleMilestone: true, idProject: item.id, projectname: item.projectname }) }}>
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
                  <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
                    {String(item.projectname).slice(0, 1)}
                  </Text>
                </View>
                <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
                  <Text style={{ fontSize: 18 }}>
                    {item.projectname}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          );

          filter = async () => {
            if( this.state.searchTextinput) 
             await this.setState({
              search : true,
                dataMilestones : this.state.dataMilestones2.filter((i,n) =>
                 i[`${this.state.searchby }`].toUpperCase().includes(this.state.searchTextinput.toUpperCase()))
            })
        }
    render(){
        return(
            <View style={{flex:1}}>
             <ModalSearchBy
                          MSearchVisibility={this.state.MSearchVisibility}
                          Items={[
                            { title: 'Nom de Milestone',  value: 'projectmilestonename', colorname: 'BG1' },
                            { title: 'Date de début'   ,  value: 'startdate', colorname: 'BG2' },
                            { title: 'Echéance fin',   value: 'targetenddate', colorname: 'BG3' }
                          ]}
                          colorObject={this.state.colorobject}
                          searchby={this.state.searchby}
                          onpresst={(a, b, c) => { this.SubmitItem(a, b, c) }}
                          onCancel={() => this.setState({ MSearchVisibility: false })}
                        />  
                <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                        <Header
                                placement="right"
                                leftComponent={
                                <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'stretch'}}>  
                                        <Button onPress={()=> {this.props.navigation.toggleDrawer();}} icon={<Icon name='menu' color='white'/>}/>
                                        <Text style={{fontSize:22,color:'white',fontWeight:'500',marginTop:8}}>Jalons du projet</Text>
                                </View>}
                                
                        />
                </View>

                <View style={{flex:1,justifyContent:'space-evenly',alignItems:'stretch'}}>       
                <View style={{ alignItems: 'center' }}>
                            <View style={{ borderWidth: 0.5, backgroundColor: 'white', borderRadius: 30, overflow: 'hidden', flexDirection: 'row', width: '95%', marginTop: 5, alignItems: 'center' }}>
                            <SearchBar
                                placeholder={'Search'}
                                platform='android'
                                value={this.state.searchTextinput}
                                onChangeText={(text) => this.setState({searchTextinput : text})}
                                containerStyle={{ flex : 7 , height :50,alignItems : 'center',justifyContent : 'center' }}
                                onSubmitEditing={async () =>  this.filter()}
                                placeholderTextColor={'#g5g5g5'}
                                onClear={() => this.setState({dataMilestones : this.state.dataMilestones2,search : false , loadingShow: true })}

                              />
                              <View style={{flex : 2,flexDirection : 'row',justifyContent : 'space-evenly' }} >
                              
                              
                              <Icon
                                name='caret-down'
                                color='#00aced'
                                type='font-awesome'
                                size={27}
                                onPress={() => this.setState({MSearchVisibility : true})}
                              />
                              
                              <Icon
                                name='filter'
                                color='#00aced'
                                type='font-awesome'
                                size={23}
                                onPress={this.filter}
                              />
                             
                              </View>
                              
                            </View>
                          </View>
                        <FlatList
                               data={this.state.dataMilestones}
                               renderItem={this.renderElements}
                               numColumns={1}
                               keyExtractor={item => item.id}
                               ItemSeparatorComponent={this.renderSeperator}
                               onEndReached={this.LoadMore}
                               onEndReachedThreshold={0.0001}
                        />
                        <FAB.Group
                                open={this.state.open}
                                icon={ 'plus'}
                                color='white'
                                fabStyle={{backgroundColor : '#53aefe'}}
                                actions={[
                                { icon: 'plus', onPress: () => console.log('Pressed add') },
                                ]}
                                onStateChange={() => {this.setState({modalVisibleMilestone:true}) }}
                                onPress={() => {this.setState({modalVisibleMilestone:true})
                                }}
                        />
                      
                        {/* <Text>select item</Text> */}
                      
                        <ModalMilestone
                            modalVisibleMilestone={this.state.modalVisibleMilestone}
                            
                            onPressSave={this.fetching}
                            onPressCancel={() => { this.setState({ modalVisibleMilestone: false }); }}
                            idProject={this.state.idProject}
                            projectname={this.state.projectname}
                            type={this.state.type}
                            onPressProject={() => this.setState({ modalVisibleProjects: true, modalVisibleMilestone: false })}
                            onPressType={() => this.setState({ modalVisibleType: true, modalVisibleMilestone: false })}
                        />
                        <ModalSelectItemType
                        modalVisibleItem={this.state.modalVisibleType}
                        onPressOption={this.onPressOptionType}
                        onPressClose={() => this.setState({ modalVisibleType: false, modalVisibleMilestone: true })}
                        select={'Select Item'}

                        />
                        <ModalSelectProject
                        modalVisibleProject={this.state.modalVisibleProjects}
                        
                        renderItemProject={this.renderItemProjects}
                        onPressClose={() => this.setState({ modalVisibleProjects: false, modalVisibleMilestone: true })}
                        />
                        <Overlay isVisible={this.state.modalVisibleDelete} >
                                <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                  <Text>Voulez-vous supprimer cet objet?</Text>

                                </View>
                                <View style={{flexDirection:'row-reverse'}}>
                                  <Button title="Oui" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}} onPress={()=> {
                                      fetch('https://portail.crm4you.ma/webservice.php', {
                                        method: 'POST',
                                        headers: {
                                          "Content-Type": "application/x-www-form-urlencoded"
                                      },
                                      body:'operation=delete&sessionName='+this.props.route.params.result+'&id='+this.state.id
                                      });this.fetching();this.setState({modalVisibleDelete:false})}}
                                  />
                                  <Button title="Non" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}}  onPress={()=>this.setState({modalVisibleDelete:false})}/>
                                </View>
                            </Overlay>

        
    
                </View>
     </View>
     
        )
    }
}

  