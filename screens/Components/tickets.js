import React, { Component } from 'react';
import { StyleSheet, View ,FlatList,Text,TouchableOpacity,Modal,Image} from 'react-native';
import { SearchBar,Icon,Header,Button,Overlay} from 'react-native-elements';
import { FAB } from 'react-native-paper';
import ItemTicket from '../ItemsRender/ItemTickets';
import ModalSearchBy from '../Modals/ModalSearchBy';
import ModalSelectCategorie from '../Modals/ModalSelectCategorie';
import ModalSelectContacts from '../Modals/ModalSelectContact';
import ModalSelectEngagement from '../Modals/ModalSelectEngagement';
import ModalSelectItemStatut from '../Modals/ModalSelectItemStatut';
import ModalSelectItemPriority from '../Modals/ModalSelectItemTickets';
import ModalSelectSociete from '../Modals/ModalSelectSociete';
import AddTicket from '../Modals/Modal_AddTickets';
import AsyncStorage from '@react-native-async-storage/async-storage';

import styles from '../styles';
import ItemAffaire from '../ItemsRender/affaireitem';
import ItemRender from '../ItemsRender/itemrender';
class Tickets extends Component {
    constructor(props) {
        super(props);
        this.state = { 
          loadingShow:true,
          modalVisibleDelete:false,
          dataFet:'',
          recordN: 0 ,
          pagesN : 0 ,
          ActuallPage: 0,
          search : false ,

          id:'',
          link:'',
          dataTickets:[],
          dataTickets2:[],
          modalVisibleItemPriority:false,
          modalVisibleItemContacts:false,
          modalVisibleCompte:false,
          modalVisibleCategorie:false,
          modalVisibleItemStatut:false,
          modalVisibleEngagement:false,
          modalVisibleTickets:false,
          accountname:'Select Account',
          lastname:'Select Contact',
          categorie:'Petit problème',
          engagement:'Mineur',
          priority:'Normale',
          statut:'Ouvert',
          Moreinfo: false,
          MoreinfoIcon: "chevron-down",
         MoreinfoText: 'Plus',
          colorobject : {
            BG1 : '#53aefe',
            BG2 : 'gray',
            BG3 : 'gray',
          },
          searchby : 'ticket_title', 
          searchbytitle : 'Titre', 
          searchTextinput : '',
          MSearchVisibility : false,
         }
    }
    initialiserValues=()=>{
      this.setState({
        accountname:'Select Account',
        lastname:'Select Contact',
        categorie:'Petit problème',
        engagement:'Mineur',
        priority:'Normale',
        statut:'Ouvert',
        Moreinfo: false,
        MoreinfoIcon: "chevron-down",
       MoreinfoText: 'Plus',
      })
    }

    onPressInfo = () => {
      if (this.state.Moreinfo) {
        this.setState({
          Moreinfo: false,
          MoreinfoIcon: "chevron-down",
          MoreinfoText: 'Plus'
        })
      } else {
        this.setState({
          Moreinfo: true,
          MoreinfoIcon: "chevron-up",
          MoreinfoText: 'Moins'
        })
      }
    }
    
    getRecordsNumber = async () => {
      this.setState({loadingShow:true})
      try{
        await AsyncStorage.multiGet(['SESSION', 'CRM_LINK'], async (err, res)=>{ 
          var dataFet= res[0][1];
          this.setState({dataFet: dataFet});
          var link = res[1][1];
          this.setState({link: link});
      await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select COUNT(*)  from HelpDesk ;')
      .then((response) => response.json())
      .then((json)=>{
        console.log(json.result[0].count);
        this.setState({ 
          recordN : json.result[0].count ,
          pagesN : Math.ceil(parseFloat(json.result[0].count) /50),
        })
      
      }).catch((error)=>
      console.log(error))
    })
          
  }catch(e){
    console.log('error: ', e);
  }}
    fetching = async () => {
      this.setState({
        dataTickets : [],
        dataTickets2 : [],
        recordN: 0 ,
        pagesN : 0 ,
        ActuallPage: 0,
        loadingShow : true ,
      })
      this.getRecordsNumber();
      this.getRecord(1);
      
      }
    
    
    getRecord = async (page) => {
      
        let offset = ((page-1) * 50) + 1 
        var dataFet=this.props.route.params.result;
            await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select *  from HelpDesk limit ' + offset+ ' ,' + 50 +' ;')
            .then((response) => response.json())
            .then(
              (json) => {
              console.log('tickets here '+JSON.stringify(json));
              this.setState({
                dataTickets : this.state.dataTickets.concat(json.result),
                dataTickets2 : this.state.dataTickets2.concat(json.result),
                ActuallPage : page,
                loadingShow:false
                
              })
            }
            )
      .catch((error)=>
        console.log(error))
}        

    LoadMore = () =>  {
      if(this.state.ActuallPage + 1 <= this.state.pagesN && !this.state.search) this.getRecord(this.state.ActuallPage + 1);
      else this.setState({ loadingShow : false})
    }
   
         
    componentDidMount(){
      this.getRecordsNumber();
      this.getRecord(1);
    }
    renderElements =({item,index})=>{
      return(
          <View>
              <ItemRender Name={item.ticket_title} val={item.ticketstatus}
            onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} 
              onPress={()=>this.props.navigation.navigate('DetailsTickets',{title:item.ticket_title,id:item.id,index:index,data:this.state.dataTickets,result:this.props.route.params.result})}/>
          </View>
      )}
      SubmitItem = (s , d , cc) => {
        let c = {[`${s}`] : '#53aefe'}
        let initial = {BG1 : 'gray',BG2 : 'gray',BG3 : 'gray',}
        let obj =  Object.assign(this.state.colorobject,initial,c)
        this.setState({
          colorobject : obj,
          searchby : d,
          searchbytitle : cc

        })
  
      }


     
      filter = async () => {
        if( this.state.searchTextinput) 
         await this.setState({
          search : true,
          dataTickets : this.state.dataTickets2.filter((i,n) =>
             i[`${this.state.searchby }`].toUpperCase().includes(this.state.searchTextinput.toUpperCase()))
        })
    }

  onPressOptionPriority = (val) =>  this.setState({ modalVisibleTickets: true, modalVisibleItemPriority: false, priority: val }); 
  onPressOptionStatut = (val) => { this.setState({ modalVisibleTickets: true, modalVisibleItemStatut: false, statut: val }); }
  onPressOptionEngagement = (val) => { this.setState({ modalVisibleTickets: true, modalVisibleEngagement: false, engagement: val }); }
  onPressOptionCategorie = (val) => { this.setState({ modalVisibleTickets: true, modalVisibleCategorie: false, categorie: val }); }
  onPressCancelCompte = () => this.setState({ modalVisibleCompte: false, modalVisibleTickets: true,   })
  onPressCancelContact = () => this.setState({ modalVisibleItemContacts: false, modalVisibleTickets: true, })


  renderItemComptes = ({ item, x }) => (

    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => { this.setState({ modalVisibleCompte: false, modalVisibleTickets: true,  idCompte: item.id, accountname: item.accountname, }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.accountname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.accountname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );


  renderItemContacts = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity style={styles.company} onPress={() => {this.setState({ modalVisibleItemContacts: false, modalVisibleTickets: true, idContact: item.id, lastname: item.lastname }) }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
          <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
            {String(item.lastname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>
            {item.lastname}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );


    render() {

        return (

            
          <View style={{flex:1}}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.loadingShow}
          >
            <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal>      

            
            <ModalSearchBy
              MSearchVisibility={this.state.MSearchVisibility}
              Items={[
                {title : 'Titre' , value : 'ticket_title' , colorname : 'BG1' } ,
                {title : 'Priorité' , value : 'ticketpriorities' , colorname : 'BG2' },
                {title : 'status' , value : 'ticketstatus' , colorname : 'BG3' },
              ]}
              colorObject={this.state.colorobject}
              searchby={this.state.searchby}
              onpresst={(a,b,c) => {  this.SubmitItem(a,b,c)}}
              onCancel= {() => this.setState({MSearchVisibility : false})}
            /> 

            <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                  <Header
                          placement="right"
                          leftComponent={
                          <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'stretch'}}>  
                                  <Button onPress={()=> {this.props.navigation.toggleDrawer();}} icon={<Icon name='menu' color='white'/>}/>
                                  <Text style={{fontSize:22,color:'white',fontWeight:'500',marginTop:8}}>Tickets</Text>
                          </View>}
                         
                  />
          </View>

          <View style={{flex:3,justifyContent:'space-evenly',alignItems:'stretch'}}>            
              
          <View style={{ alignItems: 'center' }}>
            <View style={{ borderWidth: 0.5, backgroundColor: 'white', borderRadius: 30, overflow: 'hidden', flexDirection: 'row', width: '95%', marginTop: 5, alignItems: 'center' }}>
              <SearchBar
                placeholder={`Search by ${this.state.searchbytitle}`}
                platform='android'
                value={this.state.searchTextinput}
                onChangeText={(text) => this.setState({searchTextinput : text})}
                containerStyle={{ flex : 7 , height :50,alignItems : 'center',justifyContent : 'center' }}
                onSubmitEditing={async () =>  this.filter()}
                placeholderTextColor={'#g5g5g5'}
                onClear={() => this.setState({dataTickets : this.state.dataTickets2  ,search : false , loadingShow: true})}
                />
              <View style={{flex : 2,flexDirection : 'row',justifyContent : 'space-evenly' }} >                                       
                <Icon
                  name='caret-down'
                  color='#00aced'
                  type='font-awesome'
                  size={27}
                  onPress={() => this.setState({MSearchVisibility : true})}
                />                 
                <Icon
                  name='filter'
                  color='#00aced'
                  type='font-awesome'
                  size={23}
                  onPress={this.filter}
                />
              </View>
            </View>
          </View>
          <FlatList                    
            data={this.state.dataTickets}
            renderItem={this.renderElements}
            numColumns={1}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={this.renderSeperator}
            onEndReached={this.LoadMore}
            onEndReachedThreshold={0.0001}
          />
          <FAB.Group
             //open={this.state.open}
            icon={ 'plus'}
            color='white'
            fabStyle={{backgroundColor : '#53aefe'}}
            actions={[
            { icon: 'plus', onPress: () => console.log('Pressed add') },]}
            onStateChange={() => this.setState({ modalVisibleTickets: true })}
            onPress={() => {
              this.setState({ modalVisibleTickets: true })
            }}
          />
          <AddTicket
            modalVisibleTickets={this.state.modalVisibleTickets}
            onPressPriority={() => this.setState({ modalVisibleItemPriority: true, modalVisibleTickets: false })}
            onPressStatut={() => this.setState({ modalVisibleItemStatut: true, modalVisibleTickets: false })}
            onPressItemContact={() => this.setState({ modalVisibleItemContacts: true, modalVisibleTickets: false })}
            onPressCompte={() => this.setState({ modalVisibleCompte: true, modalVisibleTickets: false })}
            onPressEngagement={() => this.setState({ modalVisibleEngagement: true, modalVisibleTickets: false })}
            onPressCategorie={() => this.setState({ modalVisibleCategorie: true, modalVisibleTickets: false })}
            accountname={this.state.accountname}
            selectPriority={this.state.priority}
            selectStatut={this.state.statut}
            
            onPressInfo={this.onPressInfo}
            MoreinfoText={this.state.MoreinfoText}
            MoreinfoIcon={this.state.MoreinfoIcon}
            Moreinfo={this.state.Moreinfo}
            onPressCancel={()=>{this.setState({modalVisibleTickets:false});this.initialiserValues()}}
            onPressSave={()=>{this.fetching();this.initialiserValues()}}
            lastname={this.state.lastname}
            idCompte={this.state.idCompte}
            idContact={this.state.idContact}
            engagement={this.state.engagement}
            categorie={this.state.categorie}
          />
          <ModalSelectItemPriority
            modalVisibleItemTickets={this.state.modalVisibleItemPriority}
            onPressOption={this.onPressOptionPriority}
            priorite={'Select Priorité'}
            onPressClose={() => this.setState({ modalVisibleItemPriority: false, modalVisibleTickets: true })}
           />
                <ModalSelectItemStatut
                  modalVisibleItemTickets={this.state.modalVisibleItemStatut}
                  onPressOption={this.onPressOptionStatut}
                  select={'Select Statut'}
                  onPressClose={() => this.setState({ modalVisibleItemStatut: false, modalVisibleTickets: true })}
                />
                <ModalSelectEngagement
                  modalVisibleEngagement={this.state.modalVisibleEngagement}
                  onPressOption={this.onPressOptionEngagement}
                  select={'Select Statut'}
                  onPressClose={() => this.setState({ modalVisibleEngagement: false, modalVisibleTickets: true })}

                />
                <ModalSelectCategorie
                  modalVisibleCategorie={this.state.modalVisibleCategorie}
                  onPressOption={this.onPressOptionCategorie}
                  select={'Select catégorie'}
                  onPressClose={() => this.setState({ modalVisibleCategorie: false, modalVisibleTickets: true })}

                />
                <ModalSelectContacts
                  modalVisibleContacts={this.state.modalVisibleItemContacts}
                  
                  renderItem={this.renderItemContacts}
                  onPressClose={this.onPressCancelContact}
                />
                <ModalSelectSociete
                  modalVisibleCompte={this.state.modalVisibleCompte}
                  
                  renderItem={this.renderItemComptes}
                  onPressClose={this.onPressCancelCompte}
                />         
              </View>
              <Overlay isVisible={this.state.modalVisibleDelete} >
                  <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                   <Text>Voulez-vous supprimer cet objet?</Text>

                    </View>
                                <View style={{flexDirection:'row-reverse'}}>
                                  <Button title="Oui" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}} onPress={()=> {
                                      fetch('https://portail.crm4you.ma/webservice.php', {
                                        method: 'POST',
                                        headers: {
                                          "Content-Type": "application/x-www-form-urlencoded"
                                      },
                                      body:'operation=delete&sessionName='+this.state.dataFet+'&id='+this.state.id
                                      });this.fetching();this.setState({modalVisibleDelete:false})}}
                                  />
                                  <Button title="Non" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}}  onPress={()=>this.setState({modalVisibleDelete:false})}/>
                                </View>
                            </Overlay>
            </View>
            
  
          );
    }
}
 
export default Tickets;