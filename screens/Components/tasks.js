import React from 'react';
import {  View ,FlatList,Text,TouchableOpacity,Image,Modal} from 'react-native';
import { SearchBar,Icon,Header,Button,Divider,Overlay} from 'react-native-elements';
import { FAB } from 'react-native-paper';
import ItemAffaire from '../ItemsRender/affaireitem';
import styles from '../styles';
import ModalSelectSociete from '../Modals/ModalSelectSociete';
import ModalSearchBy from '../Modals/ModalSearchBy';
import ModalSelectItemType from '../Modals/ModalSelectItemType';
import ModalSelectCategorie from '../Modals/ModalSelectCategorie';
import ModalSelectEngagement from '../Modals/ModalSelectEngagement';
import ModalSelectItemStatut from '../Modals/ModalSelectItemStatut';
import ModalSelectItemPriority from '../Modals/ModalSelectItemTickets';
import ModalTask from '../Modals/ModalTask';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalSelectProject from '../Modals/ModalSelectProject';

//affaires qui est dans drawer
export default class Tasks extends React.Component{
    constructor(props){
        super(props);
        this.state={ 
          loadingShow:true,
          link:'',
          dataFet:'',
        dataTasks: [],
        dataTasks2: [],
          dataUsers:[],
          recordN: 0 ,
          pagesN : 0 ,
          ActuallPage: 0,
          search : false ,
          id:'',
          modalVisibleTask:false,
          modalVisibleContacts:false,
          modalVisibleProjects:false,
          modalVisibleMilestone:false,
          modalVisibleAffaires : 'false',
          modalVisibleItemPriority:false,
          modalVisibleCategorie:false,
          modalVisibleEngagement:false,
          modalVisibleItemStatut:false,
          modalVisibleType:false,
          modalVisibleCompte:false,
          modalVisibleDelete:false,
          Moreinfo : false,
          MoreinfoIcon : "chevron-down",
          MoreinfoText : 'Plus',
          potentialname:'',
          idAffaire:'',
          projectname:'',
          idProject:'',
          priority:'Normale',
          statut:'Ouvert',
          engagement:'Mineur',
          categorie:'',
          idCompte:'',
          modalVisibleDelete:false,
          modalVisibleCompte:false,
          phase:'Select phase de vente',
          type:'Autre',

          colorobject : {
            BG1 : '#53aefe',
            BG2 : 'gray',
            BG3 : 'gray',
          },
          searchTextinput : '',
          MSearchVisibility : false,
      
          

      }}
      initialiserValues=()=>{
        this.setState({
          Moreinfo : false,
          MoreinfoIcon : "chevron-down",
          MoreinfoText : 'Plus',
          potentialname:'',
          idAffaire:'',
          projectname:'Select Project',
          idProject:'',
          priority:'Normale',
          statut:'Ouvert',
          engagement:'Mineur',
          categorie:'Autre Probléme',
          idCompte:'',
          phase:'Select phase de vente',
          type:'Autre',

        })
      }
      _keyExtractor = (item, index) => item.key;
      
      renderSeperator=()=> {
        return(
        <View style={{justifyContent:'center',alignItems:'center'}}>
            <Divider style={{width:'95%'}}/>
        </View>
        )}
        getRecordsNumber = async () => {
          this.setState({loadingShow:true})
          try{
            await AsyncStorage.multiGet(['SESSION', 'CRM_LINK'], async (err, res)=>{ 
    
              var dataFet= res[0][1];
              this.setState({dataFet: dataFet});
              var link = res[1][1];
              this.setState({link: link});
          await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select COUNT(*)  from ProjectTask ;')
          .then((response) => response.json())
          .then((json)=>{
            console.log(json.result[0].count);
            this.setState({ 
              recordN : json.result[0].count ,
              pagesN : Math.ceil(parseFloat(json.result[0].count) /50),
            })
          
          }).catch((error)=>
          console.log(error))
        })
              
      }catch(e){
        console.log('error: ', e);
      }}
        fetching = async () => {
          this.setState({
            dataTasks : [],
            dataTasks2 : [],
            recordN: 0 ,
            pagesN : 0 ,
            ActuallPage: 0,
            loadingShow : true ,
          })
          this.getRecordsNumber();
          this.getRecord(1);
          
          }
        
        
        getRecord = async (page) => {
          this.setState({loadingShow:true})
            let offset = ((page-1) * 50) + 1 
            var dataFet=this.props.route.params.result;
                await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select *  from ProjectTask limit ' + offset+ ' ,' + 50 +' ;')
                .then((response) => response.json())
                .then(
                  (json) => {
                  console.log(json);
                  this.setState({
                    dataTasks : this.state.dataTasks.concat(json.result),
                    dataTasks2 : this.state.dataTasks2.concat(json.result),
                    ActuallPage : page,
                    loadingShow:false
                    
                  })
                }
                )
          .catch((error)=>
            console.log(error))
    }        
    
        LoadMore = () =>  {
          if(this.state.ActuallPage + 1 <= this.state.pagesN && !this.state.search) this.getRecord(this.state.ActuallPage + 1);
          else this.setState({ loadingShow : false})
        }
       
             
        componentDidMount(){
          this.getRecordsNumber();
          this.getRecord(1);
        }
        
          renderElements = ({ item, index }) => {
            return (
              <View >
                <ItemAffaire Name={item.projecttaskname} 
                onPressDelete={() => { this.setState({ id: item.id, modalVisibleDelete: true }) }} 
                onPress={() => this.props.navigation.navigate('DetailsTasks', { title:item.projecttaskname,projectname: item.projecttaskname, id: item.id, data: this.state.dataTasks, index: index, result: this.props.route.params.result })} />
        
              </View>
            )
          }
       
        ChangeNomSociete = (soc) => {
                this.setState({nomCompte: soc})
        }
        renderItemComptes = ({ item,x }) => (
              
                <View style={styles.flatView}>
                <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({modalVisibleCompte:false,modalVisibleTask:true,idCompte:item.id,accountname:item.accountname}) }}>
                  <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
                    <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>
                     {String(item.accountname).slice(0,1)}                 
                        </Text>
                  </View>
                  <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
                    <Text style={{fontSize : 18}}>                    
                     {item.accountname}               
                    </Text>
                  </View>
                </TouchableOpacity>          
                </View>
              );
              renderItemContacts = ({ item,x }) => (
              
                <View style={styles.flatView}>
                <TouchableOpacity style={styles.company} onPress={() => {/*this.ChangeNomSociete(item.accountname) ;*/ this.setState({modalVisibleContacts:false,modalVisibleAffaires:true,idContact:item.id,lastname:item.lastname}) }}>
                  <View style={{alignItems : 'center',justifyContent:'center',flex : 1,backgroundColor : '#53aefe' , borderTopRightRadius : 10, borderBottomRightRadius : 10}} >
                    <Text style={{fontSize : 20,fontWeight : 'bold',color : 'white'}}>
                     {String(item.lastname).slice(0,1)}                          
                        </Text>
                  </View>
                  <View style={{flex : 7,justifyContent:'center',marginStart : 10}}>
                    <Text style={{fontSize : 18}}>
                     {item.lastname }
                    </Text>
                  </View>
                </TouchableOpacity>           
                </View>
              );
        onPressOption=(x)=>   
        {this.setState({modalVisibleMilestone:true,modalVisibleItem:false,phase:x});}
        onPressOptionType=(x)=>   
        {this.setState({modalVisibleMilestone:true,modalVisibleType:false,type:x});}
        onPressCancelCompte=()=>this.setState({modalVisibleCompte:false,modalVisibleMilestone:true})
        onPressCancelContact=()=>this.setState({modalVisibleContacts:false,modalVisibleMilestone:true})
        setVisible=()=>{
          this.setState({modalVisibleAffaires:false})
        }

        SubmitItem = (s , d , cc) => {
          let c = {[`${s}`] : '#53aefe'}
          let initial = {BG1 : 'gray',BG2 : 'gray',BG3 : 'gray',}
          let obj =  Object.assign(this.state.colorobject,initial,c)
          this.setState({
            colorobject : obj,
            searchby : d,
            searchbytitle : cc
  
          })
    
        }
        renderItemProjects = ({ item, x }) => (
            <View style={styles.flatView}>
              <TouchableOpacity style={styles.company} onPress={() => { this.setState({ modalVisibleProjects: false, modalVisibleTask: true, idProject: item.id, projectname: item.projectname }) }}>
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
                  <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
                    {String(item.projectname).slice(0, 1)}
                  </Text>
                </View>
                <View style={{ flex: 7, justifyContent: 'center', marginStart: 10 }}>
                  <Text style={{ fontSize: 18 }}>
                    {item.projectname}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          );
  
  
          filter = async () => {
            if( this.state.searchTextinput) 
             await this.setState({
              search : true,
                dataTasks : this.state.dataTasks2.filter((i,n) =>
                 i[`${this.state.searchby }`].toUpperCase().includes(this.state.searchTextinput.toUpperCase()))
            })
        }
      onPressOptionPriority = (val) => { this.setState({ modalVisibleButton: true, modalVisibleTask: true, modalVisibleTickets: true, modalVisibleItemPriority: false, priority: val, chosen: val, value: val }); }
      onPressOptionStatut = (val) => { this.setState({ modalVisibleTask: true, modalVisibleTickets: true, modalVisibleItemStatut: false, statut: val, modalVisibleButton: true, chosen: val, value: val }); }
      onPressOptionEngagement = (val) => { this.setState({ modalVisibleTask: true, modalVisibleEngagement: false, modalVisibleTickets: true, engagement: val }); }
      onPressOptionCategorie = (val) => { this.setState({ modalVisibleTask: true, modalVisibleCategorie: false, modalVisibleTickets: true, categorie: val }); }
      onPressItemContact = () => {
        this.setState({ modalVisibleItemContacts: true, modalVisibleTickets: false })
      }
      onPressCancelCompte = () => { this.setState({ modalVisibleCompte: false, value: '', modalVisibleTask: true, modalVisibleTickets: true, modalVisibleButton: true }); }

      onPressInfo = () => {
        if (this.state.Moreinfo) {
          this.setState({
            Moreinfo: false,
            MoreinfoIcon: "chevron-down",
            MoreinfoText: 'Plus'
          })
        } else {
          this.setState({
            Moreinfo: true,
            MoreinfoIcon: "chevron-up",
            MoreinfoText: 'Moins'
          })
        }
      }
      onPressOptionType = (x) => { this.setState({ modalVisibleButton: true, modalVisibleMilestone: true, modalVisibleTask: true, modalVisibleType: false, type: x, value: x, chosen: x }); }

    
    render(){
        return(
          <View style={{flex:1}}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.loadingShow}
          >
            <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'rgba(14,14,14,0.6)'
            }}>
            <View>
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/crm_icon.png')} />
              <Image style={{
                width: 100,
                height: 100
              }} source={require('../../assets/Spinner.gif')} />
            </View>
          </View>
        </Modal> 
             <ModalSearchBy
                          MSearchVisibility={this.state.MSearchVisibility}
                          Items={[
                            { title: 'Nom de Task',  value: 'projectmilestonename', colorname: 'BG1' },
                        
                          ]}
                          colorObject={this.state.colorobject}
                          searchby={this.state.searchby}
                          onpresst={(a, b, c) => { this.SubmitItem(a, b, c) }}
                          onCancel={() => this.setState({ MSearchVisibility: false })}
                        />  
                <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                        <Header
                                placement="right"
                                leftComponent={
                                <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'stretch'}}>  
                                        <Button onPress={()=> {this.props.navigation.toggleDrawer();}} icon={<Icon name='menu' color='white'/>}/>
                                        <Text style={{fontSize:22,color:'white',fontWeight:'500',marginTop:8}}>Tasks</Text>
                                </View>}
                                
                        />
                </View>

                <View style={{flex:1,justifyContent:'space-evenly',alignItems:'stretch'}}>       
                <View style={{ alignItems: 'center' }}>
                            <View style={{ borderWidth: 0.5, backgroundColor: 'white', borderRadius: 30, overflow: 'hidden', flexDirection: 'row', width: '95%', marginTop: 5, alignItems: 'center' }}>
                            <SearchBar
                                placeholder={'Search'}
                                platform='android'
                                value={this.state.searchTextinput}
                                onChangeText={(text) => this.setState({searchTextinput : text})}
                                containerStyle={{ flex : 7 , height :50,alignItems : 'center',justifyContent : 'center' }}
                                onSubmitEditing={ this.filter}
                                placeholderTextColor={'#g5g5g5'}
                                onClear={() => this.setState({dataTasks : this.state.dataTasks2,search : false , loadingShow: true })}

                              />
                              <View style={{flex : 2,flexDirection : 'row',justifyContent : 'space-evenly' }} >
                              
                              
                              <Icon
                                name='caret-down'
                                color='#00aced'
                                type='font-awesome'
                                size={27}
                                onPress={() => this.setState({MSearchVisibility : true})}
                              />
                              
                              <Icon
                                name='filter'
                                color='#00aced'
                                type='font-awesome'
                                size={23}
                                onPress={this.filter}
                              />
                             
                              </View>
                              
                            </View>
                          </View>
                        <FlatList
                                data={this.state.dataTasks}
                                renderItem={this.renderElements}
                                numColumns={1}
                                keyExtractor={item => item.id}
                                ItemSeparatorComponent={this.renderSeperator}
                                onEndReached={this.LoadMore}
                                onEndReachedThreshold={0.0001}
                        />
                        <FAB.Group
                                open={this.state.open}
                                icon={ 'plus'}
                                color='white'
                                fabStyle={{backgroundColor : '#53aefe'}}
                                actions={[
                                { icon: 'plus', onPress: () => console.log('Pressed add') },
                                ]}
                                onStateChange={() => {this.setState({modalVisibleTask:true}) }}
                                onPress={() => {this.setState({modalVisibleTask:true})
                                }}
                        />
                      
                        {/* <Text>select item</Text> */}
                      
                        <ModalTask
                            modalVisibleTask={this.state.modalVisibleTask}
                            onPressPriority={this.onPressPriority}
                            onPressStatut={() => this.setState({ modalVisibleItemStatut: true, modalVisibleTask: false })}
                            onPressProject={() => this.setState({ modalVisibleProjects: true, modalVisibleTask: false })}
                            onPressEngagement={() => this.setState({ modalVisibleEngagement: true, modalVisibleTask: false })}
                            onPressCategorie={() => this.setState({ modalVisibleCategorie: true, modalVisibleTask: false })}
                            idProject={this.state.idProject}
                            projectname={this.state.projectname}
                            selectPriority={this.state.priority}
                            selectStatut={this.state.statut}
                            
                            onPressInfo={this.onPressInfo}
                            MoreinfoText={this.state.MoreinfoText}
                            MoreinfoIcon={this.state.MoreinfoIcon}
                            Moreinfo={this.state.Moreinfo}
                            onPressCancel={() => {this.setState({ modalVisibleTask: false });this.initialiserValues()}}
                            onPressSave={()=>{this.fetching();this.initialiserValues()}}
                            engagement={this.state.engagement}
                            categorie={this.state.categorie}
                            type={this.state.type}
                            onPressType={() => this.setState({ modalVisibleType: true, modalVisibleTask: false })}

                        />
                        <ModalSelectItemPriority
                        modalVisibleItemTickets={this.state.modalVisibleItemPriority}
                        onPressOption={this.onPressOptionPriority}
                        priorite={'Select Priorité'}
                        onPressClose={() => this.setState({ modalVisibleItemPriority: false,modalVisibleTask:true })}
                        />
                        <ModalSelectItemStatut
                        modalVisibleItemTickets={this.state.modalVisibleItemStatut}
                        onPressOption={this.onPressOptionStatut}
                        select={'Select Statut'}
                        onPressClose={() => this.setState({ modalVisibleItemStatut: false,modalVisibleTask:true })}
                        />
                        <ModalSelectEngagement
                        modalVisibleEngagement={this.state.modalVisibleEngagement}
                        onPressOption={this.onPressOptionEngagement}
                        select={'Select Statut'}
                        onPressClose={() => this.setState({ modalVisibleEngagement: false,modalVisibleTask:true })}
                        />
                        <ModalSelectCategorie
                        modalVisibleCategorie={this.state.modalVisibleCategorie}
                        onPressOption={this.onPressOptionCategorie}
                        select={'Select catégorie'}
                        onPressClose={() => this.setState({ modalVisibleCategorie: false ,modalVisibleTask:true})}
                        />
                        <ModalSelectProject
                        modalVisibleProject={this.state.modalVisibleProjects}
                        
                        renderItemProject={this.renderItemProjects}
                        onPressClose={() => this.setState({ modalVisibleProjects: false, modalVisibleTask: true })}
                        />
                        <ModalSelectItemType
                        modalVisibleItem={this.state.modalVisibleType}
                        onPressOption={this.onPressOptionType}
                        onPressClose={() => this.setState({ modalVisibleType: false,modalVisibleTask:true })}
                        select={'Select Item'}
                        />
                         <Overlay isVisible={this.state.modalVisibleDelete} >
                                <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                  <Text>Voulez-vous supprimer cet objet?</Text>

                                </View>
                                <View style={{flexDirection:'row-reverse'}}>
                                  <Button title="Oui" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}} onPress={()=> {
                                      fetch('https://portail.crm4you.ma/webservice.php', {
                                        method: 'POST',
                                        headers: {
                                          "Content-Type": "application/x-www-form-urlencoded"
                                      },
                                      body:'operation=delete&sessionName='+this.props.route.params.result+'&id='+this.state.id
                                      });this.fetching();this.setState({modalVisibleDelete:false})}}
                                  />
                                  <Button title="Non" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}}  onPress={()=>this.setState({modalVisibleDelete:false})}/>
                                </View>
                            </Overlay>

        
    
                </View>
     </View>
     
        )
    }
}

  