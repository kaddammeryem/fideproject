import React from "react";
import {
  View,
  FlatList,
  Text,
  TouchableOpacity,
  Image,
  Modal,
} from "react-native";
import {
  SearchBar,
  Icon,
  Header,
  Button,
  Divider,
  Overlay,
} from "react-native-elements";
import { FAB } from "react-native-paper";
import styles from "../styles";
import ModalSelectSociete from "../Modals/ModalSelectSociete";
import ModalSelectContacts from "../Modals/ModalSelectContact";
import ModalSelectType from "../Modals/ModalSelectType";
import ModalSearchBy from "../Modals/ModalSearchBy";
import ModalActivities from "../Modals/ModalActivitie";
import ModalSelectItemPriority from "../Modals/ModalSelectItemTickets";
import Contact from "../ItemsRender/contact";
import ModalSelectStatutActivitie from "../Modals/ModalSelectStatutActvitie";
import ModalTypeActivities from "../Modals/ModalTypeActivities";
import ModalVisibilite from "../Modals/ModalVisibilite";
import AsyncStorage from "@react-native-async-storage/async-storage";
import ItemAct from "../ItemsRender/itemActivities";
let session = null;
//affaires qui est dans drawer
export default class Activities extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingShow: true,
      link: "",
      dataFet: "",
      dataActivities: [],
      dataActivities2: [],
      modalVisibleItem: false,
      modalVisibleSociete: false,
      dataUsers: [],
      modalVisibleItem: false,
      modalVisibleType: false,
      modalVisibleContacts: false,
      modalVisibleProjects: false,
      modalVisibleMilestone: false,
      modalVisibleDelete: false,
      idActivitie: "",
      modalVisibleItemContacts: false,
      modalVisibleActivities: false,
      modalVisibleItemPriority: false,
      modalVisibleCategorie: false,
      modalVisibleStatutActivitie: false,
      modalVisibleEngagement: false,
      modalVisibleVisibilite: false,
      modalVisibleAffaires: "false",
      Moreinfo: false,
      MoreinfoIcon: "chevron-down",
      MoreinfoText: "Plus",
      potentialname: "",
      idAffaire: "",
      projectname: "",
      idProject: "",
      idContact: "",
      id: "",
      lastname: "Select Contact",
      accountname: "Select Account",
      statut: "Plannifiée",
      type: " Rendez-vous",
      visibilite: "",
      priority: "Normale",
      modalVisibleCompte: false,
      phase: "Proposition",

      colorobject: {
        BG1: "#53aefe",
        BG2: "gray",
        BG3: "gray",
      },
      searchTextinput: "",
      MSearchVisibility: false,
    };
  }
  initaliserValues = () => {
    this.setState({
      idActivitie: "",
      Moreinfo: false,
      MoreinfoIcon: "chevron-down",
      MoreinfoText: "Plus",
      potentialname: "",
      idAffaire: "",
      projectname: "",
      idProject: "",
      idContact: "",
      id: "",
      lastname: "Select Contact",
      accountname: "Select Account",
      statut: "Plannifiée",
      type: " Rendez-vous",
      visibilite: "",
      priority: "Normale",
      phase: "Proposition",
    });
  };
  _keyExtractor = (item, index) => item.key;

  renderSeperator = () => {
    return (
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <Divider style={{ width: "95%" }} />
      </View>
    );
  };
  fetching = async () => {
    this.setState({ loadingShow: true });
    try {
      await AsyncStorage.multiGet(
        ["SESSION", "CRM_LINK", "USERID"],
        async (err, res) => {
          var dataFet = res[0][1];
          this.setState({ dataFet: dataFet });
          var link = res[1][1];
          var username = res[2][1];
          this.setState({ link: link });
          await fetch(
            "https://portail.crm4you.ma/webservice.php?operation=query&sessionName=" +
              dataFet +
              "&query=select * from Events where assigned_user_id=" +
              username +
              ";"
          )
            .then((response) => response.json())
            .then((json) => {
              this.setState({
                dataActivities: json.result,
                dataActivities2: json.result,
              });
              this.setState({ loadingShow: false });
            })
            .catch((error) => console.log(error));
        }
      );
    } catch (e) {
      console.log("error: ", e);
    }
  };

  async componentDidMount() {
    this.fetching();
  }

  renderElements = ({ item, index }) => {
    return (
      <View>
        <ItemAct
          Name={item.subject}
          status={item.eventstatus}
          date={item.date_start + " - " + item.time_start}
          onPressDelete={() => {
            this.setState({ id: item.id, modalVisibleDelete: true });
          }}
          onPress={() =>
            this.props.navigation.navigate("DetailsActivities", {
              title: item.subject,
              id: item.id,
              data: this.state.dataActivities,
              index: index,
              result: session,
            })
          }
        />
      </View>
    );
  };

  ChangeNomSociete = (soc) => {
    this.setState({ nomCompte: soc });
  };
  renderItemComptes = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity
        style={styles.company}
        onPress={() => {
          /*this.ChangeNomSociete(item.accountname) ;*/ this.setState({
            modalVisibleCompte: false,
            modalVisibleActivities: true,
            idCompte: item.id,
            accountname: item.accountname,
          });
        }}
      >
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
            backgroundColor: "#53aefe",
            borderTopRightRadius: 10,
            borderBottomRightRadius: 10,
          }}
        >
          <Text style={{ fontSize: 20, fontWeight: "bold", color: "white" }}>
            {String(item.accountname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: "center", marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>{item.accountname}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
  renderItemContacts = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity
        style={styles.company}
        onPress={() => {
          /*this.ChangeNomSociete(item.accountname) ;*/ this.setState({
            modalVisibleItemContacts: false,
            modalVisibleActivities: true,
            idContact: item.id,
            lastname: item.lastname,
          });
        }}
      >
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
            backgroundColor: "#53aefe",
            borderTopRightRadius: 10,
            borderBottomRightRadius: 10,
          }}
        >
          <Text style={{ fontSize: 20, fontWeight: "bold", color: "white" }}>
            {String(item.lastname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: "center", marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>{item.lastname}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );

  onPressOptionVisibilite = (val) => {
    this.setState({
      modalVisibleActivities: true,
      modalVisibleVisibilite: false,
      visibilite: val,
    });
  };

  onPressOptionType = (x) => {
    this.setState({
      modalVisibleActivities: true,
      modalVisibleType: false,
      type: x,
    });
  };
  onPressOptionPriority = (val) => {
    this.setState({
      modalVisibleActivities: true,
      modalVisibleItemPriority: false,
      priority: val,
    });
  };
  onPressOptionStatut = (val) => {
    this.setState({
      modalVisibleActivities: true,
      modalVisibleStatutActivitie: false,
      statut: val,
    });
  };
  onPressCancelCompte = () =>
    this.setState({ modalVisibleCompte: false, modalVisibleActivities: true });
  onPressCancelContact = () =>
    this.setState({
      modalVisibleItemContacts: false,
      modalVisibleActivities: true,
    });

  SubmitItem = (s, d, cc) => {
    let c = { [`${s}`]: "#53aefe" };
    let initial = { BG1: "gray", BG2: "gray", BG3: "gray" };
    let obj = Object.assign(this.state.colorobject, initial, c);
    this.setState({
      colorobject: obj,
      searchby: d,
      searchbytitle: cc,
    });
  };
  onPressInfo = () => {
    if (this.state.Moreinfo) {
      this.setState({
        Moreinfo: false,
        MoreinfoIcon: "chevron-down",
        MoreinfoText: "Plus",
      });
    } else {
      this.setState({
        Moreinfo: true,
        MoreinfoIcon: "chevron-up",
        MoreinfoText: "Moins",
      });
    }
  };
  renderItemProjects = ({ item, x }) => (
    <View style={styles.flatView}>
      <TouchableOpacity
        style={styles.company}
        onPress={() => {
          this.setState({
            modalVisibleProjects: false,
            modalVisibleMilestone: true,
            idProject: item.id,
            projectname: item.projectname,
          });
        }}
      >
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
            backgroundColor: "#53aefe",
            borderTopRightRadius: 10,
            borderBottomRightRadius: 10,
          }}
        >
          <Text style={{ fontSize: 20, fontWeight: "bold", color: "white" }}>
            {String(item.projectname).slice(0, 1)}
          </Text>
        </View>
        <View style={{ flex: 7, justifyContent: "center", marginStart: 10 }}>
          <Text style={{ fontSize: 18 }}>{item.projectname}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );

  filter = async () => {
    await this.setState({
      dataActivities: this.state.dataActivities2.filter((i, n) =>
        i.subject
          .toUpperCase()
          .includes(this.state.searchTextinput.toUpperCase())
      ),
    });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.loadingShow}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "rgba(14,14,14,0.6)",
            }}
          >
            <View>
              <Image
                style={{
                  width: 100,
                  height: 100,
                }}
                source={require("../../assets/crm_icon.png")}
              />
              <Image
                style={{
                  width: 100,
                  height: 100,
                }}
                source={require("../../assets/Spinner.gif")}
              />
            </View>
          </View>
        </Modal>
        <ModalSearchBy
          MSearchVisibility={this.state.MSearchVisibility}
          Items={[{ title: "subject", value: "projectname", colorname: "BG1" }]}
          colorObject={this.state.colorobject}
          searchby={this.state.searchby}
          onpresst={(a, b, c) => {
            this.SubmitItem(a, b, c);
          }}
          onCancel={() => this.setState({ MSearchVisibility: false })}
        />
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: "flex-start",
          }}
        >
          <Header
            placement="right"
            leftComponent={
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-evenly",
                  alignItems: "stretch",
                }}
              >
                <Button
                  onPress={() => {
                    this.props.navigation.toggleDrawer();
                  }}
                  icon={<Icon name="menu" color="white" />}
                />
                <Text
                  style={{
                    fontSize: 22,
                    color: "white",
                    fontWeight: "500",
                    marginTop: 8,
                  }}
                >
                  Activities
                </Text>
              </View>
            }
          />
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: "space-evenly",
            alignItems: "stretch",
          }}
        >
          <View style={{ alignItems: "center" }}>
            <View
              style={{
                borderWidth: 0.5,
                backgroundColor: "white",
                borderRadius: 30,
                overflow: "hidden",
                flexDirection: "row",
                width: "95%",
                marginTop: 5,
                alignItems: "center",
              }}
            >
              <SearchBar
                placeholder={"Search"}
                platform="android"
                value={this.state.searchTextinput}
                onChangeText={(text) =>
                  this.setState({ searchTextinput: text })
                }
                containerStyle={{
                  flex: 7,
                  height: 50,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                onSubmitEditing={async () => this.filter()}
                placeholderTextColor={"#g5g5g5"}
                onClear={() =>
                  this.setState({ dataActivities: this.state.dataActivities2 })
                }
              />
              <View
                style={{
                  flex: 2,
                  flexDirection: "row",
                  justifyContent: "space-evenly",
                }}
              >
                <Icon
                  name="caret-down"
                  color="#00aced"
                  type="font-awesome"
                  size={27}
                  onPress={() => this.setState({ MSearchVisibility: true })}
                />
                <Icon
                  name="filter"
                  color="#00aced"
                  type="font-awesome"
                  size={23}
                  onPress={this.filter}
                />
              </View>
            </View>
          </View>
          <FlatList
            data={this.state.dataActivities}
            renderItem={this.renderElements}
            numColumns={1}
            keyExtractor={(item) => item.id}
            ItemSeparatorComponent={this.renderSeperator}
          />
          <FAB.Group
            open={this.state.open}
            icon={"plus"}
            color="white"
            fabStyle={{ backgroundColor: "#53aefe" }}
            actions={[
              { icon: "plus", onPress: () => console.log("Pressed add") },
            ]}
            onStateChange={() => {
              this.setState({ modalVisibleActivities: true });
            }}
            onPress={() => {
              this.setState({ modalVisibleActivities: true });
            }}
          />

          {/* <Text>select item</Text> */}

          <ModalActivities
            modalVisibleActivities={this.state.modalVisibleActivities}
            onPressPriority={() =>
              this.setState({
                modalVisibleItemPriority: true,
                modalVisibleActivities: false,
              })
            }
            onPressStatut={() =>
              this.setState({
                modalVisibleStatutActivitie: true,
                modalVisibleActivities: false,
              })
            }
            onPressItemContact={() =>
              this.setState({
                modalVisibleItemContacts: true,
                modalVisibleActivities: false,
              })
            }
            onPressCompte={() =>
              this.setState({
                modalVisibleCompte: true,
                modalVisibleActivities: false,
              })
            }
            onPressType={() =>
              this.setState({
                modalVisibleType: true,
                modalVisibleActivities: false,
              })
            }
            onPressCategorie={() =>
              this.setState({
                modalVisibleCategorie: true,
                modalVisibleActivities: false,
              })
            }
            onPressVisibilite={() =>
              this.setState({
                modalVisibleActivities: false,
                modalVisibleVisibilite: true,
              })
            }
            accountname={this.state.accountname}
            selectPriority={this.state.priority}
            selectStatut={this.state.statut}
            selectType={this.state.type}
            onPressInfo={this.onPressInfo}
            MoreinfoText={this.state.MoreinfoText}
            MoreinfoIcon={this.state.MoreinfoIcon}
            Moreinfo={this.state.Moreinfo}
            onPressCancel={() => {
              this.setState({ modalVisibleActivities: false });
              this.initaliserValues();
            }}
            onPressSave={() => {
              this.fetching();
              this.initaliserValues();
            }}
            lastname={this.state.lastname}
            idCompte={this.state.idCompte}
            idContact={this.state.idContact}
            engagement={this.state.engagement}
            categorie={this.state.categorie}
            selectVisibilite={this.state.visibilite}
          />
          <ModalSelectItemPriority
            modalVisibleItemTickets={this.state.modalVisibleItemPriority}
            onPressOption={this.onPressOptionPriority}
            priorite={"Select Priorité"}
            onPressClose={() =>
              this.setState({
                modalVisibleItemPriority: false,
                modalVisibleActivities: true,
              })
            }
          />
          <ModalSelectStatutActivitie
            modalVisibleStatutActivitie={this.state.modalVisibleStatutActivitie}
            onPressOption={this.onPressOptionStatut}
            select={"Select Statut"}
            onPressClose={() =>
              this.setState({
                modalVisibleStatutActivitie: false,
                modalVisibleActivities: true,
              })
            }
          />
          <ModalVisibilite
            modalVisibleVisibilite={this.state.modalVisibleVisibilite}
            onPressOption={this.onPressOptionVisibilite}
            select={"Select Visibilité"}
            onPressClose={() =>
              this.setState({
                modalVisibleVisibilite: false,
                modalVisibleActivities: true,
              })
            }
          />
          <ModalTypeActivities
            modalVisibleTypeActivities={this.state.modalVisibleType}
            onPressOption={this.onPressOptionType}
            select={"Select Type"}
            onPressClose={() =>
              this.setState({
                modalVisibleType: false,
                modalVisibleActivities: true,
              })
            }
          />

          <ModalSelectContacts
            modalVisibleContacts={this.state.modalVisibleItemContacts}
            renderItem={this.renderItemContacts}
            onPressClose={this.onPressCancelContact}
          />
          <ModalSelectSociete
            modalVisibleCompte={this.state.modalVisibleCompte}
            renderItem={this.renderItemComptes}
            onPressClose={this.onPressCancelCompte}
          />
          <Overlay isVisible={this.state.modalVisibleDelete}>
            <View
              style={{
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text>Voulez-vous supprimer cet objet?</Text>
            </View>
            <View style={{ flexDirection: "row-reverse" }}>
              <Button
                title="Oui"
                titleStyle={{ color: "black" }}
                buttonStyle={{ backgroundColor: "white" }}
                onPress={() => {
                  fetch("https://portail.crm4you.ma/webservice.php", {
                    method: "POST",
                    headers: {
                      "Content-Type": "application/x-www-form-urlencoded",
                    },
                    body:
                      "operation=delete&sessionName=" +
                      session +
                      "&id=" +
                      this.state.id,
                  });
                  this.fetching();
                  this.setState({ modalVisibleDelete: false });
                }}
              />
              <Button
                title="Non"
                titleStyle={{ color: "black" }}
                buttonStyle={{ backgroundColor: "white" }}
                onPress={() => this.setState({ modalVisibleDelete: false })}
              />
            </View>
          </Overlay>
        </View>
      </View>
    );
  }
}
