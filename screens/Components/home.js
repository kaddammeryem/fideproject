import React from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  BackHandler,
  Alert,
} from "react-native";
import { Icon, Header, Button } from "react-native-elements";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Info from "../ItemsRender/info";
import styles from "../styles";
export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataComptes: [],
      session: "",
      username: "",
      crmLink: "",
      email: "",
      userId: "",
      Detail: false,
    };
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

  backPressed = () => {
    Alert.alert(
      "Quitter l'application",
      "Voulez vous quitter l'application?",
      [
        { text: "Non", onPress: () => console.log("Cancel Pressed") },
        { text: "Oui", onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false }
    );
    return true;
  };

  async componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backPressed);
    await AsyncStorage.getItem("SESSION", (err, result) => {
      this.setState({ session: result });
    });
    await AsyncStorage.getItem("USER_NAME", (err, result) => {
      this.setState({ username: result });
    });
    await AsyncStorage.getItem("LINK", (err, result) => {
      this.setState({ crmLink: result });
    });
    await AsyncStorage.getItem("EMAIL", (err, result) => {
      this.setState({ email: result });
    });
    await await AsyncStorage.getItem("USERID", (err, result) => {
      this.setState({ userId: result });
    });
    // this.props.navigation.navigate('Comptes');
  }
  renderElements = ({ item, index }) => {
    return (
      <ItemAffaire
        onPressDelete={() => {
          this.setState({ idCompte: item.id, modalVisibleDelete: true });
        }}
        Name={item.accountname}
        onPress={() =>
          this.props.navigation.navigate("DetailsComptes", {
            accountname: item.accountname,
            idCompte: item.id,
            data: this.state.dataComptes,
            index: index,
            result: this.state.session,
            accountname: item.accountname,
          })
        }
      />
    );
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: "flex-start",
          }}
        >
          <Header
            placement="right"
            leftComponent={
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-evenly",
                  alignItems: "stretch",
                }}
              >
                <Button
                  onPress={() => {
                    this.props.navigation.toggleDrawer();
                  }}
                  icon={<Icon name="menu" color="white" />}
                />
                <Text
                  style={{
                    fontSize: 22,
                    color: "white",
                    fontWeight: "500",
                    marginTop: 8,
                  }}
                >
                  Home
                </Text>
              </View>
            }
          />
        </View>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Image
            source={require("../../assets/user.png")}
            width={100}
            height={100}
          />
        </View>
        <View
          style={{
            width: "100%",
            justifyContent: "center",
            justifyContent: "center",
            alignItems: "center",
            elevation: 5,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            height: 70,
            backgroundColor: "white",
            borderColor: "gray",
            borderBottomWidth: 1,
          }}
        >
          <Text style={styles.textTitle}>Détail Du Compte</Text>
        </View>
        <View
          style={{
            overflow: "hidden",
            backgroundColor: "white",
            borderBottomLeftRadius: 10,
            borderBottomRightRadius: 10,
            width: "100%",
            flexDirection: "column",
            justifyContent: "center",
            elevation: 5,
          }}
        >
          <Info
            title={"Username"}
            modifiable={true}
            borderBottom={0.6}
            modifiable={false}
            content={this.state.username}
          />
          <Info
            title={"Your CRM Link"}
            modifiable={true}
            borderBottom={0.6}
            modifiable={false}
            content={this.state.crmLink}
          />
          <Info
            title={"Email"}
            modifiable={true}
            borderBottom={0.6}
            modifiable={false}
            content={this.state.email}
          />
        </View>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Image source={require("../../assets/home.png")} />
        </View>
      </View>
    );
  }
}
const styles2 = StyleSheet.create({
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 0,
    backgroundColor: "#2860b6",
  },
});
