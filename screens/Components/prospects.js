import React from 'react';
import { StyleSheet, View ,FlatList,Text,Modal,Image,Linking} from 'react-native';
import { SearchBar,Icon,Header,Button,Divider,Overlay} from 'react-native-elements';
import { FAB } from 'react-native-paper';
import ItemAffaire from '../ItemsRender/affaireitem';
import ModalComptes from '../Modals/ModalAddCompte';
import ModalSearchBy from '../Modals/ModalSearchBy';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Contact from '../ItemsRender/contact';
import { ActivityIndicator } from 'react-native';
import ModalProspects from '../Modals/ModalProspects';
import ModalSelectSecteur from '../Modals/ModalSelectSecteur';
import ModalStatutProspect from '../Modals/ModalStatutProspect';
import ModalSourceProspect from '../Modals/ModalSourceProspect';
import ModalSelectNote from '../Modals/ModalSelectNote';



export default class Prospects extends React.Component{
      constructor(props){
        super(props);
        this.state={ 
          loadingShow:true,
          link:'',
          dataFet:'',
          id:'',
          dataLeads:[],
          dataLeads2:[],
          recordN: 0 ,
          pagesN : 0 ,
          ActuallPage: 0,
          search : false ,
          open:false,
          session:[],
          modalVisible:false,
          modalVisibleDelete:false,
          modalVisibleSecteur:false,
          modalVisibleNote:false,
          modalVisibleStatut:false,
          modalVisibleSource:false,
          secteur:'Apparel',
          note:'Sans',
          statut:'A contacter',
          source:'Client existant',
          idCompte:'',
          colorobject : {
            BG1 : '#53aefe',
            BG2 : 'gray',
            BG3 : 'gray',
          },
          searchby : 'lastname', 
          searchbytitle : 'Nom', 
          searchTextinput : '',
          MSearchVisibility : false,
        }
       }
       initialiserValues=()=>{
         this.setState({
          idCompte:'',
          secteur:'Apparel',
          note:'Sans',
          statut:'A contacter',
          source:'Client existant',
         })
       }
      
      getRecord = async (page) => {
        
        let offset = ( (page-1) * 50 ) +1
   
        this.setState({loadingShow:true})
        try{
          await AsyncStorage.multiGet(['SESSION', 'CRM_LINK'], async (err, res)=>{ 
            var dataFet= res[0][1];
            this.setState({dataFet: dataFet});
            var link = res[1][1];
            this.setState({link: link});
            await fetch( 'https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select *  from Leads;')
            .then((response) => response.json())
            .then(
              async (json) => { 
               
              await this.setState({
                dataLeads : this.state.dataLeads.concat(json.result),
                dataLeads2 : this.state.dataLeads2.concat(json.result),
                ActuallPage : page ,
                loadingShow:false              
              })
              
            }
            )})
      .catch((error)=>
        console.log(error))
      }catch(e){
        console.log('error: ', e);
      }}
    // 

      componentDidMount(){
        this.getRecordsNumber();
        this.getRecord(1);
      }
      getFunction = async () => {
        this.setState({
          dataLeads : [],
          dataLeads2 : [],
          recordN: 0 ,
          pagesN : 0 ,
          ActuallPage: 0,
          loadingShow: true ,
        })
        this.getRecordsNumber();
        this.getRecord(1);}
      

      renderSeperator=()=> {
        return(
        <View style={{justifyContent:'center',alignItems:'center'}}>
            <Divider style={{width:'95%'}}/>
        </View>
        )}
        makeCall = (phone) => {

          let phoneNumber = '';
      
          if (Platform.OS === 'android') {
            phoneNumber = `tel:${phone}`;
          } else {
            phoneNumber = `telprompt:${phone}`;
          }
      
          Linking.openURL(phoneNumber);
        };
   
   
      renderElements =({item,index})=>{
            return(
                  <Contact onPressDelete={()=> {this.setState({id:item.id,modalVisibleDelete:true})}}
                  Name={item.firstname+' '+item.lastname} compte={item.city}  email={item.phone} onPressPhone={()=>this.makeCall(item.phone)}
                  onPress={()=> this.props.navigation.navigate('DetailsProspects',{title:item.firstname+' '+item.lastname,data:this.state.dataLeads,index:index,result:this.props.route.params.result,idCompte:item.id,accountname:item.lastname+' '+item.firstname})} />                        
      )}

      SubmitItem = (s , d , cc) => {
        let c = {[`${s}`] : '#53aefe'}
        let initial = {BG1 : 'gray',BG2 : 'gray',BG3 : 'gray',}
        let obj =  Object.assign(this.state.colorobject,initial,c)
        this.setState({
          colorobject : obj,
          searchby : d,
          searchbytitle : cc

        })
  
      }
      // add this 
      getRecordsNumber = async () => {
        var dataFet=this.props.route.params.result;
        await fetch('https://portail.crm4you.ma/webservice.php?operation=query&sessionName='+dataFet+'&query=select COUNT(*)  from Leads ;')
        .then((response) => response.json())
        .then((json)=>{
          this.setState({ 
            recordN : json.result[0].count ,
            pagesN : Math.ceil(parseFloat(json.result[0].count) /50),
          })
        
        })
        .catch((error)=>
          console.log(error))  
      }
      // 

        // add this
        LoadMore = (test) =>  {
          if(this.state.ActuallPage + 1 <= this.state.pagesN && !this.state.search) {
            this.getRecord(this.state.ActuallPage + 1);
          }
          else this.setState({ loadingShow : false})
        }
        // add this
        footer = () => {
          return (
  
            <View style ={{ height : 50  }} >
              <ActivityIndicator animating={this.state.isLoding} size="large" color='#00aced' />
            </View>
  
          )
        }
      


        filter = async () => {
          if( this.state.searchTextinput) 
           await this.setState({
            search : true,
            dataLeads : this.state.dataLeads2.filter((i,n) =>
               i[`${this.state.searchby }`].toUpperCase().includes(this.state.searchTextinput.toUpperCase()))
          })
    }
    onPressOptionStatut = (x) => { this.setState({  modalVisibleStatut: false, statut: x,modalVisible:true  });console.log(x) }
    onPressOptionSource = (x) => { this.setState({ modalVisibleSource: false, source: x, modalVisible:true }); }
    onPressOptionNote = (x) => { this.setState({  modalVisibleNote: false, note: x, modalVisible:true }); }
    onPressOptionSecteur = (x) => { this.setState({ modalVisibleSecteur: false, secteur: x,modalVisible:true }); }
  



        render(){
            return(
              <View style={{flex:1}}>
              <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.loadingShow}
              >
                <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: 'rgba(14,14,14,0.6)'
                }}>
                <View>
                  <Image style={{
                    width: 100,
                    height: 100
                  }} source={require('../../assets/crm_icon.png')} />
                  <Image style={{
                    width: 100,
                    height: 100
                  }} source={require('../../assets/Spinner.gif')} />
                </View>
              </View>
            </Modal> 

                      <ModalSearchBy
                          MSearchVisibility={this.state.MSearchVisibility}
                          Items={[
                            {title : 'Nom de Propect' , value : 'firstname' , colorname : 'BG1' } ,
                            {title : 'Prenom du Prospect' , value : 'lastname' , colorname : 'BG2' },
                            {title : 'Ville' , value : 'city' , colorname : 'BG3' }
                          ]}
                          colorObject={this.state.colorobject}
                          searchby={this.state.searchby}
                          onpresst={(a,b,c) => {  this.SubmitItem(a,b,c)}}
                          onCancel= {() => this.setState({MSearchVisibility : false})}
                      /> 

                        <View style={{flexDirection :'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
                                <Header
                                        placement="right"
                                        leftComponent={
                                        <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'stretch'}}>  
                                                <Button onPress={()=> {this.props.navigation.toggleDrawer();}} icon={<Icon name='menu' color='white'/>} 
                                                        />
                                                <Text style={{fontSize:22,color:'white',fontWeight:'500',marginTop:8}}>Prospects</Text>
                                        </View>}
                                      
                                />
                        </View>

                        <View style={{flex:3,justifyContent:'space-evenly',alignItems:'stretch'}}>

                         
                            
                        <View style={{ alignItems: 'center' }}>
                            <View style={{ borderWidth: 0.5, backgroundColor: 'white', borderRadius: 30, overflow: 'hidden', flexDirection: 'row', width: '95%', marginTop: 5, alignItems: 'center' }}>
                              <SearchBar
                                placeholder={`Search by ${this.state.searchbytitle}`}
                                platform='android'
                                value={this.state.searchTextinput}
                                onChangeText={(text) => this.setState({searchTextinput : text})}
                                containerStyle={{ flex : 7 , height :50,alignItems : 'center',justifyContent : 'center' }}
                                onSubmitEditing={async () =>  this.filter()}
                                placeholderTextColor={'#g5g5g5'}
                                onClear={() => this.setState({dataLeads : this.state.dataLeads2 ,search : false , loadingShow : true })}

                              />
                              <View style={{flex : 2,flexDirection : 'row',justifyContent : 'space-evenly' }} >
                              
                              
                              <Icon
                                name='caret-down'
                                color='#00aced'
                                type='font-awesome'
                                size={27}
                                onPress={() => this.setState({MSearchVisibility : true})}
                              />
                              
                              <Icon
                                name='filter'
                                color='#00aced'
                                type='font-awesome'
                                size={23}
                                onPress={this.filter}
                              />
                             
                              </View>
                              
                            </View>
                          </View>


                            <FlatList
                              data={this.state.dataLeads}
                              renderItem={this.renderElements}
                              numColumns={1}
                              keyExtractor={item => item.id}
                              ItemSeparatorComponent={this.renderSeperator}
                              onEndReachedThreshold={0.0001}
                              onEndReached={this.LoadMore}
                              
                            />
                            <FAB.Group
                                icon={ 'plus'}
                                color='white'
                                fabStyle={{backgroundColor : '#53aefe'}}
                                actions={[
                                  { icon: 'plus', },
                                ]}
                                onStateChange={() => { this.setState({modalVisible:true}) }}
                                onPress={() => {this.setState({modalVisible:true})
                                }}
                            />
                            <ModalProspects 
                              modalVisibleItem={this.state.modalVisible}
                              onPressInfo={this.onPressInfo}
                              MoreinfoText={this.state.MoreinfoText}
                              MoreinfoIcon={this.state.MoreinfoIcon}
                              Moreinfo={this.state.Moreinfo}
                              onPressCancel={()=>{this.setState({modalVisible:false});this.initialiserValues()}}
                              onPressSave={()=>{this.getFunction();this.initialiserValues()}}
                              onPressStatut={() => this.setState({ modalVisibleStatut: true, modalVisible: false })}
                              onPressNote={() => this.setState({ modalVisibleNote: true, modalVisible: false })}
                              onPressSource={() => this.setState({ modalVisibleSource: true, modalVisible: false })}
                              onPressSecteur={() => this.setState({ modalVisibleSecteur: true, modalVisible: false })}

                              secteur={this.state.secteur}
                              statut={this.state.statut}
                              note={this.state.note}
                              source={this.state.source}
                            />
                             <ModalSelectSecteur
                                modalVisibleSecteur={this.state.modalVisibleSecteur}
                                onPressOption={this.onPressOptionSecteur}
                                onPressClose={() => this.setState({ modalVisibleSecteur: false, modalVisibleButton: true })}
                                select={'Select Item'}
                              />
                              <ModalStatutProspect
                                modalVisibleItem={this.state.modalVisibleStatut}
                                onPressOption={this.onPressOptionStatut}
                                onPressClose={() => this.setState({ modalVisibleStatut: false, modalVisibleButton: true })}
                                select={'Select Item'}
                              />
                            <ModalSourceProspect
                                modalVisibleItem={this.state.modalVisibleSource}
                                onPressOption={this.onPressOptionSource}
                                onPressClose={() => this.setState({ modalVisibleSource: false, modalVisibleButton: true })}
                                select={'Select Item'}
                              />
                              <ModalSelectNote
                              modalVisibleItem={this.state.modalVisibleNote}
                              onPressOption={this.onPressOptionNote}
                              onPressClose={() => this.setState({ modalVisibleNote: false, modalVisibleButton: true })}
                              select={'Select Item'}
                            />
                        
                         
                         
                            <Overlay isVisible={this.state.modalVisibleDelete} >
                                <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                  <Text>Voulez-vous supprimer cet objet?</Text>

                                </View>
                                <View style={{flexDirection:'row-reverse'}}>
                                  <Button title="yes" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}} onPress={()=> {
                                      fetch('https://portail.crm4you.ma/webservice.php', {
                                        method: 'POST',
                                        headers: {
                                          "Content-Type": "application/x-www-form-urlencoded"
                                      },
                                      body:'operation=delete&sessionName='+this.state.dataFet+'&id='+this.state.id
                                      });this.getFunction();this.setState({modalVisibleDelete:false})}}
                                  />
                                  <Button title="no" titleStyle={{color:'black'}} buttonStyle={{backgroundColor:"white"}}  onPress={()=>this.setState({modalVisibleDelete:false})}/>
                                </View>
                                    </Overlay>
                        </View>
                   </View>
            
        )
    }
}
const styles = StyleSheet.create({
    fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
      backgroundColor:'blue'
    },
  })
