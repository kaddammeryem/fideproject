import React, { Component} from 'react';
import { TouchableOpacity } from 'react-native';
import { View ,Text } from 'react-native';
import {  Checkbox  } from 'react-native-paper';


class SearchbyItem extends Component {
    
    render() { 
        return ( 
            <TouchableOpacity activeOpacity={0.6} style={{ flexDirection: 'row', padding: 10, alignItems: 'center', borderBottomWidth: 1, borderRadius: 25, borderColor: 'gray' }} onPress={this.props.onPress}>
                
                    <Text style={{ fontSize: 17, marginStart: 10, flex: 7, color: this.props.color ,fontWeight : 'bold' }}>{this.props.title}</Text>
                    <Checkbox
                        style={{ flex: 1 }}
                        color={'blue'}
                        status={this.props.chosenValue == this.props.value ? 'checked' : 'unchecked'}
                        onPress={this.props.onPress}
                    />
                
            </TouchableOpacity>
         );
    }
}
 
export default SearchbyItem;