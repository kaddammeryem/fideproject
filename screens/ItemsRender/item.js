import React from 'react';
import { Dimensions ,Text, View,TouchableOpacity } from 'react-native';
import {Icon,} from 'react-native-elements';
const width = Dimensions.get('window').width;
import styles from '../styles';
const  Item = (props) =>  {

    return (
        <View style={{padding : 2 }}>
          
          <TouchableOpacity style={{width : width*0.8,height : 50,backgroundColor :'white',}} onPress={props.onPress}>
            <View style={{width : '100%',justifyContent:'center',marginStart : 10,borderBottomWidth : 1 , borderBottomColor : 'gray'}}>
              <Text style={{fontSize : 19,padding : 10,fontWeight : '400'}}>
              {props.text}
              </Text>
            </View>
          </TouchableOpacity>
     </View>
    );
    
}
export  default Item;