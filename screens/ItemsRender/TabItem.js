import React, {Component } from 'react';
import {  Dimensions,StyleSheet,Text ,TouchableOpacity,View } from 'react-native';
const height = Dimensions.get('window').height;


class TabItem extends Component {

    render() { 
        return (
            <View>
            <TouchableOpacity activeOpacity={0.7} style={[styles.tab, { scaleX: this.props.scale, scaleY: this.props.scale, borderColor: this.props.BackGround }]} onPress={this.props.onPress} >
                <Text style={styles.textt}>{this.props.title}</Text>
            </TouchableOpacity></View>
        );
    }
}
 
export default TabItem;

const styles = StyleSheet.create({
    tab :{
        height: height * 0.1,
        width : 95,
        backgroundColor : 'white',
        borderRadius : 10,
        alignItems :'center',
        justifyContent : 'center',
        elevation : 1 ,
        marginEnd :10,
        marginBottom : 5,
        borderWidth :2
      },
      textt : {
        fontSize : 20,
        textAlign : 'center',
        color : '#0e49b5',
        fontWeight : 'bold'
      },
})