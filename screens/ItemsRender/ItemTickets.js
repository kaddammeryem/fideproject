import React from 'react';
import { Text, View,TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const  ItemTicket = (props) =>  {
    return (
        <View style={{alignItems : 'center' ,padding :10}}>
              <View  style={[styles.viewaffaire,{borderLeftWidth : 4,borderLeftColor : '#53aefe'}]} >
                <TouchableOpacity style={{ flex: 7, justifyContent: 'center', marginStart: 10 }} onPress={props.onPress}>
                  <Text style={{ fontSize: 18 }}>
                    {props.Name}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flex : 1,alignItems : 'center',justifyContent : 'center'}} onPress={props.onPressDelete}>
                  <Icon name="trash" size={20} color='#53aefe'/>
                </TouchableOpacity>
              </View>
        </View>
    );
    
}
const styles = StyleSheet.create({
    viewaffaire:{
        flexDirection: 'row',
        backgroundColor :'white',
        borderRadius : 5 ,
        elevation : 5,
        overflow : 'hidden',  
        width: '90%',
        height : 70 
      }

    

})
export  default ItemTicket;