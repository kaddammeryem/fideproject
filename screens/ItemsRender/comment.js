import React from 'react';
import { View ,TouchableOpacity,Text} from 'react-native';
import {TextInput} from 'react-native-paper';
import styles from '../styles';
export default class Comments extends React.Component{
    render(){
        return(
            <View style={{alignItems : 'center'}} >
               <View style={styles.view_comentaire}>
                    <View style={{ marginBottom: 10, justifyContent: 'center' }}>
                    <Text style={styles.textTitle}>
                        Commentaires
                    </Text>
                </View>
                <View style={{ marginBottom: 10, justifyContent: 'center', textAlign: 'center' }}>

                </View>
            </View>
            <View style={styles.view_textinput_commentaire}>
              <TextInput
                multiline={true}
                placeholder={"  Add your comments here"}
                style={{ borderRadius: 5, height: 100, width: '90%', borderColor: 'gray', borderWidth: 1, marginTop: 10 }}
              />

              <View style={styles.viewPost}>
                <TouchableOpacity activeOpacity={0.7} style={styles.buttonPost} >
                  <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: 'white' }}>Post</Text>
                </TouchableOpacity>
              </View>

              <Text style={{ fontSize: 18, fontWeight: 'bold', marginTop: 20 }}>No Result</Text>
            </View>
          </View>);}}