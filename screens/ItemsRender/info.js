import React from 'react';
import { Text, View,TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';


const  Info = (props) =>  {
    return (
            <View style={{ overflow : 'hidden',width: '100%', borderBottomWidth : props.borderBottom,flexDirection: 'row', height: 70, backgroundColor: 'white' ,marginTop : 7}}>
                <View style={{ flex: 7 }}>
                    <Text style={{ marginStart: 10, fontSize: 17, color: '#a4a4a7' }} >{props.title}</Text>
                    <Text style={{ marginStart: 30, fontSize: 18, fontWeight: 'bold' }} onPress={props.onPressContent} onLongPress={props.onLongPress}>{props.content}</Text>
                </View>
                {props.modifiable ?
                <TouchableOpacity style={{ flex: 1, alignItems: "center", justifyContent: 'center' }} onPress={props.onPress}>
                <Icon name="edit" size={20} color='#51adcf' />
            </TouchableOpacity>
                : null}
                
            </View>
            

      );
    
}
export  default Info;