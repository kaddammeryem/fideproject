import React from 'react';
import { Text, View,TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const  Contact = (props) =>  {
    

    return (
        <View style={{alignItems : 'center' ,padding :10}}>
              <View  style={[styles.company]} >
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1.7, backgroundColor: '#53aefe', borderTopRightRadius: 10, borderBottomRightRadius: 10 }} >
                  <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>{`${props.Name}`.substring(0, 1).toUpperCase()}</Text>
                </View>
   
                <TouchableOpacity style={{ flex: 7, justifyContent: 'center', marginStart: 10 }} onPress={props.onPress}>
                  <Text style={{ fontSize: 18 }}>
                  {props.Name}
                  </Text>
                  <Text style={{ fontSize: 15,color:'grey' }}>
                  {props.compte}
                  </Text>

                  <Text style={{ fontSize: 15,color :'blue',fontStyle:'italic',textDecorationLine: 'underline' }} onPress={props.onPressPhone}>
                    {props.email}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={props.onPressDelete}style={{flex : 1,alignItems : 'center',justifyContent : 'center'}}>
                  <Icon name="trash" size={20} color='#53aefe'/>
              </TouchableOpacity>
              </View>
              
            </View>
    );
    
}
const styles = StyleSheet.create({
    company:{
        flexDirection: 'row',
        backgroundColor :'white',
        borderRadius : 10 ,
        elevation : 5,
        overflow : 'hidden',  
        width: '90%',
        height : 70 }

    

})
export  default Contact;