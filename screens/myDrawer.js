import React from 'react';
import { View, TouchableOpacity, ScrollView, Text, Image } from 'react-native';
import { List } from 'react-native-paper';
import { Divider, } from 'react-native-elements';
import * as Linking from 'expo-linking';
import AsyncStorage from '@react-native-async-storage/async-storage';



export default class MyDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
      session: [],
      Campaigns: false,
      Vendors: false,
      Faq: false,
      username:'',
      Quotes: false,
      PurchaseOrder: false,
      SalesOrder: false,
      Invoice: false,
      Calendar: false,
      Leads: false,
      Accounts: false,
      Contacts: false,
      Potentials: false,
      Products: false,
      Documents: false,
      Emails: false,
      HelpDesk: false,
      Events: false,
      ServiceContracts: false,
      Services: false,
      Assets: false,
      ModComments: false,
      ProjectMilestone: false,
      ProjectTask: false,
      Project: false,
      CTAttendance: false,
      Groups: false,
      Currency: false,
      DocumentFolders: false,
      CompanyDetails: false,
      LineItem: false,
      Tax: false,
      ProductTaxes: false
    }


  }


  _handlePress = () =>
    this.setState({
      expanded: !this.state.expanded
    });

  async componentDidMount() {

    try {
      await AsyncStorage.getItem('SESSION', (err, result)=>{ this.setState({session: result})});
      await AsyncStorage.getItem('TYPES', (err, array) =>{
        types = array.split(',');
    
        if (types.includes('Campaigns')) this.setState({ Campaigns: true });
      if (types.includes('Vendors')) this.setState({ Vendors: true });
      if (types.includes('Faq')) this.setState({ Faq: true });
      if (types.includes('Quotes')) this.setState({ Quotes: true });
      if (types.includes('PurchaseOrder')) this.setState({ PurchaseOrder: true });
      if (types.includes('SalesOrder')) this.setState({ SalesOrder: true });
      if (types.includes('Invoice')) this.setState({ Invoice: true });
      if (types.includes('Calendar')) this.setState({ Calendar: true });
      if (types.includes('Leads')) this.setState({ Leads: true });
      if (types.includes('Accounts')) this.setState({ Accounts: true });
      if (types.includes('Contacts')) this.setState({ Contacts: true });
      if (types.includes('Potentials')) this.setState({ Potentials: true });
      if (types.includes('Products')) this.setState({ Products: true });
      if (types.includes('Documents')) this.setState({ Documents: true });
      if (types.includes('Emails')) this.setState({ Emails: true });
      if (types.includes('HelpDesk')) this.setState({ HelpDesk: true });
      if (types.includes('Events')) this.setState({ Events: true });
      if (types.includes('ServiceContracts')) this.setState({ ServiceContracts: true });
      if (types.includes('Services')) this.setState({ Services: true });
      if (types.includes('Assets')) this.setState({ Assets: true });
      if (types.includes('ModComments')) this.setState({ ModComments: true });
      if (types.includes('ProjectMilestone')) this.setState({ ProjectMilestone: true });
      if (types.includes('ProjectTask')) this.setState({ ProjectTask: true });
      if (types.includes('Project')) this.setState({ Project: true });
      if (types.includes('CTAttendance')) this.setState({ CTAttendance: true });
      if (types.includes('Groups')) this.setState({ Groups: true });
      if (types.includes('Currency')) this.setState({ Currency: true });
      if (types.includes('DocumentFolders')) this.setState({ DocumentFolders: true });
      if (types.includes('CompanyDetails')) this.setState({ CompanyDetails: true });
      if (types.includes('LineItem')) this.setState({ LineItem: true });
      if (types.includes('Tax')) this.setState({ Tax: true });
      if (types.includes('ProductTaxes')) this.setState({ ProductTaxes: true });
      });
      
      await AsyncStorage.getItem('USER_NAME', (err, result)=>{ this.setState({username: result})});
    
      
      
    } catch (e) {
      console.log(e);
    }
  }

  logout=async()=>{
    const keys = ['IS_LOGED','USER_NAME','EMAIL','CRM_LINK','USERID','LINK','USER_KEY','TYPES','SESSION']

    try {
      await AsyncStorage.clear()
      .then(()=>{this.props.navigation.navigate('Sign');})
      .catch((r)=>console.log(r))
    } catch(e) {
      // remove error
    }

  }


  render() {



    return (
      <View style={{ flex: 1 }}>
        <View style={{ height: '20%', justifyContent: 'flex-start', flexDirection: 'row', backgroundColor: 'lightblue', alignItems: 'center' }}>

          <Image source={require('../assets/user.png')} style={{ margin: 5 }} />
          <Text style={{ marginTop: 8, fontSize: 18, fontWeight: 'bold' }}> {this.state.username}</Text>
        </View>
        <Divider style={{ borderWidth: 0.5 }} />
             
             <TouchableOpacity style={{ padding: 20 }} onPress={() => this.props.navigation.navigate('Activities', { result: this.state.session })} >
               <Text>
                 Activities
               </Text>
               
             </TouchableOpacity >
             <Divider style={{ borderWidth: 0.5 }} />
        <ScrollView>
          <View style={{ flex: 1, justifyContent: 'space-evenly', alignItems: 'flex-start' }}>
            <View style={{ width: '100%' }}>

              <List.AccordionGroup>
                <List.Accordion
                  expanded={this.state.expanded}
                  onPress={this._handlePress}
                  id="1"
                  style={{ width: '100%' }}
                  title="Marketing"
                  right={props => <Text style={{ fontWeight: 'bold' }}> Marketing</Text>}>
                  <List.Item style={{ padding: 1 }} titleStyle={{ marginLeft: '10%' }} title="Comptes" onPress={() => {
                    this.props.navigation.navigate('Comptes', { result: this.state.session }); 
                  
                    }} />
                  <Divider style={{ borderWidth: 0.2 }} />
                  {(this.state.Contacts === true)  && <List.Item title="Contact" style={{ padding: 2 }} titleStyle={{ marginLeft: '10%' }} onPress={() => this.props.navigation.navigate('Contacts', { result: this.state.session })} />}
                  <Divider style={{ borderWidth: 0.2 }} />
                  {(this.state.Leads === true)  && <List.Item title="Propects" style={{ padding: 2 }} titleStyle={{ marginLeft: '10%' }} onPress={() => this.props.navigation.navigate('Prospects', { result: this.state.session })} />}
                  <Divider style={{ borderWidth: 0.2 }} />
                </List.Accordion>
                <Divider style={{ borderWidth: 0.5 }} />
                <List.Accordion
                  id="2"
                  style={{ width: '100%' }}
                  title="Ventes"
                  right={props => <Text style={{ fontWeight: 'bold' }}> Ventes</Text>}>
                   {(this.state.Potentials === true)  && <List.Item title="Affaires" style={{ padding: 2 }} titleStyle={{ marginLeft: '10%' }} onPress={() => this.props.navigation.navigate('Affaires', { result: this.state.session })} />}
                 
                  
                  <Divider style={{ borderWidth: 0.2 }} />
                </List.Accordion>
               
                <Divider style={{ borderWidth: 0.5 }} />
                <List.Accordion
                  id="3"
                  style={{ width: '100%' }}
                  title="Support"
                  right={props => <Text style={{ fontWeight: 'bold' }}> Ventes</Text>}>
                   {(this.state.HelpDesk === true)  &&<List.Item title="Tickets" style={{ padding: 2 }} titleStyle={{ marginLeft: '10%' }} onPress={() => this.props.navigation.navigate('Tickets', { result: this.state.session })} />}
                  <Divider style={{ borderWidth: 0.2 }} />
                  {(this.state.ServiceContracts === true)  &&<List.Item title="Contrats de service" style={{ padding: 2 }} titleStyle={{ marginLeft: '10%' }} onPress={() => this.props.navigation.navigate('Contrats', { result: this.state.session })} />}
                  <Divider style={{ borderWidth: 0.2 }} />
                </List.Accordion>
                
                <Divider style={{ borderWidth: 0.5 }} />
                <List.Accordion
                  id="4"
                  style={{ width: '100%' }}
                  title="Projets"
                  right={props => <Text style={{ fontWeight: 'bold' }}> Projets</Text>}>
                   {(this.state.Project === true)  &&<List.Item title="Projets" style={{ padding: 2 }} titleStyle={{ marginLeft: '10%' }} onPress={() => this.props.navigation.navigate('Projects', { result: this.state.session })} />}
                  <Divider style={{ borderWidth: 0.2 }} />
                  {(this.state.ProjectMilestone === true)  &&<List.Item title="Jalons du projet" style={{ padding: 2 }} titleStyle={{ marginLeft: '10%' }} onPress={() => this.props.navigation.navigate('Milestones', { result: this.state.session })} />}
                  <Divider style={{ borderWidth: 0.2 }} />
                  {(this.state.ProjectTask === true)  &&<List.Item title="Tasks" style={{ padding: 2 }} titleStyle={{ marginLeft: '10%' }} onPress={() => this.props.navigation.navigate('Tasks', { result: this.state.session })} />} 
                  <Divider style={{ borderWidth: 0.2 }} />
                </List.Accordion>
                
              </List.AccordionGroup>
             
              
              <Divider style={{ borderWidth: 0.5 }} />
              <TouchableOpacity style={{ padding: 20 }} onPress={() => Linking.openURL('http://www.cjem.co.ma/index.html')}>
                <Text>
                  Help
                 </Text>
              </TouchableOpacity>
              <Divider style={{ borderWidth: 0.5 }} />
              <TouchableOpacity style={{ padding: 20 }} onPress={() => Linking.openURL('http://www.cjem.co.ma/contact3.html')}>
                <Text>
                  Contacts
                </Text>
              </TouchableOpacity>
              <Divider style={{ borderWidth: 0.5 }} />
              <TouchableOpacity style={{ padding: 20 }} onPress={() => Linking.openURL('http://www.cjem.co.ma/actu.html')}>
                <Text>
                  Update
                </Text>
              </TouchableOpacity>
              <Divider style={{ borderWidth: 0.5 }} />
              <TouchableOpacity style={{ padding: 20 }} onPress={this.logout}>
                <Text>
                  Logout
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
