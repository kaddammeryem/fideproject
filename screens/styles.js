import {StyleSheet, Dimensions }from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
 const styles =StyleSheet.create({
    textTitle: { 
      margin: 10, 
      fontWeight: 'bold', 
      fontSize: 25 ,
      color:'#1089ff'},
  View_Champ_cle :{
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'center',
    elevation: 5,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    height: 70,
    backgroundColor: 'white',
    borderColor: 'gray',
    borderBottomWidth: 1 
  },
  styleItem:{
    padding:1,
    backgroundColor:'white',
  },
  titleItemStyle:{
    backgroundColor: 'white', 
    width: width * 0.8,
    fontWeight : 'bold', 
    marginTop: 20, 
    color: '#53aefe'
},
  descriptionStyleItem:{
    marginLeft:'15%',
    color:'black',
    fontWeight:'500',
    fontSize:16,color:'black'
  },
  titleStyleItem:{
      marginLeft:'10%'
  },
  titleStyleAccordian:{fontWeight:'bold',color:'#306EFF'},
  styleAccordian:{backgroundColor:'white',width:'100%'},


  content_champ_cle:{
     overflow: 'hidden',
     backgroundColor: 'white',
     borderBottomLeftRadius: 10, 
     borderBottomRightRadius: 10, 
     width: '90%', 
     flexDirection: 'column', 
     justifyContent: 'center', 
     elevation: 5 
    },
  buttonFullDetails: { 
    elevation: 5, 
    borderRadius: 10, 
    marginLeft: 30, 
    width: '40%', 
    height: 48, 
    backgroundColor: '#53aefe', 
    alignItems: "center", 
    justifyContent: 'center' },
  View_documents :{ 
    width: '90%', 
    flexDirection: 'row', 
    justifyContent: 'center', 
    elevation: 5, 
    borderTopLeftRadius: 10, 
    borderTopRightRadius: 10, 
    height: 70, 
    backgroundColor: 'white', 
    borderBottomWidth: 1, 
    borderColor: 'gray' },
  fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
      backgroundColor : '#53aefe',
     
    
  },
  noresult :{ 
    height: 80, 
    borderBottomWidth: 0, 
    overflow: 'hidden', 
    backgroundColor: 'white', 
    borderBottomLeftRadius: 10, 
    borderBottomRightRadius: 10, 
    width: '90%', 
    alignItems: 'center', 
    justifyContent: 'center', 
    elevation: 5, 
    marginBottom: 40 },
  view_comentaire :{ 
    width: '90%', 
    flexDirection: 'row', 
    elevation: 5, 
    borderTopLeftRadius: 10, 
    borderTopRightRadius: 10, 
    height: 70, 
    backgroundColor: 'white', 
    borderBottomWidth: 1, 
    borderColor: 'gray' 
  },
  Modal:{
    width:'90%',
    height:'70%',
    borderRadius:100,

  },
  view_textinput_commentaire :{ 
    height: 270, 
    overflow: 'hidden', 
    backgroundColor: 'white', 
    borderBottomLeftRadius: 10, 
    borderBottomRightRadius: 10, 
    width: '90%', 
    alignItems: 'center', 
    justifyContent: 'center', 
    elevation: 5, 
    marginBottom: 40 },
  viewPost :{ 
    borderBottomWidth: 0.5, 
    width: '100%', 
    flexDirection: 'row-reverse', 
    height: 70, 
    backgroundColor: 'white', 
    marginTop: 10 },
  buttonPost :{ 
    elevation: 5, 
    borderRadius: 10, 
    marginRight: 30, 
    width: '40%', 
    height: 38, 
    backgroundColor: '#53aefe', 
    alignItems: "center", 
    justifyContent: 'center' },
  buttonAdd :{ 
      elevation: 5, 
      borderRadius: 10, 
      margin:10,
      justifyContent:'center',
      width: '20%', 
      height: 40, 
      backgroundColor: '#53aefe', 
      },
    textt : {
      fontSize : 20,
      textAlign : 'center',
      color : '#0e49b5',
      fontWeight : 'bold'
    },
    scroll :{
      marginTop : 10,
      marginStart : 5,
      height: height * 0.109,
      alignItems :'center',
      
    },
    tab :{
      height: 50,
      width : 135,
      backgroundColor : 'white',
      borderRadius : 10,
      alignItems :'center',
      justifyContent : 'center',
      elevation : 1 ,
      marginEnd :10,
      marginBottom : 0,
      borderWidth :2
    },
    view_Check:{
      marginTop : 20,
      flexDirection : 'row',
      height : 30 ,
      justifyContent:'center',
      alignItems:'center',
      width : width*0.8,
      borderRadius : 5,
      borderWidth : 1.2,
      borderColor : '#3E89D6'
    },
    align_center_view:{
      flex:1,
      justifyContent:'center',
      alignItems:'center',
    }
    ,
     company :{
      flexDirection: 'row',
      width : width*0.8,
      height : 50,
      backgroundColor :'white',
      borderRadius : 10 ,
      elevation : 5,
      overflow : 'hidden'
      
    },
    titleModal1 : {
      flexDirection : 'row',
      height : 50,
      width : width*0.9,
      padding : 7,
      backgroundColor :'#53aefe',
      fontSize : 25,
      alignSelf : 'center',
      marginBottom : 10,
    },
    searchbar:{
      justifyContent : 'center',
      alignItems : 'center',
      marginBottom : 5,
      flexDirection : 'row',
      padding : 10,
      elevation : 5,
      backgroundColor: 'white',

    },
    flatView :{
      padding : 7,
      alignItems : 'center',

    },
    scrollView: {
      backgroundColor: 'white',
    },
    text: {
      fontSize: 42,
    },
    block: {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'row',
      borderBottomColor: '#225fbf',
      borderBottomWidth: 1,
      paddingBottom: 10,
      paddingTop: 10,
    },
    icon: {
      height: 50,
      width: 50,
      borderRadius: 25,
      borderColor: '#225fbf',
      borderWidth: 2,
      padding: 10,
    },
    icon_container: {
      flex: 1.5,
    },
    details: {
      flex: 5,
      marginLeft: 15,
      
    },
    title: {
      fontSize: 18,
      color: '#225fbf',
      fontWeight: 'bold'
    },
    description: {
      fontSize: 16,
      color: '#787e8a',
    },
    date_container:{
      alignSelf: "flex-start",
      padding: 10,
      alignItems:'center',
      flex:1.5
    },
    date: {
      fontSize: 14,
      color: '#787e8a',
    }
  
  });

  export default styles;
  