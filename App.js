import React from "react";
import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import Container from "./screens/navigator";
import Sign from "./screens/sign";
import AsyncStorage from "@react-native-async-storage/async-storage";
const App = (props) => {
  return (
    <NavigationContainer>
      <Container />
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});

export default App;
